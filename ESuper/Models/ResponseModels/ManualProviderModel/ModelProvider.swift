//
//	ModelProvider.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelProvider : NSObject, NSCoding{

	var v : Int!
	var id : String = ""
    var firstName : String = ""
    var lastName : String = ""
    var address : String = ""
    var userRate : Int = 0
    var imageUrl = ""
    var isSelected : Bool = false
    var business_location : [Double] = []

	init(fromDictionary dictionary: [String:Any]){
		id = dictionary["_id"] as? String ?? ""
		firstName = dictionary["first_name"] as? String ?? ""
		imageUrl = dictionary["image_url"] as? String ?? ""
        lastName = dictionary["last_name"] as? String ?? ""
        address = dictionary["address"] as? String ?? ""
        userRate = dictionary["user_rate"] as? Int ?? 0
        business_location = dictionary["business_location"] as? [Double] ?? [0.0,0.0]
        isSelected = false
	}

	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        dictionary["_id"] = id
        dictionary["first_name"] = firstName
        dictionary["image_url"] = imageUrl
        dictionary["last_name"] = lastName
        dictionary["user_rate"] = userRate
        dictionary["address"] = address
        dictionary["business_location"]  = business_location
        return dictionary
	}

    @objc required init(coder aDecoder: NSCoder){
        id = aDecoder.decodeObject(forKey: "_id") as? String ?? ""
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String ?? ""
         imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String ?? ""
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String ?? ""
        address = aDecoder.decodeObject(forKey: "address") as? String ?? ""
         userRate = aDecoder.decodeObject(forKey: "user_rate") as? Int ?? 0
        business_location = aDecoder.decodeObject(forKey: "business_location") as? [Double] ?? [0.0,0.0]
	}

    @objc func encode(with aCoder: NSCoder){
        aCoder.encode(id, forKey: "_id")
        aCoder.encode(firstName, forKey: "first_name")
        aCoder.encode(imageUrl, forKey: "image_url")
        aCoder.encode(lastName, forKey: "last_name")
        aCoder.encode(userRate, forKey: "user_rate")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(business_location, forKey: "business_location")
    }

}
