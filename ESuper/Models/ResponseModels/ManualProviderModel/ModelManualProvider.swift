//
//	ModelManualProvider.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelManualProvider : NSObject, NSCoding{

	var providers : [ModelProvider]!
	var success : Bool!

	init(fromDictionary dictionary: [String:Any]){
		providers = [ModelProvider]()
		if let providersArray = dictionary["provider_list"] as? [[String:Any]]{
			for dic in providersArray{
				let value = ModelProvider(fromDictionary: dic)
				providers.append(value)
			}
		}
		success = dictionary["success"] as? Bool
	}

	func toDictionary() -> [String:Any]{
		var dictionary = [String:Any]()
		if providers != nil{
			var dictionaryElements = [[String:Any]]()
			for providersElement in providers {
				dictionaryElements.append(providersElement.toDictionary())
			}
			dictionary["provider_list"] = dictionaryElements
		}
		if success != nil{
			dictionary["success"] = success
		}
		return dictionary
	}

    @objc required init(coder aDecoder: NSCoder){
         providers = aDecoder.decodeObject(forKey :"provider_list") as? [ModelProvider]
         success = aDecoder.decodeObject(forKey: "success") as? Bool

	}

    @objc func encode(with aCoder: NSCoder){
		if providers != nil{
			aCoder.encode(providers, forKey: "provider_list")
		}
		if success != nil{
			aCoder.encode(success, forKey: "success")
		}
	}
}
