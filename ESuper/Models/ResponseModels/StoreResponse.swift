import Foundation
public class StoreResponse {
	public var success : Bool = false
	public var message : Int?
	public var stores : StoreDictionary?
    public var server_time : String?
    public var ads:[AdItem] = []
    
    
    
    public class func modelsFromDictionaryArray(array:NSArray, defaultImage : String) -> [StoreResponse] {
        var models:[StoreResponse] = []
        for item in array {
            models.append(StoreResponse(dictionary: item as! NSDictionary, defaultImage: defaultImage)!)
        }
        return models
    }
    required public init?(dictionary: NSDictionary, defaultImage : String) {

		success = (dictionary["success"] as? Bool)!
		message = dictionary["message"] as? Int
        server_time = dictionary["server_time"] as? String
        if dictionary["stores"] != nil {
            stores = StoreDictionary.init(dictionary: dictionary["stores"]  as! NSDictionary, defaultImage: defaultImage)
        }
        if let adsArray =  dictionary[ "ads"] as? [[String:Any]] {
            for item in adsArray {
                let adItem:AdItem = AdItem.init(fromDictionary: item)
                  self.ads.append(adItem)
            }
        }
	}

	public func dictionaryRepresentation() -> NSDictionary {
		let dictionary = NSMutableDictionary()
		dictionary.setValue(self.success, forKey: "success")
		dictionary.setValue(self.message, forKey: "message")
        dictionary.setValue(self.server_time, forKey: "server_time")
		return dictionary
	}

}
