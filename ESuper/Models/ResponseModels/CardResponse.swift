
import Foundation


public class CardResponse {
	public var success : Bool?
	public var message : Int?
	public var cards : Array<CardItem>?
    public var card : CardItem?

    public class func modelsFromDictionaryArray(array:NSArray) -> [CardResponse] {
        var models:[CardResponse] = []
        for item in array {
            models.append(CardResponse(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {
		success = dictionary["success"] as? Bool
		message = dictionary["message"] as? Int
		if (dictionary["cards"] != nil) { cards = CardItem.modelsFromDictionaryArray(array: dictionary["cards"] as! NSArray) }
        if (dictionary["card"] != nil) {
            card = CardItem.init(dictionary: dictionary["card"] as! NSDictionary)
        }
	}

	public func dictionaryRepresentation() -> NSDictionary {
		let dictionary = NSMutableDictionary()
		dictionary.setValue(self.success, forKey: "success")
		dictionary.setValue(self.message, forKey: "message")
		return dictionary
	}

}
