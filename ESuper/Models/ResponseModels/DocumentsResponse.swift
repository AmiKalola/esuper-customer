import Foundation


class DocumentsResponse{
    
    var documents : [Document]!
    var isDocumentUploaded : Bool!
    var message : Int!
    var success : Bool!
    
    init(fromDictionary dictionary: [String:Any]){
        documents = [Document]()
        if let documentsArray = dictionary["documents"] as? [[String:Any]]{
            for dic in documentsArray{
                let value = Document(fromDictionary: dic)
                documents.append(value)
            }
        }
        isDocumentUploaded = dictionary["is_document_uploaded"] as? Bool
        message = dictionary["message"] as? Int
        success = dictionary["success"] as? Bool
    }
    
}
