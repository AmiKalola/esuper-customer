//
//  IsSuccessResponse.swift
//  
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
public class IsSuccessResponse {
    public var success : Bool?
    public var message : Int?
    public var status_phrase : String = ""
    public var errorCode : Int?
    public var no_avaiable_products : [String]?
    
    required public init?(dictionary: NSDictionary) {
        success = (dictionary["success"] as? Bool) ?? false
        status_phrase = (dictionary["status_phrase"] as? String) ?? ""
        if  success! {
            message =  (dictionary["message"] as? Int) ?? 0
        }else {
            errorCode = (dictionary["error_code"] as? Int) ?? 0
            no_avaiable_products = (dictionary["no_avaiable_products"] as? [String])
        }
    }
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.message, forKey: "message")
        dictionary.setValue(self.status_phrase, forKey: "status_phrase")
        if  success! {
            dictionary.setValue(self.message, forKey: "message")
            
        }else {
            dictionary.setValue(self.errorCode, forKey: "error_code")
            dictionary.setValue(self.no_avaiable_products, forKey: "no_avaiable_products")
        }
        return dictionary
    }
}
