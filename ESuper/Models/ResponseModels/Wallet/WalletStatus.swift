import Foundation
 

public class WalletStatusResponse {
	 var success : Bool!
	 var message : Int!
	 var isUseWallet : Bool!

    public class func modelsFromDictionaryArray(array:NSArray) -> [WalletStatusResponse] {
        var models:[WalletStatusResponse] = []
        for item in array {
            models.append(WalletStatusResponse(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		success = (dictionary["success"] as? Bool) ?? false
		message = (dictionary["message"] as? Int) ?? 0
		isUseWallet = (dictionary["is_use_wallet"] as? Bool) ?? false
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.success, forKey: "success")
		dictionary.setValue(self.message, forKey: "message")
		dictionary.setValue(self.isUseWallet, forKey: "is_use_wallet")
        return dictionary
	}

}
