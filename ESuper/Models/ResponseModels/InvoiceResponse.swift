
import Foundation


public class InvoiceResponse {
	public var success : Bool?
	public var message : Int?
    public var status_phrase : String!
	public var order_payment : OrderPayment?
    public var store: StoreItem?
    public var itemDetail: ProductItemsItem?
    
    public var serverTime:String = ""
    public var timeZone:String = ""
    var vehicles:[VehicleDetail]
    var isAllowContactlessDelivery = false
    var isAllowUserToGiveTip = false
    public var tip_type:Int = 0
    var isTaxIncluded = false
    var isUseItemTax = false


/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let InvoiceResponse_list = InvoiceResponse.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of InvoiceResponse Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [InvoiceResponse] {
        var models:[InvoiceResponse] = []
        for item in array {
            models.append(InvoiceResponse(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {
        isAllowContactlessDelivery = (dictionary["is_allow_contactless_delivery"] as? Bool ?? false)
		success = (dictionary["success"] as? Bool?)!
		message = dictionary["message"] as? Int
        status_phrase = dictionary["status_phrase"] as? String ?? ""
		if (dictionary["order_payment"] != nil) { order_payment = OrderPayment(dictionary: dictionary["order_payment"] as! NSDictionary) }
        if (dictionary["store"] != nil) {
            store = StoreItem(dictionary: dictionary["store"] as! NSDictionary, defaultImage: "")
        }
        vehicles = []
        if let vehicleArray = dictionary["vehicles"] as? [[String:Any]]{
            for dic in vehicleArray{
                let value = VehicleDetail(fromDictionary: dic)
                vehicles.append(value)
            }
        }
        serverTime = (dictionary["server_time"] as? String) ?? ""
        timeZone = (dictionary["timezone"] as? String) ?? ""
        
        if dictionary["is_allow_user_to_give_tip"] != nil{
            isAllowUserToGiveTip = (dictionary["is_allow_user_to_give_tip"] as? Bool ?? false)
        }
        if dictionary["tip_type"] != nil{
            tip_type = (dictionary["tip_type"] as? Int)!
        }
        
        if dictionary["is_tax_included"] != nil{
            isTaxIncluded = (dictionary["is_tax_included"] as? Bool ?? false)
        }
        if dictionary["is_use_item_tax"] != nil{
            isUseItemTax
                = (dictionary["is_use_item_tax"] as? Bool ?? false)
        }
        
        if let itemDetail = dictionary["item_detail"] as? [String:Any] {
            self.itemDetail = ProductItemsItem(dictionary: itemDetail as NSDictionary)
        }

    }
}



public class VehicleDetail{
    
    var descriptionField : String!
    var imageUrl : String!
    var isBusiness : Bool!
    var mapPinImageUrl : String!
    var uniqueId : Int!
    var vehicleName : String!
    var vehicleId : String!
    var isSelected: Bool = false
    var price_per_unit_distance: Double = 0.0
    var total_delivery_price: Double = 0.0
    var service_id: String = ""
    var is_round_trip = false
    var round_trip_charge: Double = 0
    var additional_stop_price: Double = 0
    var size_type: Int = 0 // 1 = meter, 2 = centimeter, else no unit show
    var weight_type: Int = 0 // 1 = kg, 2 = gram, else no unit show
    var length: Double = 0
    var width: Double = 0
    var height: Double = 0
    var min_weight: Double = 0
    var max_weight: Double = 0

    var rich_area_surge_multiplier : Int = 1
    var service_tax : Int = 0
    var price_per_unit_time : Double = 0.0
    var surge_multiplier : Double = 0.0
    var total_surge_price : Double = 0.0
    var total_service_price : Double = 0.0
    var base_price : Double = 0.0
    var base_price_distance : Double = 0.0
    var is_surge_hours : Bool = false
    var is_surge_on_night : Bool = false
    var price_formula : Int = 0
    var total_admin_tax_price : Double = 0.0
    var is_min_fare_applied : Bool = false
    var is_default : Bool = false
    var min_fare : Double = 0.0
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]) {
        descriptionField = (dictionary["description"] as? String) ?? ""
        imageUrl = (dictionary["image_url"] as? String) ?? ""
        isBusiness = (dictionary["is_business"] as? Bool) ?? false
        mapPinImageUrl = (dictionary["map_pin_image_url"] as? String) ?? ""
        uniqueId = (dictionary["unique_id"] as? Int) ?? 0
        vehicleName = (dictionary["vehicle_name"] as? String) ?? ""
        vehicleId = (dictionary["_id"] as? String) ?? ""
        price_per_unit_distance = (dictionary["price_per_unit_distance"] as? Double) ?? 0.0
        total_delivery_price = (dictionary["total_delivery_price"] as? Double) ?? 0.0
        service_id = dictionary["service_id"] as? String ?? ""
        is_round_trip = (dictionary["is_round_trip"] as? Bool) ?? false
        is_default = (dictionary["is_default"] as? Bool) ?? false
        round_trip_charge = (dictionary["round_trip_charge"] as? Double) ?? 0
        additional_stop_price = (dictionary["additional_stop_price"] as? Double) ?? 0
        size_type = (dictionary["size_type"] as? Int) ?? 0
        weight_type = (dictionary["weight_type"] as? Int) ?? 0
        length = (dictionary["length"] as? Double) ?? 0
        width = (dictionary["width"] as? Double) ?? 0
        height = (dictionary["height"] as? Double) ?? 0
        min_weight = (dictionary["min_weight"] as? Double) ?? 0
        max_weight = (dictionary["max_weight"] as? Double) ?? 0

        rich_area_surge_multiplier = (dictionary["rich_area_surge_multiplier"] as? Int) ?? 1
        service_tax = (dictionary["service_tax"] as? Int) ?? 0
        price_per_unit_time = (dictionary["price_per_unit_time"] as? Double) ?? 0.0
        surge_multiplier = (dictionary["surge_multiplier"] as? Double) ?? 0.0
        total_surge_price = (dictionary["total_surge_price"] as? Double) ?? 0.0
        total_service_price = (dictionary["total_service_price"] as? Double) ?? 0.0
        base_price = (dictionary["base_price"] as? Double) ?? 0.0
        base_price_distance = (dictionary["base_price_distance"] as? Double) ?? 0.0
        is_surge_hours = (dictionary["is_surge_hours"] as? Bool) ?? false
        is_surge_on_night = (dictionary["is_surge_on_night"] as? Bool) ?? false
        price_formula = (dictionary["price_formula"] as? Int) ?? 0
        total_admin_tax_price = (dictionary["total_admin_tax_price"] as? Double) ?? 0.0
        is_min_fare_applied = (dictionary["is_min_fare_applied"] as? Bool) ?? false
        min_fare = (dictionary["min_fare"] as? Double) ?? 0.0
    }
}
