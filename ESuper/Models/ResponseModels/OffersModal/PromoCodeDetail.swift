//
//  PromoCodeDetail.swift
//  
//
//  Created by Elluminati on 4/7/21.
//  Copyright © 2021 Elluminati. All rights reserved.
//

import Foundation
class PromoCodeDetail : NSObject, NSCoding{

    var message : Int!
    var promoCodes : [PromoCodeModal]!
    var success : Bool!


    init(fromDictionary dictionary: [String:Any]){
        message = dictionary["message"] as? Int
        promoCodes = [PromoCodeModal]()
        if let promoCodesArray = dictionary["promo_code_detail"] as? [String:Any]{
            let dict = PromoCodeModal(fromDictionary: promoCodesArray)
            promoCodes.append(dict)
        }
        success = dictionary["success"] as? Bool
    }

    func toDictionary() -> [String:Any]{
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if promoCodes != nil{
            var dictionaryElements = [[String:Any]]()
            for promoCodesElement in promoCodes {
                dictionaryElements.append(promoCodesElement.toDictionary())
            }
            dictionary["promo_code_detail"] = dictionaryElements
        }
        if success != nil{
            dictionary["success"] = success
        }
        return dictionary
    }

   
    @objc required init(coder aDecoder: NSCoder){
         message = aDecoder.decodeObject(forKey: "message") as? Int
         promoCodes = aDecoder.decodeObject(forKey :"promo_codes") as? [PromoCodeModal]
         success = aDecoder.decodeObject(forKey: "success") as? Bool

    }

    @objc func encode(with aCoder: NSCoder){
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if promoCodes != nil{
            aCoder.encode(promoCodes, forKey: "promo_codes")
        }
        if success != nil{
            aCoder.encode(success, forKey: "success")
        }
    }
}
