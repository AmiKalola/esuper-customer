
import Foundation
 
public class GoogleDistanceMatrixResponse {
	public var destination_addresses : Array<String>?
	public var origin_addresses : Array<String>?
	public var rows : Array<Rows>?
	public var status : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [GoogleDistanceMatrixResponse] {
        var models:[GoogleDistanceMatrixResponse] = []
        for item in array {
            models.append(GoogleDistanceMatrixResponse(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
	required public init?(dictionary: NSDictionary) {

		if (dictionary["destination_addresses"] != nil) {
            destination_addresses = dictionary["destination_addresses"] as? Array<String>
        }
		if (dictionary["origin_addresses"] != nil) {
            origin_addresses = dictionary["origin_addresses"] as? Array<String>
        }
		if (dictionary["rows"] != nil) {
            rows = Rows.modelsFromDictionaryArray(array: dictionary["rows"] as! NSArray) }
		status = dictionary["status"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.status, forKey: "status")

		return dictionary
	}
}
