import Foundation

public class RedeemPointHistoryResponse {
	public var success : Bool?
	public var message : Int?
	public var redeem_history : Array<RedeemHistory>?
	public var total_redeem_point : Int?
	public var user_redeem_point_value : Double?
	public var user_minimum_point_require_for_withdrawal : Int?
	public var provider_redeem_point_value : Double?
	public var provider_minimum_point_require_for_withdrawal : Int?
	public var status_phrase : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [RedeemPointHistoryResponse]{
        var models:[RedeemPointHistoryResponse] = []
        for item in array{
            models.append(RedeemPointHistoryResponse(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		success = dictionary["success"] as? Bool
		message = dictionary["message"] as? Int
        if (dictionary["redeem_history"] != nil) { redeem_history = RedeemHistory.modelsFromDictionaryArray(array: dictionary["redeem_history"] as! NSArray) }
		total_redeem_point = dictionary["total_redeem_point"] as? Int
		user_redeem_point_value = dictionary["user_redeem_point_value"] as? Double
		user_minimum_point_require_for_withdrawal = dictionary["user_minimum_point_require_for_withdrawal"] as? Int
		provider_redeem_point_value = dictionary["provider_redeem_point_value"] as? Double
		provider_minimum_point_require_for_withdrawal = dictionary["provider_minimum_point_require_for_withdrawal"] as? Int
		status_phrase = dictionary["status_phrase"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {
		let dictionary = NSMutableDictionary()
		dictionary.setValue(self.success, forKey: "success")
		dictionary.setValue(self.message, forKey: "message")
		dictionary.setValue(self.total_redeem_point, forKey: "total_redeem_point")
		dictionary.setValue(self.user_redeem_point_value, forKey: "user_redeem_point_value")
		dictionary.setValue(self.user_minimum_point_require_for_withdrawal, forKey: "user_minimum_point_require_for_withdrawal")
		dictionary.setValue(self.provider_redeem_point_value, forKey: "provider_redeem_point_value")
		dictionary.setValue(self.provider_minimum_point_require_for_withdrawal, forKey: "provider_minimum_point_require_for_withdrawal")
		dictionary.setValue(self.status_phrase, forKey: "status_phrase")

		return dictionary
	}

}
