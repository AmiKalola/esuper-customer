

import Foundation
public class Invoice {
	public var title : String?
	public var subTitle : String?
	public var price : String?
	public var isHideSubTitle : Bool?
    public var type: Int = InvoiceCellType.Regular.rawValue

    required public init?(title:String, subTitle:String, price:String, type: Int) {
		self.title = title
		self.subTitle = subTitle
		self.price = price
        self.type = type
	}

	public func dictionaryRepresentation() -> NSDictionary {
		let dictionary = NSMutableDictionary()
		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.subTitle, forKey: "subTitle")
		dictionary.setValue(self.price, forKey: "price")
        dictionary.setValue(self.type, forKey: "type")
		return dictionary
	}
}
