

import Foundation
 

public class Order_details {
	public var order_items : Array<Items>?
	public var total_item_price : Int?
	public var product_name : String?
	public var unique_id : Int?
	public var product_id : String?


    public class func modelsFromDictionaryArray(array:NSArray) -> [Order_details] {
        var models:[Order_details] = []
        for item in array {
            models.append(Order_details(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {
		if (dictionary["items"] != nil) { order_items = Items.modelsFromDictionaryArray(array: dictionary["items"] as! NSArray) }
		total_item_price = dictionary["total_item_price"] as? Int
		product_name = dictionary["product_name"] as? String
		unique_id = dictionary["unique_id"] as? Int
		product_id = dictionary["product_id"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {
		let dictionary = NSMutableDictionary()
		dictionary.setValue(self.total_item_price, forKey: "total_item_price")
		dictionary.setValue(self.product_name, forKey: "product_name")
		dictionary.setValue(self.unique_id, forKey: "unique_id")
		dictionary.setValue(self.product_id, forKey: "product_id")
		return dictionary
	}
}

public class OrderPaymentDetail {
    public var _id : String?
    public var cart_id : String?
    public var store_id : String?
    public var user_id : String?
    public var payment_intent_id : String?
    public var city_id : String?
    public var capture_amount : Double?
    public var tip_amount : Int?
    public var tip_value : Int?
    public var country_id : String?
    public var promo_id : String?
    public var taxes : Array<String>?
    public var delivery_price_used_type : Int?
    public var delivery_price_used_type_id : String?
    public var currency_code : String?
    public var admin_currency_code : String?
    public var order_currency_code : String?
    public var current_rate : Int?
    public var wallet_to_admin_current_rate : Int?
    public var wallet_to_order_current_rate : Int?
    public var total_distance : Double?
    public var total_time : Double?
    public var total_item_count : Int?
    public var is_distance_unit_mile : Bool?
    public var service_tax : Int?
    public var total_service_price : Double?
    public var total_admin_tax_price : Double?
    public var total_delivery_price : Double?
    public var pay_to_provider : Int?
    public var admin_profit_mode_on_delivery : Int?
    public var admin_profit_value_on_delivery : Int?
    public var total_admin_profit_on_delivery : Int?
    public var total_provider_income : Int?
    public var item_tax : Int?
    public var total_cart_price : Int?
    public var total_store_tax_price : Int?
    public var total_order_price : Int?
    public var total_waiting_time : Double?
    public var total_waiting_time_charge : Double?
    public var other_promo_payment_loyalty : Int?
    public var pay_to_store : Int?
    public var admin_profit_mode_on_store : Int?
    public var admin_profit_value_on_store : Int?
    public var total_admin_profit_on_store : Int?
    public var total_store_income : Int?
    public var promo_payment : Double?
    public var user_pay_payment : Double?
    public var wallet_payment : Int?
    public var total_after_wallet_payment : Double?
    public var remaining_payment : Int?
    public var total : Int?
    public var cash_payment : Int?
    public var card_payment : Int?
    public var is_paid_from_wallet : Bool?
    public var is_promo_for_delivery_service : Bool?
    public var is_payment_mode_cash : Bool?
    public var is_payment_paid : Bool?
    public var is_order_price_paid_by_store : Bool?
    public var is_store_pay_delivery_fees : Bool?
    public var payment_id : String?
    public var is_min_fare_applied : Bool?
    public var is_transfered_to_store : Bool?
    public var is_transfered_to_provider : Bool?
    public var is_user_pick_up_order : Bool?
    public var is_order_payment_status_set_by_store : Bool?
    public var is_cancellation_fee : Bool?
    public var order_cancellation_charge : Int?
    public var is_order_payment_refund : Bool?
    public var refund_amount : Int?
    public var is_provider_income_set_in_wallet : Bool?
    public var is_store_income_set_in_wallet : Bool?
    public var provider_income_set_in_wallet : Int?
    public var store_income_set_in_wallet : Int?
    public var booking_fees : Int?
    public var service_taxes : Array<String>?
    public var completed_date_tag : String?
    public var completed_date_in_city_timezone : String?
    public var created_at : String?
    public var updated_at : String?
    public var unique_id : Int?
    public var __v : Int?
    public var invoice_number : String?
    public var order_id : String?
    public var order_unique_id : Int?
    public var total_base_price : Double?
    public var total_distance_price : Double?
    public var total_time_price : Double?
    public var promo_code_name : String?
    public var additional_stop_price : Double = 0.0
    public var price_formula : Int = 0
    public var total_surge_price : Double = 0.0
    public var surge_charges: Double = 0.0
    public var surge_multiplier : Double = 0.0
    public class func modelsFromDictionaryArray(array:NSArray) -> [OrderPaymentDetail]{
        var models:[OrderPaymentDetail] = []
        for item in array{
            models.append(OrderPaymentDetail(dictionary: item as! NSDictionary)!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary) {
        _id = dictionary["_id"] as? String
        cart_id = dictionary["cart_id"] as? String
        store_id = dictionary["store_id"] as? String
        user_id = dictionary["user_id"] as? String
        payment_intent_id = dictionary["payment_intent_id"] as? String
        city_id = dictionary["city_id"] as? String
        capture_amount = dictionary["capture_amount"] as? Double
        tip_amount = dictionary["tip_amount"] as? Int
        tip_value = dictionary["tip_value"] as? Int
        country_id = dictionary["country_id"] as? String
        promo_id = dictionary["promo_id"] as? String
        //if (dictionary["taxes"] != nil) { taxes = Taxes.modelsFromDictionaryArray(dictionary["taxes"] as! NSArray) }
        delivery_price_used_type = dictionary["delivery_price_used_type"] as? Int
        delivery_price_used_type_id = dictionary["delivery_price_used_type_id"] as? String
        currency_code = dictionary["currency_code"] as? String
        admin_currency_code = dictionary["admin_currency_code"] as? String
        order_currency_code = dictionary["order_currency_code"] as? String
        current_rate = dictionary["current_rate"] as? Int
        wallet_to_admin_current_rate = dictionary["wallet_to_admin_current_rate"] as? Int
        wallet_to_order_current_rate = dictionary["wallet_to_order_current_rate"] as? Int
        total_distance = dictionary["total_distance"] as? Double
        total_time = dictionary["total_time"] as? Double
        total_item_count = dictionary["total_item_count"] as? Int
        is_distance_unit_mile = dictionary["is_distance_unit_mile"] as? Bool
        service_tax = dictionary["service_tax"] as? Int
        total_service_price = dictionary["total_service_price"] as? Double
        total_admin_tax_price = dictionary["total_admin_tax_price"] as? Double
        total_delivery_price = dictionary["total_delivery_price"] as? Double
        additional_stop_price = dictionary["additional_stop_price"] as? Double ?? 0.0
        total_surge_price = dictionary["total_surge_price"] as? Double ?? 0.0
        surge_charges = dictionary["surge_charges"] as? Double ?? 0.0
        surge_multiplier = dictionary["surge_multiplier"] as? Double ?? 0.0
        pay_to_provider = dictionary["pay_to_provider"] as? Int
        price_formula = dictionary["price_formula"] as? Int ?? 0
        admin_profit_mode_on_delivery = dictionary["admin_profit_mode_on_delivery"] as? Int
        admin_profit_value_on_delivery = dictionary["admin_profit_value_on_delivery"] as? Int
        total_admin_profit_on_delivery = dictionary["total_admin_profit_on_delivery"] as? Int
        total_provider_income = dictionary["total_provider_income"] as? Int
        item_tax = dictionary["item_tax"] as? Int
        total_cart_price = dictionary["total_cart_price"] as? Int
        total_store_tax_price = dictionary["total_store_tax_price"] as? Int
        total_order_price = dictionary["total_order_price"] as? Int
        total_waiting_time = dictionary["total_waiting_time"] as? Double ?? 0
        total_waiting_time_charge = dictionary["total_waiting_time_charge"] as? Double ?? 0
        other_promo_payment_loyalty = dictionary["other_promo_payment_loyalty"] as? Int
        pay_to_store = dictionary["pay_to_store"] as? Int
        admin_profit_mode_on_store = dictionary["admin_profit_mode_on_store"] as? Int
        admin_profit_value_on_store = dictionary["admin_profit_value_on_store"] as? Int
        total_admin_profit_on_store = dictionary["total_admin_profit_on_store"] as? Int
        total_store_income = dictionary["total_store_income"] as? Int
        promo_payment = dictionary["promo_payment"] as? Double
        user_pay_payment = dictionary["user_pay_payment"] as? Double ?? 0
        wallet_payment = dictionary["wallet_payment"] as? Int
        total_after_wallet_payment = dictionary["total_after_wallet_payment"] as? Double
        remaining_payment = dictionary["remaining_payment"] as? Int
        total = dictionary["total"] as? Int
        cash_payment = dictionary["cash_payment"] as? Int
        card_payment = dictionary["card_payment"] as? Int
        is_paid_from_wallet = dictionary["is_paid_from_wallet"] as? Bool
        is_promo_for_delivery_service = dictionary["is_promo_for_delivery_service"] as? Bool
        is_payment_mode_cash = dictionary["is_payment_mode_cash"] as? Bool
        is_payment_paid = dictionary["is_payment_paid"] as? Bool
        is_order_price_paid_by_store = dictionary["is_order_price_paid_by_store"] as? Bool
        is_store_pay_delivery_fees = dictionary["is_store_pay_delivery_fees"] as? Bool
        payment_id = dictionary["payment_id"] as? String
        is_min_fare_applied = dictionary["is_min_fare_applied"] as? Bool
        is_transfered_to_store = dictionary["is_transfered_to_store"] as? Bool
        is_transfered_to_provider = dictionary["is_transfered_to_provider"] as? Bool
        is_user_pick_up_order = dictionary["is_user_pick_up_order"] as? Bool
        is_order_payment_status_set_by_store = dictionary["is_order_payment_status_set_by_store"] as? Bool
        is_cancellation_fee = dictionary["is_cancellation_fee"] as? Bool
        order_cancellation_charge = dictionary["order_cancellation_charge"] as? Int
        is_order_payment_refund = dictionary["is_order_payment_refund"] as? Bool
        refund_amount = dictionary["refund_amount"] as? Int
        is_provider_income_set_in_wallet = dictionary["is_provider_income_set_in_wallet"] as? Bool
        is_store_income_set_in_wallet = dictionary["is_store_income_set_in_wallet"] as? Bool
        provider_income_set_in_wallet = dictionary["provider_income_set_in_wallet"] as? Int
        store_income_set_in_wallet = dictionary["store_income_set_in_wallet"] as? Int
        booking_fees = dictionary["booking_fees"] as? Int
 
        completed_date_tag = dictionary["completed_date_tag"] as? String
        completed_date_in_city_timezone = dictionary["completed_date_in_city_timezone"] as? String
        created_at = dictionary["created_at"] as? String
        updated_at = dictionary["updated_at"] as? String
        unique_id = dictionary["unique_id"] as? Int
        __v = dictionary["__v"] as? Int
        invoice_number = dictionary["invoice_number"] as? String
        order_id = dictionary["order_id"] as? String
        order_unique_id = dictionary["order_unique_id"] as? Int
        total_base_price = dictionary["total_base_price"] as? Double ?? 0
        total_distance_price = dictionary["total_distance_price"] as? Double ?? 0
        total_time_price = dictionary["total_time_price"] as? Double ?? 0
        total_waiting_time = dictionary["total_waiting_time"] as? Double ?? 0
        promo_code_name = dictionary["promo_code_name"] as? String ?? ""
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self._id, forKey: "_id")
        dictionary.setValue(self.cart_id, forKey: "cart_id")
        dictionary.setValue(self.store_id, forKey: "store_id")
        dictionary.setValue(self.user_id, forKey: "user_id")
        dictionary.setValue(self.payment_intent_id, forKey: "payment_intent_id")
        dictionary.setValue(self.city_id, forKey: "city_id")
        dictionary.setValue(self.capture_amount, forKey: "capture_amount")
        dictionary.setValue(self.tip_amount, forKey: "tip_amount")
        dictionary.setValue(self.tip_value, forKey: "tip_value")
        dictionary.setValue(self.country_id, forKey: "country_id")
        dictionary.setValue(self.promo_id, forKey: "promo_id")
        dictionary.setValue(self.delivery_price_used_type, forKey: "delivery_price_used_type")
        dictionary.setValue(self.delivery_price_used_type_id, forKey: "delivery_price_used_type_id")
        dictionary.setValue(self.currency_code, forKey: "currency_code")
        dictionary.setValue(self.admin_currency_code, forKey: "admin_currency_code")
        dictionary.setValue(self.order_currency_code, forKey: "order_currency_code")
        dictionary.setValue(self.current_rate, forKey: "current_rate")
        dictionary.setValue(self.wallet_to_admin_current_rate, forKey: "wallet_to_admin_current_rate")
        dictionary.setValue(self.wallet_to_order_current_rate, forKey: "wallet_to_order_current_rate")
        dictionary.setValue(self.total_distance, forKey: "total_distance")
        dictionary.setValue(self.total_time, forKey: "total_time")
        dictionary.setValue(self.total_item_count, forKey: "total_item_count")
        dictionary.setValue(self.is_distance_unit_mile, forKey: "is_distance_unit_mile")
        dictionary.setValue(self.service_tax, forKey: "service_tax")
        dictionary.setValue(self.total_service_price, forKey: "total_service_price")
        dictionary.setValue(self.total_admin_tax_price, forKey: "total_admin_tax_price")
        dictionary.setValue(self.total_delivery_price, forKey: "total_delivery_price")
        dictionary.setValue(self.pay_to_provider, forKey: "pay_to_provider")
        dictionary.setValue(self.admin_profit_mode_on_delivery, forKey: "admin_profit_mode_on_delivery")
        dictionary.setValue(self.admin_profit_value_on_delivery, forKey: "admin_profit_value_on_delivery")
        dictionary.setValue(self.total_admin_profit_on_delivery, forKey: "total_admin_profit_on_delivery")
        dictionary.setValue(self.total_provider_income, forKey: "total_provider_income")
        dictionary.setValue(self.item_tax, forKey: "item_tax")
        dictionary.setValue(self.total_cart_price, forKey: "total_cart_price")
        dictionary.setValue(self.total_store_tax_price, forKey: "total_store_tax_price")
        dictionary.setValue(self.total_order_price, forKey: "total_order_price")
        dictionary.setValue(self.total_waiting_time, forKey: "total_waiting_time")
        dictionary.setValue(self.total_waiting_time_charge, forKey: "total_waiting_time_charge")
        dictionary.setValue(self.other_promo_payment_loyalty, forKey: "other_promo_payment_loyalty")
        dictionary.setValue(self.pay_to_store, forKey: "pay_to_store")
        dictionary.setValue(self.admin_profit_mode_on_store, forKey: "admin_profit_mode_on_store")
        dictionary.setValue(self.admin_profit_value_on_store, forKey: "admin_profit_value_on_store")
        dictionary.setValue(self.total_admin_profit_on_store, forKey: "total_admin_profit_on_store")
        dictionary.setValue(self.total_store_income, forKey: "total_store_income")
        dictionary.setValue(self.promo_payment, forKey: "promo_payment")
        dictionary.setValue(self.user_pay_payment, forKey: "user_pay_payment")
        dictionary.setValue(self.wallet_payment, forKey: "wallet_payment")
        dictionary.setValue(self.total_after_wallet_payment, forKey: "total_after_wallet_payment")
        dictionary.setValue(self.remaining_payment, forKey: "remaining_payment")
        dictionary.setValue(self.total, forKey: "total")
        dictionary.setValue(self.cash_payment, forKey: "cash_payment")
        dictionary.setValue(self.card_payment, forKey: "card_payment")
        dictionary.setValue(self.is_paid_from_wallet, forKey: "is_paid_from_wallet")
        dictionary.setValue(self.is_promo_for_delivery_service, forKey: "is_promo_for_delivery_service")
        dictionary.setValue(self.is_payment_mode_cash, forKey: "is_payment_mode_cash")
        dictionary.setValue(self.is_payment_paid, forKey: "is_payment_paid")
        dictionary.setValue(self.is_order_price_paid_by_store, forKey: "is_order_price_paid_by_store")
        dictionary.setValue(self.is_store_pay_delivery_fees, forKey: "is_store_pay_delivery_fees")
        dictionary.setValue(self.payment_id, forKey: "payment_id")
        dictionary.setValue(self.is_min_fare_applied, forKey: "is_min_fare_applied")
        dictionary.setValue(self.is_transfered_to_store, forKey: "is_transfered_to_store")
        dictionary.setValue(self.is_transfered_to_provider, forKey: "is_transfered_to_provider")
        dictionary.setValue(self.is_user_pick_up_order, forKey: "is_user_pick_up_order")
        dictionary.setValue(self.is_order_payment_status_set_by_store, forKey: "is_order_payment_status_set_by_store")
        dictionary.setValue(self.is_cancellation_fee, forKey: "is_cancellation_fee")
        dictionary.setValue(self.order_cancellation_charge, forKey: "order_cancellation_charge")
        dictionary.setValue(self.is_order_payment_refund, forKey: "is_order_payment_refund")
        dictionary.setValue(self.refund_amount, forKey: "refund_amount")
        dictionary.setValue(self.is_provider_income_set_in_wallet, forKey: "is_provider_income_set_in_wallet")
        dictionary.setValue(self.is_store_income_set_in_wallet, forKey: "is_store_income_set_in_wallet")
        dictionary.setValue(self.provider_income_set_in_wallet, forKey: "provider_income_set_in_wallet")
        dictionary.setValue(self.store_income_set_in_wallet, forKey: "store_income_set_in_wallet")
        dictionary.setValue(self.booking_fees, forKey: "booking_fees")
        dictionary.setValue(self.completed_date_tag, forKey: "completed_date_tag")
        dictionary.setValue(self.completed_date_in_city_timezone, forKey: "completed_date_in_city_timezone")
        dictionary.setValue(self.created_at, forKey: "created_at")
        dictionary.setValue(self.updated_at, forKey: "updated_at")
        dictionary.setValue(self.unique_id, forKey: "unique_id")
        dictionary.setValue(self.__v, forKey: "__v")
        dictionary.setValue(self.invoice_number, forKey: "invoice_number")
        dictionary.setValue(self.order_id, forKey: "order_id")
        dictionary.setValue(self.order_unique_id, forKey: "order_unique_id")
        dictionary.setValue(self.total_base_price, forKey: "total_base_price")
        dictionary.setValue(self.total_distance_price, forKey: "total_distance_price")
        dictionary.setValue(self.total_time_price, forKey: "total_time_price")
        dictionary.setValue(self.promo_code_name, forKey: "promo_code_name")
        
        dictionary.setValue(self.additional_stop_price, forKey: "additional_stop_price")
        dictionary.setValue(self.total_surge_price, forKey: "total_surge_price")
        dictionary.setValue(self.surge_charges, forKey: "surge_charges")
        dictionary.setValue(self.surge_multiplier, forKey: "surge_multiplier")
        dictionary.setValue(self.price_formula, forKey: "price_formula")
        
       
        
        return dictionary
    }
}

