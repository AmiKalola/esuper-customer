

import Foundation

public class User_detail {
	public var first_name : String?
	public var last_name : String?
	public var image_url : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [User_detail] {
        var models:[User_detail] = []
        for item in array {
            models.append(User_detail(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {
		first_name = dictionary["first_name"] as? String
		last_name = dictionary["last_name"] as? String
		image_url = dictionary["image_url"] as? String
	}
		
	public func dictionaryRepresentation() -> NSDictionary {
		let dictionary = NSMutableDictionary()
		dictionary.setValue(self.first_name, forKey: "first_name")
		dictionary.setValue(self.last_name, forKey: "last_name")
		dictionary.setValue(self.image_url, forKey: "image_url")
		return dictionary
	}
}

public class ProviderDetail {
    public var name : String?
    public var phone : String?
    public var address : String?
    public var image_url : String?
    public var email : String?
    public var _id: String?
    public var user_rate: Double?
    public var vehicle_name: String = ""
    public var vehicle_plate_no: String = ""
    public var location: [Double] = [0,0]
    public var previous_location: [Double] = [0,0]
    public var device_token : String?
    public var vehicle_model : String?
    public var vehicle_color : String?
    
    
    //"vehicle_name" = Audi;
     //"vehicle_plate_no" = shocks;
     //"vehicle_model" = hood;
     //"vehicle_color" = blue;
    
    required public init?(dictionary: NSDictionary) {
        name = dictionary["name"] as? String
        phone = dictionary["phone"] as? String
        address = dictionary["address"] as? String
        image_url = dictionary["image_url"] as? String
        email = dictionary["email"] as? String
        _id = dictionary["_id"] as? String
        user_rate = dictionary["user_rate"] as? Double ?? 0
        vehicle_name = dictionary["vehicle_name"] as? String ?? ""
        vehicle_plate_no = dictionary["vehicle_plate_no"] as? String ?? ""
        device_token = dictionary["device_token"] as? String ?? ""
        vehicle_model = dictionary["vehicle_model"] as? String ?? ""
        vehicle_color = dictionary["vehicle_color"] as? String ?? ""
        
        if let value = dictionary["location"] as? [Double] {
            location = value
        }
        if let value = dictionary["previous_location"] as? [Double] {
            previous_location = value
        }
    }
    
    func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.phone, forKey: "phone")
        dictionary.setValue(self.image_url, forKey: "image_url")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self._id, forKey: "_id")
        dictionary.setValue(self.vehicle_name, forKey: "vehicle_name")
        dictionary.setValue(self.vehicle_plate_no, forKey: "vehicle_plate_no")
        dictionary.setValue(self.location, forKey: "location")
        dictionary.setValue(self.previous_location, forKey: "previous_location")
        dictionary.setValue(self.address, forKey: "address")
        return dictionary
    }
}
