//
//  StoreData.swift
//  ESuper
//
//  Created by Rohit on 04/03/24.
//  Copyright © 2024 Elluminati. All rights reserved.
//

import Foundation
public class StoreDatatResponse {
    var Promo_codes : PromoCodeModal?
    var message : Int?
    var status_phrase : String?
    var store_detail : StoreDataDetail?
    var success : Bool?
    
    required public init?(dictionary: NSDictionary) {
        if (dictionary["store_detail"] != nil) {
            if let store = (dictionary["store_detail"] as? NSDictionary){
                store_detail = StoreDataDetail.init(dictionary: store)
            }
        }
        message = dictionary["message"] as? Int
//        currency = dictionary["currency"] as? String
        success = dictionary["success"] as? Bool
        
    }
}

class StoreDataDetail{
    var table_settings_details : TableSettingsDetails?
    required public init?(dictionary: NSDictionary) {
        if let store = (dictionary["table_settings_details"] as? NSDictionary){
            table_settings_details = TableSettingsDetails.init(dictionary: store)
        }
    }
}
class TableSettingsDetails{
    var reservation_max_days : Int?
    required public init?(dictionary: NSDictionary) {
        reservation_max_days = dictionary["reservation_max_days"] as? Int
    }
}
