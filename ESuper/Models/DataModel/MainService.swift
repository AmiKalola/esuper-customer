//
//  MainService.swift
//  Decagon
//
//  Created by MacPro3 on 18/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import Foundation
struct MainService: Equatable {
    var delivery_type : Int = 0
    var image_url : String = ""
    var name : String = ""
}
