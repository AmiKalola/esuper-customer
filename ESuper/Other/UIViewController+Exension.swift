//
//  UIViewController+Exension.swift
//  Edelivery
//
//  Created by Rohit on 31/03/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController{
    func presentFromBottom(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.fade
        self.view.window!.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false)
    }
    
    func getCountries(completion: (([CountryCode])->())?) {
        var listCountryCode = [CountryCode]()
        self.getWebData(method: "GET", webURL: WebService.WS_GET_COUNTRY_LIST, parameters: "", completion: { parsedData, status in
            if  (parsedData["success"] as! Bool){
                let info = parsedData["country_list"] as! [JSONType]
                listCountryCode.append(contentsOf: info.compactMap(CountryCode.init))
                DispatchQueue.main.async {
                    completion?(listCountryCode)
                }
                
            }else{
                DispatchQueue.main.async {
                    completion?(listCountryCode)
                }
            }
        })
    }
    func getWebData(method : String = "POST", webURL : String, parameters: String,isHeader : Bool = false, completion:(([String:Any],(Int))->())?) {
        WebService.webserviceFetch(method: method, webURL: webURL, parameters: parameters,isHeader: isHeader) { (parsedData,error,httpResponse) in
            
            if(error){
                DispatchQueue.main.async {
                    let parsedData1:[String:Any] = [
                        "A":"Please check network connection and try again.",
                    ]
                    completion?(parsedData1,1)
                }
            }else if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 300{
                DispatchQueue.main.async {
                    completion?(parsedData,2)
                }
            }else if httpResponse.statusCode == 401{
                var Error : String!
                Error = (parsedData["message"] as! String)
                DispatchQueue.main.async {
                    let parsedData1:[String:Any] = [
                        "A":"401",
                    ]
                    completion?(parsedData1,3)
                }
            }else if httpResponse.statusCode == 403{
//                UserDefaults().setLoggedIn(value: false)
//                let mainStoryboard:UIStoryboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
//                let changePassword = mainStoryboard.instantiateViewController(identifier: "LoginVC") as! LoginVC
//                self.present(changePassword, animated: true, completion: nil)
            }
        }
    }
}

extension WebService {
    static func webserviceFetch(method : String = "POST", webURL : String, parameters: String,isHeader : Bool = false, completion:(([String:Any],(Bool),(HTTPURLResponse))->())?) {
        var urlPath = ""
        urlPath = WebService.BASE_URL.appending(webURL)
        if method == "GET" || method == "DELETE"{
            urlPath = urlPath.appending(parameters)
        }
        let url = NSURL(string:urlPath as String)
        //print(url!)
        var request = URLRequest(url: url! as URL)
        if isHeader{
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Authorization")
//            request.setValue(UserDefaults().getToken(), forHTTPHeaderField: "authorization")
        }
        if method == "POST" || method == "PUT" {
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }
        request.httpMethod = method
        if method == "POST" || method == "PUT" {
            request.httpBody = parameters.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        }
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let httpResponse = response as? HTTPURLResponse
            print("error \(String(describing: httpResponse?.statusCode))")
            if error != nil {
                DispatchQueue.main.async {
                    let parsedData:[String:Any] = [  // ["b": 12]
                        "A":"A",
                    ]
                    completion?(parsedData,true,HTTPURLResponse())
                }
            }
            guard let data = data, error == nil else {
                //print(error!)                                 // some fundamental network error
                return
            }
            do {
                if let parsedData = try? (JSONSerialization.jsonObject(with: data) as! [String:Any]) {
                    DispatchQueue.main.async {
                        completion?(parsedData, false,httpResponse!)
                    }
                }else if let parsedData = try? (JSONSerialization.jsonObject(with: data) as! NSDictionary){
//                    //print(parsedData)
                }
            }
        }
        task.resume()
    }
}

