//
//  UITableView+Extension.swift
//  ESuper
//
//  Created by Rohit on 29/06/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit
extension UITableView{
    var isTableHeaderViewVisible: Bool {
        guard let tableHeaderView = tableHeaderView else {
            return false
        }
        let currentYOffset = self.contentOffset.y
        let headerHeight = tableHeaderView.frame.size.height - 160
        return currentYOffset < headerHeight
    }
    
}
