//
//  URL+Extension.swift
//  ESuper
//
//  Created by Rohit on 06/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
extension URL {
    var queryDictionary: [String: String]? {
        guard let query = self.query else {
            return nil
        }
        var queryStrings = [String: String]()
        for pair in query.components(separatedBy: "&") {
            if pair.contains("=") {
                let key = pair.components(separatedBy: "=")[0]
                let value = pair
                    .components(separatedBy:"=")[1]
                    .replacingOccurrences(of: "+", with: " ")
                    .removingPercentEncoding ?? ""
                queryStrings[key] = value
            }
        }
        return queryStrings
    }
}
