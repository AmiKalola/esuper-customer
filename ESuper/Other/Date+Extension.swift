//
//  Date+Extension.swift
//  ESuper
//
//  Created by Rohit on 24/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
extension Date{
    
    func convertZDateMMMddyyyy(date : String ) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        let myString = formatter.date(from: date) // string purpose I add here
        formatter.dateFormat = "MMM dd, yyyy"
        let fromDate = formatter.string(from: myString!)
        return fromDate
    }
    func convertZDate(date : String ) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        let myString = formatter.date(from: date) // string purpose I add here
        formatter.dateFormat = "HH:mm:ss"
        let fromDate = formatter.string(from: myString!)
        return fromDate
    }
    var todays : String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let CreatedDate = formatter.string(from: Date())
        return CreatedDate
    }
    var todaysTime : String{
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let CreatedDate = formatter.string(from: Date())
        return CreatedDate
    }
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "HH:mm:ss"

        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }

        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        
        let hourSec = ((Int(hour)) * 3600)
        let minutesSec = ((Int(minute)) * 60)
        
        var seconds = (Int(interval)) - hourSec
        seconds = (Int(seconds)) - minutesSec
        
        return  "\(intervalInt)"
    }
    
}

