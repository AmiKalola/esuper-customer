//
//  WhichDeliveryCell.swift
//  Decagon
//
//  Created by MacPro3 on 09/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class WhichDeliveryCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDeliveryName: LabelDefault!
    @IBOutlet weak var imgDelivery: ImageViewDefault!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgDelivery.applyRoundedCornersWithHeight(10)
    }
    
    func configData(data: DeliveriesItem) {
        lblDeliveryName.text = data.delivery_name ?? "-"
        
        imgDelivery.downloadedFrom(link: data.image_url ?? "", placeHolder: "placeholder", isFromCache: true, isIndicator: false, mode: .scaleAspectFit, isAppendBaseUrl: true, isFromResize: false, completion: nil)
        
    }

}
