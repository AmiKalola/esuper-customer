//
//  CustomPhotoDialog.swift
//  
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
import UIKit


public class DeliveryListPopUp: CustomDialog {
 
    //MARK: - OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var colletionView: UICollectionView!
    
    @IBOutlet weak var heightColletioView: NSLayoutConstraint!
 
    //MARK:Variables
    var onClickRightButton : ((_ obj: DeliveriesItem) -> Void)? = nil
    var onClickChoose : (() -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    static let  dialogNibName = "DeliveryListPopUp"
    var arrDelivery: [DeliveriesItem] = []
    static let spacing: CGFloat = 15
    static let edge: CGFloat = 20
    
    var selectedIndex = 0
    
    let colletionCellSize: CGSize = {
        let width = screenWidth - 2*spacing - 2*edge
        let returnWidth = width/3
        return CGSize(width: returnWidth, height: returnWidth*0.8 + 40)
    }()
    
    public static func  showDeliveryListDialog
        (title:String,
         strRightButton:String,
         deliveryType: Int,
         arrDelivery: [DeliveriesItem],
         tag:Int = 999
         ) -> DeliveryListPopUp
     {
        let view = UINib(nibName: dialogNibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DeliveryListPopUp
        view.tag = tag
        if ((APPDELEGATE.window?.viewWithTag(tag)) != nil) {
            APPDELEGATE.window?.bringSubviewToFront((APPDELEGATE.window?.viewWithTag(tag))!)
        }else {
            view.setLocalization()
            let frame = (APPDELEGATE.window?.frame)!;
            view.frame = frame;
            view.lblTitle.text = title;
            view.btnRight.setTitle(strRightButton, for: UIControl.State.normal)
            view.colletionView.addObserver(view, forKeyPath: "contentSize", options: .new, context: nil)
            view.arrDelivery = arrDelivery
            view.colletionView.reloadData()
            view.setColletionView()
        
            DispatchQueue.main.async{
                UIApplication.shared.keyWindow?.addSubview(view)
                APPDELEGATE.window?.bringSubviewToFront(view);
                view.alertView.layoutIfNeeded()
                view.animationBottomTOTop(view.alertView)
            }
        }
       
        return view;
    }

    func setLocalization() {
        alertView.backgroundColor = UIColor.themeAlertViewBackgroundColor
        backgroundColor = UIColor.themeOverlayColor
        lblTitle.textColor = UIColor.themeTextColor;
        lblTitle.font = FontHelper.textLarge(size: FontHelper.large)
        
        btnRight.titleLabel?.font = FontHelper.textRegular(size: 14)
        btnLeft.setImage(UIImage.init(named: "cancelIcon")?.imageWithColor(color: .themeColor), for: .normal)
        self.alertView.updateConstraintsIfNeeded()
        self.alertView.roundCorner(corners: [.topRight , .topLeft], withRadius: 20)
    }
    
    func setColletionView() {
        colletionView.delegate = self
        colletionView.dataSource = self
        colletionView.register(UINib(nibName: "WhichDeliveryCell", bundle: nil), forCellWithReuseIdentifier: "WhichDeliveryCell")
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        colletionView.collectionViewLayout = layout
    }
    
    override func updateUIAccordingToTheme() {
        self.setLocalization()
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        heightColletioView.constant = colletionView.contentSize.height
    }

    //ActionMethods
    @IBAction func onClickBtnLeft(_ sender: Any) {
      
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                       animations: {
                        self.alertView.frame = CGRect.init(x: self.alertView.frame.origin.x, y: self.frame.height, width: self.alertView.frame.size.width, height: self.alertView.frame.size.height)
                        
        }, completion: { test in
            if self.onClickLeftButton != nil {
                self.onClickLeftButton!();
                self.removeFromSuperview()
            }
            
        })
    }
    
    @IBAction func onClickBtnRight(_ sender: UIButton) {
        if self.onClickRightButton != nil {
            if selectedIndex >= 0 {
                self.onClickRightButton!(arrDelivery[selectedIndex])
            }
        }
    }
    
    public override func removeFromSuperview() {
        super.removeFromSuperview()
        print("removeFromSuperview")
    }
}

extension DeliveryListPopUp: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDelivery.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WhichDeliveryCell", for: indexPath) as! WhichDeliveryCell
        let obj = arrDelivery[indexPath.row]
        cell.configData(data: obj)
        if selectedIndex == indexPath.row {
            cell.lblDeliveryName.textColor = UIColor.themeColor
        } else {
            cell.lblDeliveryName.textColor = UIColor.themeTitleColor
        }
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        collectionView.reloadData()
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return colletionCellSize
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}


