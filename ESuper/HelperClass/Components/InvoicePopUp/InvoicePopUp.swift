//
//  CustomPhotoDialog.swift
//  
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
import UIKit


public class InvoicePopUp: CustomDialog, keyboardWasShownDelegate, keyboardWillBeHiddenDelegate {
 
    //MARK:- OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var viewPriceFormula: UIView!
    @IBOutlet weak var lblPriceFormula: UILabel!
    
    @IBOutlet weak var btnApplyPromocode: UIButton!
    @IBOutlet weak var lblHaveCouponCode: LabelDefault!
    @IBOutlet weak var txtCouponCode: TextFieldDefault!
    
    @IBOutlet weak var tblInvoice: UITableView!
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    
    @IBOutlet weak var lblPrice: LabelDefault!
    @IBOutlet weak var lblPriceValue: LabelDefault!
    
    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewPromocode: ViewDefault!
    
    @IBOutlet weak var viewButtonDone: ViewDefault!
    @IBOutlet weak var btnDone: CustomBottomButton!
    
    
 
    //MARK: Variables
    var onClickRightButton : ((_ obj: InvoicePopUp) -> Void)? = nil
    var onClickApplyPromoCode : ((_ obj: InvoicePopUp) -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    
    static let  dialogNibName = "InvoicePopUp"
    
    var arrInvoice = [InvoicePopUpData]()
    var totalPrice: Double = 0
    var currency = ""
    var isPriceFormula = false
    var priceFormula = ""
    
    public static func showInvoicePopUp
        (title:String,
         strRight:String,
         currency: String,
         isHidePromo: Bool = false,
         isPromoEdit: Bool = true,
         strPromo: String = "",
         arrInvoice: [InvoicePopUpData] = [],
         totalPrice: Double,
         isButtonDone: Bool = false,
         isPriceFormula : Bool = false,
         priceFormula : String = "",
         tag:Int = 959
         ) -> InvoicePopUp {
        var view = UINib(nibName: dialogNibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! InvoicePopUp
        view.tag = tag
        view.viewPriceFormula.isHidden = true
        if ((APPDELEGATE.window?.viewWithTag(tag)) != nil) {
            APPDELEGATE.window?.bringSubviewToFront((APPDELEGATE.window?.viewWithTag(tag))!)
            if let vw = APPDELEGATE.window?.viewWithTag(tag) as? InvoicePopUp {
                view = vw
            }
        } else {
            let frame = (APPDELEGATE.window?.frame)!;
            view.frame = frame;
            
            DispatchQueue.main.async {
                UIApplication.shared.keyWindow?.addSubview(view)
                APPDELEGATE.window?.bringSubviewToFront(view);
                view.alertView.layoutIfNeeded()
                view.animationBottomTOTop(view.alertView)
            }
        }
        
        view.lblTitle.text = title;
        view.currency = currency

        if isHidePromo {
            view.viewPromocode.isHidden = true
        } else {
            view.viewPromocode.isHidden = false
        }
        
        if isPromoEdit {
            view.txtCouponCode.isUserInteractionEnabled = true
            view.btnApplyPromocode.isHidden = false
        } else {
            view.txtCouponCode.isUserInteractionEnabled = false
            view.txtCouponCode.text = strPromo
            view.btnApplyPromocode.isHidden = true
        }
        
        view.btnDone.setTitle(strRight, for: .normal)
        
        view.viewButtonDone.isHidden = isButtonDone
        
        view.arrInvoice = arrInvoice
        view.totalPrice = totalPrice

        view.registerForKeyboardNotifications()
        view.delegatekeyboardWasShown = view
        view.delegatekeyboardWillBeHidden = view
        view.isPriceFormula = isPriceFormula
        view.priceFormula = priceFormula
        
        view.setLocalization()
    
        
        return view;
    }
    
    public override func removeFromSuperview() {
        self.deregisterFromKeyboardNotifications()
        super.removeFromSuperview()
        
    }

    func setLocalization() {
        alertView.backgroundColor = UIColor.themeAlertViewBackgroundColor
        backgroundColor = UIColor.themeOverlayColor
        lblTitle.textColor = UIColor.themeTextColor;
        lblTitle.font = FontHelper.textLarge(size: FontHelper.large)
        btnLeft.setImage(UIImage.init(named: "cancelIcon")?.imageWithColor(color: .themeColor), for: .normal)
        self.alertView.updateConstraintsIfNeeded()
        btnApplyPromocode.setTitle("".localized, for: .normal)
        self.alertView.roundCorner(corners: [.topRight , .topLeft], withRadius: 20)
                
        txtCouponCode.setRound(withBorderColor: UIColor.themeLightTextColor, andCornerRadious: 3, borderWidth: 0.5)
        txtCouponCode.delegate = self
        
        lblPrice.font = FontHelper.textRegular()
        lblPrice.textColor = UIColor.themeLightTextColor
        lblPriceValue.font = FontHelper.textRegular(size: 22)
        
        lblHaveCouponCode.text = "txt_a_coupon_code".localized
        lblHaveCouponCode.textColor = UIColor.themeLightTextColor
        
        btnApplyPromocode.setTitle("TXT_APPLY".localized, for: UIControl.State.normal)
        
        tblInvoice.delegate = self
        tblInvoice.dataSource = self
        tblInvoice.backgroundColor = UIColor.themeViewBackgroundColor
        tblInvoice.separatorColor = .clear
        tblInvoice.register(UINib(nibName: "InvoicePopUpCell", bundle: nil), forCellReuseIdentifier: "InvoicePopUpCell")
        tblInvoice.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        tblInvoice.isScrollEnabled = false
        tblInvoice.reloadData()
        
        lblPriceValue.text = "\(currentBooking.currency)\(totalPrice.toString())"
        if isPriceFormula{
            viewPriceFormula.isHidden = false
            self.lblPriceFormula.text = priceFormula
        }
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        heightTableView.constant = tblInvoice.contentSize.height
    }
    
    override func updateUIAccordingToTheme() {
        self.setLocalization()
    }

    @IBAction func onClickBtnLeft(_ sender: Any) {
      
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                       animations: {
                        self.alertView.frame = CGRect.init(x: self.alertView.frame.origin.x, y: self.frame.height, width: self.alertView.frame.size.width, height: self.alertView.frame.size.height)
                        
        }, completion: { test in
            if self.onClickLeftButton != nil {
                self.onClickLeftButton!();
                self.removeFromSuperview()
            }
            
        })
    }
    
    @IBAction func onClickBtnRight(_ sender: UIButton) {
        if self.onClickRightButton != nil {
            self.onClickRightButton!(self)
        }
    }
    
    @IBAction func onClickApplyPromocode(_ sender: UIButton) {
        if self.onClickApplyPromoCode != nil {
            self.onClickApplyPromoCode!(self)
        }
    }
    
    //MARK: Keyboard Delegates
    @objc override func keyboardWasShown(notification: NSNotification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.bottomAnchorConstraint.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            DispatchQueue.main.async {
                self.bottomAnchorConstraint.constant = keyboardSize!.height
            }
        }
    }
    
    @objc override func keyboardWillBeHidden(notification: NSNotification)
    {
        UIView.animate(withDuration: 0.5) {
            DispatchQueue.main.async {
                self.bottomAnchorConstraint.constant = 0.0
            }
        }
    }
}

extension InvoicePopUp: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
}

extension InvoicePopUp: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInvoice.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoicePopUpCell", for: indexPath) as! InvoicePopUpCell
        let obj = arrInvoice[indexPath.row]
        cell.setData(data: obj)
        cell.selectionStyle = .none
        return cell
    }
}


