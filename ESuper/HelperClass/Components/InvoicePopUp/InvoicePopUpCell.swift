//
//  InvoicePopUpCell.swift
//  Decagon
//
//  Created by MacPro3 on 13/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

public enum InvoiceCellType: Int {
    case Regular = 0
    case Bold
    case Empty
    case Tax
    case Promo
}

public struct InvoicePopUpData {
    var title: String = ""
    var value: String = ""
    var type: InvoiceCellType = .Regular
}

class InvoicePopUpCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: LabelDefault!
    @IBOutlet weak var lblValue: LabelDefault!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(data :InvoicePopUpData) {
        
        lblTitle.isHidden = false
        lblValue.isHidden = false
        lblTitle.text = data.title
        lblValue.text = data.value
        
        switch data.type {
        case .Regular:
            //Regular
            lblTitle.font = FontHelper.textRegular()
            lblValue.font = FontHelper.textRegular()
        case .Bold:
            //Bold
            lblTitle.font = FontHelper.textMedium()
            lblValue.font = FontHelper.textMedium()
        case .Tax:
            //Bold
            lblTitle.font = FontHelper.textRegular()
            lblValue.font = FontHelper.textRegular()
        case .Promo:
            //Empty
            lblTitle.font = FontHelper.textRegular()
            lblValue.font = FontHelper.textRegular()
        case .Empty:
            //Empty
            lblTitle.text = ""
            lblValue.text = ""
            lblTitle.isHidden = true
            lblValue.isHidden = true
        default:
            //Regular
            lblTitle.font = FontHelper.textRegular()
            lblValue.font = FontHelper.textRegular()
        }
    }
}
