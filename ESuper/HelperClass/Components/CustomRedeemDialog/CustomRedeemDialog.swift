//
//  CustomPhotoDialog.swift
//  
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
import UIKit


public class CustomRedeemDialog:CustomDialog,UITextFieldDelegate,keyboardWasShownDelegate,keyboardWillBeHiddenDelegate {

    //MARK:- OUTLETS
    @IBOutlet weak var stkDialog: UIStackView!
    @IBOutlet weak var stkBtns: UIStackView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var editTextOne: UITextField!
    
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var viewForEditTextOne: UIView!
    
    @IBOutlet weak var heightEditTextOne: NSLayoutConstraint!
    
    @IBOutlet var bottomAnchorView: NSLayoutConstraint!
    var activeField: UITextField?

    //MARK: - Variables
    var onClickRightButton : ((_ text1:String) -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    
    weak var timer: Timer? = nil
    var isForVerifyOtp:Bool = false
    var isFromMainVC:Bool = false
    var isForgotPasswordOtp:Bool = false
    var param:[String:Any] = [:]
    
    var walletCurrencyCode:String = ""
    var totalRedeemPoints: Int = 0
    var userMinimumRedeemPointRequired: Int = 0
    var userRedeemPointValue: Double = 0.0
    
    static let verificationDialog = "dialogForRedeem"

    public override func awakeFromNib() {
        super.awakeFromNib()
    }

    public static func showCustomRedeemDialog
        (title:String,
         message:String,
         titleLeftButton:String,
         titleRightButton:String,
         editTextOneHint:String,
         editTextTwoHint:String,
         isEdiTextTwoIsHidden:Bool,
         isEdiTextOneIsHidden:Bool = false,
         editTextOneInputType:Bool = false,
         editTextTwoInputType:Bool = false,
         isForVerifyOtp:Bool = false,
         isForgotPasswordOtp:Bool = false,
         param:[String:Any] = [:], isFromMainVC:Bool = false
         ) -> CustomRedeemDialog {

        let view = UINib(nibName: verificationDialog, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomRedeemDialog
        view.alertView.setShadow()
        let frame = (APPDELEGATE.window?.frame)!
        view.frame = frame
        view.lblTitle.text = title
        view.lblMessage.text = message
        view.lblMessage.isHidden = message.isEmpty()
        view.isFromMainVC = isFromMainVC
        view.setLocalization()
        view.btnRight.setTitle(titleRightButton, for: UIControl.State.normal)
        view.editTextOne.isSecureTextEntry = editTextOneInputType
        view.editTextOne.keyboardType = .numberPad
        
        view.isForgotPasswordOtp = isForgotPasswordOtp
        view.isForVerifyOtp = isForVerifyOtp
        view.param = param
        
        view.editTextOne.placeholder = editTextOneHint
        
        view.viewForEditTextOne.isHidden = isEdiTextOneIsHidden
        view.heightEditTextOne.constant = view.editTextOne.isHidden ? 0:34
        view.totalRedeemPoints = param["total_redeem_points"] as? Int ?? 0
        view.userMinimumRedeemPointRequired = param["user_minimum_redeem_points_required"] as? Int ?? 0
        view.userRedeemPointValue = param["user_redeem_point_value"] as? Double ?? 0.0
        
        DispatchQueue.main.async {
            APPDELEGATE.window?.addSubview(view)
            APPDELEGATE.window?.bringSubviewToFront(view)
            view.animationBottomTOTop(view.alertView)
        }
        return view
    }

    public override func layoutSubviews() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.alertView.applyTopCornerRadius()
        }
        self.btnRight.applyRoundedCornersWithHeight()
    }

    func setLocalization() {
        /* Set Color */
        self.backgroundColor = UIColor.themeOverlayColor
        btnRight.setTitleColor(UIColor.themeButtonTitleColor, for: UIControl.State.normal)
        btnRight.setBackgroundColor(color: UIColor.themeColor, forState: .normal)
        lblTitle.textColor = UIColor.themeTitleColor
        lblMessage.textColor = UIColor.themeLightTextColor
        editTextOne.textColor = UIColor.themeTextColor
        
        lblTitle.font = FontHelper.textLarge()
        lblMessage.font = FontHelper.textRegular()
        editTextOne.font = FontHelper.textRegular()
        
        editTextOne.delegate = self
        
        self.backgroundColor = UIColor.themeOverlayColor
        self.alertView.backgroundColor = UIColor.themeViewBackgroundColor
        self.alertView.setRound(withBorderColor: .clear, andCornerRadious: 3.0, borderWidth: 1.0)
        deregisterFromKeyboardNotifications()
        registerForKeyboardNotifications()
        self.delegatekeyboardWasShown = self
        self.delegatekeyboardWillBeHidden = self
       
        if isFromMainVC{
            btnLeft.setImage(UIImage(named:"menuLogout")?.imageWithColor(color: .themeColor), for: .normal)
        }else{
            btnLeft.setImage(UIImage(named:"cancelIcon")?.imageWithColor(color: UIColor.themeColor), for: .normal)
        }
    }

    override func updateUIAccordingToTheme() {
        if isFromMainVC{
            btnLeft.setImage(UIImage(named:"menuLogout")?.imageWithColor(color: .themeColor), for: .normal)
        }else{
            btnLeft.setImage(UIImage(named:"cancelIcon")?.imageWithColor(color: UIColor.themeColor), for: .normal)
        }
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        return true
    }

  public  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let previousText:NSString = textField.text! as NSString
        let enteredRedeemPoints =  previousText.replacingCharacters(in: range, with: string)
        
        if (string == "") && enteredRedeemPoints == "" {
            self.lblMessage.isHidden = true
            return true
        } else {
            if self.checkValidRedeemPoints(enteredRedeemPoints: enteredRedeemPoints) {
                
            }
        }
        return true
    }

    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        return true
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        activeField = nil
    }

    
    func checkValidRedeemPoints(enteredRedeemPoints: String) -> Bool {
        if enteredRedeemPoints == "" {
            self.lblMessage.isHidden = false
            self.lblMessage.text = "VALIDATION_MSG_VALID_AMOUNT".localized
            self.lblMessage.textColor  = UIColor.themeRedBGColor
            return false
        } else if self.totalRedeemPoints == 0  ||  enteredRedeemPoints.integerValue ?? 0 > totalRedeemPoints {
            self.lblMessage.isHidden = false
            self.lblMessage.text = "VALIDATION_MSG_INSUFFICIENT_BALANCE".localized
            self.lblMessage.textColor  = UIColor.themeRedBGColor
            return false
        } else if enteredRedeemPoints.integerValue ?? 0 < self.userMinimumRedeemPointRequired {
            self.lblMessage.isHidden = false
            self.lblMessage.text = String(format: "VALIDATION_MSG_POINT_LIMIT".localized,self.userMinimumRedeemPointRequired.toString())
            self.lblMessage.textColor  = UIColor.themeRedBGColor
            return false
        } else {
            let pointsToValue = self.userRedeemPointValue*(enteredRedeemPoints.doubleValue ?? 0.0)
            self.lblMessage.isHidden = false
            self.lblMessage.textColor = UIColor.themeWalletAddedColor
            self.lblMessage.text = String(format: "MSG_POINT_TO_VALUE".localized,enteredRedeemPoints,pointsToValue.toString(decimalPlaced: 2),preferenceHelper.WalletCurrencyCode)
            return true
        }
    }
    

    //MARK: - Action Methods
    @IBAction func onClickBtnLeft(_ sender: Any) {
        if self.onClickLeftButton != nil {
            self.animationForHideView(alertView) {
                self.endEditing(true)
                self.onClickLeftButton!()
            }
        }
    }

    

    @IBAction func onClickBtnRight(_ sender: Any) {
        if self.onClickRightButton != nil {
            if self.checkValidRedeemPoints(enteredRedeemPoints: self.editTextOne.text ?? "") {
                self.endEditing(true)
                self.onClickRightButton!(self.editTextOne.text!)
            }
        }
    }

    

    @IBAction func onClickBtnShowHideEditTextTwo(_ sender: Any) {
        
    }

    deinit {
        deregisterFromKeyboardNotifications()
    }

    @objc override func keyboardWasShown(notification: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.frame
        aRect.size.height -= keyboardSize!.height
        if let activeFieldPresent = activeField {
            _ = (activeFieldPresent.superview as? UIStackView)?.convert((activeFieldPresent.superview as? UIStackView)?.frame.origin ?? CGPoint.zero, to: nil) ?? CGPoint.zero
            self.bottomAnchorView.constant = 0.0
            UIView.animate(withDuration: 0.5) {
                DispatchQueue.main.async {
                    self.bottomAnchorView.constant = keyboardSize!.height
                }
            }
        }
    }

    @objc override func keyboardWillBeHidden(notification: NSNotification) {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var _ : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        UIView.animate(withDuration: 0.5) {
            DispatchQueue.main.async {
                self.bottomAnchorView.constant = 0.0
            }
        }
        self.endEditing(true)
    }
}

