//
//  CustomPhotoDialog.swift
//  
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
import UIKit


public class TripDetailDialog: CustomDialog {
 
    //MARK: - OUTLETS
    @IBOutlet weak var btnOffer: UIButton!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var btnCall: ButtonDefault!
    @IBOutlet weak var btnChat: ButtonDefault!
    @IBOutlet weak var btnShare: ButtonDefault!
    
    @IBOutlet weak var lblTitle: LabelDefault!
    @IBOutlet weak var lblTripNo: LabelDefault!
    
    @IBOutlet weak var lblDriverName: LabelDefault!
    @IBOutlet weak var lblVehicalDetail: LabelDefault!
    
    @IBOutlet weak var btnRating: CustomBottomButton!
    @IBOutlet weak var imgDriver: UIImageView!
    
    @IBOutlet weak var stackViewButtons: UIStackView!
    
    @IBOutlet weak var btnCode: UIButton!
    
    @IBOutlet weak var lblDistance: LabelDefault!
    @IBOutlet weak var lblTime: LabelDefault!
    
    var scheduleData: ScheduleDialogData?
 
    var onClickRightButton : ((_ obj: TripDetailDialog) -> Void)? = nil
    var onClickButtonStatus : ((_ obj: TripDetailDialog) -> Void)? = nil
    var onClickButtonShare : ((_ obj: TripDetailDialog) -> Void)? = nil
    var onClickButtonChat : ((_ obj: TripDetailDialog) -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    var onClickButtonCode : ((_ obj: TripDetailDialog) -> Void)? = nil
    var onClickButtonCall : ((_ obj: TripDetailDialog) -> Void)? = nil
    
    var bottomConstrint: NSLayoutConstraint?
    
    var isShow = false

    public override func awakeFromNib() {
        super.awakeFromNib()
        setLocalization()
        self.tintColor = UIColor.themeColor
    }

    func setLocalization() {
        alertView.backgroundColor = UIColor.themeAlertViewBackgroundColor
        backgroundColor = UIColor.clear
        
        btnStatus.titleLabel?.font = FontHelper.textRegular(size: FontHelper.medium)
        btnStatus.setTitle("txt_create_trip_now".localized, for: .normal)
        
        lblTripNo.font = FontHelper.labelRegular(size: 12)
        lblTripNo.textColor = UIColor.themeLightTextColor
        
        lblDriverName.font = FontHelper.textMedium()
        
        lblTitle.text = "txt_ride".localized
        lblTitle.textColor = UIColor.themeStatusYellow;
        lblTitle.font = FontHelper.textLarge(size: FontHelper.medium)
        lblTitle.numberOfLines = 0
        
        btnCode.setTitleColor(UIColor.themeColor, for: .normal)
        btnCode.setTitle("TXT_GET_CODE".localized, for: .normal)
        btnCode.titleLabel?.font = FontHelper.textRegular(size: FontHelper.labelRegular)
        
        btnStatus.setTitle("txt_cancel_trip".localized, for: .normal)
        
        btnCall.tintColor = UIColor.themeColor
        btnChat.tintColor = UIColor.themeColor
        btnShare.tintColor = UIColor.themeColor
        btnRating.tintColor = .white
        
        btnCall.setTitleColor(UIColor.themeColor, for: .normal)
        btnChat.setTitleColor(UIColor.themeColor, for: .normal)
        btnShare.setTitleColor(UIColor.themeColor, for: .normal)
        
        btnCall.titleLabel?.font = FontHelper.textRegular(size: 12)
        btnChat.titleLabel?.font = FontHelper.textRegular(size: 12)
        btnShare.titleLabel?.font = FontHelper.textRegular(size: 12)
        btnRating.titleLabel?.font = FontHelper.textSmall()
        
        btnCall.setTitle("txt_call_user".localized, for: .normal)
        btnChat.setTitle("TXT_CHAT".localized, for: .normal)
        btnShare.setTitle("TXT_SHARE".localized, for: .normal)
        
        btnCall.setRound(withBorderColor: UIColor.themeLightLineColor, andCornerRadious: 0.1, borderWidth: 0.5)
        btnChat.setRound(withBorderColor: UIColor.themeLightLineColor, andCornerRadious: 0.1, borderWidth: 0.5)
        btnShare.setRound(withBorderColor: UIColor.themeLightLineColor, andCornerRadious: 0.1, borderWidth: 0.5)
        
        lblTime.textColor = UIColor.themeLightTextColor
        lblTime.text = "N/A"
        
        lblDistance.textColor = UIColor.themeLightTextColor
        lblDistance.text = "N/A"
        
        DispatchQueue.main.async {
            self.imgDriver.applyRoundedCornersWithHeight(self.imgDriver.frame.size.height/2)
        }
    }
    
    func addIn(vw: UIView) {
        vw.addSubview(self)
        alertView.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vw, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0).isActive = true
        
        NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vw, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0).isActive = true

        NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.alertView.frame.size.height).isActive = true
        
        self.bottomConstrint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vw, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: self.alertView.frame.size.height)
        self.bottomConstrint?.isActive = true
    
    }
    
    func show() {
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn) {
            self.bottomConstrint?.constant = 0.0
            self.superview?.layoutIfNeeded()
        } completion: { [weak self] _ in
            guard let self = self else { return }
            self.isShow = true
        }
    }
    
    func hide() {
        UIView.animate(withDuration: 0.1,
                       animations: {
            self.bottomConstrint?.constant = self.alertView.frame.size.height
            self.superview?.layoutIfNeeded()
        }) { [weak self] _ in
            guard let self = self else { return }
            self.isShow = false
        }
    }
    
    override func updateUIAccordingToTheme() {
        //self.setLocalization()
    }
    //ActionMethods
    @IBAction func onClickBtnLeft(_ sender: Any) {
      
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                       animations: {
                        self.alertView.frame = CGRect.init(x: self.alertView.frame.origin.x, y: self.frame.height, width: self.alertView.frame.size.width, height: self.alertView.frame.size.height)
                        
        }, completion: { test in
            if self.onClickLeftButton != nil {
                self.onClickLeftButton!();
                self.removeFromSuperview()
            }
            
        })
    }
    
    @IBAction func onClickBtnRight(_ sender: UIButton) {
        if self.onClickRightButton != nil {
            
        }
    }
    
    @IBAction func onClickBtnStatus(_ sender: UIButton) {
        if self.onClickButtonStatus != nil {
            onClickButtonStatus!(self)
        }
    }
    
    @IBAction func onClickBtnShare(_ sender: UIButton) {
        if self.onClickButtonShare != nil {
            onClickButtonShare!(self)
        }
    }
    
    @IBAction func onClickBtnChat(_ sender: UIButton) {
        if self.onClickButtonChat != nil {
            onClickButtonChat!(self)
        }
    }
    
    @IBAction func onClickBtnCode(_ sender: UIButton) {
        if self.onClickButtonCode != nil {
            onClickButtonCode!(self)
        }
    }
    
    @IBAction func onClickBtnCall(_ sender: UIButton) {
        if self.onClickButtonCall != nil {
            onClickButtonCall!(self)
        }
    }
    
    public override func removeFromSuperview() {
        super.removeFromSuperview()
        print("removeFromSuperview")
    }
}
