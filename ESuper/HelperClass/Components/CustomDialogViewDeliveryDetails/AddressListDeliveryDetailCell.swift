//
//  TableViewCell.swift
//  Edelivery
//
//  Created by MacPro3 on 13/10/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class AddressListDeliveryDetailCell: UITableViewCell {
    
    @IBOutlet weak var viewAddressNo: UIView!
    @IBOutlet weak var lblAddresNo: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserPhone: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTotalWaitingTime: UILabel!
    
    var timer = Timer()
    var dates = ""
    var count = 0
    var waitingTimes = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewAddressNo.setRound()
        viewAddressNo.backgroundColor = .themeColor
        lblAddresNo.textColor = .white
        
        lblUserName.textColor = .themeTextColor
        lblUserPhone.textColor = .themeTextColor
        lblAddress.textColor = .themeTextColor
        lblDateTime.textColor = .themeLightGrayColor
        
        lblUserName.font = FontHelper.textMedium(size: FontHelper.regular)
        lblUserPhone.font = FontHelper.textRegular(size: FontHelper.regular)
        lblAddress.font = FontHelper.textRegular(size: FontHelper.regular)
        lblDateTime.font = FontHelper.textRegular(size: FontHelper.labelRegular)
        lblTotalWaitingTime.font = FontHelper.textRegular(size: FontHelper.labelRegular)
    }

    func stopTimer(){
        self.timer.invalidate()
    }
    func setTimerLables(date : String, seconds : Int,waitingTimes : Int){
        count = seconds
        self.waitingTimes = waitingTimes
        self.scheduledTimerWithTimeInterval()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func scheduledTimerWithTimeInterval(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    @objc func updateCounting(){
        let pendingSecond = (self.waitingTimes)
        count += 1
        var message = "Waiting Time Start After:"
        var currentSecond = (pendingSecond - count)
        if currentSecond < 0{
            currentSecond = count - pendingSecond
            message = "Waiting Time:"
        }
        let minutes = Int(currentSecond / 60)
        let second = currentSecond - (minutes * 60)
        lblTotalWaitingTime.text = "\(message) \(minutes):\(second)"
    }
}
