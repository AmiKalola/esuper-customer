//
//  CustomProductFilter.swift
//  
//
//  Created by Elluminati on 3/10/21.
//  Copyright © 2021 Elluminati. All rights reserved.
//

import Foundation
import  UIKit

class CustomOffersDialog: CustomDialog  {
    
    @IBOutlet weak var viewForSearchItem: UIView!
    @IBOutlet weak var tblV: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var imgNoOffers: UIImageView!

    var onClickLeftButton : (() -> Void)? = nil
    var onClickApply : ((_ _id: String) -> Void)? = nil
    static let  dialogForOffers = "dialogForOffers"
    var arrPromoCodes:Array<PromoCodeModal>? = nil
    var maskLayer: CAShapeLayer?
    var isFromHome: Bool = false
    var preferredContentSize: CGSize {
        get {
            // Force the table view to calculate its height
            self.tblV.layoutIfNeeded()
            return self.tblV.contentSize
        }
        set {}
    }

    public static func showOffers
    (title:String,
     message:String,
     arrPromoCodes:Array<PromoCodeModal>?,
     isFromHome:Bool
    ) ->
    CustomOffersDialog
    {
        let view = UINib(nibName: dialogForOffers, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomOffersDialog
        let frame = (APPDELEGATE.window?.frame)!
        view.frame = frame
        view.lblTitle.text = title
        DispatchQueue.main.async {
            APPDELEGATE.window?.addSubview(view)
            APPDELEGATE.window?.bringSubviewToFront(view)
            view.animationBottomTOTop(view.viewForSearchItem)
        }
        view.tblV.register(UINib.init(nibName: "PromoCodeCell", bundle: nil), forCellReuseIdentifier: "PromoCodeCell")
        view.arrPromoCodes = arrPromoCodes
        view.imgNoOffers.isHidden = arrPromoCodes?.count != 0
        view.tblV.isHidden = arrPromoCodes?.count == 0
        view.tblV.reloadData()
        view.setLocalization()
        view.isFromHome = isFromHome
        return view
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        viewForSearchItem.applyTopCornerRadius()
    }
    
    func setLocalization(){
        self.backgroundColor = UIColor.themeOverlayColor
        self.viewForSearchItem.backgroundColor = UIColor.themeViewBackgroundColor
        self.tblV.backgroundColor = UIColor.themeViewBackgroundColor
        
        lblTitle.textColor = UIColor.themeTextColor
        lblTitle.font = FontHelper.textMedium(size: FontHelper.large)

        tblV.reloadData()
        tblV.tableFooterView = UIView()
        if arrPromoCodes?.count ?? 0 > 0 {
            if self.preferredContentSize.height <= UIScreen.main.bounds.height - 100 {
                self.viewHeightConstarint.constant = self.preferredContentSize.height + tblV.frame.origin.y
            } else {
                self.viewHeightConstarint.constant = UIScreen.main.bounds.height - 100
            }
        } else {
            self.viewHeightConstarint.constant = 400
        }
        

        updateUIAccordingToTheme()
        self.layoutSubviews()
    }
    
    override func updateUIAccordingToTheme(){
        btnClose.setImage(UIImage(named: "cancelIcon")?.imageWithColor(color: .themeColor), for: .normal)
        self.tblV.reloadData()
    }
    
    //ActionMethods
    @IBAction func onClickBtnClose(_ sender: Any) {
      /*  if self.onClickLeftButton != nil {
            self.onClickLeftButton!()
        } */
        self.animationForHideView(viewForSearchItem) {
            
            self.removeFromSuperview()
        }
    }
    @IBAction func onClickBtnApplySearch(_ sender: Any) {
//        let searchText = searchBarItem.text ?? ""
        
    }
    func onClickApply(promoName: String) {
        if self.onClickApply != nil {
            self.onClickApply!(promoName)
        }
    }
    
}

extension CustomOffersDialog:UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return UIView.init()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrPromoCodes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:PromoCodeCell? =  tableView.dequeueReusableCell(with: PromoCodeCell.self, for: indexPath)
        if cell == nil {
            cell = PromoCodeCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "PromoCodeCell")
        }
        cell?.setData(promoObj: arrPromoCodes![indexPath.row], isApplyPromo: !isFromHome, parent: self)
//        cell?.setCellData(cellItem:(arrProductList?[indexPath.row])!)
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        tblV.reloadData()
        
    }
}
