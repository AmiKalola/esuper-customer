//
//  File.swift
//  ESuper
//
//  Created by Rohit on 22/01/24.
//  Copyright © 2024 Elluminati. All rights reserved.
//

import Foundation
import UIKit
public class ScheduleDialog: CustomDialog, keyboardWasShownDelegate, keyboardWillBeHiddenDelegate {
    
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblTitle: LabelDefault!
    @IBOutlet weak var btnLeft: UIButton!
    
    @IBOutlet weak var lblAsap: LabelDefault!
    @IBOutlet weak var lblSchedule: LabelDefault!
    @IBOutlet weak var lblYoucanSelectUpToWeek: LabelDefault!
    @IBOutlet weak var lblDate: LabelDefault!
    @IBOutlet weak var lblTime: LabelDefault!
    
    @IBOutlet weak var txtDate: TextFieldDefault!
    @IBOutlet weak var txtTime: TextFieldDefault!
    
    @IBOutlet weak var btnAsap: UIButton!
    @IBOutlet weak var btnSchedual: UIButton!
    
    @IBOutlet weak var viewDateAndTime: ViewDefault!
    
    @IBOutlet weak var btnASAPIcon: UIButton!
    @IBOutlet weak var btnScheduleIcon: UIButton!
    
    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewDateIcon: UIView!
    @IBOutlet weak var viewTimeIcon: UIView!

    var selectedStoreSlotTime:[StoreTime]=[]
    var timeZone:TimeZone = TimeZone.init(identifier:currentBooking.selectedCityTimezone)!
    var futureDateMillisecond:Int64 = 0
    var futureTimeMillisecond:Int64 = 0
    var data: ScheduleDialogData?
        
    var onClickRightButton : ((_ obj: ScheduleDialog) -> Void)? = nil
    
    var onClickLeftButton : (() -> Void)? = nil
    var selectedDayInd : Int = 0
    var selectedDateStr : String = ""
    var isTaxi = false
    
    public override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public static func showScheduleDialog
        (title:String,
         strRightButton:String,
         data: ScheduleDialogData? = nil,
         isTaxi : Bool = false,
         tag:Int = 4575
         ) -> ScheduleDialog
     {
        let view = UINib(nibName: "ScheduleDialog", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ScheduleDialog
        view.tag = tag
        if ((APPDELEGATE.window?.viewWithTag(tag)) != nil) {
            APPDELEGATE.window?.bringSubviewToFront((APPDELEGATE.window?.viewWithTag(tag))!)
        }else {
            view.setLocalization()
            let frame = (APPDELEGATE.window?.frame)!;
            view.frame = frame;
            view.lblTitle.text = title;
            view.btnRight.setTitle(strRightButton, for: UIControl.State.normal)
            view.lblTitle.text = title
            view.isTaxi = isTaxi
            view.data = data
            view.getCategoryTime { arrForCountryList in
                view.selectedStoreSlotTime = arrForCountryList
            }
            view.setData()
            DispatchQueue.main.async{
                UIApplication.shared.keyWindow?.addSubview(view)
                APPDELEGATE.window?.bringSubviewToFront(view);
                view.alertView.layoutIfNeeded()
                view.animationBottomTOTop(view.alertView)
            }
        }
        return view;
    }
    
    func setData() {
        guard let data = data else {
            return
        }

        txtTime.text = data.strTime
        txtDate.text = data.strDate
        futureDateMillisecond = data.futureDateMillisecond
        futureTimeMillisecond = data.futureTimeMillisecond
        selectedDateStr = data.selectedDateStr
        selectedDayInd = data.selectedDayInd
        if data.isAsap {
            onClickBtnAsap(btnAsap)
        } else {
            onClickBtnSchedualOrder(btnSchedual)
        }
    }
    
    func setLocalization() {
        lblAsap.text = "TXT_ASAP".localized
        lblSchedule.text = "txt_schedule_trip".localized
        lblDate.text = "TXT_DATE".localized
        lblTime.text = "txt_time_only".localized
        
        lblYoucanSelectUpToWeek.text = "txt_you_can_select_up_to_week".localized
        lblYoucanSelectUpToWeek.font = FontHelper.textRegular(size: 12)
        lblYoucanSelectUpToWeek.textColor = UIColor.themeLightTextColor
        
        lblDate.textColor = UIColor.themeLightTextColor
        lblTime.textColor = UIColor.themeLightTextColor
        
        alertView.backgroundColor = UIColor.themeAlertViewBackgroundColor
        backgroundColor = UIColor.themeOverlayColor
        
        lblTitle.font = FontHelper.textLarge(size: FontHelper.large)
        
        btnRight.titleLabel?.font = FontHelper.textRegular()
        btnLeft.setImage(UIImage.init(named: "cancelIcon")?.imageWithColor(color: .themeColor), for: .normal)
        self.alertView.updateConstraintsIfNeeded()
        self.alertView.roundCorner(corners: [.topRight , .topLeft], withRadius: 20)
        
        txtDate.setRound(withBorderColor: UIColor.themeLightTextColor, andCornerRadious: 3, borderWidth: 0.5)
        txtDate.delegate = self
        
        txtTime.setRound(withBorderColor: UIColor.themeLightTextColor, andCornerRadious: 3, borderWidth: 0.5)
        txtTime.delegate = self
        
        self.registerForKeyboardNotifications()
        self.delegatekeyboardWasShown = self
        self.delegatekeyboardWillBeHidden = self
        btnASAPIcon.isSelected = btnAsap.isSelected
        btnASAPIcon.setImage(UIImage(named:"asap_icon")?.imageWithColor(color: UIColor.themeTitleColor), for: .normal)
        btnScheduleIcon.setImage(UIImage(named:"schedule_gray_icon")?.imageWithColor(color: UIColor.themeTitleColor), for: .normal)
        btnASAPIcon.setImage(UIImage(named:"asap_icon")?.imageWithColor(color: UIColor.themeColor), for: .selected)
        btnScheduleIcon.setImage(UIImage(named:"schedule_gray_icon")?.imageWithColor(color: UIColor.themeColor), for: .selected)
        viewDateIcon.setRound(withBorderColor: UIColor.themeLightTextColor, andCornerRadious: 3.0, borderWidth: 0.1)
        viewTimeIcon.setRound(withBorderColor: UIColor.themeLightTextColor, andCornerRadious: 3.0, borderWidth: 0.1)
    }
    
    @IBAction func onClickBtnAsap(_ sender: UIButton) {
        viewDateAndTime.isHidden = true
        
        btnAsap.isSelected = true
        btnSchedual.isSelected = false
        
        btnASAPIcon.isSelected = true
        btnScheduleIcon.isSelected = false
        
        self.lblAsap.textColor = UIColor.themeColor
        self.lblSchedule.textColor = UIColor.themeTextColor
    }
    
    @IBAction func onClickBtnSchedualOrder(_ sender: Any) {
        viewDateAndTime.isHidden = false
        
        btnAsap.isSelected = false
        btnSchedual.isSelected = true
        
        btnASAPIcon.isSelected = false
        btnScheduleIcon.isSelected = true
        
        self.lblAsap.textColor = UIColor.themeTextColor
        self.lblSchedule.textColor = UIColor.themeColor
    }
    
    @IBAction func onClickBtnSchedualTime(_ sender: Any) {
        self.onClickBtnDate(true)
    }
    
    func onClickBtnDate(_ sender: Bool) {
//        lblReopenAt.isHidden = true
//        self.isShowSlots = true // This is because we want to open slot date picker everywhere.
        if sender {
            let datePickerDialog:CustomDatePickerDialog = CustomDatePickerDialog.showCustomDatePickerDialog(title: "TXT_SELECT_FUTURE_ORDER_TIME".localized, titleLeftButton: "".localized, titleRightButton: "TXT_SELECT".localized,mode: .dateAndTime)
            datePickerDialog.datePicker.locale = Locale(identifier: "en_GB")
            datePickerDialog.datePicker.timeZone = self.timeZone
            datePickerDialog.setMinDate(mindate: Date().addingTimeInterval((1)*60))
            datePickerDialog.onClickLeftButton = { [unowned datePickerDialog] in
                datePickerDialog.removeFromSuperview()
            }
            datePickerDialog.onClickRightButton = { [unowned self,unowned datePickerDialog] (selectedDate:Date) in
                _ = Calendar.current
                currentBooking.futureTaxiDateMilliSecond = Utility.convertSelectedDateToMilliSecond(serverDate: selectedDate, strTimeZone: self.timeZone.identifier)
                datePickerDialog.removeFromSuperview()
            }
        }else{
//            if sender.view?.tag == 0 {
                let datePickerDialog:CustomDateSlotPickerDialog = CustomDateSlotPickerDialog.showCustomDatePickerSlotDialog(title: "TXT_SELECT_FUTURE_ORDER_DATE".localized, titleLeftButton: "".localized, titleRightButton: "TXT_SELECT".localized, mode: .date, reservationMaxDays:  7)
                datePickerDialog.onClickLeftButton = {
                    [unowned datePickerDialog] in
                    datePickerDialog.removeFromSuperview()
                }
                datePickerDialog.onClickRightButton = { [unowned datePickerDialog] (selectedDate:String, selectedDayInd:Int) in
                    self.selectedDateStr = selectedDate
                    self.selectedDayInd = selectedDayInd
                    datePickerDialog.removeFromSuperview()
                    if self.selectedStoreSlotTime.count-1 >= self.selectedDayInd {
                            self.showCustomDialogTime(ind: self.selectedDayInd)
                    }
                }
        }
        self.getCategoryTime { arrForCountryList in
            self.selectedStoreSlotTime = arrForCountryList
        }
    }
    
    func showCustomDialogTime(ind:Int) {
//        get_courier_order_invoice
//        get_order_cart_invoice
         
        if self.selectedDateStr.count <= 0 {
            Utility.showToast(message: "TXT_SELECT_DATE_FIRST".localized)
        } else {
            var slotInterval = preferenceHelper.CourierBookingTimeslot
            if self.isTaxi{
                 slotInterval = preferenceHelper.TaxiBookingTimeslot
            }
            let selectedDateDay = Utility.getDayFromDate(date:self.selectedDateStr)
//            if self.selectedStoreSlotTime.count - 1 >= ind {
            let datePickerDialog:CustomTimeSlotPickerDialog = CustomTimeSlotPickerDialog.showCustomTimePickerSlotDialog(title: "TXT_SELECT_FUTURE_ORDER_TIME".localized, titleLeftButton: "".localized, titleRightButton: "TXT_SELECT".localized, selectedStoreSlot:selectedStoreSlotTime[ind], selectedDayInd: ind, selectedDateDay: selectedDateDay, scheduleTimeInterval: Double(slotInterval),timeZone: self.timeZone,isTableBooking: Utility.isTableBooking())
                datePickerDialog.onClickLeftButton = { [unowned datePickerDialog] in
                    datePickerDialog.removeFromSuperview()
                }
                datePickerDialog.onClickRightButton = { [unowned datePickerDialog] (selectedTimeSlot:String, isStoreClosed:Bool) in
                    if selectedTimeSlot.count > 0 {
                        DispatchQueue.main.async {
                            let selectedDate : Date = Utility.stringToDate(strDate: "\(self.selectedDateStr) \(selectedTimeSlot.components(separatedBy: "-")[0].removingWhitespaces())", withFormat: DATE_CONSTANT.DATE_FORMATE_SLOT)
                            let selectedDate2 : Date = Utility.stringToDate(strDate: "\(self.selectedDateStr) \(selectedTimeSlot.components(separatedBy: "-")[1].removingWhitespaces())", withFormat: DATE_CONSTANT.DATE_FORMATE_SLOT)
                            currentBooking.futureTaxiDateMilliSecond = Utility.convertSelectedDateToMilliSecond(serverDate: selectedDate, strTimeZone: self.timeZone.identifier)
                            currentBooking.futureTaxiDateMilliSecond2 = Utility.convertSelectedDateToMilliSecond(serverDate: selectedDate2, strTimeZone: self.timeZone.identifier)
                            let offSetMiliSecond = self.timeZone.secondsFromGMT() * 1000
                            
                            currentBooking.futureTaxiUTCMilliSecond = currentBooking.futureTaxiDateMilliSecond - Int64.init(offSetMiliSecond)
                            currentBooking.futureTaxiUTCMilliSecond2 = currentBooking.futureTaxiDateMilliSecond2 - Int64.init(offSetMiliSecond)
                            
                            self.futureDateMillisecond = currentBooking.futureTaxiUTCMilliSecond
                            self.futureTimeMillisecond =  currentBooking.futureTaxiDateMilliSecond2 - Int64.init(offSetMiliSecond)
                            let components = Calendar.current.dateComponents(in: self.timeZone, from: selectedDate)
                            self.txtDate.text = String(components.day!)  +  "-" + String(components.month!) +  "-" +  String(components.year!)
                            self.txtTime.text = selectedTimeSlot
                        }
                    }
                    datePickerDialog.removeFromSuperview()
                }
//            }
        }
    }
    typealias CompletionHandler = (_ arrForCountryList:[StoreTime]) -> Void

    func getCategoryTime(completionHandler: CompletionHandler) {
//        self.selectedStoreSlotTime:[StoreTime]=[]
           var arrForCountryList = [StoreTime]()
           let url = Bundle.main.url(forResource: "store_delivery_time", withExtension: ".json")!
           do {
               let jsonData = try Data(contentsOf: url)
               let json = try JSONSerialization.jsonObject(with: jsonData) as! NSArray
               print("json --------------- \(json)")
               arrForCountryList.removeAll()
               for obj in json {
                   let value = obj as! [String : Any]
                   arrForCountryList.append(StoreTime(fromDictionary: value))
               }
           } catch {
               //print(error)
           }
           completionHandler(arrForCountryList)
       }

    
    @IBAction func onClickBtnLeft(_ sender: Any) {
        if self.onClickLeftButton != nil {
            self.onClickLeftButton!();
            self.removeFromSuperview()
        }
    }
    
    @IBAction func onClickBtnRight(_ sender: UIButton) {
        self.endEditing(true)
        if !btnAsap.isSelected {
            if futureDateMillisecond == 0 {
                Utility.showToast(message: "MSG_PLEASE_SELECT_VALID_DATE".localized)
            } else {
                if self.onClickRightButton != nil {
                    self.onClickRightButton!(self)
                }
            }
        } else {
            if self.onClickRightButton != nil {
                self.onClickRightButton!(self)
            }
        }
    }
    
    //MARK: Keyboard Delegates
    @objc override func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.bottomAnchorConstraint.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            DispatchQueue.main.async {
                self.bottomAnchorConstraint.constant = keyboardSize!.height
            }
        }
    }
    
    
    @objc override func keyboardWillBeHidden(notification: NSNotification){
        UIView.animate(withDuration: 0.5) {
            DispatchQueue.main.async {
                self.bottomAnchorConstraint.constant = 0.0
            }
        }
    }
}

extension ScheduleDialog: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate {
//            onClickBtnSchedualTime(UIButton())
            self.onClickBtnDate(false)

            return false
        } else if textField == txtTime {
//            self.onClickBtnDate(false)
            self.showCustomDialogTime(ind: selectedDayInd)
//            onClickBtnSchedualTime(UIButton())
            return false
        }
        return true
    }
}
