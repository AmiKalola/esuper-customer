//
//  StoreTimeCell.swift
//  ESuper
//
//  Created by Rohit on 14/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit
class StoreTimeCell: CustomTableCell {
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    //MARK:- LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        setLocalization()
    }
    
    //MARK:- setLocationzation
    func setLocalization() {
        lblDay.textColor = UIColor.themeLightTextColor
        lblTime.textColor = UIColor.themeLightTextColor
        lblDay.font = FontHelper.textMedium()
        /*Set Color*/
    }
    
    //MARK:- SET CELL DATA
    func setCellData(storeTimeItem:StoreTime,indexPath:IndexPath) {

        let currentDay:DateComponents = Calendar.current.dateComponents(in:TimeZone.init(identifier: currentBooking.selectedCityTimezone) ?? TimeZone.current, from: Date())
        if (currentDay.weekday!-1 == indexPath.section) {
            lblDay.textColor = UIColor.themeTextColor
            lblTime.textColor = UIColor.themeTextColor
        }
        let day:Day = Day(rawValue: indexPath.section)!
        lblDay.text = day.text()
        if (storeTimeItem.isStoreOpenFullTime) {
           lblDay.isHidden = false
           lblTime.text = "TXT_OPEN_24_HOURS".localizedCapitalized
        }
        else if (!storeTimeItem.isStoreOpenFullTime && !storeTimeItem.isStoreOpen) {
            lblDay.isHidden = false
            lblDay.textColor = UIColor.themeRedBGColor
            lblTime.textColor = UIColor.themeRedBGColor
            lblTime.text = "TXT_STORE_CLOSED".localizedCapitalized
        }
        else if (storeTimeItem.dayTime.count == 0) {
            lblDay.isHidden = false
            lblDay.textColor = UIColor.themeRedBGColor
            lblTime.textColor = UIColor.themeRedBGColor
            lblTime.text = "TXT_STORE_CLOSED".localizedCapitalized
        }else {
            if (indexPath.row == 0) {
                lblDay.isHidden = false
            }else {
                lblDay.isHidden = true
            }
            if storeTimeItem.dayTime[indexPath.row].storeOpenTime != nil && storeTimeItem.dayTime[indexPath.row].storeOpenTime != nil {
                lblTime.text = storeTimeItem.dayTime[indexPath.row].storeOpenTime + " - " + storeTimeItem.dayTime[indexPath.row].storeCloseTime
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

