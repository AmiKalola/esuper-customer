//
//  File.swift
//  Decagon
//
//  Created by MacPro3 on 11/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

public class CouponCodePopUp: CustomDialog, keyboardWasShownDelegate, keyboardWillBeHiddenDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblTitle: LabelDefault!
    @IBOutlet weak var btnLeft: UIButton!
    
    @IBOutlet weak var lblHaveCouponCode: LabelDefault!
    @IBOutlet weak var txtCouponCode: TextFieldDefault!
    
    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var lblViewOffer : UILabel!
    
    var onClickRightButton : ((_ obj: CouponCodePopUp) -> Void)? = nil
    
    var onClickLeftButton : (() -> Void)? = nil
    var onClickRemoveButton : (() -> Void)? = nil
    var strPromo: String?
    
    public override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public static func  showCouponCodeDialog
        (title:String,
         strRightButton:String,
         strPromo: String?,
         tag:Int = 4574,
         isPromoApplied : Bool = false
         ) -> CouponCodePopUp
     {
         var view = UINib(nibName: "CouponCodePopUp", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CouponCodePopUp
         view.tag = tag
         if ((APPDELEGATE.window?.viewWithTag(tag)) != nil) {
             if let couponPopUp = APPDELEGATE.window?.viewWithTag(tag) as? CouponCodePopUp {
                 view = couponPopUp
             }
             APPDELEGATE.window?.bringSubviewToFront((APPDELEGATE.window?.viewWithTag(tag))!)
         } else {
             DispatchQueue.main.async{
                 UIApplication.shared.keyWindow?.addSubview(view)
                 APPDELEGATE.window?.bringSubviewToFront(view);
                 view.alertView.layoutIfNeeded()
                 view.animationBottomTOTop(view.alertView)
             }
         }
         
         view.setLocalization()
         let frame = (APPDELEGATE.window?.frame)!;
         view.frame = frame;
         view.lblTitle.text = title;
         view.btnApply.setTitle(strRightButton, for: UIControl.State.normal)
         view.lblTitle.text = title
         if strPromo != nil {
             view.txtCouponCode.text = strPromo
             view.btnApply.isHidden = true
             view.btnRemove.isHidden = false
         } else {
             view.btnApply.isHidden = false
         }
         
         view.btnRemove.isHidden = !view.btnApply.isHidden
         
         view.strPromo = strPromo
         if isPromoApplied{
             
         }
        return view;
    }
    
    
    func setLocalization() {
        lblHaveCouponCode.text = "txt_a_coupon_code".localized
        lblHaveCouponCode.textColor = UIColor.themeLightTextColor
        
        alertView.backgroundColor = UIColor.themeAlertViewBackgroundColor
        backgroundColor = UIColor.themeOverlayColor
        
        lblTitle.font = FontHelper.textLarge(size: FontHelper.large)
        lblViewOffer.font = FontHelper.textRegular()
        lblViewOffer.textColor = UIColor.themeColor
        
        btnApply.titleLabel?.font = FontHelper.textRegular()
        btnRemove.titleLabel?.font = FontHelper.textRegular()
        btnLeft.setImage(UIImage.init(named: "cancelIcon")?.imageWithColor(color: .themeColor), for: .normal)
        self.alertView.updateConstraintsIfNeeded()
        self.alertView.roundCorner(corners: [.topRight , .topLeft], withRadius: 20)
        
        txtCouponCode.setRound(withBorderColor: UIColor.themeLightTextColor, andCornerRadious: 3, borderWidth: 0.5)
        txtCouponCode.delegate = self
        
        self.registerForKeyboardNotifications()
        self.delegatekeyboardWasShown = self
        self.delegatekeyboardWillBeHidden = self
    }
    
    @IBAction func onClickBtnLeft(_ sender: Any) {
        if self.onClickLeftButton != nil {
            self.onClickLeftButton!();
            self.removeFromSuperview()
        }
    }
    @IBAction func actionViewoffer(_ sender: Any) {
        self.wsGetStoreOffers()
    }
    
    @IBAction func onClickBtnRight(_ sender: UIButton) {
        self.endEditing(true)
        if txtCouponCode.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            Utility.showToast(message: "txt_please_enter_coupon_code".localized)
        } else {
            if self.onClickRightButton != nil {
                self.onClickRightButton!(self)
            }
        }
    }
    
    @IBAction func onClickRemove(_ sender: UIButton) {
        self.endEditing(true)
        if self.onClickRemoveButton != nil {
            self.onClickRemoveButton!();
            self.removeFromSuperview()
        }
    }
    func wsGetStoreOffers() {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> = [
            PARAMS.STORE_ID: "",
            PARAMS.DELIVERY_TYPE:DeliveryType.taxi,
            PARAMS.IS_ADMIN_SERVICES : false,
//            PARAMS.USER_ID:preferenceHelper.UserId,
            PARAMS.CITY_ID:currentBooking.bookCityId ?? "",
            PARAMS.COUNTRY_ID: currentBooking.bookCountryId ?? ""
        ]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_STORE_DETAIL, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response,error) -> (Void) in
            Utility.hideLoading()
            if (Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true)) {
                let objOffers:OffersModal = OffersModal.init(fromDictionary: response as! [String : Any])
                print(objOffers.promoCodes.count)
                self.openOffersDialog(arrPromoCodes: objOffers.promoCodes)
            }
        }
    }
    func openOffersDialog(arrPromoCodes:Array<PromoCodeModal>)  {
        let dialogOfferList = CustomOffersDialog.showOffers(title: "Offers", message: "", arrPromoCodes: arrPromoCodes, isFromHome: false)
        dialogOfferList.onClickLeftButton = {
            dialogOfferList.removeFromSuperview()
        }
        dialogOfferList.onClickApply = {
            (promoName) in
            dialogOfferList.removeFromSuperview()
            self.txtCouponCode.text = promoName
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.onClickRightButton!(self)

            }
        }
    }
    //MARK: Keyboard Delegates
    @objc override func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.bottomAnchorConstraint.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            DispatchQueue.main.async {
                self.bottomAnchorConstraint.constant = keyboardSize!.height
            }
        }
    }
    
    @objc override func keyboardWillBeHidden(notification: NSNotification)
    {
        UIView.animate(withDuration: 0.5) {
            DispatchQueue.main.async {
                self.bottomAnchorConstraint.constant = 0.0
            }
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
}
