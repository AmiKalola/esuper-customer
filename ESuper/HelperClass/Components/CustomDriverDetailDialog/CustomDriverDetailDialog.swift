//
//  CustomDriverDetailDialog.swift
//  Eber
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
import UIKit


public class CustomDriverDetailDialog: CustomDialog
{
   //MARK: - OUTLETS
    @IBOutlet weak var stkBtns: UIStackView!
    @IBOutlet weak var lblMessage: LabelDefault!
    @IBOutlet weak var btnCancelTrip: CustomBottomButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK:Variables
    var onClickCancelTrip : (() -> Void)? = nil
    var onClickBack : (() -> Void)? = nil
    
    static let  verificationDialog = "dialogForDriverDetail"
    
    public static func  showCustomDriverDetailDialog
    (message:String,
     cancelButton:String) ->
    CustomDriverDetailDialog{
        
        let view = UINib(nibName: verificationDialog, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomDriverDetailDialog
        view.tag = DialogTag.driverDetailDialog
        view.alertView.setShadow()
        view.alertView.backgroundColor = UIColor.white
        view.backgroundColor = UIColor.themeOverlayColor
        let frame = (APPDELEGATE.window?.frame)!;
        view.frame = frame;
        view.imgDriver.image =  UIImage.animatedImage(with: [UIImage.init(named: "asset-lookingForProvider1")!,UIImage.init(named: "asset-lookingForProvider2")!], duration: 0.5)
        view.lblMessage.text = message;
        view.setLocalization()
        view.btnCancelTrip.setTitle(cancelButton.localizedCapitalized, for: UIControl.State.normal)
        view.btnBack.tintColor = UIColor.themeWhiteTransparentColor
        if let view = (APPDELEGATE.window?.viewWithTag(DialogTag.driverDetailDialog)){
            UIApplication.shared.keyWindow?.bringSubviewToFront(view);
        }
        else{
            UIApplication.shared.keyWindow?.addSubview(view)
            UIApplication.shared.keyWindow?.bringSubviewToFront(view);
        }
        
        if let sVw = view.superview {
            view.btnBack.topAnchor.constraint(equalTo: sVw.safeAreaLayoutGuide.topAnchor).isActive = true
        }
        return view;
    }
    
    func setLocalization(){
        btnCancelTrip.setTitleColor(UIColor.themeButtonTitleColor, for: UIControl.State.normal)
        btnCancelTrip.backgroundColor = UIColor.themeButtonBackgroundColor
        lblMessage.textColor = UIColor.themeTextColor
        
        /* Set Font */
        btnCancelTrip.titleLabel?.font = FontHelper.textLarge()
        self.backgroundColor = UIColor.themeOverlayColor
        self.alertView.backgroundColor = UIColor.white
        self.alertView.setRound(withBorderColor: .clear, andCornerRadious: 10.0, borderWidth: 1.0)
    }
    
    @IBAction func onClickBtnCancel(_ sender: Any){
        if self.onClickCancelTrip != nil{
            self.onClickCancelTrip!()
        }
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        if self.onClickBack != nil {
            self.removeFromSuperview()
            self.onClickBack!()
        }
    }
}


