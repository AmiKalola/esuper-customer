//
//  CustomPhotoDialog.swift
//  
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

public class FilterProviderPopUp: CustomDialog, keyboardWasShownDelegate, keyboardWillBeHiddenDelegate {
 
    //MARK:- OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var bottomAnchorConstraint: NSLayoutConstraint!

    @IBOutlet weak var btnHourly: UIButton!
    @IBOutlet weak var btnFixed: UIButton!
        
    @IBOutlet weak var lblRating: LabelDefault!
    @IBOutlet weak var lblPricing: LabelDefault!
    
    @IBOutlet weak var cosmosVw: CosmosView!
    
    @IBOutlet weak var subViewPricing: ViewDefault!
    
    @IBOutlet weak var btnApply: CustomBottomButton!
    @IBOutlet weak var btnClear: CustomBottomButton!
    
    var rate: Double = 0
    var priceHourly = 0
    var filter: FilterProviderPopUp?
 
    //MARK:Variables
    var onClickRightButton : ((_ obj: FilterProviderPopUp) -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    var onClickClearButton : ((_ obj: FilterProviderPopUp) -> Void)? = nil
    
    static let  dialogNibName = "FilterProviderPopUp"
        
    public static func  showProvierFilterPopUp
        (title:String,
         strRight:String,
         obj: FilterProviderPopUp? = nil,
         tag:Int = 959
         ) -> FilterProviderPopUp {
        let view = UINib(nibName: dialogNibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FilterProviderPopUp
        view.tag = tag
        if ((APPDELEGATE.window?.viewWithTag(tag)) != nil) {
            APPDELEGATE.window?.bringSubviewToFront((APPDELEGATE.window?.viewWithTag(tag))!)
        }else {
            view.filter = obj
            let frame = (APPDELEGATE.window?.frame)!;
            view.frame = frame;
            view.lblTitle.text = title;
    
            DispatchQueue.main.async{
                UIApplication.shared.keyWindow?.addSubview(view)
                APPDELEGATE.window?.bringSubviewToFront(view);
                view.alertView.layoutIfNeeded()
                view.animationBottomTOTop(view.alertView)
            }
        }
       
        view.cosmosVw.rating = 0.0
        view.cosmosVw.didTouchCosmos = { (rating: Double) -> () in
        }
        view.cosmosVw.didFinishTouchingCosmos = {(rating: Double) -> () in
            view.rate = rating
        }
        
        view.cosmosVw.settings.fillMode = .half
        view.cosmosVw.settings.starSize = 30//Double(view.cosmosVw.frame.size.height)
        view.cosmosVw.settings.minTouchRating = 0.0
        view.subViewPricing.setRound(withBorderColor: UIColor.themeLightTextColor, andCornerRadious: view.subViewPricing.frame.size.height/2, borderWidth: 1.0)
        view.setLocalization()
        return view;
    }
    
    public override func removeFromSuperview() {
        self.deregisterFromKeyboardNotifications()
        super.removeFromSuperview()
    }

    func setLocalization() {
        alertView.backgroundColor = UIColor.themeAlertViewBackgroundColor
        backgroundColor = UIColor.themeOverlayColor
        lblTitle.textColor = UIColor.themeTextColor;
        lblTitle.font = FontHelper.textLarge(size: FontHelper.large)
        btnLeft.setImage(UIImage.init(named: "cancelIcon")?.imageWithColor(color: .themeColor), for: .normal)
        self.alertView.updateConstraintsIfNeeded()
        self.alertView.roundCorner(corners: [.topRight , .topLeft], withRadius: 20)
        
        self.cosmosVw.settings.filledImage = UIImage(named: "ff")?.imageWithColor(color: UIColor.themeColor)
        self.cosmosVw.settings.emptyImage = UIImage(named: "nf")?.imageWithColor(color: UIColor.themeTitleColor)
        
        self.btnFixed.applyRoundedCornersWithHeight()
        self.btnHourly.applyRoundedCornersWithHeight()
        
        btnFixed.setTitleColor(UIColor.white, for: .selected)
        btnFixed.setTitleColor(UIColor.themeTextColor, for: .normal)
        
        btnHourly.setTitleColor(UIColor.white, for: .selected)
        btnHourly.setTitleColor(UIColor.themeTextColor, for: .normal)
        
        btnHourly.setBackgroundColor(color: UIColor.themeButtonBackgroundColor, forState: .selected)
        btnFixed.setBackgroundColor(color: UIColor.themeButtonBackgroundColor, forState: .selected)
        btnHourly.setBackgroundColor(color: UIColor.themeViewBackgroundColor  , forState: .normal)
        btnFixed.setBackgroundColor(color: UIColor.themeViewBackgroundColor  , forState: .normal)
        
        btnHourly.setTitle("txt_hourly_price".localized, for: .normal)
        btnFixed.setTitle("txt_fixed_price".localized, for: .normal)
        btnApply.setTitle("TXT_APPLY".localized, for: .normal)
        btnClear.setTitle("TXT_CLEAR".localized, for: .normal)
        
        lblPricing.text = "txt_pricing".localized
        lblRating.text = "txt_pricing".localized
        
        if let filter = filter {
            self.cosmosVw.rating = filter.cosmosVw.rating
            self.rate = filter.rate
            self.btnFixed.isSelected = filter.btnFixed.isSelected
            self.btnHourly.isSelected = filter.btnHourly.isSelected
        }
    }

    override func updateUIAccordingToTheme() {
        self.setLocalization()
    }
    
    @IBAction func onClickBtnPrice(_ sender: UIButton) {
        
        btnHourly.backgroundColor = UIColor.themeViewBackgroundColor
        btnFixed.backgroundColor = UIColor.themeViewBackgroundColor
        btnHourly.isSelected = false
        btnFixed.isSelected = false
        
        sender.backgroundColor = UIColor.themeButtonBackgroundColor
        sender.isSelected = true
        switch sender.tag {
        case 0:
            priceHourly = 0
            break
        case 1:
            priceHourly = 1
            break
        default:
            print("default")
        }
    }

    @IBAction func onClickBtnLeft(_ sender: Any) {
      
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                       animations: {
                        self.alertView.frame = CGRect.init(x: self.alertView.frame.origin.x, y: self.frame.height, width: self.alertView.frame.size.width, height: self.alertView.frame.size.height)
                        
        }, completion: { test in
            if self.onClickLeftButton != nil {
                self.onClickLeftButton!();
            }
            self.removeFromSuperview()
        })
    }
    
    @IBAction func onClickBtnRight(_ sender: UIButton) {
        if self.onClickRightButton != nil {
            self.onClickRightButton!(self)
        }
    }
    
    @IBAction func onClickClear(_ sender: UIButton) {
        if self.onClickClearButton != nil {
            self.onClickClearButton!(self)
        }
    }
}


