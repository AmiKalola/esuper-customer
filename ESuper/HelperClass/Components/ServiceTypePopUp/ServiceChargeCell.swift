//
//  ServiceChargeCell.swift
//  ESuper
//
//  Created by Rohit on 27/12/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import UIKit

class ServiceChargeCell: UITableViewCell {
    @IBOutlet weak var lblService : UILabel!
    @IBOutlet weak var lblCharge : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
