//
//  WhichDeliveryCell.swift
//  Decagon
//
//  Created by MacPro3 on 09/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class SelectServiceCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDeliveryName: LabelDefault!
    @IBOutlet weak var imgDelivery: ImageViewDefault!
    @IBOutlet weak var viewMain: ViewDefault!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgDelivery.applyRoundedCornersWithHeight(10)
    }
    
    func configData(data: VehicleDetail) {
        lblDeliveryName.text = data.vehicleName
        imgDelivery.downloadedFrom(link: data.imageUrl ?? "", placeHolder: "placeholder", isFromCache: true, isIndicator: false, mode: .scaleAspectFit, isAppendBaseUrl: true, isFromResize: false, completion: nil)
    }

}
