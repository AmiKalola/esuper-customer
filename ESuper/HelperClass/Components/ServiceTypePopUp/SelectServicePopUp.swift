//
//  CustomPhotoDialog.swift
//  
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
import UIKit
struct FareEstimateValues{
    var pickupAddress : String?
    var vehical_name : String?
    var vehical_image : String?
    var destinationAddress : String?
    var price_formula = 0
    var total_service_price : Double = 0.0
    var total_admin_tax_price : Double = 0.0
    var total_surge_price : Double = 0.0
    var total_delivery_price : Double = 0.0
    var total : Double = 0.0
    var promo_payment : Double = 0.0
    var promo_code_name : String = ""
    var ETA = 0
    var distance = 0.0
    var user_pay_payment = 0.0
    var base_price : Double = 0.0
    var base_price_distance : Double = 0.0
    var price_per_unit_distance : Double = 0.0
    var additional_stop_price : Double = 0.0
    var cancalation_price : Double = 0.0
    var service_tax : Double = 0.0
    var surge_multiplier : Double = 1.0
    var is_min_fare_applied : Bool = false
    var min_fare : Double = 0.0
    var arrayInvoice = [ServiceCharge]()
    var arrayFixedRateInvoice = [ServiceCharge]()
}

public class SelectServicePopUp: CustomDialog {
 
    //MARK: - OUTLETS
    @IBOutlet weak var btnOffer: UIButton!
    @IBOutlet weak var btnCreateTrip: UIButton!
    @IBOutlet weak var alertView: ShadowWithCornerView!
    @IBOutlet weak var colletionView: UICollectionView!
    @IBOutlet weak var heightColletioView: NSLayoutConstraint!
    
    @IBOutlet weak var tableInvoice: UITableView!
    @IBOutlet weak var ConstraintInvoiceHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imgCalender: UIImageView!
    @IBOutlet weak var viewCalender: UIView!
        
    @IBOutlet weak var imgDistance: UIImageView!
    @IBOutlet weak var imgTime: UIImageView!
    @IBOutlet weak var imgPrice: UIImageView!
    
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblTitle: LabelDefault!
    
    @IBOutlet weak var imgPromoRightCheck: UIImageView!
    @IBOutlet weak var stackViewButtons: UIStackView!
    var scheduleData: ScheduleDialogData?
    var startAddress  = ""
    var destinationAddress = ""
    var listDestinationAddres = [Address]()
    var listPickupAddres = Address()
    var onSelectVehical : ((_ obj: VehicleDetail) -> Void)? = nil
    var onClickChoose : (() -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    var onClickCreateTrip : ((_ obj: SelectServicePopUp) -> Void)? = nil
    
    var onClickPromoCode : ((_ obj: SelectServicePopUp) -> Void)? = nil
    
    var arrServiceType: [VehicleDetail] = [] {
        didSet {
            setColletionView()
        }
    }
    static let spacing: CGFloat = 15
    static let edge: CGFloat = 20
    var selectedVehical : VehicleDetail?
    var bottomConstrint: NSLayoutConstraint?
    var isShow = false
    var selectedIndex = -1
    var parent: UIViewController?
    var isCourierDataFilled = false
    var isSelectSchedule = false
    var arrayInvoice = [ServiceCharge]()
    var fareEstimateValues = FareEstimateValues()
    var invoiceResponse: InvoiceResponse!
    var promoCode: String?
    var promoID: String?
    var isPromoApplied = false
    var paramPromoInvoice = [String: Any]()

    let colletionCellSize: CGSize = {
        let width = screenWidth - 6*spacing
        let returnWidth = width/4
        return CGSize(width: returnWidth, height: returnWidth + 48)
    }()
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setLocalization()
        setColletionView()
        scheduleData = ScheduleDialogData()
        scheduleData?.isAsap = true
        
        btnOffer.setImage(UIImage(named: "offers_icon_tint"), for: .normal)
        btnOffer.tintColor = UIColor.black
        imgPromoRightCheck.isHidden = true
        self.tintColor = UIColor.themeColor
    }

    func setLocalization() {
        alertView.backgroundColor = UIColor.themeAlertViewBackgroundColor
        backgroundColor = UIColor.clear
        
        btnCreateTrip.titleLabel?.font = FontHelper.textRegular(size: FontHelper.medium)
        
        if let parent = parent as? RideHomeVC {
            if parent.deliveryType == DeliveryType.courier {
                btnCreateTrip.setTitle("txt_add_courier_detail".localized, for: .normal)
            } else {
                btnCreateTrip.setTitle("txt_create_trip_now".localized, for: .normal)
            }
        } else {
            btnCreateTrip.setTitle("txt_create_trip_now".localized, for: .normal)
        }
        
        imgCalender.tintColor = UIColor.white
        
        viewCalender.backgroundColor = UIColor.themeColor
        viewCalender.cornerRadiuss = viewCalender.frame.size.height / 2
        
        lblTitle.text = "txt_ride".localized
        lblTitle.textColor = UIColor.themeTextColor;
        lblTitle.font = FontHelper.textLarge(size: FontHelper.large)
        
//        btnDistance.tintColor = UIColor.themeTextColor
//        btnTime.tintColor = UIColor.themeTextColor
//        btnPrice.tintColor = UIColor.themeTextColor
        
        lblDistance.textColor = UIColor.themeTextColor
        lblTime.textColor = UIColor.themeTextColor
        lblPrice.textColor = UIColor.themeTextColor
        
        lblDistance.font = FontHelper.textLarge(size: FontHelper.regular)
        lblTime.font = FontHelper.textLarge(size: FontHelper.regular)
        lblPrice.font = FontHelper.textLarge(size: FontHelper.regular)
        
    }
    
    func setColletionView() {
        tableInvoice.register(ServiceChargeCell.nib, forCellReuseIdentifier: ServiceChargeCell.identifier)
        
        colletionView.delegate = self
        colletionView.dataSource = self
        colletionView.register(UINib(nibName: "SelectServiceCell", bundle: nil), forCellWithReuseIdentifier: "SelectServiceCell")
        colletionView.contentInset = UIEdgeInsets(top: 0, left: SelectServicePopUp.spacing, bottom: 0, right: SelectServicePopUp.spacing)
        
        heightColletioView.constant = colletionCellSize.height
        ConstraintInvoiceHeight.constant = 0
        colletionView.reloadData()
        
//        if arrServiceType.count > 0 {
//            let obj = arrServiceType[0]
//            lblPrice.text = ("\(CurrentBooking.shared.currency)\(obj.total_delivery_price.toString())")
//            self.updateInvoice()
//        }
        for i in 0..<arrServiceType.count{
            if arrServiceType[i].is_default {
                let obj = arrServiceType[i]
                print(obj.vehicleName)
                lblPrice.text = ("\(CurrentBooking.shared.currency)\(obj.total_delivery_price.toString())")
                selectedIndex = i
                let indexPath = IndexPath(item: i, section: 0)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.colletionView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
                }
                self.updateInvoice(index: i)
            }
        }
        
    }
    func uppdateDataForInvoice(isPromo : Bool = false, totalValue: Double = 0.0 , promoValue : Double = 0.0,promoName : String = ""){
        self.arrayInvoice = [ServiceCharge]()
        let obj = self.invoiceResponse.order_payment!
        let vehicles = self.invoiceResponse.vehicles[0]
        self.fareEstimateValues = FareEstimateValues()
        self.fareEstimateValues.pickupAddress = self.startAddress
        self.fareEstimateValues.destinationAddress = self.destinationAddress
        self.fareEstimateValues.vehical_name = vehicles.vehicleName
        self.fareEstimateValues.vehical_image = vehicles.imageUrl
        self.fareEstimateValues.total_service_price = obj.total_service_price!
        self.fareEstimateValues.total_admin_tax_price = obj.total_admin_tax_price!
        self.fareEstimateValues.total_surge_price = obj.total_surge_price!
        self.fareEstimateValues.total_service_price = obj.total_service_price!
        self.fareEstimateValues.price_formula = obj.price_formula
        self.fareEstimateValues.total_delivery_price = obj.total_delivery_price!
        self.fareEstimateValues.ETA = currentBooking.estimateTime
        self.fareEstimateValues.distance = currentBooking.estimateDistance
        self.fareEstimateValues.total = obj.total ?? 0.0
        self.fareEstimateValues.user_pay_payment = obj.user_pay_payment
        self.fareEstimateValues.base_price = obj.base_price!
        self.fareEstimateValues.base_price_distance = obj.base_price_distance!
        self.fareEstimateValues.price_per_unit_distance   = obj.price_per_unit_distance!
        self.fareEstimateValues.additional_stop_price  = obj.additional_stop_price
        //        self.fareEstimateValues.cancalation_price   = obj.cancalation_price
        self.fareEstimateValues.service_tax   = Double(obj.service_tax!)
        self.fareEstimateValues.surge_multiplier = Double(obj.surge_multiplier!)
        self.fareEstimateValues.is_min_fare_applied = obj.isMinFareApplied
        self.fareEstimateValues.min_fare = obj.min_fare ?? 0.0
        if isPromo{
            self.fareEstimateValues.total_delivery_price = totalValue
            self.fareEstimateValues.promo_payment = promoValue
            self.fareEstimateValues.promo_code_name = promoName
            self.promoCode = promoName
        }else{
            self.fareEstimateValues.promo_payment = 0.0
            self.self.fareEstimateValues.promo_code_name = ""
        }
        lblPrice.text = ("\(CurrentBooking.shared.currency)\(self.fareEstimateValues.total_delivery_price.toString())")
        if obj.total_base_price > 0{
            arrayInvoice.append(ServiceCharge(strService: "Base_Price".localized,strCharges:"\u{20B9}" + String(obj.total_base_price.toString())))
        }
        if obj.total_distance_price! > 0{
            var disPrice = obj.total_distance_price!
            if obj.total_distance_price! >= obj.total_base_price{
                disPrice = (obj.total_distance_price! -  obj.total_base_price)
            }
            if disPrice > 0{
                arrayInvoice.append(ServiceCharge(strService: "Distance_Price".localized,strCharges: "\u{20B9}" + String(disPrice.toString())))
            }
        }
        if obj.total_time_price! > 0{
            arrayInvoice.append(ServiceCharge(strService: "Time_Price".localized,strCharges: "\u{20B9}" + String(obj.total_time_price!.toString())))
        }
        if obj.additional_stop_price > 0{
            arrayInvoice.append(ServiceCharge(strService: "Stop_Price".localized,strCharges: "\u{20B9}" + String(obj.additional_stop_price.toString())))
        }
        if obj.price_formula > 0{
            let fiexed_price = (obj.total_delivery_price! - obj.total_surge_price!)
        }
        if self.fareEstimateValues.promo_payment > 0{
            arrayInvoice.append(ServiceCharge(strService: "Promo_Bonus".localized,strCharges:  "\u{20B9}" + String(self.fareEstimateValues.promo_payment.toString())))
        }
        if obj.service_tax! > 0{
            arrayInvoice.append(ServiceCharge(strService: "Tax \(obj.service_tax!)%".localized,strCharges:  "\u{20B9}" + String(obj.total_admin_tax_price!)))
        }
        
        self.fareEstimateValues.arrayInvoice = arrayInvoice
        ConstraintInvoiceHeight.constant = 0//CGFloat((arrayInvoice.count * 33))
        tableInvoice.isScrollEnabled = false
        self.tableInvoice.reloadData()
    }
    
    func wsGetInvoice(dictParam:Dictionary<String,Any>){
        Utility.showLoading()
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_COURIER_INVOICE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response,error) -> (Void) in
            print(response)
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                if let invoiceResponse = InvoiceResponse.init(dictionary: response){
                    self.invoiceResponse = invoiceResponse
                    self.uppdateDataForInvoice()
                }
            }
        }
    }
    func parmaforGetInvoiceAPI(obj : VehicleDetail,isScheduleOrder : Bool = false,orderStartAt : String = "",orderStartAt2 : String = "") {
       
        var localTimeZoneIdentifier: String { return TimeZone.current.identifier }
        print(localTimeZoneIdentifier)
        var dictParam:[String:Any] = [PARAMS.USER_ID : preferenceHelper.UserId]
        dictParam[PARAMS.TOTAL_TIME] = currentBooking.estimateTime
        dictParam[PARAMS.TOTAL_DISTANCE] = currentBooking.estimateDistance
        dictParam[PARAMS.COUNTRY_ID] = currentBooking.bookCountryId ?? ""
        dictParam[PARAMS.CITY_ID] = currentBooking.bookCityId ?? ""
        dictParam[PARAMS.VEHICLE_ID] = obj.vehicleId//self.viewServiceType.getSelectedVehicle()?.vehicleId ?? ""
        dictParam[PARAMS.DELIVERY_TYPE] = DeliveryType.taxi //self.deliveryType
        dictParam[PARAMS.SERVER_TOKEN] = preferenceHelper.SessionToken
        dictParam[PARAMS.TIMEZONE] = localTimeZoneIdentifier//"Asia/Kolkata"
        dictParam[PARAMS.is_round_trip] = false
        dictParam[PARAMS.TIP_AMOUNT] = 0
        dictParam[PARAMS.no_of_stop] = (self.listDestinationAddres.count - 1)
        dictParam[PARAMS.SERVICE_ID] = obj.service_id
        dictParam[PARAMS.TOTAL_CART_PRICE] = 0//obj.total_delivery_price
        if currentBooking.isFutureTaxiOrder{
            dictParam[PARAMS.order_Start_At] = currentBooking.futureTaxiUTCMilliSecond
            dictParam[PARAMS.order_Start_At2] = currentBooking.futureTaxiUTCMilliSecond2
        }
        
        var myPickupArray: [Any] = []
        var myDestinationArray:[Any] = []
        myPickupArray.append(listPickupAddres.toDictionary())
        for address in self.listDestinationAddres {
          myDestinationArray.append(address.toDictionary())
        }
        let cartParam : [String:Any] =
        [PARAMS.DESTINATION_ADDRESS:myDestinationArray,
         PARAMS.PICKUP_ADDRESSES:myPickupArray
        ]
        dictParam[PARAMS.cart] = cartParam
        self.wsGetInvoice(dictParam: dictParam)
    }
    
    func updateInvoice(index : Int = 0){
        self.arrayInvoice = [ServiceCharge]()
        let obj = arrServiceType[index]
        selectedVehical = obj
        self.parmaforGetInvoiceAPI(obj: obj)

    }
    func addIn(vw: UIView, parent: UIViewController? = nil) {
        vw.addSubview(self)
        self.parent = parent
        alertView.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vw, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0).isActive = true
        
        NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vw, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0).isActive = true

        NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.alertView.frame.size.height).isActive = true
        
        self.bottomConstrint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: vw, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: self.alertView.frame.size.height)
        
        self.bottomConstrint?.isActive = true
    
        setLocalization()
    }
    
    func show(pickupOreder : String , destinationAddress : String) {
        self.startAddress = pickupOreder
        self.destinationAddress = destinationAddress
        
        self.fareEstimateValues.pickupAddress = self.startAddress
        self.fareEstimateValues.destinationAddress = self.destinationAddress

        UIView.animate(withDuration: 0.3, delay: 0.5, options: .curveEaseIn) {
            self.bottomConstrint?.constant = 0.0
            self.superview?.layoutIfNeeded()
        } completion: { [weak self] _ in
            guard let self = self else { return }
            self.isShow = true
        }
    }
    
    func hide(complition: (() -> ())? = nil) {
        UIView.animate(withDuration: 0.3,
                       animations: {
            self.bottomConstrint?.constant = self.alertView.frame.size.height
            self.superview?.layoutIfNeeded()
        }) { [weak self] _ in
            guard let self = self else { return }
            self.isShow = false
            if complition != nil {
                complition!()
            }
        }
    }

    override func updateUIAccordingToTheme() {
        self.setLocalization()
    }
        //ActionMethods
    @IBAction func onClickBtnLeft(_ sender: Any) {
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                       animations: {
            self.alertView.frame = CGRect.init(x: self.alertView.frame.origin.x, y: self.frame.height, width: self.alertView.frame.size.width, height: self.alertView.frame.size.height)
        }, completion: { test in
            if self.onClickLeftButton != nil {
                self.onClickLeftButton!();
                self.removeFromSuperview()
            }
        })
    }
    
    @IBAction func onClickOffer(_ sender: UIButton) {
        self.showPromoCode()
        
//        call RideHome screen function
//        if self.onClickPromoCode != nil {
//            self.onClickPromoCode!(self)
//        }
    }
    func showPromoCode() {
        let dailog = CouponCodePopUp.showCouponCodeDialog(title: "TXT_OFFERS".localized, strRightButton: "TXT_APPLY".localized, strPromo: promoCode,isPromoApplied: self.isPromoApplied)
        dailog.onClickLeftButton = {
            dailog.removeFromSuperview()
        }
        dailog.onClickRightButton = { [weak self] obj in
            guard let self = self else { return }
            dailog.removeFromSuperview()
            self.wsCheckPromoCode(strPromo: obj.txtCouponCode.text!)
        }
        dailog.onClickRemoveButton = { [weak self] in
            guard let self = self else { return }
            self.removePromocode()
        }
    }
    func wsCheckPromoCode(strPromo: String) {
        Utility.showLoading()
        let obj = self.invoiceResponse.order_payment!
        
        let dictOrderPaymentParam: Dictionary<String,Any> = [
            PARAMS.USER_ID:preferenceHelper.UserId,
            PARAMS.TOTAL_DELIVERY_PRICE:obj.total_delivery_price ?? 0.0,
            PARAMS.TOTAL:obj.total_delivery_price ?? 0.0,
            PARAMS.user_pay_payment:obj.user_pay_payment ,
            PARAMS.TOTAL_CART_PRICE:0,
            PARAMS.is_store_pay_delivery_fees : false,
            PARAMS.TIP_AMOUNT:0,
            PARAMS.BOOKING_FEES:0
        ]
        
        let dictCartParam: Dictionary<String,Any> = [
            //            PARAMS.USER_ID:preferenceHelper.UserId,
            PARAMS.TOTAL_ITEM_COUNT : 1,
            PARAMS.DELIVERY_TYPE : DeliveryType.taxi,
            PARAMS.IS_ADMIN_SERVICES :false,
            PARAMS.ORDER_DETAILS : []
        ]
        
        let dictParam: Dictionary<String,Any> = [
            PARAMS.USER_ID:preferenceHelper.UserId,
            PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
            PARAMS.CITY_ID:currentBooking.bookCityId ?? "",
            PARAMS.cart:dictCartParam,
            PARAMS.ORDER_PAYMENT:dictOrderPaymentParam,
            PARAMS.PROMO_CODE: strPromo,
            PARAMS.STORE_DELIVERY_ID : currentBooking.taxiDeliveryId
        ]
        
        self.paramPromoInvoice = [String: Any]()
        self.paramPromoInvoice[PARAMS.USER_ID] = preferenceHelper.UserId
        self.paramPromoInvoice[PARAMS.SERVER_TOKEN] = preferenceHelper.SessionToken
        self.paramPromoInvoice[PARAMS.CITY_ID] = currentBooking.bookCityId ?? ""
        self.paramPromoInvoice[PARAMS.PROMO_CODE] = strPromo
        self.paramPromoInvoice[PARAMS.STORE_DELIVERY_ID] = currentBooking.taxiDeliveryId
        
        print(self.paramPromoInvoice)
        //APPLY PROMO
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_CHECK_PROMO_COURIER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response,error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                let invoiceResponse:InvoiceResponse = InvoiceResponse.init(dictionary: response)!
                Utility.showToast(message: invoiceResponse.status_phrase)
                self.isPromoApplied = false
                if invoiceResponse.success!{
                    self.isPromoApplied = true
                    self.promoID = (invoiceResponse.order_payment?.promo_id)
                    self.imgPromoRightCheck.isHidden = false
                    self.uppdateDataForInvoice(isPromo: true,totalValue: (invoiceResponse.order_payment?.total_delivery_price)!, promoValue: (invoiceResponse.order_payment?.promo_payment ?? 0.0)!,promoName: invoiceResponse.order_payment!.promo_code_name)
                    
                }else{
                    self.imgPromoRightCheck.isHidden = true
                }
            } else{
            }
        }
    }
    
    func removePromocode() {
        imgPromoRightCheck.isHidden = true
        self.isPromoApplied = false
        self.uppdateDataForInvoice()
        self.promoCode = nil
        self.promoID = nil
    }
    
    @IBAction func onClickCharges(_ sender: UIButton) {
        _ = FareEstimateDialog.showFareEstimateDialog(fareEstimateValues: self.fareEstimateValues)
    }
    
    @IBAction func onClickScheduleButton(_ sender: UIButton) {
        let dailog = ScheduleDialog.showScheduleDialog(title: "TXT_WHEN".localized, strRightButton: "TXT_CONTINUE".localized, data: self.scheduleData,isTaxi: true)
        dailog.onClickLeftButton = {
            dailog.removeFromSuperview()
        }
        dailog.onClickRightButton = { [weak self] obj in
            guard let self = self else { return }
            if !obj.btnAsap.isSelected {
                self.scheduleData?.miliSecond = obj.futureDateMillisecond
                self.scheduleData?.strDate = obj.txtDate.text!
                self.scheduleData?.strTime = obj.txtTime.text!
                self.scheduleData?.isAsap = obj.btnAsap.isSelected
                self.scheduleData?.futureDateMillisecond = obj.futureDateMillisecond
                self.scheduleData?.futureTimeMillisecond = obj.futureTimeMillisecond
                self.scheduleData?.selectedDateStr = obj.selectedDateStr
                self.scheduleData?.selectedDayInd = obj.selectedDayInd
                currentBooking.isFutureTaxiOrder = true
                if let date = Utility.LocalMillisecondToDateComponents(milliSeconds: currentBooking.futureTaxiDateMilliSecond, strTimeZone: currentBooking.selectedCityTimezone).date {
                    let timezonez = TimeZone.init(identifier:currentBooking.selectedCityTimezone) ?? TimeZone.ReferenceType.default
                    currentBooking.isFutureTaxiOrder = true
                    let printDate = "\(Utility.dateToString(date: date, withFormat: "dd MMM",withTimezone: timezonez))"
                    var  printTIimrr = obj.txtTime.text ?? ""
                    self.parmaforGetInvoiceAPI(obj: self.selectedVehical!)
                    self.btnCreateTrip.setTitle("\(printDate), \(printTIimrr)", for: .normal)
                } else {
                    self.btnCreateTrip.setTitle("txt_create_trip_to_time".localized.replacingOccurrences(of: "****", with: ""), for: .normal)
                }
            } else {
                self.scheduleData?.miliSecond = 0
                self.scheduleData?.strDate = ""
                self.scheduleData?.strTime = ""
                self.scheduleData?.isAsap = obj.btnAsap.isSelected
                self.btnCreateTrip.setTitle("txt_create_trip_now".localized, for: .normal)
            }
            dailog.removeFromSuperview()
        }
    }
    
    
    
    @IBAction func onClickBtnRight(_ sender: UIButton) {
        
    }
    
    @IBAction func onClickCreateTrip(_ sender: UIButton) {
        if self.onClickCreateTrip != nil {
            if selectedIndex >= 0 {
                self.onClickCreateTrip!(self)
            }
        }
    }
    
    func getSelectedVehicle() -> VehicleDetail? {
        if arrServiceType.count > 0 {
            let obj = arrServiceType[selectedIndex]
            return obj
        }
        return nil
    }
    
    public override func removeFromSuperview() {
        super.removeFromSuperview()
        print("removeFromSuperview")
    }
}

extension SelectServicePopUp: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrServiceType.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectServiceCell", for: indexPath) as! SelectServiceCell
        let obj = arrServiceType[indexPath.row]
        cell.configData(data: obj)
        if selectedIndex == indexPath.row {
            cell.viewMain.setRound(withBorderColor: UIColor.themeColor, andCornerRadious: 5, borderWidth: 1)
            cell.lblDeliveryName.font = FontHelper.textMedium(size: FontHelper.regular)
        } else {
            cell.viewMain.setRound(withBorderColor: UIColor.themeSeperatorColor, andCornerRadious: 5, borderWidth: 1)
            cell.lblDeliveryName.font = FontHelper.textRegular()
        }
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        if  self.isPromoApplied{
            self.removePromocode()
        }
        
        let obj = arrServiceType[indexPath.row]
//        lblPrice.text = ("\(CurrentBooking.shared.currency)\(obj.total_delivery_price.toString())")
        self.updateInvoice(index: indexPath.row)
        collectionView.reloadData()
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return colletionCellSize
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}
extension SelectServicePopUp : UITableViewDelegate,UITableViewDataSource{
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayInvoice.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ServiceChargeCell.identifier) as! ServiceChargeCell
        cell.lblService.text = arrayInvoice[indexPath.row].strService ?? ""
        cell.lblCharge.text = arrayInvoice[indexPath.row].strCharges ?? ""
        return cell
    }
}
struct ServiceCharge{
    var strService : String?
    var strCharges : String?
}
