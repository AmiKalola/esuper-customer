//
//  PublicReviewCell.swift
//  ESuper
//
//  Created by Rohit on 14/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit

class PublicReviewCell: CustomTableCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var starImage : UIImageView!
    @IBOutlet weak var heighForStack: NSLayoutConstraint!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnDisLike: UIButton!
    
    var reviewId:String = ""
    var storeListReview:StoreReviewList? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        
        /*Set Font*/
        lblName.font = FontHelper.textMedium()
        lblDate.font = FontHelper.textRegular(size: FontHelper.small)
        lblRate.font = FontHelper.textRegular(size: FontHelper.medium)
        lblComment.font = FontHelper.textRegular(size: FontHelper.medium)
        
        /*Set Color*/
        lblName.textColor = UIColor.themeTextColor
        lblRate.textColor = UIColor.themeTextColor
        lblComment.textColor = UIColor.themeTextColor
        lblDate.textColor = UIColor.themeLightTextColor
        imgProfilePic.setRound()
        btnLike.setTitleColor(UIColor.themeTextColor, for: .normal)
        btnDisLike.setTitleColor(UIColor.themeTextColor, for: .normal)
        btnLike.setImage(UIImage(named: "thumbsUp")?.imageWithColor(color: .themeTextColor), for: .normal)
        btnDisLike.setImage(UIImage(named: "thumbsDown")?.imageWithColor(color: .themeTextColor), for: .normal)
        btnLike.setBackgroundColor(color: UIColor.themeViewBackgroundColor, forState: .normal)
        btnDisLike.setBackgroundColor(color: UIColor.themeViewBackgroundColor, forState: .normal)
        if LocalizeLanguage.isRTL {
            btnLike.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 5.0)
            btnDisLike.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 5.0)
        }
        else {
            btnLike.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 0.0)
            btnDisLike.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 0.0)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
    }
    
    @IBAction func onClikeLikeDislikeReview(_ sender: UIButton) {
        
        if sender == btnLike {
            storeListReview?.isLike = !(storeListReview?.isLike)!
            storeListReview?.isDisLike = false
        }else {
            storeListReview?.isDisLike = !(storeListReview?.isDisLike)!
            storeListReview?.isLike = false
        }
        wsLikeDisLikeReviews()
    }
    
    func setCellData(cellData:StoreReviewList) {
        storeListReview = cellData
        lblComment.text = cellData.userReviewToStore
        lblRate.text = String(cellData.userRatingToStore)
        lblDate.text = Utility.stringToString(strDate: (storeListReview?.createdAt)!, fromFormat: DATE_CONSTANT.DATE_TIME_FORMAT_WEB, toFormat: DATE_CONSTANT.DATE_DD_MMM_YY)
        reviewId = (storeListReview?.id)!
        imgProfilePic.downloadedFrom(link: Utility.getDynamicResizeImageURL(width: imgProfilePic.frame.width, height: imgProfilePic.frame.height, imgUrl: (storeListReview?.userDetail.imageUrl)!),isFromResize: true)
        lblName.text = (storeListReview?.userDetail.firstName)! +  " " + (storeListReview?.userDetail.lastName)!
        if cellData.userReviewToStore.isEmpty() {
            lblComment.isHidden = true
            btnLike.isHidden = true
            btnDisLike.isHidden = true
            heighForStack.constant = 0
        }else {
            heighForStack.constant = 15
            lblComment.isHidden = false
            btnLike.isHidden = false
            btnDisLike.isHidden = false
            storeListReview?.isDisLike = (storeListReview?.idOfUsersDislikeStoreComment.contains(preferenceHelper.UserId))!
            storeListReview?.isLike = (storeListReview?.idOfUsersLikeStoreComment.contains(preferenceHelper.UserId))!
            btnLike.isSelected = (storeListReview?.isLike)!
            btnDisLike.isSelected = (storeListReview?.isDisLike)!
            btnDisLike.setTitle(String(cellData.idOfUsersDislikeStoreComment.count), for: .normal)
            btnLike.setTitle(String(cellData.idOfUsersLikeStoreComment.count), for: .normal)
        }
        starImage.image = UIImage.init(named: "star_black")?.imageWithColor(color: .themeTextColor)
    }
    
    func wsLikeDisLikeReviews() {
        if preferenceHelper.UserId.isEmpty() {
            Utility.showToast(message: "MSG_LOGIN_FIRST".localized)
        }else {
            Utility.showLoading()
            
            let dictParam: Dictionary<String,Any> =
            [
                PARAMS.USER_ID:preferenceHelper.UserId,
                PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
                PARAMS.REVIEW_ID:reviewId,
                PARAMS.IS_USER_CLICKED_LIKE_STORE_REVIEW:storeListReview?.isLike ?? false,
                PARAMS.IS_USER_CLICKED_DISLIKE_STORE_REVIEW:storeListReview?.isDisLike ?? false
                ,
            ]
            
            let afn:AlamofireHelper = AlamofireHelper.init()
            afn.getResponseFromURL(url: WebService.WS_LIKE_DISLIKE_STORE_REVIEW, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
                Utility.hideLoading()
                if Parser.isSuccess(response: response)
                {
                    
                    if (self.storeListReview?.isLike)!
                    {
                        if let index = self.storeListReview?.idOfUsersDislikeStoreComment.firstIndex(where: { ($0 == preferenceHelper.UserId)})
                        {
                            self.storeListReview?.idOfUsersDislikeStoreComment.remove(at: index)
                        }
                        self.storeListReview?.idOfUsersLikeStoreComment.append(preferenceHelper.UserId)
                    }
                    else if (self.storeListReview?.isDisLike)! {
                        if let index = self.storeListReview?.idOfUsersLikeStoreComment.firstIndex(where: { ($0 == preferenceHelper.UserId)})
                        {
                            self.storeListReview?.idOfUsersLikeStoreComment.remove(at: index)
                        }
                        self.storeListReview?.idOfUsersDislikeStoreComment.append(preferenceHelper.UserId)
                        
                    }else {
                        if let index = self.storeListReview?.idOfUsersLikeStoreComment.firstIndex(where: { ($0 == preferenceHelper.UserId)})
                        {
                            self.storeListReview?.idOfUsersLikeStoreComment.remove(at: index)
                        }
                        if let index = self.storeListReview?.idOfUsersDislikeStoreComment.firstIndex(where: { ($0 == preferenceHelper.UserId)})
                        {
                            self.storeListReview?.idOfUsersDislikeStoreComment.remove(at: index)
                        }
                    }
                    self.setCellData(cellData: self.storeListReview!)
                }
            }
        }
    }
}
