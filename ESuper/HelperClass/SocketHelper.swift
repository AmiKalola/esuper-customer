//
//  SocketHelper.swift
//  Eber
//
//  Created by Elluminati on 13.03.19.
//  Copyright © 2019 Elluminati. All rights reserved.
//

import Foundation
import SocketIO

class SocketHelper:NSObject {

    let manager = SocketManager(socketURL: URL(string:WebService.BASE_URL)!, config: [.log(true), .compress])

    var socket:SocketIOClient? = nil
    let tripDetailNotify:String = "trip_detail_notify"
    let joinTrip:String = "join_trip"
    let locationEmit:String = "update_location"
    
    static let shared = SocketHelper()

    private override init() {
        super.init()
    }

    deinit {
        printE("\(self) \(#function)")
    }

    func connectSocket() {
        socket = manager.defaultSocket

        socket?.on(clientEvent: .connect) { (data, ack) in
            printE("Socket Connection Connected")
        }

        socket?.on(clientEvent: .error) { (data, ack) in
            print("Socket Connection Error = \(data.description) and ack = \(ack.description)")
            self.disConnectSocket()
        }

        socket?.on(clientEvent: .pong) { (data, ack) in
            printE("Socket Connection Pong \(data) Ack \(ack)")
        }
        socket?.connect()
    }

    func disConnectSocket() {
        if self.socket?.status == .connected {
            print("Socket Connection disconnected")
            self.socket?.disconnect()
        }
    }
    func updatePayment(paymentId : String){
        self.socket?.on(paymentId) {
            [weak self] (data, ack) in
            
            guard let self = self else {
                return
            }
            guard let response = data.first as? [String:Any] else {
                return
            }
            printE("Soket Response\(response)")
        }
    }
    func updatePaymentOrder(paymentId : String){
        self.socket?.on(paymentId) {
            [weak self] (data, ack) in
            guard let self = self else {
                return
            }
            guard let response = data.first as? [String:Any] else {
                return
            }
            printE("Soket Response\(response)")
        }
    }
}
