//
//  PreferenceHelper.swift
//  tableViewDemo
//
//  Created by Elluminati on 12/01/17.
//  Copyright © 2017 tag. All rights reserved.
//

import UIKit
class PreferenceHelper: NSObject{
    let ph = UserDefaults.standard
    static let preferenceHelper = PreferenceHelper()
    
    private override init() {}
    
    func clearAll() {
        ph.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        ph.synchronize()
    }
    var IsProfilePicRequired : Bool{
        get {
            return (ph.value(forKey : "is_profile_picture_required") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey: "is_profile_picture_required")
            ph.synchronize()
            
        }
    }
    var IsShowOptionalFieldInRegister : Bool{
        get {
            return (ph.value(forKey :  "is_show_optional_field_in_register") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_show_optional_field_in_register")
            ph.synchronize()
            
        }
    }
    var DeviceToken : String{
        get {
            return (ph.value(forKey :  "device_token") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "device_token")
            ph.synchronize()
            
        }
    }
    
    var IsLoginByEmail : Bool{
        get {
            return (ph.value(forKey :  "is_login_by_email") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_login_by_email")
            ph.synchronize()
            
        }
    }
    var IsLoginByPhone : Bool{
        get {
            return (ph.value(forKey :  "is_login_by_phone") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_login_by_phone")
            ph.synchronize()
            
        }
    }
    var IsReferralInCountry : Bool{
        get {
            return (ph.value(forKey :  "is_referral") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_referral")
            ph.synchronize()
            
        }
    }
    var IsReferralOn : Bool{
        get {
            return (ph.value(forKey :  "is_referral_on") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_referral_on")
            ph.synchronize()
            
        }
    }
    var IsRequiredForceUpdate : Bool{
        get {
            return (ph.value(forKey :  "is_required_force_update") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_required_force_update")
            ph.synchronize()
            
        }
    }
    var LatestAppVersion : String{
        get {
            return (ph.value(forKey :  "app_latest_version") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "app_latest_version")
            ph.synchronize()
            
        }
    }
    
    //MARK: - User Preference Keys
    var FirstName : String{
        get {
            return (ph.value(forKey :  "first_name") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "first_name")
            ph.synchronize()
            
        }
    }
    var LastName : String{
        get {
            return (ph.value(forKey :  "last_name") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "last_name")
            ph.synchronize()
            
        }
    }
    var UserId : String{
        get {
            return (ph.value(forKey :  "user_id") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "user_id")
            ph.synchronize()
            
        }
    }
    var PhoneNumberLength : Int{
        get {
            return (ph.value(forKey :  "phone_number_length") as? Int) ?? 10
        }
        set(value){
            ph.set(value, forKey:  "phone_number_length")
            ph.synchronize()
            
        }
    }
    var KEY_PHONE_NUMBER_MAX_LENGTH : String{
        get {
            return (ph.value(forKey :  "phone_number_max_length") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "phone_number_max_length")
            ph.synchronize()
            
        }
    }
    var KEY_PHONE_NUMBER_MIN_LENGTH : String{
        get {
            return (ph.value(forKey :  "phone_number_min_length") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "phone_number_min_length")
            ph.synchronize()
            
        }
    }
    var SessionToken : String{
        get {
            return (ph.value(forKey :  "session_token") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "session_token")
            ph.synchronize()
            
        }
    }
    var Bio : String{
        get {
            return (ph.value(forKey :  "bio") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "bio")
            ph.synchronize()
            
        }
    }
    var Address : String{
        get {
            return (ph.value(forKey :  "address") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "address")
            ph.synchronize()
            
        }
    }
    var ProfilePicUrl : String{
        get {
            return (ph.value(forKey :  "profile_pic") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "profile_pic")
            ph.synchronize()
            
        }
    }
    var PhoneNumber : String{
        get {
            return (ph.value(forKey :  "phone_number") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "phone_number")
            ph.synchronize()
            
        }
    }
    var PhoneCountryCode : String{
        get {
            return (ph.value(forKey :  "phone_country_code") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "phone_country_code")
            ph.synchronize()
            
        }
    }
    var CountryCode : String{
        get {
            return (ph.value(forKey :  "country_code") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "country_code")
            ph.synchronize()
            
        }
    }
    var CountryId : String{
        get {
            return (ph.value(forKey :  "country_id") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "country_id")
            ph.synchronize()
            
        }
    }
    var Email : String{
        get {
            return (ph.value(forKey :  "email") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "email")
            ph.synchronize()
            
        }
    }
    var TempEmail : String{
        get {
            return (ph.value(forKey :  "temp_email") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "temp_email")
            ph.synchronize()
            
        }
    }
    var MinMobileLength : Int{
        get {
            return (ph.value(forKey :  "minimum_phone_number_length") as? Int) ?? 8
        }
        set(value){
            ph.set(value, forKey:  "minimum_phone_number_length")
            ph.synchronize()
            
        }
    }
    var MaxMobileLength : Int{
        get {
            return (ph.value(forKey :  "maximum_phone_number_length") as? Int) ?? 12
        }
        set(value){
            ph.set(value, forKey:  "maximum_phone_number_length")
            ph.synchronize()
            
        }
    }
    var TaxiBookingTimeslot : Int{
        get {
            return (ph.value(forKey :  "taxi_booking_timeslot") as? Int) ?? 12
        }
        set(value){
            ph.set(value, forKey:  "taxi_booking_timeslot")
            ph.synchronize()
            
        }
    }
    var CourierBookingTimeslot : Int{
        get {
            return (ph.value(forKey :  "maximum_phone_number_length") as? Int) ?? 12
        }
        set(value){
            ph.set(value, forKey:  "maximum_phone_number_length")
            ph.synchronize()
            
        }
    }
    var TempPhoneNumber : String{
        get {
            return (ph.value(forKey :  "temp_phone_number") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "temp_phone_number")
            ph.synchronize()
            
        }
    }
    
    var CurrentAppMode : Int{
        get {
            return (ph.value(forKey :  "current_app_mode") as? Int) ?? 0
        }
        set(value){
            ph.set(value, forKey:  "current_app_mode")
            ph.synchronize()
            
        }
    }
    
    /*Check User Detail*/
    var IsUserApprove : Bool{
        get {
            return (ph.value(forKey :  "is_approve") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_approve")
            ph.synchronize()
            
        }
    }
    var IsUserDocumentUploaded : Bool{
        get {
            return (ph.value(forKey :  "is_user_document_uploaded") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_user_document_uploaded")
            ph.synchronize()
            
        }
    }
    var IsEmailVerified : Bool{
        get {
            return (ph.value(forKey :  "is_email_verified") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_email_verified")
            ph.synchronize()
            
        }
    }
    var IsPhoneNumberVerified : Bool{
        get {
            return (ph.value(forKey :  "is_phone_number_verified") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_phone_number_verified")
            ph.synchronize()
            
        }
    }
    
    /*check Admin Flags*/
    var IsAdminDocumentMandatory : Bool{
        get {
            return (ph.value(forKey :  "is_admin_document_mandatory") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_admin_document_mandatory")
            ph.synchronize()
            
        }
    }
    var IsPhoneNumberVerification : Bool{
        get {
            return (ph.value(forKey :  "is_phone_number_verification") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_phone_number_verification")
            ph.synchronize()
            
        }
    }
    var IsEmailVerification : Bool{
        get {
            return (ph.value(forKey :  "is_email_verification") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_email_verification")
            ph.synchronize()
            
        }
    }
    
    /**Referral code*/
    var ReferralCode : String{
        get {
            return (ph.value(forKey :  "referral_code") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "referral_code")
            ph.synchronize()
            
        }
    }
    var WalletAmount : String{
        get {
            return (ph.value(forKey :  "wallet") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "wallet")
            ph.synchronize()
            
        }
    }
    var WalletCurrencyCode : String{
        get {
            return (ph.value(forKey :  "wallet_currency_code") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "wallet_currency_code")
            ph.synchronize()
            
        }
    }
    var IsSocialLoginEnable : Bool{
        get {
            return (ph.value(forKey :  "is_login_by_social") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_login_by_social")
            ph.synchronize()
            
        }
    }
    var SocialId : String{
        get {
            return (ph.value(forKey :  "social_id") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "social_id")
            ph.synchronize()
            
        }
    }
    var Language : Int{
        get {
            return (ph.value(forKey :  "language") as? Int) ?? 0
        }
        set(value){
            ph.set(value, forKey:  "language")
            ph.synchronize()
            
        }
    }
    var KEY_IPHONE_ID : String{
        get {
            return (ph.value(forKey :  "iphone_id") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "iphone_id")
            ph.synchronize()
            
        }
    }
    var IsShowTutorial : Bool{
        get {
            return (ph.value(forKey :  "is_show_tutorial") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_show_tutorial")
            ph.synchronize()
            
        }
    }
    
    /**Admin Detail*/
    var AdminEmail : String{
        get {
            return (ph.value(forKey :  "admin_email") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "admin_email")
            ph.synchronize()
            
        }
    }
    var AdminContact : String{
        get {
            return (ph.value(forKey :  "admin_contact") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "admin_contact")
            ph.synchronize()
            
        }
    }
    var TermsAndCondition : String{
        get {
            return (ph.value(forKey :  "terms_and_condition") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "terms_and_condition")
            ph.synchronize()
            
        }
    }
    var PrivacyPolicy : String{
        get {
            return (ph.value(forKey :  "privacy_policy") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "privacy_policy")
            ph.synchronize()
            
        }
    }
    var RandomCartID : String{
        get {
            return (ph.value(forKey :  "random_cart_id") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "random_cart_id")
            ph.synchronize()
            
        }
    }
    var IsLoadStoreImage : Bool{
        get {
            return (ph.value(forKey :  "is_load_store_image") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_load_store_image")
            ph.synchronize()
            
        }
    }
    var IsLoadItemImage : Bool{
        get {
            return (ph.value(forKey :  "is_load_item_image") as? Bool) ?? true
        }
        set(value){
            ph.set(value, forKey:  "is_load_item_image")
            ph.synchronize()
            
        }
    }
    var SelectedLanguage : Int{
        get {
            return (ph.value(forKey :  "language_Selcted") as? Int) ?? 0
        }
        set(value){
            ph.set(value, forKey:  "language_Selcted")
            ph.synchronize()
            
        }
    }
    var SelectedLanguageCode : String{
        get {
            return (ph.value(forKey :  "languageCode_Selcted") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "languageCode_Selcted")
            ph.synchronize()
            
        }
    }
    var ChatName : String{
        get {
            return (ph.value(forKey :  "chat_name") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "chat_name")
            ph.synchronize()
            
        }
    }
    var SigninWithAppleUserName : String{
        get {
            return (ph.value(forKey :  "apple_user_name") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "apple_user_name")
            ph.synchronize()
            
        }
    }
    var SigninWithAppleEmail : String{
        get {
            return (ph.value(forKey :  "apple_email") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "apple_email")
            ph.synchronize()
            
        }
    }
    var AuthToken : String{
        get {
            return (ph.value(forKey :  "user_token") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "user_token")
            ph.synchronize()
            
        }
    }
    var UserPanelUrl : String{
        get {
            return (ph.value(forKey :  "term_privacy_base_url") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "term_privacy_base_url")
            ph.synchronize()
            
        }
    }
    var IsAllowBringChange : Bool{
        get {
            return (ph.value(forKey :  "is_allow_bring_change") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_allow_bring_change")
            ph.synchronize()
            
        }
    }
    var IsQRUser : Bool{
        get {
            return (ph.value(forKey :  "is_qr_user_register") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_qr_user_register")
            ph.synchronize()
            
        }
    }
    var IsTwillowMaskEnable : Bool{
        get {
            return (ph.value(forKey :  "is_enable_twilio_call_masking") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_enable_twilio_call_masking")
            ph.synchronize()
            
        }
    }
    var MaxCourierStop : Int{
        get {
            return (ph.value(forKey :  "max_courier_stop_limit") as? Int) ?? 0
        }
        set(value){
            ph.set(value, forKey:  "max_courier_stop_limit")
            ph.synchronize()
            
        }
    }
    var MaxTaxiStop : Int{
        get {
            return (ph.value(forKey :  "max_courier_stop_limit") as? Int) ?? 0
        }
        set(value){
            ph.set(value, forKey:  "max_courier_stop_limit")
            ph.synchronize()
            
        }
    }
    var DarkMode : Bool{
        get {
            return (ph.value(forKey :  "APP_DARK_MODE_CONFIG") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "APP_DARK_MODE_CONFIG")
            ph.synchronize()
            
        }
    }
    
    var PaypalClientId : String{
        get {
            return (ph.value(forKey :  "paypal_client_id") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "paypal_client_id")
            ph.synchronize()
            
        }
    }
    var PaypalEnvironment : String{
        get {
            return (ph.value(forKey :  "paypal_environment") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "paypal_environment")
            ph.synchronize()
            
        }
    }
    
    var IsSendMoneyForUser : Bool{
        get {
            return (ph.value(forKey :  "is_send_money_for_user") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_send_money_for_user")
            ph.synchronize()
            
        }
    }
    var IsUseCaptcha : Bool{
        get {
            return (ph.value(forKey :  "is_use_captcha") as? Bool) ?? false
        }
        set(value){
            ph.set(value, forKey:  "is_use_captcha")
            ph.synchronize()
            
        }
    }
    var CaptchaSiteKeyForIos : String{
        get {
            return (ph.value(forKey :  "captcha_site_key_for_ios") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "captcha_site_key_for_ios")
            ph.synchronize()
            
        }
    }
    var FireBaseKey : String{
        get {
            return (ph.value(forKey :  "KEY_FIREBASE") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "KEY_FIREBASE")
            ph.synchronize()
            
        }
    }
    var CustomerAppGoogleDirectionMatrixKey : String{
        get {
            return (ph.value(forKey :  "ios_customer_app_google_direction_matrix_key") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "ios_customer_app_google_direction_matrix_key")
            ph.synchronize()
            
        }
    }
    var CustomerAppGoogleDistanceMatrixKey : String{
        get {
            return (ph.value(forKey :  "ios_customer_app_google_distance_matrix_key") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "ios_customer_app_google_distance_matrix_key")
            ph.synchronize()
            
        }
    }
    var CustomerAppGoogleGeocodingKey : String{
        get {
            return (ph.value(forKey :  "ios_customer_app_google_geocoding_key") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "ios_customer_app_google_geocoding_key")
            ph.synchronize()
            
        }
    }
    var CustomerAppGoogleMapKey : String{
        get {
            return (ph.value(forKey :  "ios_customer_app_google_map_key") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "ios_customer_app_google_map_key")
            ph.synchronize()
            
        }
    }
    var CustomerAppGooglePlacesAutocompleteKey : String{
        get {
            return (ph.value(forKey :  "ios_customer_app_google_places_autocomplete_key") as? String) ?? ""
        }
        set(value){
            ph.set(value, forKey:  "ios_customer_app_google_places_autocomplete_key")
            ph.synchronize()
            
        }
    }
}
/*
class PreferenceHelper: NSObject {
    private let KEY_DEVICE_TOKEN = "device_token"

    //MARK: - Setting Preference Keys
    private let KEY_IS_PROFILE_PICTURE_REQUIRED = "is_profile_picture_required"
    private let KEY_IS_SHOW_OPTIONAL_FIELD_IN_REGISTER = "is_show_optional_field_in_register"
    private let KEY_IS_LOGIN_BY_EMAIL = "is_login_by_email"
    private let KEY_IS_LOGIN_BY_PHONE = "is_login_by_phone"
    private let KEY_IS_REFERRAL = "is_referral"
    private let KEY_IS_REFERRAL_ON = "is_referral_on"
    private let KEY_IS_REQUIRED_FORCE_UPDATE = "is_required_force_update"
    private let KEY_APP_VERSION = "app_latest_version"

    //MARK: - User Preference Keys
    private let KEY_FIRST_NAME = "first_name"
    private let KEY_LAST_NAME = "last_name"
    private let KEY_USER_ID = "user_id"
    private let KEY_PHONE_NUMBER_LENGTH = "phone_number_length"
    private let KEY_PHONE_NUMBER_MAX_LENGTH = "phone_number_max_length"
    private let KEY_PHONE_NUMBER_MIN_LENGTH = "phone_number_min_length"
    private let KEY_SESSION_TOKEN = "session_token"
    private let KEY_BIO = "bio"
    private let KEY_ADDRESS = "address"
    private let KEY_PROFILE_PIC = "profile_pic"
    private let KEY_PHONE_NUMBER = "phone_number"
    private let KEY_PHONE_COUNTRY_CODE = "phone_country_code"
    private let KEY_COUNTRY_CODE = "country_code"
    private let KEY_COUNTRY_ID = "country_id"
    private let KEY_EMAIL = "email"
    private let KEY_TEMP_EMAIL = "temp_email"
    private let KEY_MIN_MOBILE_LENGTH = "minimum_phone_number_length"
    private let KEY_MAX_MOBILE_LENGTH = "maximum_phone_number_length"
    private let KEY_TEMP_PHONE_NUMBER = "temp_phone_number"
    
    static let KEY_CURRENT_APP_MODE = "current_app_mode"

    /*Check User Detail*/
    private let KEY_IS_APPROVE = "is_approve"
    private let KEY_IS_USER_DOCUMENT_UPLOADED = "is_user_document_uploaded"
    private let KEY_IS_EMAIL_VERIFIED = "is_email_verified"
    private let KEY_IS_PHONE_NUMBER_VERIFIED = "is_phone_number_verified"

    /*check Admin Flags*/
    private let KEY_IS_ADMIN_DOCUMENT_MANDATORY = "is_admin_document_mandatory"
    private let KEY_IS_PHONE_NUMBER_VERIFICATION = "is_phone_number_verification"
    private let KEY_IS_EMAIL_VERIFICATION = "is_email_verification"

    /**Referral code*/
    private let KEY_REFERRAL_CODE = "referral_code"
    private let KEY_WALLET = "wallet"
    private let KEY_WALLET_CURRENCY_CODE = "wallet_currency_code"
    private let KEY_IS_SOCIAL_LOGIN_ENABLE = "is_login_by_social"
    private let KEY_SOCIAL_ID = "social_id"
    private let KEY_LANGUAGE = "language"
    private let KEY_IPHONE_ID = "iphone_id"
    private let KEY_IS_SHOW_TUTORIAL = "is_show_tutorial"

    /**Admin Detail*/
    private let KEY_ADMIN_EMAIL = "admin_email"
    private let KEY_ADMIN_CONTACT = "admin_contact"
    private let KEY_TERMS_AND_CONDITION = "terms_and_condition"
    private let KEY_PRIVACY_POLICY = "privacy_policy"
    private let KEY_RANDOM_CART_ID = "random_cart_id"
    private let KEY_IS_LOAD_STORE_IMAGE = "is_load_store_image"
    private let KEY_IS_LOAD_ITEM_IMAGE = "is_load_item_image"
    private let KEY_LANGUAGE_SELECTED = "language_Selcted"
    private let KEY_LANGUAGECODE_SELECTED = "languageCode_Selcted"
    private let KEY_CHAT_NAME = "chat_name"
    private let KEY_APPLE_USER_NAME = "apple_user_name"
    private let KEY_APPLE_EMAIL = "apple_email"
    private let KEY_USER_TOKEN = "user_token"
    private let KEY_TERMS_PRIVACY_BASE_URL = "term_privacy_base_url"
    private let KEY_IS_ALLOW_BRING_CHANGE = "is_allow_bring_change"
    private let KEY_IS_QR_USER_REGISTER = "is_qr_user_register"
    private let IS_ENABLE_TWILIO_CALL_MASKING = "is_enable_twilio_call_masking"
    private let MAX_COURIER_STOP_LIMIT = "max_courier_stop_limit"
    private let APP_DARK_MODE_CONFIG = "APP_DARK_MODE_CONFIG"

    private let paypal_client_id = "paypal_client_id"
    private let paypal_environment = "paypal_environment"

    private let is_send_money_for_user = "is_send_money_for_user"
    private let is_use_captcha = "is_use_captcha"
    private let captcha_site_key_for_ios = "captcha_site_key_for_ios"
    private let KEY_FIREBASE = "KEY_FIREBASE"
    private let ios_customer_app_google_direction_matrix_key = "ios_customer_app_google_direction_matrix_key"
    private let ios_customer_app_google_distance_matrix_key = "ios_customer_app_google_distance_matrix_key"
    private let ios_customer_app_google_geocoding_key = "ios_customer_app_google_geocoding_key"
    private let ios_customer_app_google_map_key = "ios_customer_app_google_map_key"
    private let ios_customer_app_google_places_autocomplete_key = "ios_customer_app_google_places_autocomplete_key"

    
    let ph = UserDefaults.standard
    static let preferenceHelper = PreferenceHelper()

    private override init() {}

    func setAuthToken = (_ token:String) {
        ph.set(token, forKey: KEY_USER_TOKEN)
        ph.synchronize()
    }
    func AuthToken -> String {
        return (ph.value(forKey: KEY_USER_TOKEN) as? String) ?? ""
    }

    func setRandomCartID = (_ id:String) {
        ph.set(id, forKey: KEY_RANDOM_CART_ID)
        ph.synchronize()
    }
    func RandomCartID -> String {
        return (ph.value(forKey: KEY_RANDOM_CART_ID) as? String) ?? ""
    }

    func setAdminEmail = (_ email:String) {
        ph.set(email, forKey: KEY_ADMIN_EMAIL)
        ph.synchronize()
    }
    func AdminEmail -> String {
        return (ph.value(forKey: KEY_ADMIN_EMAIL) as? String) ?? ""
    }

    func setAdminContact(_ contact:String) {
        ph.set(contact, forKey: KEY_ADMIN_CONTACT)
        ph.synchronize()
    }
    func AdminContact -> String {
        return (ph.value(forKey: KEY_ADMIN_CONTACT) as? String) ?? ""
    }

    func setTermsAndCondition(_ url:String) {
        ph.set(url, forKey: KEY_TERMS_AND_CONDITION)
        ph.synchronize()
    }
    func getTermsAndCondition() -> String {
        return (ph.value(forKey: KEY_TERMS_AND_CONDITION) as? String) ?? ""
    }

    func setPrivacyPolicy(_ url:String) {
        ph.set(url, forKey: KEY_PRIVACY_POLICY)
        ph.synchronize()
    }
    func getPrivacyPolicy() -> String {
        return (ph.value(forKey: KEY_PRIVACY_POLICY) as? String) ?? ""
    }

    func setLanguage = (_ index:Int) {
        ph.set(index, forKey: KEY_LANGUAGE)
        ph.synchronize()
    }

    func Language -> (Int) {
        return (ph.value(forKey: KEY_LANGUAGE) as? Int) ?? 0
    }

    //Changed
    func setSelectedLanguage = (_ index:Int) {
        ph.set(index, forKey: KEY_LANGUAGE_SELECTED)
        ph.synchronize()
    }
    func SelectedLanguage -> (Int) {
        return (ph.value(forKey: KEY_LANGUAGE_SELECTED) as? Int) ?? 0
    }

    func setSelectedLanguageCode(str:String) {
        ph.set(str, forKey: KEY_LANGUAGECODE_SELECTED)
        ph.synchronize()
    }
    func SelectedLanguageCode -> (String) {
        return (ph.value(forKey: KEY_LANGUAGECODE_SELECTED) as? String) ?? "en"
    }

    func setChatName = (_ name:String) {
        ph.set(name, forKey: KEY_CHAT_NAME);
        ph.synchronize();
    }
    func getChatName() -> String {
        return (ph.value(forKey: KEY_CHAT_NAME) as? String) ?? ""
    }

    func setIsLoadStoreImage = (_ isLoadStoreImage:Bool) {
        ph.set(isLoadStoreImage, forKey: KEY_IS_LOAD_STORE_IMAGE)
        ph.synchronize()
    }
    func IsLoadStoreImage -> Bool {
        return (ph.value(forKey: KEY_IS_LOAD_STORE_IMAGE) as? Bool) ?? true
    }

    func setIsLoadItemImage = (_ isLoadItemImage:Bool) {
        ph.set(isLoadItemImage, forKey: KEY_IS_LOAD_ITEM_IMAGE)
        ph.synchronize()
    }
    func IsLoadItemImage -> Bool {
        return (ph.value(forKey: KEY_IS_LOAD_ITEM_IMAGE) as? Bool) ?? true
    }

    func setIsSocialLoginEnable = (_ isSocialLoginEnable:Bool) {
        ph.set(isSocialLoginEnable, forKey: KEY_IS_SOCIAL_LOGIN_ENABLE)
        ph.synchronize()
    }
    func IsSocialLoginEnable -> Bool {
        return (ph.value(forKey: KEY_IS_SOCIAL_LOGIN_ENABLE) as? Bool) ?? false
    }

    func setIsShowTutorial = (_ isShowTutorial:Bool) {
        ph.set(isShowTutorial, forKey: KEY_IS_SHOW_TUTORIAL)
        ph.synchronize()
    }
    func IsShowTutorial -> Bool {
        return (ph.value(forKey: KEY_IS_SHOW_TUTORIAL) as? Bool) ?? true
    }

    func setSocialId = (_ id:String) {
        ph.set(id, forKey: KEY_SOCIAL_ID)
        ph.synchronize()
    }
    func SocialId -> String {
        return (ph.value(forKey: KEY_SOCIAL_ID) as? String) ?? ""
    }

    //MARK: - Wallet Setting Getter Setters
    func setWalletAmount(_ walletAmount:String) {
        ph.set(walletAmount, forKey: KEY_WALLET)
        ph.synchronize()
    }
    func WalletAmount -> String {
        return (ph.value(forKey: KEY_WALLET) as? String) ?? "0.00"
    }

    func setWalletCurrencyCode = (_ wallet:String) {
        ph.set(wallet, forKey: KEY_WALLET_CURRENCY_CODE)
        ph.synchronize()
    }
    func WalletCurrencyCode -> String {
        return (ph.value(forKey: KEY_WALLET_CURRENCY_CODE) as? String) ?? "INR"
    }

    func setReferralCode = (_ wallet:String) {
        ph.set(wallet, forKey: KEY_REFERRAL_CODE)
        ph.synchronize()
    }
    func ReferralCode -> String {
        return (ph.value(forKey: KEY_REFERRAL_CODE) as? String) ?? ""
    }

    //MARK: - Preference Setting Getter Setters
    func setIsEmailVerification = (_ isEmailVerification:Bool) {
        ph.set(isEmailVerification, forKey: KEY_IS_EMAIL_VERIFICATION)
        ph.synchronize()
    }
    func IsEmailVerification -> Bool {
        return (ph.value(forKey: KEY_IS_EMAIL_VERIFICATION) as? Bool) ?? false
    }

    func setIsPhoneNumberVerification(_ isPhoneNumberVerification:Bool) {
        ph.set(isPhoneNumberVerification, forKey: KEY_IS_PHONE_NUMBER_VERIFICATION)
        ph.synchronize()
    }
    func IsPhoneNumberVerification -> Bool {
        return (ph.value(forKey: KEY_IS_PHONE_NUMBER_VERIFICATION) as? Bool) ?? false
    }

    func setIsProfilePicRequired(_ isProPicRequired:Bool) {
        ph.set(isProPicRequired, forKey: KEY_IS_PROFILE_PICTURE_REQUIRED)
        ph.synchronize()
    }
    func getIsProfilePicRequired() -> Bool {
        return (ph.value(forKey: KEY_IS_PROFILE_PICTURE_REQUIRED) as? Bool) ?? false
    }

    func setIsReferralInCountry(_ isReferral:Bool) {
        ph.set(isReferral, forKey: KEY_IS_REFERRAL)
        ph.synchronize()
    }
    func getIsReferralInCountry() -> Bool {
        return (ph.value(forKey: KEY_IS_REFERRAL) as? Bool) ?? false
    }

    func setIsReferralOn(_ isReferral:Bool) {
        ph.set(isReferral, forKey: KEY_IS_REFERRAL_ON)
        ph.synchronize()
    }
    func IsReferralOn -> Bool {
        return (ph.value(forKey: KEY_IS_REFERRAL_ON) as? Bool) ?? false
    }

    func setIsRequiredForceUpdate(_ fUpdate:Bool) {
        ph.set(fUpdate, forKey: KEY_IS_REQUIRED_FORCE_UPDATE)
        ph.synchronize()
    }
    func IsRequiredForceUpdate -> Bool {
        return (ph.value(forKey: KEY_IS_REQUIRED_FORCE_UPDATE) as? Bool) ?? false
    }

    func setIsShowOptionalFieldInRegister(_ hide:Bool) {
        ph.set(hide, forKey: KEY_IS_SHOW_OPTIONAL_FIELD_IN_REGISTER)
        ph.synchronize()
    }
    func IsShowOptionalFieldInRegister -> Bool {
        return ph.value(forKey: KEY_IS_SHOW_OPTIONAL_FIELD_IN_REGISTER) as? Bool ?? true
    }

    //MARK: - Preference User Device Token
 device_token
    func setDeviceToken(_ token:String) {
        ph.set(token, forKey: KEY_DEVICE_TOKEN)
        ph.synchronize()
    }
    func DeviceToken -> String {
        return (ph.value(forKey: KEY_DEVICE_TOKEN) as? String) ?? ""
    }

    //MARK: - Preference User Getter Setters
    func setFirstName(_ fname:String) {
        ph.set(fname, forKey: KEY_FIRST_NAME)
        ph.synchronize()
    }
    func FirstName -> String {
        return (ph.value(forKey: KEY_FIRST_NAME) as? String) ?? ""
    }

    func setLastName(_ lname:String) {
        ph.set(lname, forKey: KEY_LAST_NAME)
        ph.synchronize()
    }
    func LastName -> String {
        return (ph.value(forKey: KEY_LAST_NAME) as? String) ?? ""
    }

    func setUserId(_ userId:String) {
        ph.set(userId, forKey: KEY_USER_ID)
        ph.synchronize()
    }
    func UserId -> String {
        return (ph.value(forKey: KEY_USER_ID) as? String) ?? ""
    }

    func setSessionToken(_ sessionToken:String) {
        ph.set(sessionToken, forKey: KEY_SESSION_TOKEN)
        ph.synchronize()
    }
    func SessionToken -> String {
        return (ph.value(forKey: KEY_SESSION_TOKEN) as? String) ?? ""
    }

    func setBio(_ bio:String) {
        ph.set(bio, forKey: KEY_BIO)
        ph.synchronize()
    }
    func getBio() -> String {
        return (ph.value(forKey: KEY_BIO) as? String) ?? ""
    }
    
    func setAddress(_ address:String) {
        ph.set(address, forKey: KEY_ADDRESS)
        ph.synchronize()
    }
    func Address -> String {
        return (ph.value(forKey: KEY_ADDRESS) as? String) ?? ""
    }

    func setProfilePicUrl(_ profileUrl:String) {
        ph.set(profileUrl, forKey: KEY_PROFILE_PIC)
        ph.synchronize()
    }
    func ProfilePicUrl -> String {
        return (ph.value(forKey: KEY_PROFILE_PIC) as? String) ?? ""
    }

    func setPhoneNumber(_ phoneNumber:String) {
        ph.set(phoneNumber, forKey: KEY_PHONE_NUMBER)
        ph.synchronize()
    }
    func PhoneNumber -> String {
        return (ph.value(forKey: KEY_PHONE_NUMBER) as? String) ?? ""
    }

    func setPhoneCountryCode(_ coutryCode:String) {
        ph.set(coutryCode, forKey: KEY_PHONE_COUNTRY_CODE)
        ph.synchronize()
    }
    func PhoneCountryCode -> String {
        return (ph.value(forKey: KEY_PHONE_COUNTRY_CODE) as? String) ?? ""
    }

    func setCountryCode(_ coutryCode:String) {
        ph.set(coutryCode, forKey: KEY_COUNTRY_CODE)
        ph.synchronize()
    }
    func CountryCode -> String {
        return (ph.value(forKey: KEY_COUNTRY_CODE) as? String) ?? (Locale.current.regionCode ?? "IN")
    }

    func setEmail(_ email:String) {
        ph.set(email, forKey: KEY_EMAIL)
        ph.synchronize()
    }
    func Email -> String {
        return (ph.value(forKey: KEY_EMAIL) as? String) ?? ""
    }

    func setLatestAppVersion(_ version:String) {
        ph.set(version, forKey: KEY_APP_VERSION)
        ph.synchronize()
    }
    func LatestAppVersion -> String {
        return (ph.value(forKey: KEY_APP_VERSION) as? String) ?? ""
    }

    func setPhoneNumberLength(_ length:Int) {
        ph.set(length, forKey: KEY_PHONE_NUMBER_LENGTH)
        ph.synchronize()
    }
    func getPhoneNumberLength() -> Int {
        return (ph.value(forKey: KEY_PHONE_NUMBER_LENGTH) as? Int) ?? 10
    }

    func setIsLoginByEmail(_ hide:Bool) {
        ph.set(hide, forKey: KEY_IS_LOGIN_BY_EMAIL)
        ph.synchronize()
    }
    func IsLoginByEmail -> Bool {
        return ph.value(forKey: KEY_IS_LOGIN_BY_EMAIL) as? Bool ?? false
    }

    func setIsLoginByPhone(_ hide:Bool) {
        ph.set(hide, forKey: KEY_IS_LOGIN_BY_PHONE)
        ph.synchronize()
    }
    func IsLoginByPhone -> Bool {
        return ph.value(forKey: KEY_IS_LOGIN_BY_PHONE) as? Bool ?? false
    }

    func setIsUserApprove(_ isApprove: Bool) {
        ph.set(isApprove, forKey: KEY_IS_APPROVE)
        ph.synchronize()
    }
    func IsUserApprove -> Bool {
        return (ph.value(forKey: KEY_IS_APPROVE) as? Bool) ?? true
    }

    func setIsAdminDocumentMandatory(_ isAdminDocumentMandatory: Bool) {
        ph.set(isAdminDocumentMandatory, forKey: KEY_IS_ADMIN_DOCUMENT_MANDATORY)
        ph.synchronize()
    }
    func IsAdminDocumentMandatory -> Bool {
        return (ph.value(forKey: KEY_IS_ADMIN_DOCUMENT_MANDATORY) as? Bool) ?? false
    }

    func setIsEmailVerified(_ isEmailVerified:Bool) {
        ph.set(isEmailVerified, forKey: KEY_IS_EMAIL_VERIFIED)
        ph.synchronize()
    }
    func IsEmailVerified -> Bool {
        return (ph.value(forKey: KEY_IS_EMAIL_VERIFIED) as?  Bool) ?? false
    }

    func setIsPhoneNumberVerified(_ isPhoneNumberVerified:Bool) {
        ph.set(isPhoneNumberVerified, forKey: KEY_IS_PHONE_NUMBER_VERIFIED)
        ph.synchronize()
    }
    func IsPhoneNumberVerified -> Bool {
        return (ph.value(forKey: KEY_IS_PHONE_NUMBER_VERIFIED) as? Bool) ?? false
    }

    func setIsUserDocumentUploaded(_ isProviderDocumentUploaded:Bool) {
        ph.set(isProviderDocumentUploaded, forKey: KEY_IS_USER_DOCUMENT_UPLOADED)
        ph.synchronize()
    }
    func IsUserDocumentUploaded -> Bool {
        return (ph.value(forKey: KEY_IS_USER_DOCUMENT_UPLOADED) as? Bool) ?? false
    }

    func setTempPhoneNumber(_ phoneNumber:String) {
        ph.set(phoneNumber, forKey: KEY_TEMP_PHONE_NUMBER)
        ph.synchronize()
    }
    func TempPhoneNumber -> String {
        return (ph.value(forKey: KEY_TEMP_PHONE_NUMBER) as? String) ?? ""
    }

    func setTempEmail(_ email:String) {
        ph.set(email, forKey: KEY_TEMP_EMAIL)
        ph.synchronize()
    }
    func TempEmail -> String {
        return (ph.value(forKey: KEY_TEMP_EMAIL) as? String) ?? ""
    }
    
    func setMinMobileLength(_ int:Int) {
        ph.set(int, forKey: KEY_MIN_MOBILE_LENGTH)
        ph.synchronize()
    }
    
    func MinMobileLength -> Int {
        return (ph.value(forKey: KEY_MIN_MOBILE_LENGTH) as? Int) ?? 7
    }
    
    func setMaxMobileLength(_ int:Int) {
        ph.set(int, forKey: KEY_MAX_MOBILE_LENGTH)
        ph.synchronize()
    }
    
    func MaxMobileLength -> Int {
        return (ph.value(forKey: KEY_MAX_MOBILE_LENGTH) as? Int) ?? 12
    }

    func setCountryId(_ countryId:String) {
        ph.set(countryId, forKey: KEY_COUNTRY_ID)
        ph.synchronize()
    }
    func getCountryId() -> String {
        return (ph.value(forKey: KEY_COUNTRY_ID) as? String) ?? ""
    }

    func setSigninWithAppleUserName(_ username:String) {
        ph.set(username, forKey: KEY_APPLE_USER_NAME)
        ph.synchronize()
    }
    func SigninWithAppleUserName -> String {
        return (ph.value(forKey: KEY_APPLE_USER_NAME) as? String) ?? ""
    }

    func setSigninWithAppleEmail(_ email:String) {
        ph.set(email, forKey: KEY_APPLE_EMAIL)
        ph.synchronize()
    }
    func SigninWithAppleEmail -> String {
        return (ph.value(forKey: KEY_APPLE_EMAIL) as? String) ?? ""
    }
    
    func CurrentAppMode -> Int {
        return (ph.value(forKey: PreferenceHelper.KEY_CURRENT_APP_MODE) as? Int) ?? 0
    }
    
    func setCurrentAppMode(_ length:Int) {
        ph.set(length, forKey: PreferenceHelper.KEY_CURRENT_APP_MODE);
        ph.synchronize();
    }
    
    func UserPanelUrl -> String {
        return (ph.value(forKey: KEY_TERMS_PRIVACY_BASE_URL) as? String) ?? ""
    }
    
    func setUserPanelUrl(_ str:String) {
        ph.set(str, forKey: KEY_TERMS_PRIVACY_BASE_URL);
        ph.synchronize();
    }
    
    func IsAllowBringChange -> Bool {
        return (ph.value(forKey: KEY_IS_ALLOW_BRING_CHANGE) as? Bool) ?? false
    }
    
    func setIsAllowBringChange(_ bool:Bool) {
        ph.set(bool, forKey: KEY_IS_ALLOW_BRING_CHANGE);
        ph.synchronize();
    }
    
    func setIsQRUser(_ bool:Bool) {
        ph.set(bool, forKey: KEY_IS_QR_USER_REGISTER)
        ph.synchronize()
    }
    func IsQRUser -> Bool {
        return (ph.value(forKey: KEY_IS_QR_USER_REGISTER) as? Bool) ?? false
    }
    
    func setIsTwillowMaskEnable(_ bool:Bool) {
        ph.set(bool, forKey: IS_ENABLE_TWILIO_CALL_MASKING)
        ph.synchronize()
    }
    func IsTwillowMaskEnable -> Bool {
        return (ph.value(forKey: IS_ENABLE_TWILIO_CALL_MASKING) as? Bool) ?? false
    }
    
    func setMaxCourierStop(_ int:Int) {
        ph.set(int, forKey: MAX_COURIER_STOP_LIMIT)
        ph.synchronize()
    }
    func MaxCourierStop -> Int {
        return (ph.value(forKey: MAX_COURIER_STOP_LIMIT) as? Int) ?? 0
    }
    
    func setDarkMode(_ bool:Bool) {
        ph.set(bool, forKey: APP_DARK_MODE_CONFIG)
        ph.synchronize()
    }
    
    func DarkMode -> Bool {
        return (ph.value(forKey: APP_DARK_MODE_CONFIG) as? Bool) ?? false
    }

    func clearAll() {
        ph.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        ph.synchronize()
    }
    
    func setPaypalClientId(_ id:String)
    {
        ph.set(id, forKey: paypal_client_id);
        ph.synchronize();
    }
    
    func PaypalClientId -> String
    {
        return (ph.value(forKey: paypal_client_id) as? String) ?? ""
    }
    
    func setPaypalEnvironment(_ string:String)
    {
        ph.set(string, forKey: paypal_environment);
        ph.synchronize();
    }
    
    func PaypalEnvironment -> String
    {
        return (ph.value(forKey: paypal_environment) as? String) ?? ""
    }

    
    func setIsSendMoneyForUser(_ string:Bool)
    {
        ph.set(string, forKey: is_send_money_for_user);
        ph.synchronize();
    }
    
    func IsSendMoneyForUser -> Bool
    {
        return (ph.value(forKey: is_send_money_for_user) as? Bool) ?? false
    }
    func setIsUseCaptcha(_ bool:Bool) {
        ph.set(bool, forKey: is_use_captcha)
        ph.synchronize()
    }
    
    func IsUseCaptcha -> Bool {
        return (ph.value(forKey: is_use_captcha) as? Bool) ?? false
    }
    func setCaptchaSiteKeyForIos(_ string:String)
    {
        ph.set(string, forKey: captcha_site_key_for_ios);
        ph.synchronize();
    }
    
    func CaptchaSiteKeyForIos -> String
    {
        return (ph.value(forKey: captcha_site_key_for_ios) as? String) ?? ""
    }
    func setFireBaseKey(_ string:String)
    {
        ph.set(string, forKey: KEY_FIREBASE);
        ph.synchronize();
    }
    func FireBaseKey -> String
    {
        return (ph.value(forKey: KEY_FIREBASE) as? String) ?? ""
    }
    func setCustomerAppGoogleDirectionMatrixKey (_ string : String){
        ph.set(string, forKey: ios_customer_app_google_direction_matrix_key);
        ph.synchronize();
    }
    func CustomerAppGoogleDirectionMatrixKey -> String
    {
        return (ph.value(forKey: ios_customer_app_google_direction_matrix_key) as? String) ?? ""
    }
    func setCustomerAppGoogleDistanceMatrixKey (_ string : String){
        ph.set(string, forKey: ios_customer_app_google_distance_matrix_key);
        ph.synchronize();
    }
    func CustomerAppGoogleDistanceMatrixKey -> String
    {
        return (ph.value(forKey: ios_customer_app_google_distance_matrix_key) as? String) ?? ""
    }
    func setCustomerAppGoogleGeocodingKey (_ string : String){
        ph.set(string, forKey: ios_customer_app_google_geocoding_key);
        ph.synchronize();
    }
    func CustomerAppGoogleGeocodingKey -> String
    {
        return (ph.value(forKey: ios_customer_app_google_geocoding_key) as? String) ?? ""
    }
    func setCustomerAppGoogleMapKey (_ string : String){
        ph.set(string, forKey: ios_customer_app_google_map_key);
        ph.synchronize();
    }
    func CustomerAppGoogleMapKey -> String
    {
        return (ph.value(forKey: ios_customer_app_google_map_key) as? String) ?? ""
    }
    func setCustomerAppGooglePlacesAutocompleteKey (_ string : String){
        ph.set(string, forKey: ios_customer_app_google_places_autocomplete_key);
        ph.synchronize();
    }
    func CustomerAppGooglePlacesAutocompleteKey -> String
    {
        return (ph.value(forKey: ios_customer_app_google_places_autocomplete_key) as? String) ?? ""
    }
}
*/
