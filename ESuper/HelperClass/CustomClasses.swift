//
//  CustomClasses.swift
//  Decagon
//
//  Created by MacPro3 on 29/04/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit
import Foundation

@IBDesignable
class LabelDefault: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        config()
    }
    
    private func config() {
        self.font = FontHelper.textRegular()
        self.backgroundColor = .clear
        self.textColor = UIColor.themeTitleColor
    }
}

class ViewDefault: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        config()
    }
    
    private func config() {
        self.backgroundColor = UIColor.themeViewBackgroundColor
    }
}

class ImageViewDefault: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        config()
    }
    
    private func config() {
        self.image = UIImage(named: "placeholder")
    }
}

class ButtonDefault: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        config()
    }
    
    private func config() {
        self.setTitleColor(UIColor.themeTitleColor, for: .normal)
        self.backgroundColor = .clear
        self.titleLabel?.font = FontHelper.textRegular()
    }
}

class TextFieldDefault: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        config()
    }
    
    private func config() {
        self.textColor = UIColor.themeTitleColor
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.font = FontHelper.textRegular()
        
        let padding: CGFloat = 8
        if UIApplication.isRTL() {
            self.setRightPaddingPoints(padding)
        } else {
            self.setLeftPaddingPoints(padding)
        }
        
    }
}

@IBDesignable
class ShadowWithCornerView: UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat = 25.0 {
        didSet {
            setShadow()
        }
    }
    
    @IBInspectable
    var fillColor: UIColor = UIColor.themeViewBackgroundColor {
        didSet {
            setShadow()
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor = UIColor.gray {
        didSet {
            setShadow()
        }
    }
    
    @IBInspectable
    var shadowOffSetX: CGFloat = 0 {
        didSet {
            setShadow()
        }
    }
    
    @IBInspectable
    var shadowOffSetY: CGFloat = -2 {
        didSet {
            setShadow()
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float = 0.3 {
        didSet {
            setShadow()
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat = 6 {
        didSet {
            setShadow()
        }
    }
    
    var shadowLayer: CAShapeLayer!
     
    override func layoutSubviews() {
        super.layoutSubviews()

        if shadowLayer != nil {
            shadowLayer.removeFromSuperlayer()
        }
        shadowLayer = CAShapeLayer()
        setShadowInMainThred()
        layer.insertSublayer(shadowLayer, at: 0)
    }
    
    func setShadowInMainThred(byCorner: UIRectCorner = [.topLeft, .topRight]) {
        
        if shadowLayer != nil {
            shadowLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: byCorner, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
            shadowLayer.fillColor = fillColor.cgColor

            shadowLayer.shadowColor = shadowColor.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: shadowOffSetX, height: shadowOffSetY)
            shadowLayer.shadowOpacity = shadowOpacity
            shadowLayer.shadowRadius = shadowRadius
            self.backgroundColor = .clear
        }
        //layoutSubviews()
    }
}

