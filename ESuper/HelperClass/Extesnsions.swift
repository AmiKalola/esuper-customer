//
//  Extesnsions.swift
//  ESuper
//
//  Created by MacPro3 on 28/06/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func toCall()  {
        if let url = URL(string: "tel://\(self)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            Utility.showToast(message: "Unable to call")
        }
    }
}

extension UIApplication {
    static var topSafeAreaHeight: CGFloat {
        var topSafeAreaHeight: CGFloat = 0
         if #available(iOS 11.0, *) {
               let window = UIApplication.shared.windows[0]
               let safeFrame = window.safeAreaLayoutGuide.layoutFrame
               topSafeAreaHeight = safeFrame.minY
             }
        return topSafeAreaHeight
    }
    
    static var bottomSafeAreaHeight: CGFloat {
        var bottomSafeAreaHeight: CGFloat = 0

         if #available(iOS 11.0, *) {
             let window = UIApplication.shared.windows[0]
             let safeFrame = window.safeAreaLayoutGuide.layoutFrame
             bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
        }
        
        return bottomSafeAreaHeight
    }
}
