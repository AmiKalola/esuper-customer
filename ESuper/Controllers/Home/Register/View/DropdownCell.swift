//
//  DropdownCell.swift
//  ESuper
//
//  Created by Rohit on 14/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit
class DropdownCell: CustomTableCell {
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.textColor = UIColor.themeTextColor
    }
}
