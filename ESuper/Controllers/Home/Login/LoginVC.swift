//
//  LoginVC.swift
//  
//
//  Created by Elluminati on 17/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import FirebaseMessaging
import AuthenticationServices
import CoreLocation

class LoginVC: BaseVC {

    //MARK: - Outlets
    @IBOutlet weak var viewForLanguage: UIView!
    @IBOutlet weak var lblLanguageMessage: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var scrLogin: UIScrollView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var viewForPassword: UIView!
    @IBOutlet weak var btnShowHidePassword: UIButton!
    @IBOutlet weak var btnForgetPasssword: UIButton!
    @IBOutlet weak var lblOr: UILabel!
    @IBOutlet weak var stkForSocialLogin: UIStackView!
    @IBOutlet weak var viewForSocialLogin: UIView!
    @IBOutlet weak var btnFacebook: FBLoginButton!
    @IBOutlet weak var btnGoogle: GIDSignInButton!
    @IBOutlet weak var btnSkipLogin: UIButton!
    @IBOutlet weak var lblSignInMsg: UILabel!
    @IBOutlet weak var lblUseAppLang: UILabel!
    @IBOutlet weak var lblSignUp: UILabel!
    @IBOutlet weak var viewImgBG: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblSignInWith: UILabel!
    @IBOutlet weak var txtViewHaveAccount: UITextView!
    @IBOutlet weak var imgDropDown: UIImageView!
    
    @IBOutlet weak var viewLoginBy: TagListView!
    @IBOutlet weak var heightLoginBy: NSLayoutConstraint!
    
    @IBOutlet weak var txtCountryCode: UITextField!
    var arrForCountryList = [CountryModal]()
    var listCountryCode = [CountryCode]()
    let downArrow:String = "\u{25BC}"
    var socialId:String = ""
    var loginBy = "0"
    var isCart = false
//    let captcha:ReCaptcha = ReCaptcha.shared

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        let tap = UITapGestureRecognizer(target: self, action:#selector(openLanguageDialog))
        viewForLanguage.addGestureRecognizer(tap)
        if !isCart{
            currentBooking.clearBooking()
        }
        btnGoogle.style = .wide
        btnGoogle.colorScheme = .light
        btnFacebook.delegate = self
        btnFacebook.permissions = ["public_profile", "email"]
        LoginManager.init().logOut()
        let str : NSString = "\(WebService.BASE_URL)" as NSString
        let path : NSString = str.deletingLastPathComponent as NSString
        let ext : NSString = str.lastPathComponent as NSString

        GIDSignIn.sharedInstance.signOut()
        lblOr.isHidden = true
        if preferenceHelper.IsSocialLoginEnable {
            viewForSocialLogin.isHidden = false
            lblOr.isHidden = false
            if UIApplication.isRTL() {
                btnFacebook.contentHorizontalAlignment = .left
                btnGoogle.contentHorizontalAlignment = .right
            }
            //Set up apple sign in Button
            if #available(iOS 13.2, *) {
                let btnAppleID = ASAuthorizationAppleIDButton()
                btnAppleID.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
                stkForSocialLogin.addArrangedSubview(btnAppleID)
            }
        } else {
            viewForSocialLogin.isHidden = true
            lblOr.isHidden = true
        }
        lblLanguageMessage.text = LocalizeLanguage.currentAppleLanguageFull() //+  downArrow
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        self.getCountry()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    func setLocalization() {
        /*set colors*/
        self.view.backgroundColor = UIColor.themeViewBackgroundColor
        btnForgetPasssword.setTitleColor(UIColor.themeColor, for: UIControl.State.normal)
        lblLanguageMessage.textColor = UIColor.themeTextColor
        lblUseAppLang.textColor = UIColor.themeLightTextColor
        lblSignUp.textColor = UIColor.themeLightLineColor
        lblSignInMsg.textColor = UIColor.themeTextColor
        lblSignInWith.textColor = UIColor.themeColor
        txtViewHaveAccount.textColor = UIColor.themeTextColor
      
        /* Set localization */
        btnSkipLogin.setTitleColor(UIColor.themeColor, for: UIControl.State.normal)
        btnSkipLogin.setTitle("TXT_SKIP_LOGIN".localizedCapitalized, for: UIControl.State.normal)
        btnLogin.setTitle( "TXT_SIGN_IN".localizedCapitalized, for: UIControl.State.normal)
        txtPassword.placeholder = "TXT_PASSWORD".localized
        lblLanguageMessage.text = "MSG_LANGUAGE".localized
        btnForgetPasssword.setTitle("TXT_FORGOT_PASSWORD".localized, for: .normal)
        lblSignInMsg.text = "TXT_SIGN_IN".localized
        lblUseAppLang.text = "TXT_USE_APP_LANG".localized
        lblSignInWith.text = String(format: "%@ %@", "TXT_SIGN_IN_WITH".localized,"TXT_PASSWORD".localized)
        
        /*Set Font*/
        txtEmail.font = FontHelper.textRegular()
        txtCountryCode.font = FontHelper.textRegular()
        txtPassword.font = FontHelper.textRegular()
        btnForgetPasssword.titleLabel?.font = FontHelper.textRegular()
        lblOr.font = FontHelper.textMedium()
        lblLanguageMessage.font = FontHelper.textMedium(size: FontHelper.regular)
        lblSignUp.font = FontHelper.textMedium(size: FontHelper.large)
        lblUseAppLang.font = FontHelper.textRegular()
        lblSignInMsg.font = FontHelper.textMedium(size: FontHelper.largest)
        lblSignInWith.font = FontHelper.textRegular()
        txtViewHaveAccount.font = FontHelper.textRegular()
        lblOr.textColor = UIColor.themeTextColor
        lblOr.text = "TXT_OR".localizedUppercase
        txtViewHaveAccount.isEditable = false
        txtViewHaveAccount.isSelectable = true
        txtViewHaveAccount.delegate = self
        txtViewHaveAccount.hyperLink(originalText: "TXT_HAVE_ACCOUNT_SIGNUP".localized, hyperLink: "TXT_SIGN_UP_NOW".localized, urlString: "http://signup.com/")
        enabledLoginBy()
        imgLogo.image = UIImage(named:"loginlogo")
        imgLogo.contentMode = .scaleAspectFit
        imgDropDown.image = UIImage(named:"dropdown")?.imageWithColor(color: .themeTitleColor)
        if Bundle.main.bundleIdentifier == ourProductBundleID {
            addTapOnVersion()
        }
    }
    
    func setUpLoginBy() {
        //print(currentBooking.currentSendPlaceData.country_code)
        viewLoginBy.delegate = self
        if UIApplication.isRTL() {
            viewLoginBy.alignment = .right
        }else {
            viewLoginBy.alignment = .left
        }
        viewLoginBy.tagSelectedBackgroundColor = UIColor.themeColor
        viewLoginBy.borderWidth = 1
        viewLoginBy.borderColor = .themeColor
        viewLoginBy.tagBackgroundColor = UIColor.themeViewBackgroundColor
        viewLoginBy.selectedTextColor = UIColor.white
//        viewLoginBy.textColor = UIColor.black
        viewLoginBy.textColor = UIColor.themeColor
        viewLoginBy.textFont = FontHelper.textRegular()
        viewLoginBy.cornerRadius = 8
        viewLoginBy.paddingY = 8
        viewLoginBy.paddingX = 8
        viewLoginBy.marginY = 12
        viewLoginBy.removeAllTags()
    }

    func setupLayout() {
        btnLogin.applyShadowToButton()
        //viewImgBG.setRound(withBorderColor: UIColor.themeLightLineColor, andCornerRadious: viewImgBG.frame.size.width/2.0, borderWidth: 2.0)
        //imgLogo.setRound()
        btnLogin.applyRoundedCornersWithHeight()
    }
    
    func addTapOnVersion() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onClickVersionTap(_:)))
        tap.numberOfTapsRequired = 3
        self.imgLogo.addGestureRecognizer(tap)
        self.imgLogo.isUserInteractionEnabled = true
    }
    
    @objc func onClickVersionTap(_ sender: UITapGestureRecognizer) {
        let dialog = DialogForApplicationMode.showCustomAppModeDialog()
        
        dialog.onClickLeftButton = { [unowned dialog] in
            dialog.removeFromSuperview()
        }
        
        dialog.onClickRightButton = { [unowned dialog] in
            dialog.removeFromSuperview()
            if AppMode.currentMode != dialog.appMode {
                preferenceHelper.SessionToken = ("")
                preferenceHelper.UserId = ("")
                preferenceHelper.RandomCartID = (String.random(length: 20))
                APPDELEGATE.clearFavoriteAddressEntity()
                APPDELEGATE.clearDeliveryLocationEntity()
                preferenceHelper.AuthToken = ("")
                AppMode.currentMode = dialog.appMode
                self.dismiss(animated: false, completion: nil)
                APPDELEGATE.goToHome(isSplash: true)
            }
        }
    }

    override func updateUIAccordingToTheme() {
        imgDropDown.image = UIImage(named:"dropdown")?.imageWithColor(color: .themeTitleColor)
    }
    
    //MARK: - USER DEFINE METHODS
    func checkValidation() -> Bool {
        if loginBy == "1" {
            let validEmail = txtEmail.text!.checkEmailValidation()
            if txtEmail.text!.isEmpty() {
                Utility.showToast(message: "MSG_ENTER_EMAIL".localized)
                return false
            } else if validEmail.0 == false {
                Utility.showToast(message: validEmail.1)
                return false
            } else if txtPassword.text!.isEmpty() {
                Utility.showToast(message: "MSG_PLEASE_ENTER_PASSWORD".localized)
                return false
            } else {
                return true
            }
        } else {
            
            let validMobileNumber = txtEmail.text!.isValidMobileNumber()
            
            if txtCountryCode.text!.isEmpty() {
                Utility.showToast(message: "txt_please_enter_country_code".localized)
                return false
            } else if txtEmail.text!.isEmpty() {
                Utility.showToast(message: "MSG_PLEASE_ENTER_MOBILE_NUMBER".localized)
                return false
            } else if validMobileNumber.0 == false {
                Utility.showToast(message: validMobileNumber.1)
                return false
            } else if txtPassword.text!.isEmpty() {
                Utility.showToast(message: "MSG_PLEASE_ENTER_PASSWORD".localized)
                return false
            } else {
                return true
            }
        }
    }

    func enabledLoginBy() {
        
        setUpLoginBy()
        if preferenceHelper.IsLoginByEmail && preferenceHelper.IsLoginByPhone {
            viewLoginBy.isHidden = false
            viewLoginBy.addTag("TXT_EMAIL".localized)
            viewLoginBy.addTag("TXT_PHONE".localized)
            setLoginBy(type: 1)
        } else if preferenceHelper.IsLoginByPhone {
            viewLoginBy.isHidden = true
            setLoginBy(type: 2)
        } else {
            viewLoginBy.isHidden = true
            setLoginBy(type: 1)
        }
    }
    
    func setLoginBy(type: Int) {
        if loginBy != "\(type)" {
            txtEmail.text = ""
        }
        loginBy = "\(type)"
        if type == 1 { // Email
            txtCountryCode.isHidden = true
            if viewLoginBy.tagViews.count > 0 {
                viewLoginBy.tagViews[0].isSelected = true
            }
            txtEmail.placeholder = "TXT_EMAIL".localized
            txtEmail.keyboardType = .emailAddress
        } else {
            txtCountryCode.isHidden = false
            if viewLoginBy.tagViews.count > 0 {
                viewLoginBy.tagViews[1].isSelected = true
            }
            txtEmail.placeholder = "TXT_PHONE".localized
            txtEmail.keyboardType = .numberPad
        }
    }

    //MARK: - Button action methods
    @IBAction func onClickBtnSkipLogin(_ sender: Any) {
        btnSkipLogin.isUserInteractionEnabled = false
        if isCart{
            self.dismiss(animated: true)
        }else{
            APPDELEGATE.goToMain()
        }
    }

    @IBAction func onClickBtnLogin(_ sender: Any?) {
        self.view.endEditing(true)
        self.socialId = ""
        if(self.checkValidation()) {
            wsLogin()
        }
    }

    @IBAction func onClickBtnForgetPassword(_ sender: Any) {
        self.view.endEditing(true)
        openForgetPasswordDialog()
    }

    @IBAction func onClickBtnTwitter(_ sender: Any) {
    }

    @IBAction func onClickBtnShowHidePassword(_ sender: Any) {
        if self.txtPassword.isSecureTextEntry {
            self.txtPassword.isSecureTextEntry = false
            self.btnShowHidePassword.setBackgroundImage(UIImage.init(named: "passwordShow"), for: .normal)
        } else {
            self.txtPassword.isSecureTextEntry = true
            self.btnShowHidePassword.setBackgroundImage(UIImage.init(named: "passwordHide"), for: .normal)
        }
    }

    //MARK: - Dialogs
    func openForgetPasswordDialog() {
        let dialogForForgetPassword = CustomVerificationDialog.showCustomVerificationDialog(title: "TXT_FORGOT_PASSWORD".localized, message: "MSG_FORGOT_PASSWORD".localized, titleLeftButton: "".localized, titleRightButton: "TXT_RESET_PWD".localizedCapitalized, editTextOneHint: "TXT_PHONE".localized, editTextTwoHint: "", isEdiTextTwoIsHidden: true,vcontroller : self)
        dialogForForgetPassword.onClickLeftButton = { [unowned dialogForForgetPassword] in
            dialogForForgetPassword.removeFromSuperview()
        }
        dialogForForgetPassword.onClickRightButton = { [unowned self, unowned dialogForForgetPassword] (text1:String,text2:String) in
            let validMobileNumber = text1.isValidMobileNumber()
            if validMobileNumber.0 == false {
                Utility.showToast(message: validMobileNumber.1)
            } else {
                self.wsForgetPassword(email: text1, phone: text1, dialogForForgetPassword: dialogForForgetPassword)
            }
        }
    }

    @objc func openLanguageDialog() {
        openLanguageActionSheet()
    }

    func openLanguageActionSheet() {
        let alertController = UIAlertController(title: nil, message: "TXT_CHANGE_LANGUAGE".localized, preferredStyle: .actionSheet)
        for i in arrForLanguages{
            let action = UIAlertAction(title: i.language , style: .default, handler: { (alert: UIAlertAction!) -> Void in
                //print(alert.title!)
                //print(arrForLanguages.firstIndex(where: {$0.language == alert.title!})!)
                if arrForLanguages.firstIndex(where: {$0.language == alert.title!})! == preferenceHelper.Language{
                    self.dismiss(animated: true, completion: nil)
                } else {
                    super.changed(arrForLanguages.firstIndex(where: {$0.language == alert.title!})!)
                }
            })
            alertController.addAction(action)
        }

        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alertController, animated: true, completion: nil)
    }

    
    func wsLogin() {

        if preferenceHelper.IsUseCaptcha{
            CaptchaVerification.shared.checkCaptchaC(inView: self.view,uivi: self) { token in
                //print(token)
                self.dismiss(animated: false)
                self.wsLoginAPI(token: token)
            }
        }else{
            self.wsLoginAPI(token: "")
        }
    }
    func wsLoginAPI(token : String) {
      
        Utility.showLoading()
        LoginManager.init().logOut()
        GIDSignIn.sharedInstance.signOut()

        var dictParam:[String:Any]
        
        let strCoutnryCode: String = {
            if loginBy == "1" { // Email
                return ""
            }
            return txtCountryCode.text!
        }()
  
        if socialId.isEmpty() {
            dictParam =   [PARAMS.EMAIL      : txtEmail.text!  ,
                           PARAMS.PASS_WORD  : txtPassword.text! ,
                           PARAMS.LOGIN_BY   : CONSTANT.MANUAL ,
                           PARAMS.DEVICE_TYPE: CONSTANT.IOS,
                           PARAMS.SOCIAL_ID: "",
                           PARAMS.CAPTCHA_TOKEN : token,
                           PARAMS.IPHONE_ID:preferenceHelper.RandomCartID,
                           PARAMS.DEVICE_TOKEN:preferenceHelper.DeviceToken,
                           PARAMS.login_type : self.loginBy,
                           PARAMS.COUNTRY_PHONE_CODE : strCoutnryCode
            ]
        } else {
            dictParam =   [PARAMS.EMAIL      : "",
                           PARAMS.PASS_WORD  : "",
                           PARAMS.SOCIAL_ID  : socialId,
                           PARAMS.LOGIN_BY   : CONSTANT.SOCIAL ,
                           PARAMS.DEVICE_TYPE: CONSTANT.IOS,
                           PARAMS.CAPTCHA_TOKEN : token,
                           PARAMS.IPHONE_ID:preferenceHelper.RandomCartID,
                           PARAMS.DEVICE_TOKEN:preferenceHelper.DeviceToken,
            ]
        }
        
        //print(dictParam)

        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_USER_LOGIN, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            Parser.parseUserStorageData(response: response, completion: { result in
                if result {
                    APPDELEGATE.clearOrderNotificationEntity()
                    APPDELEGATE.clearMassNotificationEntity()
                    if self.isCart{
                        self.dismiss(animated: true)
                    }else{
                        APPDELEGATE.goToMain()
                    }
                    return
                }else{
                  //print(response["status_phrase"] as? String)
                }
            })
        }
    }
    func wsForgetPassword(email:String,phone:String,dialogForForgetPassword:CustomVerificationDialog) {
        Utility.showLoading()
        let dictParam : [String : Any] = [
            PARAMS.EMAIL : "",
            PARAMS.TYPE  : CONSTANT.TYPE_USER,
            PARAMS.PHONE  : phone
        ]

        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_FORGET_PASSWORD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                dialogForForgetPassword.removeFromSuperview()
                let dialogForForgetPassword = CustomVerificationDialog.showCustomVerificationDialog(title: "TXT_FORGOT_PASSWORD".localized, message: "TXT_TITLE_MSG_VERIFICATION_CODE".localized, titleLeftButton: "TXT_CANCEL".localized, titleRightButton: "TXT_VERIFY".localizedCapitalized, editTextOneHint: "TXT_PHONE_OTP".localized, editTextTwoHint: "", isEdiTextTwoIsHidden: true, isEdiTextOneIsHidden: false, isForVerifyOtp: true, isForgotPasswordOtp: true, param: dictParam,vcontroller : self)
                dialogForForgetPassword.startTimer()
                dialogForForgetPassword.onClickLeftButton = { [unowned dialogForForgetPassword] in
                    dialogForForgetPassword.removeFromSuperview()
                }
                dialogForForgetPassword.onClickRightButton = { [unowned self] (text1:String,text2:String) in
                    if text1.count > 0 {
                        self.wsForgetPasswordVerify(email: email, phone: phone,otp: text1, dialogForForgetPassword: dialogForForgetPassword)
                    } else {
                        self.wsForgetPassword(email: email, phone: phone, dialogForForgetPassword: dialogForForgetPassword)
                    }
                }
            }
        }
    }

    func wsForgetPasswordVerify(email:String,phone:String,otp:String,dialogForForgetPassword:CustomVerificationDialog) {
        Utility.showLoading()
        let dictParam : [String : Any] = [
            PARAMS.TYPE  : CONSTANT.TYPE_USER,
            PARAMS.PHONE  : phone,
            PARAMS.OTP  : otp,
        ]
        //print(dictParam)

        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_FORGET_PASSWORD_VERIFY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if  Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                dialogForForgetPassword.timer?.invalidate()
                let idFromResponse = response["id"] as? String
                let serverTokenFromResponse = response["server_token"] as? String
                self.dialogResetPwd(dialogForForgetPassword: dialogForForgetPassword, id: idFromResponse ?? "", serverToken:  serverTokenFromResponse ?? "")
            }
        }
    }

    func dialogResetPwd(dialogForForgetPassword:CustomVerificationDialog, id: String, serverToken: String){
        dialogForForgetPassword.removeFromSuperview()
        
        let dialogForForgetPassword = CustomVerificationDialog.showCustomVerificationDialog(title: "TXT_RESET_PWD".localized, message: "TITLE_ENTER_NEW_PWD".localized, titleLeftButton: "TXT_CANCEL".localized, titleRightButton: "TXT_RESET".localizedCapitalized, editTextOneHint: "MSG_ENTER_NEW_PWD".localized, editTextTwoHint: "TXT_CONFIRM_NEW_PWD".localized, isEdiTextTwoIsHidden: false, isEdiTextOneIsHidden: false,vcontroller : self)
        
        dialogForForgetPassword.onClickLeftButton = { [unowned dialogForForgetPassword] in
            dialogForForgetPassword.removeFromSuperview()
        }
        
        dialogForForgetPassword.onClickRightButton = { [unowned self]  (text1:String,text2:String) in
            dialogForForgetPassword.removeFromSuperview()
            self.wsUpdateNewPwd(pwd: text1, id: id, serverToken:  serverToken)
        }
    }

    func wsUpdateNewPwd(pwd:String, id: String, serverToken: String){
        Utility.showLoading()
        let dictParam : [String : Any] = [
            PARAMS.TYPE  : CONSTANT.TYPE_USER,
            PARAMS.ID  : id,
            PARAMS.SERVER_TOKEN  : serverToken,
            PARAMS.PASS_WORD  : pwd,
        ]
        //print(dictParam)

        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_NEW_PASSWORD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response, withSuccessToast: true, andErrorToast: true) {} else {}
        }
    }

    //MARK: - FACEBOOK AND GOOGLE LOGIN
    @IBAction func onClickBtnGoogleLogin(_ sender: Any){
        self.socialId = ""
        GIDSignIn.sharedInstance.signOut()
        GIDSignIn.sharedInstance.configuration = signInConfig
        GIDSignIn.sharedInstance.signIn(withPresenting: self) { signInResult, error in
            
            guard error == nil else { return }
            let user = signInResult?.user
            self.socialId = user?.userID ?? ""
            self.txtEmail.text = user?.profile?.email
            self.wsLogin()
            
          }
    }

    @IBAction func onClickBtnFacebook(_ sender: Any){
        
    }

    func getFBUserData() {
        if((AccessToken.current) != nil) {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start { (connection, result, error) in
                Utility.hideLoading()
                if (error == nil) {
                    let dict = result as! [String : AnyObject]
                    let email:String = (dict["email"] as? String) ?? ""
                    Profile.loadCurrentProfile(completion:  { [unowned self] (profile, error) in
                        if (error == nil) {
                            self.socialId = (profile?.userID)!
                            self.txtEmail.text = email
                            self.wsLogin()
                        } else {
                            Utility.showToast(message: (error?.localizedDescription)!)
                        }
                    })
                }
            }
        }
    }
    func getCountry(){
//        WebService.WS_GET_COUNTRY_LIST
        let locale = Locale.current
        //print(locale.regionCode)
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            //print(countryCode)
        }
        let location = CLLocation.init(latitude: currentBooking.currentSendPlaceData.latitude, longitude: currentBooking.currentSendPlaceData.longitude)
        
        self.getWebData(method: "GET", webURL: WebService.WS_GET_COUNTRY_LIST, parameters: "", completion: { parsedData, status in
                if  (parsedData["success"] as! Bool){
                    let info = parsedData["country_list"] as! [JSONType]
                    self.listCountryCode = [CountryCode]()
                    self.listCountryCode.append(contentsOf: info.compactMap(CountryCode.init))
                    print("Country :: - \(self.listCountryCode)")
//                    self.tableForDropDown.reloadData()
                    
                    LocationCenter.default.fetchCityAndCountry(location: location) {[weak self] (city, country, error) in
                        guard let self = self else { return }
                        //print(city ?? "")
                        //print(country ?? "")
                        for list in self.listCountryCode{
                            if let count = country{
                            if list.name!.lowercased() == country!.lowercased(){
                                
                                //                                self.selectedCountryObj = list
                                self.txtCountryCode.text = list.countryCallingCodes ?? ""
                                self.txtEmail.text = ""
                            }
                        }
                        }
                    }
                }
            
        })
    }
}

extension LoginVC:LoginButtonDelegate {
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {}

    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if error != nil {} else {
            if result?.isCancelled ?? true {
                //print(error ?? "")
            } else {
                Utility.showLoading()
                self.getFBUserData()
            }
        }
    }
}

@available(iOS 13.0, *)
extension LoginVC: ASAuthorizationControllerDelegate {

    @objc func handleAppleIdRequest() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email ?? ""
            print("User id = \(userIdentifier)\nFull Name = \(String(describing: fullName)) \nEmail id = \(String(describing: email))")

            let appleIDProvider = ASAuthorizationAppleIDProvider()
            appleIDProvider.getCredentialState(forUserID: userIdentifier) {  (credentialState, error) in
                switch credentialState {
                case .authorized:
                    print("The Apple ID credential is valid.")

                    if !(appleIDCredential.email ?? "").isEmpty{
                        preferenceHelper.SigninWithAppleEmail = (appleIDCredential.email ?? "")
                        self.txtEmail.text = preferenceHelper.SigninWithAppleEmail
                    } else {
                        if preferenceHelper.SigninWithAppleEmail.count > 0{
                            DispatchQueue.main.async {
                                self.txtEmail.text = preferenceHelper.SigninWithAppleEmail
                            }
                        }
                    }

                    if !(appleIDCredential.fullName?.givenName ?? "").isEmpty{
                        preferenceHelper.SigninWithAppleUserName = ((appleIDCredential.fullName?.givenName ?? "") + " " + (appleIDCredential.fullName?.familyName ?? ""))
                    }
                    self.socialId = appleIDCredential.user

                    DispatchQueue.main.async {
                        if appleIDCredential.email?.contains("privaterelay.appleid.com") ?? false {
                            self.wsLogin()
                        } else {
                            self.wsLogin()
                        }
                    }
                    break
                case .revoked:
                    print("The Apple ID credential is revoked.")
                    break
                case .notFound:
                    print("No credential was found, so show the sign-in UI.")
                    break
                default:
                    break
                }
            }
        }
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("apple signin error = \(error.localizedDescription)")
    }

    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
extension LoginVC: TagListViewDelegate {
    func didChangeHeight(_ tagView: TagListView, height: CGFloat) {
        heightLoginBy.constant = height
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        if let index = sender.tagViews.firstIndex(of: tagView) {
            for obj in sender.selectedTags() {
                obj.isSelected = false
            }
            tagView.isSelected = true
            if index == 0 {
                setLoginBy(type: 1)
            } else {
                setLoginBy(type: 2)
            }
        }
    }
}
//MARK: - TextField Delegate Methods
extension LoginVC : UITextFieldDelegate , UITextViewDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCountryCode {
            self.view.endEditing(true)
            let dialogForCountry = CustomCountryDialog.showCustomCountryDialog(withDataSource:listCountryCode)
            dialogForCountry.onCountrySelected = {
                [weak self] (country:CountryCode) in
                guard let self = self else {
                    return
                }
                self.txtCountryCode.text = country.countryCallingCodes
                dialogForCountry.removeFromSuperview()
            }
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        } else if textField == txtPassword {
            txtPassword.resignFirstResponder()
            onClickBtnLogin(nil)
        } else {
            self.view.endEditing(true)
            return true
        }
        return true
    }
    //MARK: - UITexView Delegate
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == "http://signup.com/" {
            if self.isCart{
                let register = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                register.isCart = self.isCart
                self.present(register, animated: true)
            }else{
                (self.parent as! Home).goToSignUp()
            }
        }
        return false
    }
}
// MARK: - ReCAPTCHAViewModelDelegate
extension LoginVC: ReCAPTCHAViewModelDelegate {
    func didSolveCAPTCHA(token: String) {
        print("Token: \(token)")
        self.wsLoginAPI(token: token)
       
    }
}
