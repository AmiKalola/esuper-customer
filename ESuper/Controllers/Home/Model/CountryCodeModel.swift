//
//  CountryCodeModel.swift
//  ESuper
//
//  Created by Rohit on 02/05/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
struct CountryCode{
    var name :String?
    var countryCallingCodes : String?
    var currencies : String?
    var currency_symbol : String?
    var alpha2 : String?
    var alpha3 : String?
    var MinPhoneLength : String?
    var MaxPhoneLength : String?
    var timezone : String?
    
}
extension CountryCode : JSONParsable{
    init?(json: JSONType?) {
        self.name  = json?["name"] as? String ?? ""
        if let calling = json?["countryCallingCodes"] as?  [String]{
            self.countryCallingCodes  = calling[0]//json?["countryCallingCodes"] as?  String ?? ""
        }
        if let curren = json?["currencies"] as?  [String]{
            self.currencies  = curren[0] //json?["currencies"] as?  String ?? ""
            
        }
        self.currency_symbol  = json?["currency_symbol"] as?  String ?? ""
        self.alpha2  = json?["alpha2"] as?  String ?? ""
        self.alpha3  = json?["alpha3"] as?  String ?? ""
        self.MinPhoneLength  = json?["MinPhoneLength"] as?  String ?? ""
        self.MaxPhoneLength  = json?["MaxPhoneLength"] as?  String ?? ""
        if let timezone = json?["timezone"] as?  [String]{
            self.timezone  = timezone[0]//json?["timezone"] as?  String ?? ""
        }
    }
}
