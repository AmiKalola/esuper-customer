//
//  CaptchaVerification.swift
//  Delivery Angel
//
//  Created by Mayur on 22/11/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit
import SwiftUI
import WebKit

class CaptchaVerification: NSObject {
    static let shared = CaptchaVerification()
    var block: ((_ str:String) -> ()) = { str in }
    
    //MARK: - WEB SERVICE CALLS
    private var reCAPTCHAViewModel: ReCAPTCHAViewModel?
    private var vm: ReCAPTCHAViewModel!
    private var vc: ReCAPTCHAViewController!
    
    func checkCaptchaC(inView: UIView , uivi : UIViewController, complition: @escaping (_ token: String) -> ()) {
        self.block = complition
        if preferenceHelper.CaptchaSiteKeyForIos == ""{
            Utility.showToast(message: "Google captcha not load please contact admin.")
            return
        }
        let viewModel = ReCAPTCHAViewModel(
            siteKey: preferenceHelper.CaptchaSiteKeyForIos,
            url: URL(string: WebService.BASE_URL_ASSETS)!
        )

        viewModel.delegate = self
        self.vm = viewModel

        let vc = ReCAPTCHAViewController(viewModel: self.vm)
        self.vc = vc
        let yourViewController = UIViewController() // for example
            uivi.present(vc, animated: true, completion: nil)
    }
   
    
}


// MARK: - ReCAPTCHAViewModelDelegate
extension CaptchaVerification: ReCAPTCHAViewModelDelegate {
    func didSolveCAPTCHA(token: String) {
        Utility.hideLoading()
        self.block(token)
    }
}
