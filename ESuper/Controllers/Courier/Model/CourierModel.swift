//
//  CourierModel.swift
//  Edelivery
//
//  Created by Rohit on 19/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
struct ServiceDetails{
    var _id : String?
    var service_taxes  : [Int]?
    var created_at : String?
    var price_per_unit_distance : Int?
    var is_business : Bool?
    var unique_id : Int?
    var delivery_type : Int?
    var admin_type : Int?
    var country_id : String?
    var delivery_price_setting : [Int]?
    var want_to_set_waiting_time : Bool?
    var service_tax : Int?
    var is_default : Bool?
    var admin_profit_mode_on_delivery : Int?
    var is_round_trip : Bool?
    var night_surge_hours : [Int]?
    var type_id : Int?
    var price_per_unit_time : Int?
    var waiting_time_after_min : Int?
    var __v : Int?
    var additional_stop_price : Int?
    var vehicle_id : String?
    var min_fare : Int?
    var is_use_distance_calculation : Bool?
    var round_trip_charge : Int?
    var cancellation_charge_value : Int?
    var admin_profit_value_on_delivery : Int?
    var updated_at : String?
    var waiting_time_price : Int?
    var is_cancellation_charge : Bool?
    var base_price : Int?
    var cancellation_charge_type : Int?
    var city_id : String?
    var cancellation_fee : Int?
    var base_price_distance : Int?
    var is_surge_hours : Bool?
    var is_waiting_time_every_stop : Bool?
    var over_sized_item_charge : Int?
    var is_surge_on_night : Bool?
    var waiting_time_start_after : Int?
}

extension ServiceDetails : JSONParsable{
    init?(json: JSONType?) {
    }
    
    func setData(json : JSONType) -> ServiceDetails{
        var serviceDetails = ServiceDetails()
        serviceDetails._id = json["_id"] as? String ?? ""
        serviceDetails.service_taxes = json["service_taxes"] as? [Int]
        serviceDetails.created_at = json["created_at"] as? String ?? ""
        serviceDetails.price_per_unit_distance = json["price_per_unit_distance"] as? Int ?? 0
        serviceDetails.is_business = json["is_business"] as? Bool ?? false
        serviceDetails.unique_id = json["unique_id"] as? Int ?? 0
        serviceDetails.delivery_type = json["delivery_type"] as? Int ?? 0
        serviceDetails.admin_type = json["admin_type"] as? Int ?? 0
        serviceDetails.country_id = json["country_id"] as? String ?? ""
        serviceDetails.delivery_price_setting = json["delivery_price_setting"] as? [Int]
        serviceDetails.want_to_set_waiting_time = json["want_to_set_waiting_time"] as? Bool ?? false
        serviceDetails.service_tax = json["service_tax"] as? Int ?? 0
        serviceDetails.is_default = json["is_default"] as? Bool ?? false
        serviceDetails.admin_profit_mode_on_delivery = json["admin_profit_mode_on_delivery"] as? Int ?? 0
        serviceDetails.is_round_trip = json["is_round_trip"] as? Bool ?? false
        serviceDetails.night_surge_hours = json["night_surge_hours"] as? [Int]
        serviceDetails.type_id = json["type_id"] as? Int ?? 0
        serviceDetails.price_per_unit_time = json["price_per_unit_time"] as? Int ?? 0
        serviceDetails.waiting_time_after_min = json["waiting_time_after_min"] as? Int ?? 0
        serviceDetails.__v = json["__v"] as? Int ?? 0
        serviceDetails.additional_stop_price = json["additional_stop_price"] as? Int ?? 0
        serviceDetails.vehicle_id = json["vehicle_id"] as? String ?? ""
        serviceDetails.min_fare = json["min_fare"] as? Int ?? 0
        serviceDetails.is_use_distance_calculation = json["is_use_distance_calculation"] as? Bool ?? false
        serviceDetails.round_trip_charge = json["round_trip_charge"] as? Int ?? 0
        serviceDetails.cancellation_charge_value = json["cancellation_charge_value"] as? Int ?? 0
        serviceDetails.admin_profit_value_on_delivery = json["admin_profit_value_on_delivery"] as? Int ?? 0
        serviceDetails.updated_at = json["updated_at"] as? String ?? ""
        serviceDetails.waiting_time_price = json["waiting_time_price"] as? Int ?? 0
        serviceDetails.is_cancellation_charge = json["is_cancellation_charge"] as? Bool ?? false
        serviceDetails.base_price = json["base_price"] as? Int ?? 0
        serviceDetails.cancellation_charge_type = json["cancellation_charge_type"] as? Int ?? 0
        serviceDetails.city_id = json["city_id"] as? String ?? ""
        serviceDetails.cancellation_fee = json["cancellation_fee"] as? Int ?? 0
        serviceDetails.base_price_distance = json["base_price_distance"] as? Int ?? 0
        serviceDetails.is_surge_hours = json["is_surge_hours"] as? Bool ?? false
        serviceDetails.is_waiting_time_every_stop = json["is_waiting_time_every_stop"] as? Bool ?? false
        serviceDetails.over_sized_item_charge = json["over_sized_item_charge"] as? Int ?? 0
        serviceDetails.is_surge_on_night = json["is_surge_on_night"] as? Bool ?? false
        serviceDetails.waiting_time_start_after = json["waiting_time_start_after"] as? Int ?? 0
        return serviceDetails
    }
    
}
