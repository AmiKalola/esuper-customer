//
//  ImageCollectionViewCell.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit


class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var btnDeleteImage: UIButton!
    @IBOutlet weak var imgCollection: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        self.imgCollection.clipsToBounds = true
    }
}
