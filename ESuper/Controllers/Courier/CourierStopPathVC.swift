//
//  CourierStopPathVC.swift
//  Edelivery
//
//  Created by MacPro3 on 13/10/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit
import GoogleMaps

class CourierStopPathVC: UIViewController {

    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var btnClose: UIButton!
    
    var arrAddress = [Address]()
    var googlePathResponse: [String:Any]?
    var isTaxi = false
    
    var pickUplat: Double = 0
    var pickUplong: Double = 0
    
    var destinationlat: Double = 0
    var destinationlong: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnClose.setImage(UIImage.init(named: "close_icon_theme"), for: .normal)
        
        self.viewMap.settings.allowScrollGesturesDuringRotateOrZoom = false
        
        setPath()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                do {
                    if let styleURL = Bundle.main.url(forResource: "styleable_map", withExtension: "json") {
                        self.viewMap.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                    } else {
                        NSLog("Unable to find style.json")
                        
                    }
                } catch {
                    NSLog("One or more of the map styles failed to load. \(error)")
                    
                }
            } else {
                self.viewMap.mapStyle = .none
            }
        } else {
            // Fallback on earlier versions
            self.viewMap.mapStyle = .none
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewMap.clear()
        viewMap = nil
    }
    
    @IBAction func onClickClose(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    func setPath() {
        guard let googlePathResponse = googlePathResponse else {
            return
        }
        
        if let routes = googlePathResponse["routes"] as? [[String:Any]] {
            if let rout = routes.first {
                if let overview_polyline = rout["overview_polyline"] as? [String:Any] {
                    if let points = overview_polyline["points"] as? String {
                        self.drawPath(with: points)
                    }
                }
            }
        }
    }

    private func drawPath(with points : String) {
        let path = GMSPath(fromEncodedPath: points)
        let polyLinePath = GMSPolyline(path: path)
        polyLinePath.strokeColor = UIColor.themeGooglePath
        polyLinePath.strokeWidth = 5.0
        polyLinePath.geodesic = true
        polyLinePath.map = self.viewMap
        
        focusMap()
    }

    func focusMap() {
        var bounds = GMSCoordinateBounds()
        if isTaxi{
            let pickUpCoordinate = CLLocationCoordinate2D(latitude: pickUplat, longitude: pickUplong)
            let destinationCoordinate = CLLocationCoordinate2D(latitude: destinationlat, longitude: destinationlong)
            
            let pickupMarker = GMSMarker(position: pickUpCoordinate)
            pickupMarker.icon = UIImage.init(named: "icons8-radio-button-96")

            pickupMarker.map = viewMap
            bounds = bounds.includingCoordinate(pickupMarker.position)
            if destinationlat != 0 {
                let destinationMarker = GMSMarker(position: destinationCoordinate)
                destinationMarker.icon = UIImage.init(named: "icons8-square-96")
                destinationMarker.map = viewMap
                bounds = bounds.includingCoordinate(destinationMarker.position)
            }
        }
       
        
        var count = 1
        for arrAddress in arrAddress {
            let marker = GMSMarker.init(position: CLLocationCoordinate2D(latitude: arrAddress.location[0], longitude: arrAddress.location[1]))
            marker.map = viewMap
            bounds = bounds.includingCoordinate(marker.position)
            
            let img = UIImageView(image: UIImage(named: "map_fill")!.imageWithColor(color: .themeColor))
            img.frame.size = CGSize(width: 30, height: 30)
            
            let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            lbl.text = "\(count)"
            lbl.textColor = .white
            lbl.backgroundColor = .themeColor
            lbl.textAlignment = .center
            lbl.setRound()
            img.addSubview(lbl)
            marker.iconView = img
            //marker.iconView?.addSubview(lbl)
            lbl.center = marker.iconView?.center ?? CGPoint(x: 0, y: 0)
            lbl.center.y = img.frame.size.height/2.5
            
            count += 1
        }
        
        CATransaction.begin()
        CATransaction.setValue(1.0, forKey: kCATransactionAnimationDuration)
        viewMap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20))
        CATransaction.commit()
    }
}
