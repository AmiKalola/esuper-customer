//
//  CurrentTripCell.swift
//  Decagon
//
//  Created by MacPro3 on 17/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

protocol CurrentTripCellDelegate: AnyObject {
    func onClickCancel(cell: CurrentTripCell, sender: UIButton)
}

class CurrentTripCell: UITableViewCell {

    @IBOutlet weak var lblDate: LabelDefault!
    @IBOutlet weak var lblAddress: LabelDefault!
    @IBOutlet weak var lblTripId: LabelDefault!
    @IBOutlet weak var btnCancel: UIButton!
    
    weak var delegate: CurrentTripCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        self.backgroundColor = UIColor.themeViewBackgroundColor
        lblDate.font = FontHelper.textSmall()
        lblTripId.font = FontHelper.textRegular(size: FontHelper.labelRegular)
        lblDate.textColor = UIColor.themeLightTextColor
        btnCancel.setImage(UIImage.init(named: "cancelIcon")?.imageWithColor(color: .themeColor), for: .normal)
    }
    
    func setData(data: Order) {
        lblAddress.text = data.destination_addresses?[0].address ?? ""
        lblDate.text = Utility.relativeDateStringForDate(strDate: Utility.stringToString(strDate: (data.created_at)!, fromFormat: DATE_CONSTANT.DATE_TIME_FORMAT_WEB, toFormat: DATE_CONSTANT.DATE_FORMAT, locale: "en_GB")) as String
        lblTripId.text = "txt_trip_no".localized.replacingOccurrences(of: "****", with: "\(data.unique_id ?? 0)")

        let oderStatus = OrderStatus(rawValue: data.delivery_status ?? 0) ?? .Unknown
        
        btnCancel.isHidden = true
        
        switch oderStatus {
        case OrderStatus.ORDER_READY:
            btnCancel.isHidden = false
            break
        case OrderStatus.WAITING_FOR_DELIVERY_MAN:
            btnCancel.isHidden = false
            break
        case OrderStatus.DELIVERY_MAN_ACCEPTED:
            btnCancel.isHidden = false
            break
        case OrderStatus.DELIVERY_MAN_COMING:
            btnCancel.isHidden = false
            break
        case OrderStatus.DELIVERY_MAN_ARRIVED:
            btnCancel.isHidden = false
            break
        case OrderStatus.NO_DELIVERY_MAN_FOUND:
            btnCancel.isHidden = false
            break
        default:
            btnCancel.isHidden = true
        }
    }

    @IBAction func onClickCancel(_ sender: UIButton) {
        delegate?.onClickCancel(cell: self, sender: sender)
    }
}
