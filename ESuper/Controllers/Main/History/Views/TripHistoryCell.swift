//
//  TripHistoryCell.swift
//  Decagon
//
//  Created by MacPro3 on 17/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class TripHistoryCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: LabelDefault!
    @IBOutlet weak var lblUserName: LabelDefault!
    @IBOutlet weak var lblPrice: LabelDefault!
    @IBOutlet weak var lblTripId: LabelDefault!

    override func awakeFromNib() {
        super.awakeFromNib()
        lblDate.textColor = UIColor.themeLightTextColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        self.backgroundColor = UIColor.themeViewBackgroundColor
        lblDate.font = FontHelper.textSmall()
        lblTripId.font = FontHelper.textRegular(size: FontHelper.labelRegular)
    }

    func setData(data: Order_list) {
        lblUserName.text = data.provider_detail?.name ?? "-"
        lblPrice.text = data.currencyCode + " " + "\((data.total ?? 0).toString())"
        lblDate.text = Utility.relativeDateStringForDate(strDate: Utility.stringToString(strDate: (data.created_at)!, fromFormat: DATE_CONSTANT.DATE_TIME_FORMAT_WEB, toFormat: DATE_CONSTANT.DATE_FORMAT, locale: "en_GB")) as String
        lblTripId.text = "txt_trip_no".localized.replacingOccurrences(of: "****", with: "\(data.uniqueID ?? 0)")
       
    }
}
