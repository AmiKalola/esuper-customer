//
//  TripHistoryAddressCell.swift
//  ESuper
//
//  Created by Rohit on 23/02/24.
//  Copyright © 2024 Elluminati. All rights reserved.
//

import UIKit

class TripHistoryAddressCell: UITableViewCell {
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var imgIcon : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
