//
//  TripHistoryDetailVC.swift
//  Decagon
//
//  Created by MacPro3 on 17/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class TripHistoryDetailVC: BaseVC {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var lblDate: LabelDefault!
    @IBOutlet weak var lblDriverName: LabelDefault!
    @IBOutlet weak var lblPrice: LabelDefault!
    @IBOutlet weak var lblTime: LabelDefault!
    @IBOutlet weak var lblDistance: LabelDefault!
    @IBOutlet weak var lblPickUpAddress: LabelDefault!
    @IBOutlet weak var lblDestinationAddress: LabelDefault!
    
    @IBOutlet weak var imgDriver: UIImageView!
    
    @IBOutlet weak var btnInvoice: CustomBottomButton!
    
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var lblLine2: UILabel!
    @IBOutlet weak var viewInvoice: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var constraintHeightMap: NSLayoutConstraint!
    
    @IBOutlet weak var btnGiveRate: UIButton!
    @IBOutlet weak var tableAddress: UITableView!
    @IBOutlet weak var heightTableAddress: NSLayoutConstraint!
    var polyLinePath = GMSPolyline()
    
    var order: Order?
    var orderResponse: NSDictionary?
    var arrHistory = Order_list()
    var orderId: String = ""
    var fareEstimateValues = FareEstimateValues()
    var arrayInvoice = [ServiceCharge]()
    var delegateBackFromHistoryDetails : BackFromHistoryDetails?
    override func viewDidLoad() {
        super.viewDidLoad()
        localized()
        tableAddress.delegate = self
        tableAddress.dataSource = self
        tableAddress.separatorColor = .clear
        tableAddress.register(cellTypes: [TripHistoryAddressCell.self])
        tableAddress.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        tableAddress.reloadData()
        wsGetOrderDetail()
        
    }
    
    func localized() {
        lblTime.text = "-"//"txt_time_text_value".localized.replacingOccurrences(of: "****", with: "-")
        lblDistance.text = "-"//"TXT_DISTANCE_TEXT_VALUE".localized.replacingOccurrences(of: "****", with: "-")
        btnInvoice.setTitle("TXT_VIEW_INVOICE".localized, for: .normal)
        imgDriver.image = UIImage(named: "profile_placeholder")
        
        lblDate.font = FontHelper.textSmall()
        lblDate.textColor = UIColor.themeLightTextColor
        
        lblDriverName.font = FontHelper.textMedium()
        lblPrice.font = FontHelper.textMedium()
        
        self.setBackBarItem(isNative: false)
        self.setNavigationTitle(title:"txt_trip_history_detail".localized)
        
        lblLine.backgroundColor = UIColor.themeBlackColor
        lblLine2.backgroundColor = UIColor.themeBlackColor
        
        btnGiveRate.setTitle("Give Rate", for: .normal)
        btnGiveRate.setTitleColor(UIColor.themeColor, for: .normal)
        btnGiveRate.titleLabel?.font = FontHelper.textRegular(size: FontHelper.labelRegular)
        btnGiveRate.isHidden = true
        
        imgDriver.setRound()
        
        self.view.tintColor = UIColor.themeColor
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                do {
                    if let styleURL = Bundle.main.url(forResource: "styleable_map", withExtension: "json") {
                        self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                    } else {
                        NSLog("Unable to find style.json")
                    }
                } catch {
                    NSLog("One or more of the map styles failed to load. \(error)")
                }
            } else {
                self.mapView.mapStyle = .none
            }
        } else {
            self.mapView.mapStyle = .none
        }
    }
    override func updateUIAccordingToTheme() {
        self.localized()
    }
    @IBAction func onClickInvoice(_ sender: UIButton) {
        self.getInvoiceData()
    }
    override func onClickLeftBarItem() {
        self.delegateBackFromHistoryDetails?.backAction()
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func onClickGiveRate(_ sender: UIButton) {
        openFeedbackDialogue()
    }
    
    func wsGetOrderDetail() {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
            [PARAMS.USER_ID:preferenceHelper.UserId,
             PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
             PARAMS.ORDER_ID: orderId]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_ORDER_DETAIL, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                let orderDictionary:NSDictionary = response["order"] as! NSDictionary
                if let order = Order.init(dictionary: orderDictionary) {
                    self.order = order
                    self.tableAddress.reloadData()
                    self.setData()
                }
            }
        }
    }
    
    func setData() {
        guard let order = order else {
            return
        }
        let obj = order.order_payment_detail!
        self.fareEstimateValues = FareEstimateValues()
        if obj.total_base_price! > 0{
            arrayInvoice.append(ServiceCharge(strService: "Base_Price".localized,strCharges:"\(order.country_detail?.currency_sign ?? "")" + String(obj.total_base_price!.toString())))
        }
        if obj.total_distance_price! > 0{
            var disPrice = obj.total_distance_price!
            if obj.total_distance_price! >= obj.total_base_price!{
                disPrice = (obj.total_distance_price! -  obj.total_base_price!)
            }
            if disPrice > 0{
                arrayInvoice.append(ServiceCharge(strService: "Distance_Price".localized,strCharges: "\(order.country_detail?.currency_sign ?? "")" + String(disPrice.toString())))
            }
        }
        if obj.total_time_price! > 0{
            arrayInvoice.append(ServiceCharge(strService: "Time_Price".localized,strCharges: "\(order.country_detail?.currency_sign ?? "")" + String(obj.total_time_price!.toString())))
        }
        if obj.additional_stop_price > 0{
            arrayInvoice.append(ServiceCharge(strService: "Stop_Price".localized,strCharges: "\(order.country_detail?.currency_sign ?? "")" + String(obj.additional_stop_price.toString())))
        }
        if obj.price_formula > 0{
            let fiexed_price = (obj.total_delivery_price! - obj.total_surge_price)
        }
        if self.fareEstimateValues.promo_payment > 0{
            arrayInvoice.append(ServiceCharge(strService: "Promo_Bonus".localized,strCharges:  "\(order.country_detail?.currency_sign ?? "")" + String(self.fareEstimateValues.promo_payment.toString())))
        }
//        if obj.is_min_fare_applied!{
//            arrayInvoice.append(ServiceCharge(strService: "TXT_MINI_FARE".localized,strCharges:  "\u{20B9}" + String(obj.min_fare!)))
//        }
        if obj.service_tax! > 0{
            arrayInvoice.append(ServiceCharge(strService: "Tax \(obj.service_tax!)%".localized,strCharges:  "\(order.country_detail?.currency_sign ?? "")" + String(obj.total_admin_tax_price!)))
        }
        
        lblDriverName.text = order.request_detail?.provider_detail?.name ?? "txt_no_driver".localized
        if arrHistory.order_status == OrderStatus.CANCELED_BY_USER.rawValue {
            lblDriverName.text = "Cancel by user"
        }
        if arrHistory.order_status == OrderStatus.CANCELED_BY_ADMIN.rawValue{
            lblDriverName.text =  "Cancel by admin"
        }
        self.imgDriver.downloadedFrom(link: order.request_detail?.provider_detail?.image_url ?? "", placeHolder: "profile_placeholder",isFromCache: false,isIndicator: true)
        let strPrice: String = {
            if (order.country_detail?.currency_sign ?? "").count > 0 {
                return (order.country_detail?.currency_sign ?? "") + "\(order.total ?? 0)"
            }
            return "\(order.total ?? 0)"
        }()
        lblPrice.text = strPrice
        let pickUpaddress = order.cartDetail?.pickupAddresses[0]
        let destinationAddress = order.cartDetail?.destinationAddresses[(order.cartDetail?.destinationAddresses.count)! - 1]
        
        lblPickUpAddress.text = pickUpaddress?.address ?? ""
        lblDestinationAddress.text = destinationAddress?.address ?? ""
        
        let strDistanceUnit: String = {
            if order.order_payment_detail?.is_distance_unit_mile ?? false {
                return "UNIT_MILE".localized
            }
            return "UNIT_KM".localized
        }()
        
        lblTime.text = (order.order_payment_detail?.total_time?.toString()  ?? "-") + " " + "\("UNIT_MIN".localized)"//"txt_time_text_value".localized.replacingOccurrences(of: "****", with: order.order_payment_detail?.total_time?.toString()  ?? "-") + " " + "\("UNIT_MIN".localized)"
        lblDistance.text = (order.order_payment_detail?.total_distance?.toString() ?? "-") + " " + "\("\(strDistanceUnit)")"//"TXT_DISTANCE_TEXT_VALUE".localized.replacingOccurrences(of: "****", with: order.order_payment_detail?.total_distance?.toString() ?? "-") + " " + "\("\(strDistanceUnit)")"
        
        lblDate.text = Utility.stringToString(strDate: (order.created_at)!, fromFormat: DATE_CONSTANT.DATE_TIME_FORMAT_WEB, toFormat: DATE_CONSTANT.DATE_TIME_FORMAT_HISTORY, locale: "en_GB")
        
        if (order.is_user_rated_to_provider ?? false) {
            btnGiveRate.isHidden = false
            btnGiveRate.setTitle(" \(order.request_detail?.provider_detail?.user_rate ?? 0) ", for: .normal)
            btnGiveRate.setImage(UIImage(named: "star_feedback"), for: .normal)
            btnGiveRate.contentEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            btnGiveRate.isUserInteractionEnabled = false
        } else {
            btnGiveRate.isHidden = false
            btnGiveRate.isUserInteractionEnabled = true
        }
        
        if order.request_detail?.provider_detail == nil {
            btnGiveRate.isHidden = true
        }
 
        showMarkers()
//        showPath()
        self.wsDrawPath()
        if arrHistory.order_status == OrderStatus.CANCELED_BY_ADMIN.rawValue || arrHistory.order_status == OrderStatus.CANCELED_BY_USER.rawValue {
            constraintHeightMap.constant = 0
            lblDriverName.textColor = UIColor.red
            mapView.isHidden = true
            viewInvoice.isHidden = true
        }else{
            if (self.view.frame.height - viewTop.frame.height) > 300{
                constraintHeightMap.constant = (self.view.frame.height - viewTop.frame.height)
            }else{
                constraintHeightMap.constant = 300
            }
        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        heightTableAddress.constant = tableAddress.contentSize.height
    }
    func showMarkers() {
        guard let order = order else {
            return
        }
        mapView.clear()
        let pickUpaddress = order.cartDetail?.pickupAddresses[0]
        let destinationAddress = order.cartDetail?.destinationAddresses[(order.cartDetail?.destinationAddresses.count)! - 1]
        
        let pickUpLat = pickUpaddress?.location[0] ?? 0
        let pickUpLong = pickUpaddress?.location[1] ?? 0
        
        let destiUpLat = destinationAddress?.location[0] ?? 0
        let destiUpLong = destinationAddress?.location[1] ?? 0

        var arrMarker = [GMSMarker]()
        
        if pickUpLat != 0 && pickUpLong != 0 {
            let pickUpCoordinate = CLLocationCoordinate2D(latitude: pickUpLat, longitude: pickUpLong)
            let pickupMarker = GMSMarker(position: pickUpCoordinate)
            pickupMarker.icon = UIImage.init(named: "icons8-radio-button-96")
//            pickupMarker.icon = UIImage.init(named: "asset-pin-pickup-location")
            pickupMarker.map = mapView
            arrMarker.append(pickupMarker)
        }
        
        if destiUpLat != 0 && destiUpLong != 0 {
            let destinationCoordinate = CLLocationCoordinate2D(latitude: destiUpLat, longitude: destiUpLong)
            let destinationMarker = GMSMarker(position: destinationCoordinate)
//            destinationMarker.icon = UIImage.init(named: "asset-pin-destination-location")
            destinationMarker.icon = UIImage.init(named: "icons8-square-96")
            destinationMarker.map = mapView
            arrMarker.append(destinationMarker)
        }
        var bounds = GMSCoordinateBounds()
        
        var count = 1
        let destination = self.order?.destination_addresses
        
        for arrAddress in destination!{
            if count != destination?.count{
                let marker = GMSMarker.init(position: CLLocationCoordinate2D(latitude: arrAddress.location[0] , longitude: arrAddress.location[1]))
                marker.map = mapView
                bounds = bounds.includingCoordinate(marker.position)
                
                let img = UIImageView(image: UIImage(named: "map_fill")!.imageWithColor(color: .themeColor))
                img.frame.size = CGSize(width: 30, height: 30)
                
                let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
                lbl.text = "\(count)"
                lbl.textColor = .white
                lbl.backgroundColor = .themeColor
                lbl.textAlignment = .center
                lbl.setRound()
                img.addSubview(lbl)
                marker.iconView = img
                //marker.iconView?.addSubview(lbl)
                lbl.center = marker.iconView?.center ?? CGPoint(x: 0, y: 0)
                lbl.center.y = img.frame.size.height/2.5
                
                count += 1
            }
        }
        
        
        for obj in arrMarker {
            bounds = bounds.includingCoordinate(obj.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        mapView.animate(with: update)
    }
    
    func showPath() {
        guard let order = order else {
            return
        }
        let animationPolyline = GMSPolyline()
        let animationPath = GMSMutablePath()
        let arrCordinate = order.trip_location_details?.start_trip_to_end_trip_locations ?? []
        for obj in arrCordinate {
            animationPath.add(CLLocationCoordinate2D(latitude: obj[0], longitude: obj[1]))
        }
        animationPolyline.path = animationPath
        animationPolyline.strokeColor = UIColor.black
        animationPolyline.strokeWidth = 3
        animationPolyline.map = self.mapView
    }
    
    func getInvoiceData() {
        guard let order = order else {
            return
        }
        guard let orderPayment = OrderPayment.init(dictionary: order.order_payment_detail?.dictionaryRepresentation() ?? [:]) else { return }
        var isShowPromoApply: Bool = false
        if orderPayment.promo_payment! > 0.0 {
            isShowPromoApply = true
        }
        let arrForInvoice = NSMutableArray()
        let currency = order.country_detail?.currency_sign ?? ""
        Parser.parseInvoice(orderPayment, toArray: arrForInvoice, currency: currency, isTaxIncluded: true, isShowPromo: isShowPromoApply, deliveryType: order.delivery_type ?? DeliveryType.taxi, completetion: { (result) in
            if result {
                var arr = [InvoicePopUpData]()
                for obj in (arrForInvoice as! [Invoice]) {
                    let title: String = {
                        if (obj.subTitle ?? "").count > 0 {
                            return (obj.title ?? "") + " " + "(\((obj.subTitle ?? "")))"
                        }
                        return obj.title ?? ""
                    }()
                    if let type = InvoiceCellType(rawValue: obj.type) {
                        arr.append(InvoicePopUpData(title: title, value: obj.price ?? "", type: type))
                    } else {
                        arr.append(InvoicePopUpData(title: title, value: obj.price ?? "", type: .Regular))
                    }
                }
                let obj = order.order_payment_detail!
                
                var priceFormula = ""
                
                switch obj.price_formula {
                case 1:
                    priceFormula = "Zone Trip"
                    break
                case 2:
                    priceFormula = "Airport Trip"
                    break
                case 3:
                    priceFormula = "City Trip"
                    break
                default:
                    break
                }
                var isPriceFormula = false
                if  priceFormula != ""{
                    isPriceFormula = true
                }
                let dailog = InvoicePopUp.showInvoicePopUp(title: "TXT_INVOICE".localized, strRight: "TXT_PLACEORDER".localized, currency : currency, isHidePromo: !isShowPromoApply, isPromoEdit: false, strPromo: orderPayment.promo_code_name, arrInvoice: arr, totalPrice: order.order_payment_detail?.user_pay_payment ?? 0, isButtonDone: true,isPriceFormula: isPriceFormula,priceFormula: priceFormula)
                
                dailog.onClickLeftButton = {
                    dailog.removeFromSuperview()
                }
                
                dailog.onClickApplyPromoCode = { obj in
                    dailog.removeFromSuperview()
                }
            }
        })
    }
    
    func openFeedbackDialogue()  {
        guard let order = order else {
            return
        }
        let providerName = self.order?.request_detail?.provider_detail?.name ?? ""
        let dialogForFeedback = DailogForFeedback.showCustomFeedbackDialog(true, false, order._id ?? "", name: providerName, needCallBack: true)
        dialogForFeedback.onClickApplyButton = { [weak self]
            (obj) in
            guard let self = self else { return }
            dialogForFeedback.removeFromSuperview()
            self.wsRateToProvider(obj: obj)
        }
    }
    
    func wsRateToProvider(obj: DailogForFeedback) {
        var review:String = obj.txtComment.text ?? ""
        if review.compare("TXT_COMMENT_PLACEHOLDER".localized) == .orderedSame {
            review = ""
        }
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
            [PARAMS.USER_ID:preferenceHelper.UserId,
             PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
             PARAMS.ORDER_ID:orderId,
             PARAMS.USER_REVIEW_TO_PROVIDER:review,
             PARAMS.USER_RATING_TO_PROVIDER:obj.rate
        ]
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_USER_RATE_TO_PROVIDER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [weak self] (response, error) -> (Void) in
            Utility.hideLoading()
            guard let self = self else { return }
            if (Parser.isSuccess(response: response,withSuccessToast:true, andErrorToast: true)) {
                self.wsGetOrderDetail()
            }
        }
    }
    
    func wsDrawPath() {
        self.showMarkers()
        let pickUpaddress = order!.cartDetail?.pickupAddresses[0]
        let destinationAddress = order!.cartDetail?.destinationAddresses[0]
    
        let pickUpLat = pickUpaddress?.location[0] ?? 0
        let pickUpLong = pickUpaddress?.location[1] ?? 0
        
        let destiUpLat = destinationAddress?.location[0] ?? 0
        let destiUpLong = destinationAddress?.location[1] ?? 0
        
        let saddr = "\(pickUpLat),\(pickUpLong)"
        let daddr = "\(destiUpLat),\(destiUpLong)"
        

        var wayPoint: [String] = []
        wayPoint.append("\(pickUpLat),\(pickUpLong)")
        for i in 0..<(order!.cartDetail?.destinationAddresses.count)! {
            let obj = order!.cartDetail?.destinationAddresses[i]
            wayPoint.append("\(obj!.location[0]),\(obj!.location[1])")
        }
        var url = Google.DIRECTION_URL + "\(saddr)&destination=\(daddr)&waypoints=optimize:false|\(wayPoint.joined(separator: "|"))&key=\(preferenceHelper.CustomerAppGoogleDirectionMatrixKey)"
        //        let url = Google.DIRECTION_URL +  "\(saddr)&destination=\(daddr)&key=\(preferenceHelper.CustomerAppGoogleDirectionMatrixKey)"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        guard let url =  URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? url) else {
            return
        }
        do {
            DispatchQueue.main.async {  /*[unowned self, unowned session] in*/
                session.dataTask(with: url) { [self] (data, response, error) in
                    guard let data = data else {
                        return
                    }
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
                            ////print(error?.localizedDescription as Any)
                            return
                        }
//                        self.googlePathResponse = json
                            //print(json)
//                        self.isPathDraw = true
                        if let routes = json["routes"] as? [[String:Any]] {
                            if let rout = routes.first {
                                if let overview_polyline = rout["overview_polyline"] as? [String:Any] {
                                    if let points = overview_polyline["points"] as? String {
                                        self.drawPath(with: points)
                                    }
                                }
                            }
                        }
                    } catch let error {
                        printE(session)
                        printE("Failed to draw ",error.localizedDescription)
                    }
                }.resume()
            }
        }
    }
    
    private func drawPath(with points : String) {
        
        DispatchQueue.main.async { [weak self] in
            if self?.polyLinePath.map != nil {
                self?.polyLinePath.map = nil
            }
            if let path = GMSPath(fromEncodedPath: points) {
                self?.polyLinePath = GMSPolyline(path: path)
                self?.polyLinePath.strokeColor = UIColor.themePoliline
                self?.polyLinePath.strokeWidth = 5.0
                self?.polyLinePath.geodesic = true
                self?.polyLinePath.map = self?.mapView
            }
        }
    }
}
extension TripHistoryDetailVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var counts = (self.order?.destination_addresses?.count) ?? 0
        if counts > 0{
            return (counts - 1)
        }
        return counts
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripHistoryAddressCell", for: indexPath) as! TripHistoryAddressCell
        let address = self.order!.destination_addresses![indexPath.row]
        cell.lblAddress.text = address.address
        cell.selectionStyle = .none
        return cell
    }
}
extension TripHistoryDetailVC : BackFromHistoryDetails{
    func backAction() {
        OrderVC().backFromDetailsScreen()
    }
}
