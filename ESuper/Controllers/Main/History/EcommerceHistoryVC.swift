//
//  AppointmentHistoryVC.swift
//  Decagon
//
//  Created by MacPro3 on 17/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class EcommerceHistoryVC: UIViewController {

    @IBOutlet weak var lblCurrentOrder: LabelDefault!
    @IBOutlet weak var lblHistory: LabelDefault!
    @IBOutlet var tblCurrent: UITableView!
    @IBOutlet var tblHistory: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    
    @IBOutlet weak var heightTableCurrent: NSLayoutConstraint!
    @IBOutlet weak var heightTableHistory: NSLayoutConstraint!
    
    @IBOutlet weak var viewCurrent: ViewDefault!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var strFromDate = ""
    var strToDate = ""
    
    var arrCurrent: [Order] = []
    var arrHistory: [Order_list] = []
    var isTrackOrder = false
    var tableViewHeightCurrent: CGFloat {
        tblCurrent.layoutIfNeeded()
        return tblCurrent.contentSize.height
    }
    
    var tableViewHeightHistory: CGFloat {
        tblHistory.layoutIfNeeded()
        return tblHistory.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalized()
        setTableView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightTableCurrent.constant = tableViewHeightCurrent
        heightTableHistory.constant = tableViewHeightHistory
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblCurrent.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        tblHistory.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
//        wsGetOrders()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        heightTableCurrent.constant = tblCurrent.contentSize.height
        heightTableHistory.constant = tblHistory.contentSize.height
    }
    
    func setLocalized() {
        lblCurrentOrder.text = "TXT_CURRENT_ORDER".localized
        lblHistory.text = "TXT_ORDER_HISTORY".localized
        
        lblCurrentOrder.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        lblHistory.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        
        self.view.tintColor = UIColor.themeColor
    }
    
    func setTableView() {
        tblCurrent.dataSource = self
        tblCurrent.delegate = self
        //tblCurrentTrip.separatorColor = .clear
        tblCurrent.register(UINib(nibName: "AppointmentHistoryCell", bundle: nil), forCellReuseIdentifier: "AppointmentHistoryCell")
        
        tblHistory.dataSource = self
        tblHistory.delegate = self
        //tblHistory.separatorColor = .clear
        tblHistory.register(UINib(nibName: "AppointmentHistoryCell", bundle: nil), forCellReuseIdentifier: "AppointmentHistoryCell")
    }
    func wsGetOrder(orders : [Order] ) {
        self.arrCurrent = orders
        for obj in arrCurrent{
//                print(obj.is_user_pick_up_order!)
        }
        if self.arrCurrent.count > 0 {
            self.viewCurrent.isHidden = false
            self.imgEmpty.isHidden = true
            self.scrollView.isHidden = false
        }
        else {
            self.viewCurrent.isHidden = true
        }
        self.tblCurrent.reloadData()
        if self.arrCurrent.count > 0 {
            if isTrackOrder{
                self.isTrackOrder = false
                let index = IndexPath(row: 0, section: 0)
                self.gotoCurrentOrder(indexPath: index)
            }
        }
    }

    func historyUpdate(history : [Order_list]) {
        self.arrHistory = history
        if self.arrHistory.count == 0 {
            self.tblHistory.isHidden = true
        }else{
            self.tblHistory.isHidden = false
        }
        if self.arrHistory.count == 0 && self.arrCurrent.count == 0 {
            self.tblHistory.isHidden = true
            self.viewCurrent.isHidden = true
            self.imgEmpty.isHidden = false
            self.scrollView.isHidden = true
        } else if self.arrHistory.count > 0 {
            self.tblHistory.isHidden = false
            self.imgEmpty.isHidden = true
            self.scrollView.isHidden = false
        } else if self.arrHistory.count == 0 {
            self.tblHistory.isHidden = true
        }
        self.tblHistory.reloadData()

    }
    func wsGetHistory(startDate: String,endDate: String) {
        OrderVC().getHistoryData(startDate: startDate, endDate: endDate) { history in
            if history.count > 0 {
                let sortedArray = history.sorted{ $0.created_at! > $1.created_at! }
                let orderHistory = sortedArray.filter({$0.delivery_type == DeliveryType.ecommerce})
                self.historyUpdate(history: orderHistory)
            }
        }
    }
    
    @IBAction func onClickDate(_ sender: UIButton) {
        let dialogueFilterHistory = DailogForFilter.showCustomFilterDialog()
        dialogueFilterHistory.onClickApplyButton = {
            (strFromDate,strToDate) in
            dialogueFilterHistory.removeFromSuperview()
            if (strFromDate.isEmpty() || strToDate.isEmpty()) {
                Utility.showToast(message: "MSG_PLEASE_SELECT_DATE_FIRST".localized)
            }else {
                self.strFromDate = strFromDate
                self.strToDate = strToDate
                self.wsGetHistory(startDate: strFromDate, endDate: strToDate)
            }
        }
        dialogueFilterHistory.onClickResetButton = {
            dialogueFilterHistory.removeFromSuperview()
            self.strToDate = ""
            self.strFromDate = ""
            self.wsGetHistory(startDate: self.strFromDate, endDate: self.strToDate)
        }
    }
}

extension EcommerceHistoryVC: UITableViewDelegate, UITableViewDataSource, CurrentTripCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCurrent {
            return arrCurrent.count
        }
        return arrHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblCurrent {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentHistoryCell", for: indexPath) as! AppointmentHistoryCell
            cell.selectionStyle = .none
            let obj = arrCurrent[indexPath.row]
            cell.lblName.isHidden = true
            cell.setCellData(cellItem: obj,isEcommerce: true)
            if obj.order_change ?? false {
                cell.mainView.backgroundColor = .clear
                cell.backgroundColor = UIColor.themeRedAlphaBackground
                cell.contentView.backgroundColor = UIColor.themeRedAlphaBackground
            } else {
                cell.mainView.backgroundColor = UIColor.themeViewBackgroundColor
                cell.backgroundColor = UIColor.themeViewBackgroundColor
                cell.contentView.backgroundColor = UIColor.themeViewBackgroundColor
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentHistoryCell", for: indexPath) as! AppointmentHistoryCell
        cell.selectionStyle = .none
        let obj = arrHistory[indexPath.row]
        cell.setHistoryData(dictData: obj,isEcommerce: true)
        if obj.order_status == 113 {
            cell.lblStatus.text =  obj.cancel_reason
        }
        cell.lblName.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblCurrent {
            self.gotoCurrentOrder(indexPath: indexPath)
        } else if tableView == tblHistory {
            let dict = arrHistory[indexPath.row]
            currentBooking.selectedOrderId = dict._id
            let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "HistoryDetailVC") as! HistoryDetailVC
            vc.strOrderID = currentBooking.selectedOrderId ?? ""
            vc.deliveryType = dict.delivery_type ?? 0
            vc.isAppointment = true
            vc.delegateBackFromHistoryDetails = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
            
    func onClickCancel(cell: CurrentTripCell, sender: UIButton) {
        if let index = tblCurrent.indexPath(for: cell) {
        }
    }
    func gotoCurrentOrder(indexPath : IndexPath){
        let currentSelectedOrder = arrCurrent[indexPath.row]
        currentBooking.selectedOrderId = currentSelectedOrder._id
        let vc = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "orderStatusVC") as! OrderStatusVC
        vc.selectedOrder = currentSelectedOrder
        vc.isAppointment = true
        vc.delegateBackFromHistoryDetails = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension EcommerceHistoryVC : BackFromHistoryDetails{
    func backAction() {
//        OrderVC().backFromDetailsScreen()
        self.wsGetOrders()
        self.wsGetHistory(startDate: "", endDate: "")
    }
    func wsGetOrders() {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_ORDER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            let arrForHistory = Parser.parseOrders(response).filter({$0.delivery_type == DeliveryType.ecommerce})
            self.wsGetOrder(orders: arrForHistory)
        }
    }
}
