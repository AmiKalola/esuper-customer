//
//  TripHistoryVC.swift
//  Decagon
//
//  Created by MacPro3 on 17/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class TripHistoryVC: UIViewController {

    @IBOutlet weak var lblCurrentTrip: LabelDefault!
    @IBOutlet weak var lblTripHistory: LabelDefault!
    @IBOutlet var tblCurrentTrip: UITableView!
    @IBOutlet var tblTripHistory: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    
    @IBOutlet weak var heightTableCurrent: NSLayoutConstraint!
    @IBOutlet weak var heightTableHistory: NSLayoutConstraint!
    
    @IBOutlet var viewCurrent: ViewDefault!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var arrCurrent: [Order] = []
    var arrHistory: [Order_list] = []
    
    var strFromDate = ""
    var strToDate = ""
    var isTrackOrder = false
    var arrReason = ["txt_trip_cancel_reason_1".localized, "txt_trip_cancel_reason_2".localized, "txt_trip_cancel_reason_3".localized]
    
    var tableViewHeightCurrent: CGFloat {
        tblCurrentTrip.layoutIfNeeded()
        return tblCurrentTrip.contentSize.height
    }
    
    var tableViewHeightHistory: CGFloat {
        tblTripHistory.layoutIfNeeded()
        return tblTripHistory.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalized()
        setTableView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightTableCurrent.constant = tableViewHeightCurrent
        heightTableHistory.constant = tableViewHeightHistory
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblCurrentTrip.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        tblTripHistory.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        heightTableCurrent.constant = tblCurrentTrip.contentSize.height
        heightTableHistory.constant = tblTripHistory.contentSize.height
    }
    
    func setLocalized() {
        lblCurrentTrip.text = "txt_my_bookings".localized
        lblTripHistory.text = "txt_trip_history".localized
        
        lblCurrentTrip.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        lblTripHistory.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        
        self.view.tintColor = UIColor.themeColor
    }
    
    func setTableView() {
        tblCurrentTrip.dataSource = self
        tblCurrentTrip.delegate = self
        //tblCurrentTrip.separatorColor = .clear
        tblCurrentTrip.register(UINib(nibName: "CurrentTripCell", bundle: nil), forCellReuseIdentifier: "CurrentTripCell")
        
        tblTripHistory.dataSource = self
        tblTripHistory.delegate = self
        //tblTripHistory.separatorColor = .clear
        tblTripHistory.register(UINib(nibName: "TripHistoryCell", bundle: nil), forCellReuseIdentifier: "TripHistoryCell")
    }
    func wsGetOrder(orders : [Order] ) {
        self.arrCurrent = orders
        if self.arrCurrent.count > 0 {
            self.viewCurrent.isHidden = false
            self.imgEmpty.isHidden = true
            self.scrollView.isHidden = false
        }
        else {
            self.viewCurrent.isHidden = true
        }
        self.tblCurrentTrip.reloadData()
    }

    func historyUpdate(history : [Order_list]) {
        self.arrHistory = history
        if self.arrHistory.count == 0 && self.arrCurrent.count == 0 {
            self.tblTripHistory.isHidden = true
            self.viewCurrent.isHidden = true
            self.imgEmpty.isHidden = false
            self.scrollView.isHidden = true
        } else if self.arrHistory.count > 0 {
            self.tblTripHistory.isHidden = false
            self.imgEmpty.isHidden = true
            self.scrollView.isHidden = false
        } else if self.arrHistory.count == 0 {
            self.tblTripHistory.isHidden = true
        }
        self.tblTripHistory.reloadData()
    }
    func wsGetHistory(startDate: String,endDate: String) {
        OrderVC().getHistoryData(startDate: startDate, endDate: endDate) { history in
            if history.count > 0 {
                let sortedArray = history.sorted{ $0.created_at! > $1.created_at! }
                let orderHistory = sortedArray.filter({$0.delivery_type == DeliveryType.taxi})
                self.historyUpdate(history: orderHistory)
            }
        }
    }

    @IBAction func onClickDate(_ sender: UIButton) {
        let dialogueFilterHistory = DailogForFilter.showCustomFilterDialog()
        dialogueFilterHistory.onClickApplyButton = {
            (strFromDate,strToDate) in
            dialogueFilterHistory.removeFromSuperview()
            if (strFromDate.isEmpty() || strToDate.isEmpty()) {
                Utility.showToast(message: "MSG_PLEASE_SELECT_DATE_FIRST".localized)
            }else {
                //(self.parent as! OrderVC).onClickRightButton()
                self.strFromDate = strFromDate
                self.strToDate = strToDate
                self.wsGetHistory(startDate: strFromDate, endDate: strToDate)
            }
        }
        
        dialogueFilterHistory.onClickResetButton = {
            dialogueFilterHistory.removeFromSuperview()
            self.strToDate = ""
            self.strFromDate = ""
            //(self.parent as! OrderVC).onClickRightButton()
            self.wsGetHistory(startDate: self.strFromDate, endDate: self.strToDate)
        }
    }
 
}

extension TripHistoryVC: UITableViewDelegate, UITableViewDataSource, CurrentTripCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCurrentTrip {
            return arrCurrent.count
        }
        return arrHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblCurrentTrip {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentTripCell", for: indexPath) as! CurrentTripCell
            cell.selectionStyle = .none
            cell.delegate = self
            let obj = arrCurrent[indexPath.row]
            cell.setData(data: obj)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripHistoryCell", for: indexPath) as! TripHistoryCell
        cell.selectionStyle = .none
        let obj = arrHistory[indexPath.row]
        print("obj.order_status:- \(obj.order_status)")
        cell.setData(data: obj)
        if obj.order_status == OrderStatus.CANCELED_BY_USER.rawValue {
            cell.lblUserName.text = "Cancel by user"
            cell.lblUserName.textColor = UIColor.red
            cell.lblUserName.font = FontHelper.textRegular(size:11)
        }
        if obj.order_status == OrderStatus.CANCELED_BY_ADMIN.rawValue{
            cell.lblUserName.text = "Cancel by admin"
            cell.lblUserName.textColor = UIColor.red
            cell.lblUserName.font = FontHelper.textRegular(size:11)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblTripHistory {
            let vc  = UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "TripHistoryDetailVC") as! TripHistoryDetailVC
            let obj = arrHistory[indexPath.row]
            vc.arrHistory = obj
            vc.delegateBackFromHistoryDetails = self
            vc.orderId = obj._id ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        } else if tableView == tblCurrentTrip {
            let vc = UIStoryboard(name: "Eber", bundle: nil).instantiateViewController(withIdentifier: "RideHomeVC") as! RideHomeVC
            let obj = arrCurrent[indexPath.row]
            vc.delegateBackFromHistoryDetails = self
            vc.orderId = obj._id ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func onClickCancel(cell: CurrentTripCell, sender: UIButton) {
        if let index = tblCurrentTrip.indexPath(for: cell) {
            let obj = arrCurrent[index.row]
            wsGetCancellationCharge(obj: obj)
        }
    }
    
    func wsGetCancellationCharge(obj: Order) {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
            [PARAMS.USER_ID:preferenceHelper.UserId,
             PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
             PARAMS.ORDER_ID:obj._id ?? "",
             PARAMS.DELIVERY_TYPE : DeliveryType.taxi
            ]

        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_CANCELLATION_CHARGES, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                self.wsGetCancelReasonList(cancellationCharge: response[PARAMS.CANCELLATION_CHARGE] as? Double ?? 0, obj: obj)
            }
        }
    }

    func wsGetCancelReasonList(cancellationCharge: Double, obj: Order) {
        
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.DELIVERY_TYPE : DeliveryType.taxi,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken]
        
        Utility.showLoading()
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_CANCEL_REASON_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [weak self] (response, error) -> (Void) in
            guard let self = self else { return }
            Utility.hideLoading()
            var arrList = [String]()
            if Parser.isSuccess(response: response) {
//                print(response)
                if let list = response["reasons"] as? [String] {
                    arrList.append(contentsOf: list)
                }
            }
            self.openCancelOrderDialog(list: arrList, cancelcharge: cancellationCharge, obj: obj)
        }
    }
    
    func openCancelOrderDialog(list: [String], cancelcharge: Double, obj: Order) {
        let arrList: [String] = {
            if list.count > 0 {
                return list
            }
           return arrReason
        }()
        var cancellationCharge:String = ""
        if cancelcharge > 0.0 {
            cancellationCharge = currentBooking.currency + " " + cancelcharge.toString()
        } else {
            cancellationCharge = ""
        }
        let dialogForCancelOrder = CustomCancelOrderDialog.showCustomCancelOrderDialog(title: "txt_trip_cancel".localized, message: "", cancelationCharge: cancellationCharge, deliveryType: DeliveryType.taxi, titleLeftButton: "TXT_CANCEL".localizedCapitalized, titleRightButton: "TXT_OK".localizedCapitalized, list: arrList)
        dialogForCancelOrder.onClickLeftButton = {
            dialogForCancelOrder.removeFromSuperview()
        }
        dialogForCancelOrder.onClickRightButton = { [weak self] (cancelReason:String) in
            guard let self = self else { return }
            self.wsCancelOrder(reason: cancelReason, obj: obj)
            dialogForCancelOrder.removeFromSuperview()
        }
    }
    
    func wsCancelOrder(reason:String, obj: Order) {
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
         PARAMS.ORDER_ID: obj._id ?? "",
         PARAMS.ORDER_STATUS:OrderStatus.CANCELED_BY_USER.rawValue,
         PARAMS.CANCEL_REASON:reason
        ]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_CANCEL_TRIP, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [weak self] (response, error) -> (Void) in
            guard let self = self else { return }
            if (Parser.isSuccess(response: response)) {
                OrderVC().wsGetOrders()
            }
        }
    }
}
extension TripHistoryVC : BackFromHistoryDetails{
    func backAction() {
        self.wsGetOrders()
        self.wsGetHistory(startDate: "", endDate: "")
    }
    func wsGetOrders() {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken]
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_ORDER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            let arrForHistory = Parser.parseOrders(response).filter({$0.delivery_type == DeliveryType.taxi})
            self.wsGetOrder(orders: arrForHistory)
        }
    }
}
