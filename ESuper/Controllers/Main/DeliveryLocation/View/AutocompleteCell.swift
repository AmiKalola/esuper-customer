//
//  AutocompleteCell.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit


class AutocompleteCell: CustomTableCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var viewForAutocomplete: UIView!
    @IBOutlet weak var lblDivider: UILabel!
    @IBOutlet weak var imgMapIcon: UIImageView!
    //MARK:- LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        setLocalization()
    }
    
    //MARK:- SET CELL DATA
    func setCellData(place:(title:String,subTitle:String,address: String)) {
        lblTitle.text = place.title
        lblSubTitle.text = place.subTitle
    }
    
    func setLocalization() {
       
        //Colors
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        viewForAutocomplete.backgroundColor = UIColor.themeViewBackgroundColor
        lblTitle.textColor = UIColor.themeTextColor
        lblSubTitle.textColor = UIColor.themeLightTextColor
        lblDivider.textColor = UIColor.themeLightLineColor
        
        /*Set Font*/
        lblTitle.font =  FontHelper.textRegular(size: 14)
        lblSubTitle.font =  FontHelper.tiny()
        imgMapIcon.image = UIImage(named: "map")?.imageWithColor(color: UIColor.themeTitleColor)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func updateUIAccordingToTheme() {
        imgMapIcon.image = UIImage(named: "map")?.imageWithColor(color: UIColor.themeTitleColor)
        
    }
}
