//
//  OrderItemCell.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit


//Mark: Class For Item Cell
class OrderItemCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        self.lblName.textColor = UIColor.themeTextColor
        self.lblPrice.textColor = UIColor.themeTextColor
        self.lblDescription.textColor = UIColor.themeLightTextColor
        self.lblName.font = FontHelper.textMedium(size: FontHelper.labelRegular)
        self.lblPrice.font = FontHelper.textMedium(size: FontHelper.labelRegular)
        self.lblDescription.font = FontHelper.textSmall()
        self.imgCell.setRound(withBorderColor: .clear, andCornerRadious: 8.0, borderWidth: 1.0)
        self.imgCell.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCellData(item:ProductItemsItem,parent:OrderProductVC) {
        self.lblName.text = item.name
        self.lblPrice.text =  (item.price).toCurrencyString()
        self.lblDescription.text = item.details
        
        if item.price! > 0.0 {
            lblPrice.text =  (item.price!).toCurrencyString()
        }else {
            var price:Double = 0.0
            for specification in item.specifications! {
                for listItem in specification.list! {
                    if listItem.is_default_selected
                    {
                        price = price + ((listItem.price) ?? 0.0)
                    }
                }
            }
            lblPrice.text =  price.toCurrencyString()
        }
        
        if item.image_url!.isEmpty {
            self.imgCell.isHidden = true
        }else {
    
        }
    }
}
