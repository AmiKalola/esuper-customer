//
//  HomeVC.swift
//  
//
//  Created by Elluminati on 14/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import UIKit

enum OrderFilter {
    case all
    case courier
    case store
    case tableBook
}

class OrderVC: BaseVC,RightDelegate, LeftDelegate {
    
    //MARK: OutLets
    
    @IBOutlet weak var tabForTopBar: UITabBar!
    @IBOutlet weak var scrollViewForTab: UIScrollView!
    @IBOutlet weak var containerForTripHistory: UIView!
    @IBOutlet weak var containerForAppointmentHistory: UIView!
    @IBOutlet weak var containerForEcommerceHistory: UIView!
    @IBOutlet weak var containerForHistory: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var lblCurrentOrders: UILabel!
    
    //MARK: Variables
    var historyVC:HistoryVC? = nil
    var currentOrderVC:CurrentOrderVC? = nil
    var tripHistory:TripHistoryVC? = nil
    var serviceHistoryVC:AppointmentHistoryVC? = nil
    var ecommerceHistoryVC:EcommerceHistoryVC? = nil
    var isComeFromCompleteOrder:Bool = false
    var filterType = OrderFilter.all
    
    var arrOrders = [Order]()
    var arrStoreCourierOrder = [Order]()
    var arrTaxiOrder = [Order]()
    var arrServiceOrder = [Order]()
    var lastIndex = 0
    var isAppointment = false
    var isEcommerce = false
    var isTrackOrder = false
    //MARK: LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegateRight = self
        if isComeFromCompleteOrder {
            self.delegateLeft = self
            self.setBackBarItem(isNative: false)
        }
        else {
            self.setBackBarItem(isNative: true)
        }
        Utility.showLoading()
        scrollViewForTab.delegate = self
        setLocalization()
        self.setNavigationTitle(title:"TXT_HISTORY".localized)
        if isAppointment{
            let pageWidth:CGFloat = scrollViewForTab.frame.size.width * 2
        }
        self.wsGetOrders()
        self.wsGetHistory(startDate: "", endDate: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tabForTopBar.tintColor = UIColor.themeTitleColor
        tabForTopBar.unselectedItemTintColor = UIColor.themeLightGrayColor
        viewForShadow.setShadow(shadowColor: UIColor.lightGray.cgColor, shadowOffset: CGSize.init(width: 0.0, height: 3.0), shadowOpacity: 0.2, shadowRadius: 3.0)
        tabForTopBar.selectionIndicatorImage = APPDELEGATE.getImageWithColorPosition(color: UIColor.themeSectionBackgroundColor, size: CGSize(width:(APPDELEGATE.window?.frame.size.width)!/4,height: 49), lineSize: CGSize(width:(APPDELEGATE.window?.frame.size.width)!/4, height:2))
    }
    
    override func updateUIAccordingToTheme() {
        if isComeFromCompleteOrder {
            self.setBackBarItem(isNative: false)
        }
        else {
            self.setBackBarItem(isNative: true)
        }
    }
    
    func onClickLeftButton(){
        
        if isComeFromCompleteOrder {
            APPDELEGATE.goToMain()
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    //MARK: - Action Method
    func goToPoint(point:CGFloat){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveLinear, animations:
                            {
                self.scrollViewForTab.contentOffset.x = point
                self.view.layoutIfNeeded()
            }, completion: nil)
            
        }
    }
    
    func onClickRightButton() {
        openFilterList()
        self.historyVC?.btnFrom.setTitle("TXT_FROM".localized, for: UIControl.State.normal)
        self.historyVC?.btnTo.setTitle("TXT_TO".localized, for: UIControl.State.normal)
        if (self.historyVC?.viewForFilter.isHidden)! {
            self.historyVC?.viewForFilter.isHidden = false
            //self.setRightBarItemImage(image: UIImage.init(named: "cancel")!)
        }else {
            self.historyVC?.viewForFilter.isHidden = true
            //self.setRightBarItemImage(image: UIImage.init(named: "filter")!)
        }
    }
    
    func openFilterList() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let all = UIAlertAction(title: "TXT_ALL".localized , style: .default, handler: { [unowned self] (alert: UIAlertAction!) -> Void in
            filterType = .all
            reloadFilter()
        })
        alertController.addAction(all)
        
        let courier = UIAlertAction(title: "TXT_COURIER".localized , style: .default, handler: { [unowned self] (alert: UIAlertAction!) -> Void in
            filterType = .courier
            reloadFilter()
        })
        alertController.addAction(courier)
        
        let store = UIAlertAction(title: "TXT_STORE".localized , style: .default, handler: { [unowned self] (alert: UIAlertAction!) -> Void in
            filterType = .store
            reloadFilter()
        })
        alertController.addAction(store)
        
        let tableBook = UIAlertAction(title: "text_table_reservation".localizedCapitalized , style: .default, handler: { [unowned self] (alert: UIAlertAction!) -> Void in
            filterType = .tableBook
            reloadFilter()
        })
        alertController.addAction(tableBook)
        
        let cancel = UIAlertAction(title: "TXT_CANCEL".localized , style: .cancel, handler: { [unowned self] (alert: UIAlertAction!) -> Void in
            
        })
        alertController.addAction(cancel)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func reloadFilter() {
        historyVC?.reloadData(filter: filterType)
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        if isComeFromCompleteOrder {
            APPDELEGATE.goToMain()
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- Navigation Methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier?.compare(SEGUE.SEGUE_TO_HISTORY) == ComparisonResult.orderedSame {
            historyVC = (segue.destination as! HistoryVC)
        }
        else if segue.identifier?.compare(SEGUE.SEGUE_TO_CURRENT_ORDER) == ComparisonResult.orderedSame {
            currentOrderVC = (segue.destination as! CurrentOrderVC)
        }else if segue.identifier?.compare(SEGUE.SEGUE_TO_TRIP_HISTORY) == ComparisonResult.orderedSame {
            tripHistory = (segue.destination as! TripHistoryVC)
        }else if segue.identifier?.compare(SEGUE.SEGUE_TO_SERVICE_HISTORY) == ComparisonResult.orderedSame {
            serviceHistoryVC = (segue.destination as! AppointmentHistoryVC)
        }else if segue.identifier?.compare(SEGUE.SEGUE_TO_ECOMMERCE) == ComparisonResult.orderedSame {
            ecommerceHistoryVC = (segue.destination as! EcommerceHistoryVC)
        }else {
            printE("No Segue performed")
        }
    }
    func setupView(){
        "Order"
    }
    //MARK: Variables
    var arrForCurrentOrders:[Order] =  []
    var arrForHistory:[Order] =  []
    var arrForTrip:[Order] =  []
    var arrForAppointment:[Order] =  []
    var arrForEcommerce:[Order] =  []
    
    var arrHistory: [Order_list] = []
    var arrForCurrentOrdersHistory:[Order_list] =  []
    var arrForHistoryHistory:[Order_list] =  []
    var arrForTripHistory:[Order_list] =  []
    var arrForAppointmentHistory:[Order_list] =  []
    var arrForEcommerceHistory:[Order_list] =  []
    
    func wsGetOrders() {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken]
        
        var ids = 1
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_ORDER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            print("Rkssss\(ids)")
            ids = ids + 1
            self.arrForCurrentOrders = Parser.parseOrders(response)
            self.arrForHistory = Parser.parseOrders(response).filter({$0.delivery_type == DeliveryType.store || $0.delivery_type == DeliveryType.courier || $0.delivery_type == DeliveryType.tableBooking})
            self.arrForTrip = Parser.parseOrders(response).filter({$0.delivery_type == DeliveryType.taxi})
            self.arrForAppointment = Parser.parseOrders(response).filter({$0.delivery_type == DeliveryType.service || $0.delivery_type == DeliveryType.appoinment})
            self.arrForEcommerce = Parser.parseOrders(response).filter({$0.delivery_type == DeliveryType.ecommerce})
            if self.isTrackOrder{
                switch(self.arrForCurrentOrders[0].delivery_type){
                    case DeliveryType.store,DeliveryType.courier,DeliveryType.tableBooking:
                        self.historyVC?.isTrackOrder = self.isTrackOrder
                        break
                    case DeliveryType.service:
                        self.serviceHistoryVC?.isTrackOrder = self.isTrackOrder
                        break
                    case DeliveryType.appoinment:
                        self.serviceHistoryVC?.isTrackOrder = self.isTrackOrder
                        break
                    case DeliveryType.ecommerce:
                        self.ecommerceHistoryVC?.isTrackOrder = self.isTrackOrder
                        break
                    default:
                        break
                }
            }
            self.isTrackOrder = false
            self.historyVC?.wsGetOrder(orders : self.arrForHistory )
            self.tripHistory?.wsGetOrder(orders : self.arrForTrip )
            self.serviceHistoryVC?.wsGetOrder(orders : self.arrForAppointment )
            self.ecommerceHistoryVC?.wsGetOrder(orders : self.arrForEcommerce )
        }
    }
    func wsGetHistory(startDate: String,endDate: String) {
        Utility.showLoading()
        let dictParam:[String:String] =
        [PARAMS.SERVER_TOKEN : preferenceHelper.SessionToken,
         PARAMS.USER_ID : preferenceHelper.UserId,
         PARAMS.START_DATE : startDate,
         PARAMS.END_DATE : endDate]
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_GET_HISTORY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: false) {
                let historyOrderResposnse:OrderHistoryResponse = OrderHistoryResponse.init(dictionary: response)!
                let historyOrderList:[Order_list] = historyOrderResposnse.order_list!
                if historyOrderList.count > 0 {
                    let sortedArray = historyOrderList.sorted{ $0.created_at! > $1.created_at! }
                    
                    self.arrHistory = sortedArray.filter({$0.delivery_type == DeliveryType.store || $0.delivery_type == DeliveryType.tableBooking || $0.delivery_type == DeliveryType.courier})
                    self.arrForHistoryHistory = sortedArray.filter({$0.delivery_type == DeliveryType.store || $0.delivery_type == DeliveryType.tableBooking || $0.delivery_type == DeliveryType.courier})
                    self.arrForTripHistory = sortedArray.filter({$0.delivery_type == DeliveryType.taxi})
                    self.arrForAppointmentHistory = sortedArray.filter({$0.delivery_type == DeliveryType.service || $0.delivery_type == DeliveryType.appoinment})
                    self.arrForEcommerceHistory = sortedArray.filter({$0.delivery_type == DeliveryType.ecommerce})
                    
                    self.historyVC?.historyUpdate(history: self.arrHistory)
                    self.tripHistory?.historyUpdate(history: self.arrForTripHistory)
                    self.serviceHistoryVC?.historyUpdate(history: self.arrForAppointmentHistory)
                    self.ecommerceHistoryVC?.historyUpdate(history: self.arrForEcommerceHistory)
                }
            }
        }
    }
    func getHistoryData(startDate : String, endDate : String, completion:(([Order_list])->())?) {
        var listOrder = [Order_list]()
        Utility.showLoading()
        let dictParam:[String:String] =
        [PARAMS.SERVER_TOKEN : preferenceHelper.SessionToken,
         PARAMS.USER_ID : preferenceHelper.UserId,
         PARAMS.START_DATE : startDate,
         PARAMS.END_DATE : endDate]
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_GET_HISTORY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: false) {
                let historyOrderResposnse:OrderHistoryResponse = OrderHistoryResponse.init(dictionary: response)!
                let historyOrderList:[Order_list] = historyOrderResposnse.order_list!
                if historyOrderList.count > 0 {
                }
                completion?(historyOrderList)
            }
        }
        completion?(listOrder)
    }
}

extension OrderVC: UITabBarDelegate, UIScrollViewDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        tabForTopBar.selectedItem = tabForTopBar.items![item.tag]
        switch(item.tag) {
        case 0:
            self.scrollViewForTab.setContentOffset(self.containerForHistory.frame.origin, animated: true)
            break
        case 1:
            self.scrollViewForTab.setContentOffset(self.containerForTripHistory.frame.origin, animated: true)
            break
        case 2:
            self.scrollViewForTab.setContentOffset(self.containerForAppointmentHistory.frame.origin, animated: true)
            break
        case 3:
            self.scrollViewForTab.setContentOffset(self.containerForEcommerceHistory.frame.origin, animated: true)
            break
        default:
            break
        }
        
        self.containerForHistory.endEditing(true)
        self.containerForTripHistory.endEditing(true)
        self.containerForAppointmentHistory.endEditing(true)
        self.containerForEcommerceHistory.endEditing(true)
        self.view.layoutIfNeeded()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)    {
        if !decelerate {
            let pageWidth:CGFloat = scrollView.frame.size.width
            let fractionalPage:CGFloat = scrollView.contentOffset.x / pageWidth
            let page:Int = lroundf(Float(fractionalPage))
            switch (page) {
            case 0:
                if UIApplication.isRTL() {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![3])
                } else {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![0])
                }
                break
            case 1:
                if UIApplication.isRTL() {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![2])
                } else {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![1])
                }
                break
            case 2:
                if UIApplication.isRTL() {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![1])
                } else {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![2])
                }
            case 3:
                if UIApplication.isRTL() {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![0])
                } else {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![3])
                }
                break
            default:
                break
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth:CGFloat = scrollView.frame.size.width
        let fractionalPage:CGFloat = scrollView.contentOffset.x / pageWidth
        let page:Int = lroundf(Float(fractionalPage))
        
        switch (page) {
        case 0:
            if UIApplication.isRTL() {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![3])
            } else {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![0])
            }
            break
        case 1:
            if UIApplication.isRTL() {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![2])
            }else {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![1])
            }
            break
        case 2:
            if UIApplication.isRTL() {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![1])
            } else {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![2])
            }
            break
        case 3:
            if UIApplication.isRTL() {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![0])
            } else {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![3])
            }
            break
        default:
            break
        }
        lastIndex = page
    }
}

//MARK: - set Localization text and pree launch setup
extension OrderVC{
    func setLocalization() {
        /*set colors*/
        self.view.backgroundColor = UIColor.themeViewBackgroundColor
        viewForShadow.backgroundColor =
            UIColor.clear
        
        let item1 = tabForTopBar.addTabBarItem(title: "TXT_DELIVERIES".localized, imageName: "", selectedImageName: "", tagIndex: 0)

        let item2 = tabForTopBar.addTabBarItem(title: "txt_trip".localized, imageName: "", selectedImageName: "", tagIndex: 1)
        
        let item3 = tabForTopBar.addTabBarItem(title: "txt_appointment".localized, imageName: "", selectedImageName: "", tagIndex: 2)
        
        let item4 = tabForTopBar.addTabBarItem(title: "TXT_ECOMMERCE".localized, imageName: "", selectedImageName: "", tagIndex: 3)
        tabForTopBar.setItems([item1,item2,item3, item4], animated: false)
        tabForTopBar.selectedItem = tabForTopBar.items![0]
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if self.isAppointment{
                self.tabForTopBar.selectedItem = self.tabForTopBar.items![2]
                self.scrollViewForTab.setContentOffset(self.containerForAppointmentHistory.frame.origin, animated: true)
            }else if self.isEcommerce{
                self.tabForTopBar.selectedItem = self.tabForTopBar.items![3]
                self.scrollViewForTab.setContentOffset(self.containerForEcommerceHistory.frame.origin, animated: true)
            }else{
                self.scrollViewForTab.setContentOffset(self.containerForHistory.frame.origin, animated: false)
            }
        }
        lblCurrentOrders.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        lblCurrentOrders.textColor = UIColor.themeTitleColor
        lblCurrentOrders.text = "TXT_CURRENT_ORDER".localized
        self.hideBackButtonTitle()
    }
}
extension OrderVC{
    func backFromDetailsScreen(){
        self.wsGetOrders()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.wsGetHistory(startDate: "", endDate: "")
        }
    }
}
protocol BackFromHistoryDetails{
    func backAction()
}
