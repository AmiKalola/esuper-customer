//
//  ProductSpecificationSection.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit

class ProductSpecificationSection: CustomTableCell {
    
    //MARK: - OUTLET
    @IBOutlet weak var lblSpecificationName: UILabel!
    @IBOutlet weak var lblSpecificationRequired: UILabel!
    @IBOutlet weak var lblSelectionMessage: UILabel!
    
    var isMultipleSelect:Bool = false
    //MARK: - LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        lblSpecificationRequired.font = FontHelper.labelRegular()
        lblSpecificationName.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        lblSpecificationRequired.textColor = UIColor.themeRedBGColor
        lblSpecificationRequired.font = FontHelper.textMedium(size: FontHelper.large)
        lblSelectionMessage.textColor = UIColor.themeLightTextColor
        lblSelectionMessage.font = FontHelper.textRegular()
        lblSpecificationName.textColor = UIColor.themeTitleColor
        lblSpecificationName.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        
    }
    
    //MARK: - SET CELL DATA
    func setData(title:String, isAllowMultipleSelect:Bool, isRequired:Bool,message:String)
    {
        lblSpecificationRequired.text = "TXT_REQUIRED".localized
        lblSpecificationRequired.isHidden = !isRequired
        lblSpecificationName.text = title.appending("")
        lblSpecificationName.sizeToFit()
        lblSpecificationName.textColor = UIColor.themeTitleColor
        lblSpecificationName.backgroundColor = UIColor.clear
        lblSpecificationName.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        self.backgroundColor = UIColor.clear
        isMultipleSelect = isAllowMultipleSelect
        if message.isEmpty() {
            lblSelectionMessage.text = ""
            lblSelectionMessage.isHidden = true
        }else {
            lblSelectionMessage.text = message
            lblSelectionMessage.isHidden = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
