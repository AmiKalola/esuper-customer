//
//  RedeemVC.swift
//  ESuper
//
//  Created by Elluminati mac mini on 09/08/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import UIKit

class RedeemVC: BaseVC {

    @IBOutlet weak var lblRedeem: UILabel!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var vwRedeem: UIView!
    @IBOutlet weak var lblRedeemValue: UILabel!
    @IBOutlet weak var btnRedeem: CustomBottomButton!
    @IBOutlet weak var lblSeperator: CustomSeperator!
    @IBOutlet weak var tblRedeemHistory: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    var arrForRedeemHistory = NSMutableArray()
    var walletCurrencyCode:String = ""
    var totalRedeemPoints: Int = 0
    var userMinimumRedeemPointRequired: Int = 0
    var userRedeemPointValue: Double = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        imgEmpty.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.setBackBarItem(isNative: true)
        self.setLocalization()
        self.tblRedeemHistory.register(UINib(nibName: "RedeemHistoryCell", bundle: nil), forCellReuseIdentifier: "RedeemHistoryCell")
        self.tblRedeemHistory.estimatedRowHeight = 120
        self.tblRedeemHistory.rowHeight = UITableView.automaticDimension
        
        self.wsGetRedeemHistory()
        
    }
    func setLocalization() {
        self.view.backgroundColor = UIColor.themeViewBackgroundColor
        self.view.tintColor = UIColor.themeColor
        title = "TXT_REWARD_POINT".localized
        tblRedeemHistory.tableFooterView = UIView()
        btnRedeem.setTitleColor(UIColor.themeButtonTitleColor, for: UIControl.State.normal)
        btnRedeem.backgroundColor = UIColor.themeColor
        
        lblRedeem.text = "TXT_REWARD_POINT".localized
        lblRedeem.font = FontHelper.textRegular(size: FontHelper.large)
        
        lblRedeemValue.textColor = UIColor.themeLightTextColor
        lblRedeemValue.font = FontHelper.textRegular()
        lblRedeemValue.text = "TXT_POINTS".localized
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setUpLayout()
        
    }
    func openRedeemDialog() {
        let dict = ["total_redeem_points": totalRedeemPoints,
                    "user_minimum_redeem_points_required" : userMinimumRedeemPointRequired,
                    "user_redeem_point_value" : userRedeemPointValue] as [String : Any]
        let dialogForRedeem = CustomRedeemDialog.showCustomRedeemDialog(title: "TXT_REDEEM".localized, message: "".localized, titleLeftButton: "".localized, titleRightButton: "TXT_SUBMIT".localized, editTextOneHint: "TXT_ENTER_POINTS".localized, editTextTwoHint: "", isEdiTextTwoIsHidden: true,param: dict)
        dialogForRedeem.onClickLeftButton = { [weak dialogForRedeem] in
            dialogForRedeem?.removeFromSuperview()
        }
        dialogForRedeem.onClickRightButton = { [weak self, weak dialogForRedeem]  (text1:String) in
            dialogForRedeem?.removeFromSuperview()
            self?.wsAddRedeemPointValueToWallet(redeemPoints: text1)
        }
    }
    
    //MARK: - ACTION METHODS
    @IBAction func onClickBtnBack(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickBtnRedeem(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.openRedeemDialog()
    }
    //MARK: User Define Function
    func updateUI(isUpdate:Bool = false) {
        imgEmpty.isHidden = isUpdate
        tblRedeemHistory.isHidden = !isUpdate
    }
    //MARK: WebService Calls
    func wsGetRedeemHistory() {
        Utility.showLoading()
        let dictParam:[String:String] =
        [PARAMS.SERVER_TOKEN : preferenceHelper.SessionToken,
         PARAMS.ID : preferenceHelper.UserId,
         PARAMS.TYPE :String(CONSTANT.TYPE_USER)]
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_GET_REDEEM_POINT_HISTORY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            if (Parser.isSuccess(response: response as NSDictionary, withSuccessToast: false, andErrorToast: true)) {
                let redeemListResponse:RedeemPointHistoryResponse = RedeemPointHistoryResponse(dictionary: response)!
                self.totalRedeemPoints = redeemListResponse.total_redeem_point ?? 0
                self.userMinimumRedeemPointRequired = redeemListResponse.user_minimum_point_require_for_withdrawal ?? 0
                self.userRedeemPointValue = redeemListResponse.user_redeem_point_value ?? 0.0
                self.lblRedeemValue.text = String(format: "%@ : %@","TXT_POINTS".localized, "\(redeemListResponse.total_redeem_point ?? 0)")
                if redeemListResponse.total_redeem_point ?? 0 > 0 {
                    self.btnRedeem.isEnabled = true
                } else {
                    self.btnRedeem.isEnabled = false
                }
                Parser.parseRedeemHistory(response as! [String:Any], toArray: self.arrForRedeemHistory, completion: { (result) in
                    if result {
                        self.tblRedeemHistory.reloadData()
                        self.updateUI(isUpdate: true)
                    }else {
                        self.updateUI(isUpdate: false)
                    }
                    Utility.hideLoading()
                })
            } else {
                self.btnRedeem.isHidden = true
                self.updateUI(isUpdate: false)
                self.lblRedeemValue.text = String(format: "%@ : %@","TXT_POINTS".localized, "\(0)")

            }
        }
    }
    
    func wsAddRedeemPointValueToWallet(redeemPoints: String){
        Utility.showLoading()
        let dictParam:[String:Any] =
        [PARAMS.SERVER_TOKEN : preferenceHelper.SessionToken,
         PARAMS.USER_ID : preferenceHelper.UserId,
         PARAMS.TYPE :CONSTANT.TYPE_USER,
         PARAMS.REDEEM_POINT : redeemPoints.integerValue ?? 0
        ]
        Utility.showLoading()
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_WITHDRAW_REDEEM_POINT_TO_WALLET, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response,error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response, withSuccessToast: true,andErrorToast: true) {
                self.wsGetRedeemHistory()
            }else {
            }
        }
    }
    func setUpLayout() {
        btnRedeem.applyRoundedCornersWithHeight()
    }
}
extension  RedeemVC: UITableViewDelegate, UITableViewDataSource {
    //MARK: Tableview Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForRedeemHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemHistoryCell", for: indexPath) as! RedeemHistoryCell
        let redeemData = (arrForRedeemHistory[indexPath.row] as! RedeemHistory)
        cell.setRedeemHistoryData(redeemRequestData: redeemData)
         return cell
    }
}
