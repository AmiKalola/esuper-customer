//
//  RedeemHistoryCell.swift
//  ESuper
//
//  Created by Elluminati mac mini on 09/08/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import UIKit

class RedeemHistoryCell:  CustomTableCell {
    
    @IBOutlet weak var lblDivider: UILabel!
    @IBOutlet weak var walleBackView: UIView!
    @IBOutlet weak var lblRequestId: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /*Set Font*/
        lblAmount.font = FontHelper.textMedium()
        lblStatus.font = FontHelper.textRegular()
        lblTime.font = FontHelper.textSmall()
        lblRequestId.font = FontHelper.textMedium(size: FontHelper.labelRegular)
        
        /*Set Color*/
        
        lblRequestId.textColor = UIColor.themeTextColor
        lblAmount.textColor = UIColor.themeTextColor
        lblStatus.textColor = UIColor.themeLightTextColor
        lblTime.textColor = UIColor.themeTextColor
        self.walleBackView.backgroundColor = UIColor.themeViewBackgroundColor
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
    }
    
    
    func setRedeemHistoryData(redeemRequestData: RedeemHistory) {
        lblRequestId.text = String(format: "%@ : %@", "TXT_TOTAL_REDEEM_POINTS".localized, "\(redeemRequestData.added_redeem_point ?? 0)")
        
        let date = Utility.stringToString(strDate: redeemRequestData.created_at ?? "", fromFormat: DATE_CONSTANT.DATE_TIME_FORMAT_WEB, toFormat: DATE_CONSTANT.DATE_TIME_FORMAT_HISTORY)
        lblTime.text = "\(date)"
        let redeemHistoryStatus:WalletHistoryStatus = WalletHistoryStatus(rawValue: redeemRequestData.redeem_status ?? 0) ?? .Unknown
        lblStatus.text = redeemRequestData.redeem_point_description?.uppercased()
        switch (redeemRequestData.redeem_status) {
        
        case RewardStatus.ADD_REWARD:
            lblAmount.textColor = UIColor.themeWalletAddedColor
            lblAmount.text = "+"  +  String(redeemRequestData.added_redeem_point ?? 0)
            lblDivider.backgroundColor = UIColor.themeWalletAddedColor
            break
        
        case RewardStatus.REMOVE_REWARD:
            lblAmount.textColor = UIColor.themeWalletDeductedColor
            lblAmount.text = "-"  +  String(redeemRequestData.added_redeem_point ?? 0)
            lblDivider.backgroundColor = UIColor.themeWalletDeductedColor
            break
        default:
            break
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
