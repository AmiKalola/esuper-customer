//
//  SettingVC.swift
//  
//
//  Created by Elluminati on 14/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
class SettingVC: BaseVC {
    
    /*View For Language Notification*/
    @IBOutlet weak var tableContact : UITableView!
    @IBOutlet weak var constraintContactHeight : NSLayoutConstraint!
    @IBOutlet weak var viewAddContact : UIView!
    @IBOutlet weak var lblLanguageMessage: UILabel!
    @IBOutlet weak var viewForLanguage: UIView!
    
    /*View For Store Image Loading*/
    @IBOutlet weak var viewForStoreImage: UIView!
    @IBOutlet weak var lblStoreImageTitle: UILabel!
    @IBOutlet weak var lblStoreImageMsg: UILabel!
    @IBOutlet weak var switchForStoreImage: UISwitch!
    
    /*View For Item Image Loading*/
    @IBOutlet weak var viewForItemImage: UIView!
    @IBOutlet weak var lblItemImageTiltle: UILabel!
    @IBOutlet weak var lblItemImageMsg: UILabel!
    @IBOutlet weak var switchForItemImage: UISwitch!
    @IBOutlet weak var imgForDropDown: UIImageView!
    @IBOutlet weak var imgForLanguage: UIImageView!
    
    @IBOutlet weak var lblDarkMode: UILabel!
    @IBOutlet weak var lblDarkModeInfo: UILabel!
    @IBOutlet weak var switchDarkMode: UISwitch!
    @IBOutlet weak var viewDarkMode: UIView!
    @IBOutlet weak var lblMode: UILabel!
    
    var contactId:String = "";
    var nameToSave:String = "";
    var numberDescription :String = ""
    var arrForEmegerncyContacts:[EmergencyContactData] = []
    
    //MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action:#selector(openLanguageDialog))
        viewForLanguage.addGestureRecognizer(tap)
        viewForLanguage.isUserInteractionEnabled = true
        self.tableContact.register(UINib(nibName: "EmergencyContactCell", bundle: nil), forCellReuseIdentifier: "EmergencyContactCell")
        setLocalization()
        if preferenceHelper.DarkMode{
            self.lblMode.text = "Dark"
        }else{
            self.lblMode.text = "Light"
        }
        switchDarkMode.isOn = preferenceHelper.DarkMode
        switchForItemImage.isOn = preferenceHelper.IsLoadItemImage
        switchForStoreImage.isOn = preferenceHelper.IsLoadStoreImage
        lblLanguageMessage.text = LocalizeLanguage.currentAppleLanguageFull()
        
        if #available(iOS 13.0, *) {
            viewDarkMode.isHidden = false
        } else {
            viewDarkMode.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setBackBarItem(isNative: true)
        self.wsGetEmergencyContactList()
    }
    
    @IBAction func valueChangeForStoreImage(_ sender: Any) {
        preferenceHelper.IsLoadStoreImage = (switchForStoreImage.isOn)
    }
    
    @IBAction func valueChangeForItemImage(_ sender: Any) {
        preferenceHelper.IsLoadItemImage = (switchForItemImage.isOn)
    }
    
    @IBAction func valueChangeForDarkMode(_ sender: UISwitch) {
        if #available(iOS 13.0, *) {
            APPDELEGATE.window?.overrideUserInterfaceStyle = sender.isOn ? .dark : .light
        }
        preferenceHelper.DarkMode = (switchDarkMode.isOn)
    }
    
    override func updateUIAccordingToTheme() {
        imgForDropDown.image = UIImage.init(named: "dropdown")?.imageWithColor(color: .themeIconTintColor)!
        self.setBackBarItem(isNative: true)
        imgForLanguage.image = UIImage(named: "language")?.imageWithColor(color: .themeTitleColor)
    }
    
    
    //MARK: Button action methods
    
    @objc func openLanguageDialog() {
        openLanguageActionSheet()
    }
    
    func openLanguageActionSheet(){
        
        let alertController = UIAlertController(title: nil, message: "TXT_CHANGE_LANGUAGE".localized, preferredStyle: .actionSheet)
        
        for i in arrForLanguages{
            let action = UIAlertAction(title: i.language , style: .default, handler: { (alert: UIAlertAction!) -> Void in
                //print(alert.title!)
                
                //print(arrForLanguages.firstIndex(where: {$0.language == alert.title!})!)
                if arrForLanguages.firstIndex(where: {$0.language == alert.title!})! == preferenceHelper.Language{
                    self.dismiss(animated: true, completion: nil)
                }else{
                    super.changed(arrForLanguages.firstIndex(where: {$0.language == alert.title!})!)
                }
            })
            alertController.addAction(action)
        }
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    //arrForMode
    @IBAction func darkMode (_ sender : UIButton) {
        self.openModeActionSheet()
    }
    @IBAction func onClickBtnAddNewContact(_ sender: Any) {
        alertPromptToAllowContactAccessViaSetting()
    }
    
    func alertPromptToAllowContactAccessViaSetting() {
        let dialogForPermission = CustomAlertDialog.showCustomAlertDialog(title: "alert".localized, message: "add_contact_alert_message".localized, titleLeftButton: "".localized, titleRightButton: "TXT_OK".localized,isSingleButton: false)
        dialogForPermission.onClickLeftButton = { [/*unowned self,*/ unowned dialogForPermission] in
            dialogForPermission.removeFromSuperview();
            self.checkContactPermission()
        }
        dialogForPermission.onClickRightButton = { [/*unowned self,*/ unowned dialogForPermission] in
            dialogForPermission.removeFromSuperview();
            self.checkContactPermission()
        }
    }
    func checkContactPermission(){
        let status = CNContactStore.authorizationStatus(for: .contacts)
        if status == .denied || status == .restricted {
            presentSettingsAlert()
            return
        }
        
        // open it
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            guard granted else {
                self.presentSettingsAlert()
                return
            }
            
            // get the contacts
            self.openContactPicker()
            
        }
    }
    private func presentSettingsAlert() {
        let settingsURL = URL(string: UIApplication.openSettingsURLString)!
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Permission to Contacts", message: "This app needs access to contacts in order to ...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Go to Settings", style: .default) { _ in
                UIApplication.shared.openURL(settingsURL)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            self.present(alert, animated: true)
        }
    }
  
    
      func openContactPicker() {
          let contactPicker = CNContactPickerViewController()
          contactPicker.delegate = self
          contactPicker.displayedPropertyKeys = [CNContactPhoneNumbersKey]
          contactPicker.predicateForEnablingContact = NSPredicate(format: "phoneNumbers.@count > 0")
          contactPicker.predicateForSelectionOfContact = NSPredicate(format: "phoneNumbers.@count == 1")
          contactPicker.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'")
          contactPicker.modalPresentationStyle = .fullScreen
          self.present(contactPicker, animated: true, completion: nil)
      }
    
    func openModeActionSheet(){
        let alertController = UIAlertController(title: nil, message: "TXT_CHANGE_MODE".localized, preferredStyle: .actionSheet)
        
        for i in arrForMode{
            let action = UIAlertAction(title: i.language , style: .default, handler: { (alert: UIAlertAction!) -> Void in
                //print(alert.title!)
                //print(arrForMode.firstIndex(where: {$0.language == alert.title!})!)
                //print(" MOde:- \(alert.title)")
                self.lblMode.text = alert.title
                if #available(iOS 13.0, *) {
                    if alert.title == "Auto"{
                        if UITraitCollection.current.userInterfaceStyle == .dark{
                            APPDELEGATE.window?.overrideUserInterfaceStyle = .dark
                            preferenceHelper.DarkMode = (true)
                        } else {
                            APPDELEGATE.window?.overrideUserInterfaceStyle = .light
                            preferenceHelper.DarkMode = (false)
                            
                        }
                    }else if alert.title == "Dark"{
                        APPDELEGATE.window?.overrideUserInterfaceStyle = .dark
                        preferenceHelper.DarkMode = (true)
                    }else{
                        APPDELEGATE.window?.overrideUserInterfaceStyle = .light
                        preferenceHelper.DarkMode = (false)
                    }
                }
                if #available(iOS 13.0, *) {
                }
            })
            alertController.addAction(action)
        }
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
}
extension SettingVC: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let name =  contact.givenName + " " + contact.familyName + " " + contact.middleName
        print(name)
        if let phoneNumber = contact.phoneNumbers.first?.value.stringValue {
        print( phoneNumber)
            nameToSave = name
            numberDescription = "\(phoneNumber)"
            self.wsAddEmergencyContactList()
        }
      
      
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        let name =  contactProperty.contact.givenName + " " + contactProperty.contact.familyName + " " + contactProperty.contact.middleName + " " + contactProperty.contact.nameSuffix
        print(name)
        if let phoneNumber = contactProperty.contact.phoneNumbers.first?.value.stringValue {
            print( phoneNumber)
            nameToSave = name
            numberDescription = "\(phoneNumber)"
            self.wsAddEmergencyContactList()
        }
    }
}
extension SettingVC {
  
    func wsGetEmergencyContactList(){
        Utility.showLoading()
        var  dictParam : [String : Any] = [:]
        dictParam[PARAMS.USER_ID] = preferenceHelper.UserId
        dictParam[PARAMS.DEVICE_TOKEN] = preferenceHelper.DeviceToken
        
        let afh:AlamofireHelper = AlamofireHelper.init()
        afh.getResponseFromURL(url: WebService.WS_get_emergency_contact_list, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, error) -> (Void) in
            Utility.hideLoading()
            if let sucess = response["success"] as? Bool{
                if sucess{
                }else{
                    let code = response["error_code"] as? Int
//                    Utility.showToast(message: ("HTTP_ERROR_CODE_\((code!))").localized)
                    if (code ?? 0) == 467{
                        self.arrForEmegerncyContacts.removeAll()
                        self.constraintContactHeight.constant = 0.0 //CGFloat((self.arrForEmegerncyContacts.count) * 118)
                        self.tableContact.isScrollEnabled = false
                        self.tableContact.reloadData()
                    }
                }
            }

            if (error != nil){
                Utility.hideLoading()
            }
            else{
                if Parser.isSuccess(response: response,withSuccessToast: false,andErrorToast: false){
                    self.arrForEmegerncyContacts.removeAll()
                    let responseContact:EmergencyContactResponse = EmergencyContactResponse.init(fromDictionary:( response as! [String:Any]))
                    
                    for data in responseContact.emergencyContactData{
                        self.arrForEmegerncyContacts.append(data)
                    }
                    if self.arrForEmegerncyContacts.count >= 5{
                        self.viewAddContact.isHidden = true
                    }else{
                        self.viewAddContact.isHidden = false
                    }
                    self.constraintContactHeight.constant = CGFloat((self.arrForEmegerncyContacts.count) * 118)
                    self.tableContact.isScrollEnabled = false
                    self.tableContact.reloadData()
                }
            }
        }
    }
    func wsAddEmergencyContactList(){
        Utility.showLoading()
        var  dictParam : [String : Any] = [:]
        dictParam[PARAMS.USER_ID] = preferenceHelper.UserId
        dictParam[PARAMS.DEVICE_TOKEN] = preferenceHelper.DeviceToken
        dictParam[PARAMS.NAME] = self.nameToSave
        dictParam[PARAMS.PHONE] = self.numberDescription
        dictParam[PARAMS.IS_ALWAYS_SHARE_RIDE_DETAIL] = true
        
        let afh:AlamofireHelper = AlamofireHelper.init()
        afh.getResponseFromURL(url: WebService.WS_add_emergency_contact, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, error) -> (Void) in
            
            if (error != nil){
                Utility.hideLoading()
            }
            else{
                self.wsGetEmergencyContactList()
            }
        }
    }
    func wsDeleteEmergencyContactList(id:String){
        Utility.showLoading()
        var  dictParam : [String : Any] = [:]
        dictParam[PARAMS.USER_ID] = preferenceHelper.UserId
        dictParam[PARAMS.DEVICE_TOKEN] = preferenceHelper.DeviceToken
        dictParam[PARAMS.EMERGENCY_CONTACT_DETAIL_ID] = id

        
        let afh:AlamofireHelper = AlamofireHelper.init()
        afh.getResponseFromURL(url: WebService.WS_delete_emergency_contact, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, error) -> (Void) in
            Utility.hideLoading()
            if (error != nil){
                Utility.hideLoading()
            }
            else{
                if Parser.isSuccess(response: response){
                    self.wsGetEmergencyContactList()
                }
                else{
                    Utility.hideLoading()
                }
            }
        }
    }
    func wsUpdateEmergencyContactList(contact:EmergencyContactData, shareDetail:Int){
        
        Utility.showLoading()
        var  dictParam : [String : Any] = [:]
        
        dictParam[PARAMS.USER_ID] = preferenceHelper.UserId
        dictParam[PARAMS.DEVICE_TOKEN] = preferenceHelper.DeviceToken
        dictParam[PARAMS.NAME] = contact.name
        dictParam[PARAMS.PHONE] = contact.phone
          dictParam[PARAMS.EMERGENCY_CONTACT_DETAIL_ID] = contact.id
        dictParam[PARAMS.IS_ALWAYS_SHARE_RIDE_DETAIL] = shareDetail
        
        let afh:AlamofireHelper = AlamofireHelper.init()
        afh.getResponseFromURL(url: WebService.WS_update_emergency_contact, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, error) -> (Void) in
            Utility.hideLoading()
            if (error != nil){
                Utility.hideLoading()
            }
            else{
                if Parser.isSuccess(response: response){
                    self.wsGetEmergencyContactList()
                }
                else{
                    Utility.hideLoading()
                }
            }
        }
    }
}
//MARK: - Set localization text and pree launch setup
extension SettingVC {
    func setLocalization() {
        
        view.backgroundColor = UIColor.themeViewBackgroundColor
        self.hideBackButtonTitle()
        self.setNavigationTitle(title: "TXT_SETTING".localizedCapitalized)
        
        /*set Color*/
        lblStoreImageTitle.textColor = UIColor.themeTextColor
        lblItemImageTiltle.textColor = UIColor.themeTextColor
        lblStoreImageMsg.textColor = UIColor.themeLightTextColor
        lblItemImageMsg.textColor = UIColor.themeLightTextColor
        lblLanguageMessage.textColor = UIColor.themeTextColor
        lblDarkMode.textColor = UIColor.themeTextColor
        lblDarkModeInfo.textColor = UIColor.themeLightTextColor
        
        /*set font*/
        lblStoreImageTitle.font = FontHelper.textRegular()
        lblItemImageTiltle.font = FontHelper.textRegular()
        lblItemImageMsg.font = FontHelper.tiny()
        lblStoreImageMsg.font = FontHelper.tiny()
        lblLanguageMessage.font = FontHelper.textMedium()
        lblDarkMode.font = FontHelper.textRegular()
        lblDarkModeInfo.font = FontHelper.tiny()
        
        /*set Localized Text*/
        lblStoreImageTitle.text = "TXT_STORE_IMAGE_TITLE".localized
        lblStoreImageMsg.text = "MSG_STORE_IMAGE".localized
        lblItemImageTiltle.text = "TXT_ITEM_IMAGE_TITLE".localized
        lblItemImageMsg.text = "MSG_ITEM_IMAGE".localized
        lblLanguageMessage.text = "MSG_LANGUAGE".localized
        lblDarkMode.text = "txt_dark_mode".localized
        lblDarkModeInfo.text = "txt_dark_mode_info".localized
        self.view.layoutIfNeeded()
        
        /*View For Language Overlay*/
        updateUIAccordingToTheme()
        imgForLanguage.image = UIImage(named: "language")?.imageWithColor(color: .themeTitleColor)
    }
    
}
extension SettingVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForEmegerncyContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmergencyContactCell") as! EmergencyContactCell
        let contacts = self.arrForEmegerncyContacts[indexPath.row]
        cell.lblName.text = contacts.name
        cell.lblPhoneNumber.text = contacts.phone
        cell.swForShareDetail.isOn = contacts.isAlwaysShareRideDetail == 1
        cell.btnDelete.tag = indexPath.row
        cell.swForShareDetail.tag = indexPath.row
        cell.swForShareDetail.addTarget(self, action: #selector(self.updateContact(sender:)), for: .valueChanged)
        cell.btnDelete.addTarget(self, action: #selector(self.deleteContact(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func deleteContact(sender :UIButton)
    {
        wsDeleteEmergencyContactList(id: arrForEmegerncyContacts[sender.tag].id)
        
    }
    @objc func updateContact(sender :UISwitch)
    {
        if sender.isOn
        {
            wsUpdateEmergencyContactList(contact: arrForEmegerncyContacts[sender.tag], shareDetail: 1)
        }
        else
        {
            wsUpdateEmergencyContactList(contact: arrForEmegerncyContacts[sender.tag], shareDetail: 2)
        }
        
    }
}
