//
//  EmergencyContactCell.swift
//  ESuper
//
//  Created by Rohit on 08/03/24.
//  Copyright © 2024 Elluminati. All rights reserved.
//

import UIKit

class EmergencyContactCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imgDelete: UIImageView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblShareDetail: UILabel!
    @IBOutlet weak var swForShareDetail: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblName.textColor = UIColor.themeLightTextColor
        lblName.font = FontHelper.font(size: FontSize.regular, type: FontType.Light)
        lblPhoneNumber.textColor = UIColor.themeLightTextColor
        
        lblPhoneNumber.font = FontHelper.font(size: FontSize.regular, type: FontType.Light)
        
        lblShareDetail.textColor = UIColor.themeTextColor
        lblShareDetail.font = FontHelper.font(size: FontSize.medium, type: FontType.Bold)
        
        lblShareDetail.text = "txt_sharing_ride_details".localized
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionDeleteContact( _ sender : UIButton){
        
    }
}
