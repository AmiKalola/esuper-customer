//
//  InvoiceSection.swift
//  ESuper
//
//  Created by Rohit on 14/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit

class InvoiceSection: CustomTableCell {
    //MARK: - OUTLET
    @IBOutlet weak var imgFreeDelivery: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    //MARK: - LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        lblName.backgroundColor = UIColor.themeViewBackgroundColor
        lblName.textColor = UIColor.themeTitleColor
        lblName.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
    }

    func setData(title:String,isShowImage:Bool = false) {
        lblName.text = title.appending("     ")
        imgFreeDelivery.isHidden = currentBooking.isUserPickUpOrder ? true : !isShowImage
        lblName.sectionRound(lblName)
    }

    //MARK: - SET CELL DATA
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
