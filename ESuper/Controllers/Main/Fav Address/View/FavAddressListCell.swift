//
//  FavAddressListCell.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit

class FavAddressListCell: CustomTableCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnCancle: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = FontHelper.textMedium()
        lblAddress.font = FontHelper.textSmall()
        lblTitle.textColor = UIColor.themeTextColor
        lblAddress.textColor = UIColor.themeLightTextColor
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
    }
    
    func setData(dictData: FavouriteAddressesApi) {
        lblTitle.text = dictData.address_name
        lblAddress.text = dictData.address
        
        let flatNo = dictData.flat_no ?? ""
        let street = dictData.street ?? ""
        let landmark = dictData.landmark ?? ""
        
        var finalAddress = dictData.address ?? ""
        var newLine = "\n"
        
        if !flatNo.isEmpty && !street.isEmpty {
            finalAddress += "\n\n" + "\(flatNo), \(street)"
        } else if !flatNo.isEmpty {
            finalAddress += "\n\n" + "\(flatNo)"
        } else if !street.isEmpty {
            finalAddress += "\n\n" + "\(street)"
        } else {
            newLine = "\n\n"
        }
        if !landmark.isEmpty {
            finalAddress += newLine + "\(landmark)"
        }
        lblAddress.text = finalAddress
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func layoutSubviews() {
    }
}
