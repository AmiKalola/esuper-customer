//
//  ReviewCollectionViewCell.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
//MARK: Cell For CollectionView and TableView
class ReviewCollectionViewCell: CustomCollectionCell,/*RatingViewDelegate,*/ UITextFieldDelegate {
    @IBOutlet weak var btnWriteReview: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var viewWriteReview: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtWriteReview: UITextField!

    var parentVC:ReviewVC? = nil
    var rate:Double = 0.0

    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewWriteReview.layer.borderWidth = 1.0
        setLocalization()
    }

    func setLocalization()  {
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        self.lblOrderId.font = FontHelper.textMedium()
        self.lblOrderId.textColor = UIColor.themeTextColor
        self.btnWriteReview.setTitle("TXT_WRITE_REVIEW".localizedCapitalized, for: .normal)
        self.btnWriteReview.setTitleColor(UIColor.themeTextColor, for: .normal)
        btnWriteReview.titleLabel?.font = FontHelper.textRegular(size: FontHelper.medium)
        self.ratingView.rating = 0.0
        self.ratingView.didTouchCosmos = { /*[unowned self]*/ (rating: Double) -> () in
            //debugPrint("self.cosmosVw.didTouchCosmos: \(rating)")
        }
        self.ratingView.didFinishTouchingCosmos = { /*[unowned self]*/ (rating: Double) -> () in
            //debugPrint("self.cosmosVw.didFinishTouchingCosmos: \(rating)")
            self.rate = rating
        }
        self.ratingView.settings.filledImage = UIImage(named: "ff")?.imageWithColor(color: .themeColor)
        self.ratingView.settings.emptyImage = UIImage(named: "nf")?.imageWithColor(color: .themeTitleColor)
        self.ratingView.settings.fillMode = .half
        self.ratingView.settings.minTouchRating = 0.5
        self.ratingView.settings.starSize =  Double(self.ratingView.frame.size.height)
        self.viewWriteReview.layer.borderColor = UIColor.themeTextColor.cgColor
        
        self.txtWriteReview.placeholder = "TXT_WRITE_REVIEW".localizedCapitalized
        self.txtWriteReview.tintColor = UIColor.themeViewBackgroundColor
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
    }

    @IBAction func onClickGiveReview(_ sender: UIButton) {
        parentVC?.onClikeBtnWriteReview(sender.tag, rate: rate, strReview: txtWriteReview.text ?? "", cell: self)
    }
}
