//
//  CellForUser.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit


class CellForUser: CustomTableCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    override func awakeFromNib() {
        lblItemName.textColor = UIColor.themeTextColor
        lblItemName.font = FontHelper.textRegular()
    }
}
