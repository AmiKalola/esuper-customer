//
//  ProductSearchCell.swift
//  ESuper
//
//  Created by Rohit on 14/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit
class ProductSearchCell: CustomTableCell {
    
    //MARK: - OUTLET
    
    @IBOutlet weak var imgButtonType: UIButton!
    @IBOutlet weak var lblProductName: UILabel!
    
    //MARK: - LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor =  UIColor.themeViewBackgroundColor
        self.backgroundColor = UIColor.themeViewBackgroundColor
        lblProductName.font = FontHelper.textRegular()
        lblProductName.textColor = UIColor.themeTextColor
        imgButtonType.setImage( UIImage.init(named: "checked_checkbox_icon")?.imageWithColor(color: UIColor.themeColor)
                                , for: UIControl.State.selected)
        imgButtonType.setImage( UIImage.init(named: "unchecked_checkbox_icon"), for: UIControl.State.normal)
    }
    
    //MARK:- SET CELL DATA
    func setCellData(cellItem:ProductItem) {
        lblProductName.text = cellItem.productDetail?.name
        imgButtonType.isSelected = cellItem.isProductFiltered
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func updateUIAccordingToTheme() {
        imgButtonType.setImage( UIImage.init(named: "checked_checkbox_icon")?.imageWithColor(color: UIColor.themeTextColor)
                                , for: UIControl.State.selected)
        imgButtonType.setImage(UIImage.init(named: "unchecked_checkbox_icon")
                               , for: UIControl.State.normal)
    }
}
