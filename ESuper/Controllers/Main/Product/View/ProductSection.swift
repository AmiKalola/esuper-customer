//
//  ProductSection.swift
//  ESuper
//
//  Created by Rohit on 14/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit


class ProductSection: CustomTableCell {
    
    //MARK: - OUTLET
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lbltaxInfo: UILabel!
    @IBOutlet weak var viewName: UIView!

    //MARK: - LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        lblProductName.backgroundColor = .clear//UIColor.themeViewBackgroundColor
        lblProductName.textColor = UIColor.themeTextColor
        lblProductName.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        lbltaxInfo.textColor = UIColor.themePromocodeGreenColor
        lbltaxInfo.font = FontHelper.textSmall()
        viewName.backgroundColor = UIColor.themeLightGray246
    }
    
    func setData(title:String,isTaxIncluded:Bool) {
        lblProductName.text = title.appending("     ")
        if isTaxIncluded{
            lbltaxInfo.text = "TXT_INCLUSIVE_OF_ALL_TAXES".localized
        }else{
            lbltaxInfo.text = "TXT_EXCLUSIVE_OF_ALL_TAXES".localized
        }
    }
    
    //MARK: - SET CELL DATA
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
