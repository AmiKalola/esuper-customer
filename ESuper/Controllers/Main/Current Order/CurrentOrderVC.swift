//
//  HomeVC.swift
//  
//
//  Created by Elluminati on 14/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import UIKit

class CurrentOrderVC: BaseVC {

    //MARK: OutLets
    @IBOutlet weak var tableForCurrentOrders: UITableView!
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var viewFooter: UIView!

    //MARK: Variables
    var arrForCurrentOrders:[Order] = []
    var arrFilterOrder:[Order] =  []
    var selectedOrder:Order = Order.init()
    var isTrackOrder = false
    //MARK: LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        tableForCurrentOrders.estimatedRowHeight = 100.0
        tableForCurrentOrders.rowHeight = UITableView.automaticDimension
        self.hideBackButtonTitle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        wsGetOrders()
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        tableForCurrentOrders.tableFooterView = viewFooter
    }
    
    func reloadData(filter: OrderFilter = OrderFilter.all) {
        arrFilterOrder.removeAll()
        if filter == OrderFilter.store {
            arrFilterOrder = arrForCurrentOrders.filter({$0.delivery_type == DeliveryType.store})
        } else if filter == OrderFilter.courier {
            arrFilterOrder = arrForCurrentOrders.filter({$0.delivery_type == DeliveryType.courier})
        }
        tableForCurrentOrders.reloadData()
    }
    
    
    func setLocalization() {
        self.view.backgroundColor = UIColor.themeViewBackgroundColor
        self.tableForCurrentOrders.backgroundColor = UIColor.themeViewBackgroundColor
   }
    
    func updateUi(isUpdate:Bool = false) {
        self.tableForCurrentOrders?.isHidden = !isUpdate
        self.imgEmpty?.isHidden = isUpdate
    }
    
    //MARK: - WEB SERVICE CALLS
    func wsGetOrders() {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_ORDER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            self.arrForCurrentOrders = Parser.parseOrders(response)
            if self.arrForCurrentOrders.count > 0 {
                self.updateUi(isUpdate: true)
            }
            else {
                self.updateUi()
            }
            Utility.hideLoading()
            self.tableForCurrentOrders?.reloadData()
        }
    }

  //MARK: - USER DEFINE FUNCTION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier?.compare(SEGUE.SEGUE_ORDER_STATUS) == ComparisonResult.orderedSame) {
            let orderStatusvc = segue.destination as! OrderStatusVC
            orderStatusvc.selectedOrder = selectedOrder
            
        }
        if(segue.identifier?.compare(SEGUE.ORDER_TO_INVOICE) == ComparisonResult.orderedSame) {
            let invoiceVC = segue.destination as! HistoryInvoiceVC
            invoiceVC.isFromHistory = false
            invoiceVC.strOrderID = selectedOrder._id!
            invoiceVC.strCurrency = selectedOrder.currency!
            if let providerFirstName:String = selectedOrder.provider_first_name {
                invoiceVC.name = providerFirstName + " " + (selectedOrder.provider_last_name ?? "")
            invoiceVC.imgurl = selectedOrder.provider_image ?? ""
            }
        }
    }
}

//MARK: - UITableView Delegate and DataSource methods 
extension  CurrentOrderVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if arrFilterOrder.count > 0 {
            return arrFilterOrder.count
        }
        return arrForCurrentOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:CurrentOrderCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CurrentOrderCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let currentData:Order = {
            if arrFilterOrder.count > 0 {
                return arrFilterOrder[indexPath.row]
            }
          return arrForCurrentOrders[indexPath.row]
        }()
        cell.setCellData(cellItem: currentData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let arrOrder:[Order] = {
            if arrFilterOrder.count > 0 {
                return arrFilterOrder
            }
          return arrForCurrentOrders
        }()
        let currentSelectedOrder = arrOrder[indexPath.row]
        selectedOrder = currentSelectedOrder
        currentBooking.selectedOrderId = selectedOrder._id
        currentBooking.selectedStoreId = selectedOrder.cartDetail?.storeId
        
        if selectedOrder.delivery_type == DeliveryType.courier {
            var mainView: UIStoryboard!
            mainView = UIStoryboard(name: "Courier", bundle: nil)
            if let courierStatusVC : CourierStatusVC = mainView.instantiateViewController(withIdentifier: "courierStatusVC") as? CourierStatusVC {
                
                courierStatusVC.selectedOrder = selectedOrder
                self.navigationController?.pushViewController(courierStatusVC, animated: true)
            }
        }else {
            self.performSegue(withIdentifier: SEGUE.SEGUE_ORDER_STATUS, sender: self)
        }
    }
}
