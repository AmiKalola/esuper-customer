//
//  AddWalletAmountVC.swift
//  ESuper
//
//  Created by Rohit on 19/06/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import UIKit
import PayPalCheckout
protocol addAmountPaypal {
    func addAmount(paymentID: String,amount:String)
}
class AddWalletAmountVC: UIViewController {
    @IBOutlet weak var viewWallet : UIView!
    @IBOutlet weak var txtAmount : UITextField!
    @IBOutlet weak var lblPlaceHolder : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var vwPayPal: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var walletCurrencyCode = ""
    var delegaet : addAmountPaypal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
    }
    func setLocalization(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewWallet.clipsToBounds = true
            self.viewWallet.layer.cornerRadius = 20
        }
        //TXT_ADD_WALLET_BALANCE
        
        lblPlaceHolder.text = "TXT_ADD_WALLET_AMOUNT".localized
        lblTitle.text = "TXT_WALLET_HINT".localizedCapitalized
        btnSubmit.backgroundColor = UIColor.themeColor
    }
    
    @IBAction func actionCancel (_sender: UIButton){
        self.dismiss(animated: true)
        
    }
    @IBAction func actionSubmite (_sender: UIButton){
        let paypal = PaypalHelper.init(currrencyCode: "usd", amount: self.txtAmount.text!)
        paypal.delegate = self
    }
    private func addPayPalButtons() {
        let container = PaymentButtonContainer()
        container.frame = self.vwPayPal.bounds
        container.setRound()
        self.vwPayPal.addSubview(container)
        NSLayoutConstraint.activate(
            [
                container.centerYAnchor.constraint(equalTo: self.vwPayPal.centerYAnchor),
                container.centerXAnchor.constraint(equalTo: self.vwPayPal.centerXAnchor),
                container.widthAnchor.constraint(equalToConstant: self.vwPayPal.frame.size.width),
                container.heightAnchor.constraint(equalToConstant: self.vwPayPal.frame.size.height)
            ]
        )
        vwPayPal.isHidden = false
    }
    
    private func configurePayPalCheckoutForWallet() {
        
        Checkout.setCreateOrderCallback { action in
            if self.txtAmount.text!.length > 1{
                let walletCurrencyCode = CurrencyCode.currencyCode(from: self.walletCurrencyCode)
                let amount = PurchaseUnit.Amount(currencyCode: walletCurrencyCode ?? .usd, value: self.txtAmount.text ?? "")
                let purchaseUnit = PurchaseUnit(amount: amount)
                let order = OrderRequest(intent: .capture, purchaseUnits: [purchaseUnit])
                action.create(order: order)
            }else{
                
            }
            
        }
        Checkout.setOnApproveCallback { approval in
            approval.actions.capture { response, error in
                if response?.data.id != "" {
                    self.delegaet?.addAmount(paymentID: response?.data.id ?? "",amount: self.txtAmount.text ?? "")
                    self.dismiss(animated: true)
                }
            }
        }
    }
    
}
extension AddWalletAmountVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let isValid = newString.count > 0
        if textField == txtAmount {
            let textFieldString = textField.text! as NSString
            let newString = textFieldString.replacingCharacters(in: range, with:string)
            let floatRegEx = "^([0-9]+)?(\\.([0-9]+)?)?$"
            let floatExPredicate = NSPredicate(format:"SELF MATCHES %@", floatRegEx)
            return floatExPredicate.evaluate(with: newString)
        }
        return true
    }
}

extension AddWalletAmountVC: PaypalHelperDelegate {
    func paymentSucess(capture: PaypalCaptureResponse) {
        txtAmount.text = capture.amount
//        wsAddAmountToWallet(paymentID: "", lastFour: "paypal")
        self.delegaet?.addAmount(paymentID: capture.paymentId ,amount: self.txtAmount.text ?? "")
        self.dismiss(animated: true)
    }
    
    func paymentCancel() {
//        print("payapal payment cancel")
    }
}

