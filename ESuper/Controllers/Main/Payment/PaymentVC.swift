//
//  HomeVC.swift
//  
//
//  Created by Elluminati on 14/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import UIKit
import Stripe
import PayPalCheckout

protocol PaymentVCDelegate: AnyObject {
    func orderCreateSuccessfully(orderId: String)
}

protocol PaymentVCTripPaymentDelegate: AnyObject {
    func didTripPaymentSuccess(orderId: String)
}

class PaymentVC: BottomPopupViewController,RightDelegate,LeftDelegate,didTapOnCancel,didTapOnTrackOrder {
    
    //MARK:- OutLets
    @IBOutlet weak var lblCurrentBalance: UILabel!
    @IBOutlet weak var lblCurrentWalletAmount: UILabel!
    @IBOutlet weak var btnAddToWallet: UIButton!
    @IBOutlet weak var txtWalletAmount: UITextField!
    @IBOutlet weak var lblWalletTitle: UILabel!
    @IBOutlet weak var lblWalletMessage: UILabel!
    @IBOutlet weak var lblUseWallet: UILabel!
    
    @IBOutlet weak var viewForAddWallet: UIView!
    @IBOutlet weak var viewForWallet: UIView!
    @IBOutlet weak var paymentTab: UITabBar!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var switchWalletStatus: UISwitch!
    @IBOutlet weak var lblSelectPaymentMethod: UILabel!
    
    /*containerView*/
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerForStripe: UIView!
    @IBOutlet weak var containerForCash: UIView!
    @IBOutlet weak var containerForPaystack: UIView!
    @IBOutlet weak var containerForPaytabs: UIView!
    @IBOutlet weak var containerForRazorpay: UIView!
    
    @IBOutlet weak var lblPayMessage: UILabel!
    @IBOutlet weak var btnAddCard: UIButton!
    
    @IBOutlet weak var constraintHeightWalletView: NSLayoutConstraint!
    @IBOutlet weak var lblNoGatways: UILabel!
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var btnLeft: UIButton!
    //PAYPAL
    @IBOutlet weak var containerForPayPal: UIView!
    @IBOutlet weak var vwPayPal: UIView!
    @IBOutlet weak var vwPayPalOrder: UIView!
    @IBOutlet weak var heightVwAddWallet: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightAddWallet: NSLayoutConstraint!
   
    @IBOutlet weak var btnSenMony: UIButton!
    var walletAmount = 0.0
    var isTableZero = false
    var amount = 0.0
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var isCashShow: Bool?
    var isPaymentPending: Bool?
    var delegateTripPayment: PaymentVCTripPaymentDelegate?
    
    //MARK: - Variables
    let socket:SocketHelper = SocketHelper.shared
    
    var finalTabItems:[UITabBarItem] = []
    var viewControllers:[UIViewController]? = []
    var containerViews:[UIView]? = []
    var paymentConfig:PaymentConfig = PaymentConfig.shared
    var stripePublishableKey:String = ""
    var isFullPaymentWallet = false
    var cashVC:CashVC!
    var stripeVC:StripeVC!
    var paystackVC:PayStackVC!
    var payPalVC: PayPalVC!
    var payTabsVC:PayTabsVC!
    var razorPayVC:RazorPayVC!
    
    var delegate: PaymentVCDelegate?
    var isZeroValue = false
    var isPayment = false
    
    override var popupHeight: CGFloat { height ?? (screenHeight - 200) }
    override var popupTopCornerRadius: CGFloat {
        if self.isBeingPresented {
            return topCornerRadius ?? 25.0
        }
        return 0
    }
    override var popupPresentDuration: Double { presentDuration ?? 0.3 }
    override var popupDismissDuration: Double { dismissDuration ?? 0.3 }
    override var popupShouldDismissInteractivelty: Bool { false }
    override var popupDimmingViewAlpha: CGFloat { BottomPopupConstants.dimmingViewDefaultAlphaValue }
    
    //MARK:- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        delegateRight = self
        finalTabItems = paymentTab.items!
        paymentTab.items?.removeAll()
        paymentConfig.paymentId = Payment.CASH
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        self.view.backgroundColor = UIColor.themeViewBackgroundColor
        viewForWallet.backgroundColor = UIColor.themeViewBackgroundColor
        viewForAddWallet.isHidden = true
        self.setNavigationTitle(title: "TXT_PAYMENTS".localizedCapitalized)
        btnAddToWallet.isHidden = true
        lblNoGatways.isHidden = true
        wsGetPaymentGateways()
        socket.connectSocket()
        
        if !self.isBeingPresented {
            btnLeft.isHidden = true
        }
        if isTableZero{
            wsPayOrderPayment()
        }
        switchWalletStatus.isOn = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        btnPayNow.isHidden = currentBooking.isHidePayNow
        vwPayPalOrder.isHidden = currentBooking.isHidePayNow
        lblWalletMessage.isHidden = currentBooking.isHidePayNow
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if payTabsVC != nil{
            payTabsVC.wsGetCardList()
        }
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setUpLayout()
    }
    
   
    func setUpLayout() {
        let title = "TXT_SELECT_PAYMENT_METHOD".localized
        let walletTitle = "TXT_WALLET".localized
        
        lblSelectPaymentMethod.text = title.appending("    ")
        lblWalletTitle.text = walletTitle.appending("    ")
        
        if (viewControllers?.count ?? 0) > 0 {
            let frame = containerForCash.frame
            scrollView.contentSize = CGSize(width: CGFloat(viewControllers!.count) * frame.size.width, height: frame.size.height)
        }
        btnAddToWallet.applyRoundedCornersWithHeight()
        btnAddCard.applyRoundedCornersWithHeight()
    }
    
    func adjustPaymentTabbar() {
        paymentTab.barTintColor = UIColor.themeViewBackgroundColor
        paymentTab.backgroundColor = UIColor.themeViewBackgroundColor
        let newItems:NSArray = NSArray.init(array: finalTabItems)
        
        let frame = containerForCash.frame
        paymentTab.setItems(newItems as? [UITabBarItem], animated: true)
        
        for i in 0..<containerViews!.count {
            containerViews?[i].frame =  CGRect.init(x: (frame.size.width * CGFloat(i)), y: frame.origin.y, width: frame.size.width, height: frame.size.height)
            scrollView.addSubview(containerViews![i])
        }
        scrollView.contentSize = CGSize(width: CGFloat(viewControllers!.count) * frame.size.width, height: frame.size.height)
        if finalTabItems.count > 0 {
            paymentTab.selectedItem = paymentTab.items?[0]
        }
    }
    
    func setLocalization() {
        //Colors
        btnAddToWallet.setTitleColor(UIColor.themeButtonTitleColor, for: UIControl.State.normal)
        btnAddToWallet.backgroundColor = UIColor.themeColor
        
        btnPayNow.setTitleColor(UIColor.themeButtonTitleColor, for: UIControl.State.normal)
        btnPayNow.backgroundColor = UIColor.themeButtonBackgroundColor
        
        lblPayment.textColor = UIColor.themeTitleColor
        lblPayment.font = FontHelper.textLarge(size: FontHelper.large)
        btnLeft.setImage(UIImage(named:"cancelIcon")?.imageWithColor(color: UIColor.themeColor), for: .normal)
        
        btnSenMony.setTitle("TXT_SEND_MONEY".localizedCapitalized, for: .normal)
        btnSenMony.setTitleColor(UIColor.themeButtonBackgroundColor, for: .normal)
        btnSenMony.titleLabel?.font = FontHelper.textRegular()// font(size: FontSize.regular, type: .Light)
        
        txtWalletAmount.textColor = UIColor.themeTextColor
        lblWalletTitle.textColor = UIColor.themeTitleColor
        lblWalletMessage.textColor = UIColor.themeTextColor
        lblWalletTitle.backgroundColor = UIColor.themeViewBackgroundColor
        lblSelectPaymentMethod.backgroundColor = UIColor.themeViewBackgroundColor
        lblSelectPaymentMethod.textColor = UIColor.themeTitleColor
        lblCurrentBalance.textColor = UIColor.themeLightTextColor
        lblCurrentWalletAmount.textColor = UIColor.themeTextColor
        //localizing text
        title = "TXT_PAYMENTS".localized
        lblCurrentBalance.text = "TXT_CURRENT_BALANCE".localizedCapitalized
        lblWalletMessage.text = "TXT_HOW_WOULD_YOU_LIKE_TO_PAY".localized + " " + CurrentBooking.shared.currency + " " + paymentConfig.total.toString()
        
        txtWalletAmount.placeholder = "TXT_WALLET_HINT".localized
        lblUseWallet.text = "TXT_USE_WALLET".localizedCapitalized
        btnPayNow.setTitle("TXT_PAY_NOW".localizedCapitalized, for: .normal)
        
        /* Set Font */
        lblCurrentWalletAmount.isHidden = false
        lblCurrentWalletAmount.textColor = UIColor.themeTextColor
        
        txtWalletAmount.font = FontHelper.textRegular()
        lblWalletTitle.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        lblSelectPaymentMethod.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        lblCurrentBalance.font = FontHelper.textRegular()
        lblCurrentWalletAmount.font = FontHelper.textLarge()
        btnAddToWallet.titleLabel?.font = FontHelper.labelRegular()
        lblNoGatways.font = FontHelper.textRegular()
        lblNoGatways.textColor = UIColor.themeSectionBackgroundColor
        lblPayMessage.font = FontHelper.textRegular()
        lblPayMessage.textColor = UIColor.themeSectionBackgroundColor
        
        btnAddCard.setTitleColor(UIColor.themeButtonTitleColor, for: UIControl.State.normal)
        btnAddCard.backgroundColor = UIColor.themeColor
        btnAddCard.titleLabel?.font = FontHelper.labelRegular()
        btnAddCard.setTitle("TXT_ADD_TEXT".localized, for: .normal)
        APPDELEGATE.setupNavigationbar()
        
        super.setBackBarItem(isNative: false)
        if preferenceHelper.IsQRUser {
            constraintHeightWalletView.constant = 0
        } else {
            super.setRightBarItem(isNative: false)
            super.setRightBarItemImage(image: UIImage.init(named: "walletHistory")!)
        }
        if preferenceHelper.IsSendMoneyForUser{
            self.btnSenMony.isHidden = false
        }else{
            self.btnSenMony.isHidden = true
        }
        
        self.delegateLeft = self
        self.hideBackButtonTitle()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font:FontHelper.textRegular(), NSAttributedString.Key.foregroundColor: UIColor.themeTextColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font:FontHelper.textLarge(), NSAttributedString.Key.foregroundColor: UIColor.themeColor], for: .selected)
    }
    
    override func updateUIAccordingToTheme() {
        super.setBackBarItem(isNative: false)
        super.setRightBarItem(isNative: false)
        super.setRightBarItemImage(image: UIImage.init(named: "walletHistory")!)
    }
    
    //MARK:    - USER DEFINE FUNCTION
    func updateWalletView(isUpdate:Bool) {
        if isUpdate {
            btnAddToWallet.tag = 1
            txtWalletAmount.text = ""
            txtWalletAmount.becomeFirstResponder()
            btnAddToWallet.setTitle("TXT_SUBMIT".localized, for: .normal)
            viewForAddWallet.isHidden = false
            lblCurrentWalletAmount.isHidden = true
        } else {
            btnAddToWallet.tag = 0
            lblCurrentWalletAmount.text = paymentConfig.wallet.toString(decimalPlaced: 2) +  " " + paymentConfig.walletCurrencyCode
            wsChangeWalletStatus(status:switchWalletStatus.isOn)
            self.setSendMoneyButton(amount:paymentConfig.wallet)
            txtWalletAmount.resignFirstResponder()
            btnAddToWallet.setTitle("TXT_ADD_TEXT".localized, for: .normal)
            viewForAddWallet.isHidden = true
            lblCurrentWalletAmount.isHidden = false
        }
        switchWalletStatus.isEnabled = paymentConfig.wallet > 0
    }
    
    func onClickRightButton() {
        self.performSegue(withIdentifier: SEGUE.WALLET_HISTORY, sender: self)
    }
    
    func onClickLeftButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initiateTabbarWith(vc:UIViewController, container:UIView) {
        self.addChild(vc)
        vc.view.frame = CGRect(x: container.frame.origin.x, y: container.frame.origin.y, width: container.frame.size.width, height: container.frame.size.height)
        container.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    func openWalletDialog() {
        let dialogForWallet = CustomVerificationDialog.showCustomVerificationDialog(title: "TXT_ADD_WALLET_AMOUNT".localized, message: "".localized, titleLeftButton: "".localized, titleRightButton: "TXT_SUBMIT".localized, editTextOneHint: "TXT_WALLET_HINT".localized, editTextTwoHint: "", isEdiTextTwoIsHidden: true)
        dialogForWallet.onClickLeftButton = { [unowned dialogForWallet] in
            dialogForWallet.removeFromSuperview()
        }
        dialogForWallet.onClickRightButton = { [unowned self, unowned dialogForWallet]  (text1:String,text2:String) in
            if (text1.trimmingCharacters(in: .whitespaces).count < 1) {
                Utility.showToast(message: "MSG_ENTER_VALID_AMOUNT".localized)
            } else if ((text1.trimmingCharacters(in: .whitespaces).doubleValue) == nil)  {
                Utility.showToast(message: "MSG_ENTER_VALID_AMOUNT".localized)
            } else if (text1.trimmingCharacters(in: .whitespaces).doubleValue!) <= 0 {
                Utility.showToast(message: "MSG_ENTER_VALID_AMOUNT".localized)
            } else {
                dialogForWallet.removeFromSuperview()
                self.handleWalletDialog(amount: text1)
            }
        }
    }
    
    func openPaystackPinVerificationDialog(requiredParam:String, reference:String, isWallet:Bool) {
        self.view.endEditing(true)
        switch requiredParam {
        case VerificationParameter.SEND_PIN:
            let dialogForPromo = DialogForCardPinVerification.showCustomVerificationDialog(title: "ENTER_PIN".localized, message: "EG_1234".localized, titleLeftButton: "", titleRightButton: "TXT_APPLY".localized, txtFPlaceholder: "ENTER_PIN".localized, isHideBackButton: false, isShowBirthdayTextfield: false)
            
            dialogForPromo.onClickLeftButton =
            { [unowned dialogForPromo] in
                dialogForPromo.removeFromSuperview();
            }
            
            dialogForPromo.onClickRightButton =
            { [unowned self, unowned dialogForPromo] (text:String) in
                
                if (text.count <  1)
                {
                    Utility.showToast(message: "PLEASE_ENTER_PIN".localized)
                }
                else
                {
                    wsSendPaystackRequiredDetail(requiredParam: requiredParam, reference: reference,pin: text,otp : "", phone: "", dialog: dialogForPromo, isWallet: isWallet)
                }
            }
        case VerificationParameter.SEND_OTP:
            let dialogForPromo = DialogForCardPinVerification.showCustomVerificationDialog(title: "ENTER_OTP".localized, message: "EG_123456".localized, titleLeftButton: "", titleRightButton: "TXT_APPLY".localized, txtFPlaceholder: "ENTER_OTP".localized,isHideBackButton: false, isShowBirthdayTextfield: false)
            
            dialogForPromo.onClickLeftButton =
            { [unowned dialogForPromo] in
                dialogForPromo.removeFromSuperview();
            }
            
            dialogForPromo.onClickRightButton =
            { [unowned self, unowned dialogForPromo] (text:String) in
                if (text.count <  1)
                {
                    Utility.showToast(message: "PLEASE_ENTER_OTP".localized)
                }
                else{
                    wsSendPaystackRequiredDetail(requiredParam: requiredParam, reference: reference,pin: "",otp : text, phone: "", dialog: dialogForPromo, isWallet: isWallet)
                }
            }
        case VerificationParameter.SEND_PHONE:
            let dialogForPromo = DialogForCardPinVerification.showCustomVerificationDialog(title: "ENTER_PHONE_NUMBER".localized, message: "MINIMUM_10_DIGITS".localized, titleLeftButton: "", titleRightButton: "TXT_APPLY".localized, txtFPlaceholder: "ENTER_PHONE_NUMBER".localized,isHideBackButton: false, isShowBirthdayTextfield: false)
            
            dialogForPromo.onClickLeftButton = { [unowned dialogForPromo] in
                dialogForPromo.removeFromSuperview();
            }
            
            dialogForPromo.onClickRightButton =
            { [unowned self, unowned dialogForPromo] (text:String) in
                if (text.count <  1)
                {
                    Utility.showToast(message: "PLEASE_ENTER_PHONE_NO".localized)
                }
                else
                {
                    wsSendPaystackRequiredDetail(requiredParam: requiredParam, reference: reference,pin: "",otp : "",phone:text, dialog: dialogForPromo, isWallet: isWallet)
                }
            }
            
        case VerificationParameter.SEND_BIRTHDAY:
            let dialogForPromo = DialogForCardPinVerification.showCustomVerificationDialog(title: "ENTER_BIRTHDATE".localized, message: "EG_DD-MM-YYYY".localized, titleLeftButton: "", titleRightButton: "TXT_APPLY".localized, txtFPlaceholder: "ENTER_BIRTHDATE".localized,isHideBackButton: false, isShowBirthdayTextfield: true)
            
            dialogForPromo.onClickLeftButton =
            { [unowned dialogForPromo] in
                dialogForPromo.removeFromSuperview();
            }
            
            dialogForPromo.onClickRightButton =
            { [unowned self, unowned dialogForPromo] (text:String) in
                if (text.count <  1){
                    Utility.showToast(message: "PLEASE_ENTER_BIRTHDATE".localized)
                }
                else{
                    wsSendPaystackRequiredDetail(requiredParam: requiredParam, reference: reference,pin: "",otp : "",phone:text, dialog: dialogForPromo, isWallet: isWallet)
                }
            }
        case VerificationParameter.SEND_ADDRESS:
            print(VerificationParameter.SEND_ADDRESS)
            //Didnt tested address flow
          
        default:
            break
        }
    }
    
    func wsSendPaystackRequiredDetail(requiredParam:String, reference:String, pin:String, otp:String, phone:String, dialog:DialogForCardPinVerification, isWallet:Bool) {
        Utility.showLoading()
        var dictParam : [String : Any] =
        [PARAMS.USER_ID      : preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN  : preferenceHelper.SessionToken,
         PARAMS.TYPE : CONSTANT.TYPE_USER,
         PARAMS.PAYMENT_ID : Payment.PAYSTACK,
         PARAMS.REFERENCE : reference,
         PARAMS.REQUIRED_PARAM : requiredParam,
         PARAMS.PIN : pin,
         PARAMS.OTP : otp,
         PARAMS.BIRTHDAY : "",
         PARAMS.ADDRESS : "",
         PARAMS.PHONE : ""]
        if !isWallet {
            dictParam[PARAMS.ORDER_PAYMENT_ID] = currentBooking.orderPaymentId!
        }
        
        let alamoFire:AlamofireHelper = AlamofireHelper();
        alamoFire.getResponseFromURL(url: WebService.SEND_PAYSTACK_REQUIRED_DETAIL, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam)
        { (response, error) -> (Void) in
            Utility.hideLoading()
            //print(response)
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: false) {
                if let walletResponse:Wallet = Wallet.init(dictionary: response) {
                    dialog.removeFromSuperview()
                    if isWallet {
                        self.paymentConfig.wallet = walletResponse.wallet ?? 0.0
                        self.paymentConfig.walletCurrencyCode = walletResponse.walletCurrencyCode
                        self.updateWalletView(isUpdate: false)
                    } else {
                        self.wsCreateOrder(paymentID: Payment.PAYSTACK)
                    }
                }
            } else {
                dialog.removeFromSuperview()
                if (response[PARAMS.REQUIRED_PARAM] as? String)?.count ?? "".count > 0{
                    self.openPaystackPinVerificationDialog(requiredParam: response[PARAMS.REQUIRED_PARAM] as? String ?? "", reference: response["reference"] as? String ?? "", isWallet: isWallet)
                } else {
                    if let strMsg = response["status_phrase"] as? String {
                        Utility.showToast(message: strMsg)
                    } else if (response["error_code"] as? String)?.count ?? "".count > 0{
                        if (response["error_message"] as? String ?? "").count > 0{
                            Utility.showToast(message: (response["error_message"] as? String ?? "").localized)
                        } else {
                            Utility.showToast(message: "ERROR_CODE_\(response["error_code"] as? String ?? "")".localized)
                        }
                    } else {
                        Utility.showToast(message: (response["error_message"] as? String ?? "").localized)
                    }
                }
            }
        }
    }
    
    func handleWalletDialog(amount: String)  {
        if ((amount.doubleValue) != nil) {
            let selectedTab = paymentTab.selectedItem
            switch(selectedTab?.tag ?? 0) {
            case 0:
                Utility.showToast(message: "MSG_SELECT_OTHER_PAYMENT_MODE".localized)
                break
            case 1:
                if stripeVC.selectedCard != nil {
                    wsGetStripeIntent(amount: Double(amount)!, payment_id: Payment.STRIPE)
                } else {
                    Utility.showToast(message: "MSG_PLEASE_ADD_CARD_FIRST".localized)
                }
                break
            case 2:
                if paystackVC.selectedCard != nil {
                    wsGetStripeIntent(amount: Double(amount)!, payment_id: Payment.PAYSTACK)
                } else {
                    Utility.showToast(message: "MSG_PLEASE_ADD_CARD_FIRST".localized)
                }
            case 4:
                if payTabsVC.selectedCard != nil {
                    wsGetStripeIntent(amount: Double(amount)!, payment_id: Payment.PAYTABS)
                } else {
                    Utility.showToast(message: "MSG_PLEASE_ADD_CARD_FIRST".localized)
                }
                break
            case 5:
                    wsGetStripeIntent(amount: Double(amount)!, payment_id: Payment.RAZORPAY)
                break
            default:
                Utility.showToast(message: "MSG_SELECT_OTHER_PAYMENT_MODE".localized)
            }
        } else {
            Utility.showToast(message: "MSG_ENTER_VALID_AMOUNT".localized)
        }
    }
    
    //MARK:- ACTION METHODS
    @IBAction func onClickBtnBack(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onClickBtnAddToWallet(_ sender: UIButton) {
        if sender.tag == 3{
            btnAddToWallet.tag = 0
            lblCurrentWalletAmount.text = paymentConfig.wallet.toString(decimalPlaced: 2) +  " " + paymentConfig.walletCurrencyCode
            wsChangeWalletStatus(status:switchWalletStatus.isOn)
            self.setSendMoneyButton(amount:paymentConfig.wallet)
            txtWalletAmount.resignFirstResponder()
            btnAddToWallet.setTitle("TXT_ADD_TEXT".localized, for: .normal)
            viewForAddWallet.isHidden = true
            lblCurrentWalletAmount.isHidden = false
            self.lblCurrentBalance.isHidden = false
        }else
        if paymentConfig.paymentId == Payment.PAYPAL{
            let add = self.storyboard?.instantiateViewController(withIdentifier: "AddWalletAmountVC") as! AddWalletAmountVC
            add.modalPresentationStyle = .overCurrentContext
            add.walletCurrencyCode = paymentConfig.walletCurrencyCode
            add.delegaet = self
            self.presentFromBottom(add)
        }else{
            openWalletDialog()
        }
    }
    
    @IBAction func onClickSwitchWallet(_ sender: Any) {
        self.view.endEditing(true)
        wsChangeWalletStatus(status:switchWalletStatus.isOn)
    }
    
    @IBAction func onClickBtnPayNow(_ sender: Any) {
        self.view.endEditing(true)
        let selectedTab = paymentTab.selectedItem
        if currentBooking.deliveryType == DeliveryType.taxi {
            btnPayNow.isUserInteractionEnabled = false
            if let isCashShow = isCashShow, let payPending = isPaymentPending {
                if !isCashShow && payPending {
                    let tags = selectedTab?.tag ?? 0
                    if tags == 5{
                        wsGetTripIntent(paymentID: Payment.RAZORPAY)
                    }else{
                        wsGetTripIntent(paymentID: Payment.STRIPE)
                    }
                    return
                } else {
                    self.taxiCreateOrder()
                    return
                }
            } else {
                self.taxiCreateOrder()
                return
            }
        }
        Utility.showLoading()
        switch(selectedTab?.tag ?? 0) {
        case 0:
            wsPayOrderPayment()
            break
        case 1:
            if stripeVC.selectedCard != nil {
                wsPayOrderPayment()
            } else {
                Utility.hideLoading()
                Utility.showToast(message: "MSG_PLEASE_ADD_CARD_FIRST".localized)
            }
            break
        case 2:
            if paystackVC.selectedCard != nil {
                wsPayOrderPayment()
            } else {
                Utility.hideLoading()
                Utility.showToast(message: "MSG_PLEASE_ADD_CARD_FIRST".localized)
            }
            break
        case 3:
            Utility.hideLoading()
            let paypal = PaypalHelper.init(currrencyCode: "usd", amount: self.paymentConfig.total.toString())
            paypal.delegate = self
            break
        case 4:
            if payTabsVC.selectedCard != nil {
                wsPayOrderPayment()
            } else {
                Utility.hideLoading()
                Utility.showToast(message: "MSG_PLEASE_ADD_CARD_FIRST".localized)
            }
            break
        case 5:
            Utility.hideLoading()
            wsPayOrderPayment()
            break
        default:
            Utility.hideLoading()
            Utility.showToast(message: "MSG_SELECT_OTHER_PAYMENT_MODE".localized)
        }
    }
    
    @IBAction func onClickClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func goToPoint(point:CGFloat) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.scrollView.contentOffset.x = point
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
   
    @IBAction func onBtnClickAddCard(_ sender: UIButton) {
        if paymentConfig.paymentId == Payment.PAYTABS {
            let paytab = self.storyboard?.instantiateViewController(withIdentifier: "PaytabsWebViewVC") as! PaytabsWebViewVC
            paytab.currency = paymentConfig.walletCurrencyCode
            paytab.delegate = self
            self.present(paytab, animated: true)
            
        }else
        if paymentConfig.paymentId == Payment.STRIPE {
            stripeVC.onClickBtnAddNewCard(btnAddToWallet)
        } else if paymentConfig.paymentId == Payment.PAYSTACK {
            self.performSegue(withIdentifier: SEGUE.PAYMENT_TO_PAYSTACK_WEBVIEW, sender: self)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier?.compare(SEGUE.PAYMENT_TO_PAYSTACK_WEBVIEW) == ComparisonResult.orderedSame {
            let destVC = (segue.destination as! PaystackWebViewVC)
            destVC.delegate = self
        }
  
    }
    func didTapOnCancel() {
        currentBooking.clearBooking()
        APPDELEGATE.goToMain()
    }
    
    func didTapOnTrackOrder() {
        if currentBooking.isQrCodeScanBooking {
            APPDELEGATE.clearQRUser()
            currentBooking.clearBooking()
            APPDELEGATE.goToMain()
        } else if Utility.isTableBooking() {
            currentBooking.clearBooking()
//            APPDELEGATE.goToMain()
            goToOrderList()
        } else {
            currentBooking.clearBooking()
            goToOrderList()
        }
    }
    
    //MARK: - NAVIGATION METHODS
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if (segue.identifier?.compare(SEGUE.SEGUE_STORE_LIST) == ComparisonResult.orderedSame) {
//            //let myStoreVC:StoreVC = segue.destination as! StoreVC
//        }
//    }
    
    //MARK: - WEB SERVICE
    func wsAddAmountToWallet(paymentID: String) {
        //add_wallet_amount
        Utility.hideLoading()
        self.paymentConfig.wallet = (self.paymentConfig.wallet + self.amount)
        self.updateWalletView(isUpdate: false)
        
        socket.updatePayment(paymentId: paymentID)
    }
    
    func wsChangeWalletStatus(status:Bool) {
        Utility.showLoading()
        let dictParam : [String : Any] =
        [PARAMS.USER_ID      : preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN  : preferenceHelper.SessionToken ,
         PARAMS.IS_WALLET   :  status,
         PARAMS.TYPE : CONSTANT.TYPE_USER]
        
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_CHANGE_WALLET_STATUS, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                Utility.hideLoading()
                let walletStatus:WalletStatusResponse = WalletStatusResponse.init(dictionary: response)!
                self.switchWalletStatus.setOn(walletStatus.isUseWallet, animated: true)
                self.setMessageAsParPayment(isWalletUsed: walletStatus.isUseWallet, walletAmount: self.paymentConfig.wallet, totalOrderInvoiceAmount: PaymentConfig.shared.total)
            }
        }
    }
    
    func wsGetTripIntent(paymentID: String) {
        Utility.showLoading()
        print(paymentID)
        let dictParam : [String : Any] =
        [PARAMS.USER_ID    : preferenceHelper.UserId  ,
         PARAMS.SERVER_TOKEN  : preferenceHelper.SessionToken,
         PARAMS.ORDER_ID : currentBooking.selectedOrderId ?? "",
         PARAMS.CURRENCY : paymentConfig.walletCurrencyCode,
         PARAMS.ISWEB : false,
         PARAMS.PAYMENT_ID : paymentID
        ]
        
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_GET_TRIP_PAYMENT_INTENT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                if let isPaymentPaid = response["is_payment_paid"] as? Bool {
                    if isPaymentPaid {
                        self.delegateTripPayment?.didTripPaymentSuccess(orderId: currentBooking.selectedOrderId ?? "")
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        if PaymentConfig.shared.paymentId == Payment.PAYTABS {
                            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                                if (response["authorization_url"] as? String ?? "")! != ""{
                                    let paytab = self.storyboard?.instantiateViewController(withIdentifier: "PaytabsWebViewVC") as! PaytabsWebViewVC
                                    paytab.currency = self.paymentConfig.walletCurrencyCode
                                    paytab.isPayment = true
                                    paytab.delegate = self
                                    paytab.orderType = "3"
                                    paytab.pstkUrl = (response["authorization_url"] as? String ?? "")!
                                    paytab.tran_ref = (response["payment_intent_id"] as? String ?? "")!
                                    self.present(paytab, animated: true)
                                    self.btnPayNow.isUserInteractionEnabled = true
                                    Utility.hideLoading()
                                    self.startPaytabsListner(orderType: "3", tran_ref: (response["payment_intent_id"] as? String ?? "")!)
                                }else{
                                    Utility.hideLoading()
                                }
                            }
                        }else
                        if PaymentConfig.shared.paymentId == Payment.STRIPE {
                            if let paymentMethod =  response["payment_method"] as? String, let clientSecret: String = response["client_secret"] as? String {
                                self.openStripePaymentMethod(paymentMethod: paymentMethod, clientSecret: clientSecret, isWallet: false)
                            } else {
                                Utility.showToast(message: "MSG_PAYMENT_FAILED".localized)
                            }
                        }else
                        if PaymentConfig.shared.paymentId == Payment.RAZORPAY{
                            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                                let options = response["options"] as? [String:Any]
                            
                                if (options!["order_id"] as? String ?? "")! != ""{
                                    let vc = UIStoryboard.init(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "RazorPayWebViewVC") as! RazorPayWebViewVC
                                    vc.htmlString = response["html_form"] as? String ?? ""
                                    vc.orderType = "3"
                                    let options = response["options"] as? [String:Any]
                                    vc.tran_ref = (options!["order_id"] as? String ?? "")!
                                    vc.success_url = (response["success_url"] as? String ?? "")!
                                    vc.fail_url = (response["fail_url"] as? String ?? "")!
                                    vc.delegate = self
                                    vc.isComeFromWallet = true//isComingFromWallet
                                    self.startRazorpayListner(orderType: "3", tran_ref: (options!["order_id"] as? String ?? "")!)
                                    self.btnPayNow.isUserInteractionEnabled = true
                                    self.present(vc, animated: true)
                                }else{
                                    Utility.hideLoading()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
   
    func wsTripPayment() {
        Utility.showLoading()
        let dictParam : [String : Any] =
        [PARAMS.USER_ID      : preferenceHelper.UserId  ,
         PARAMS.SERVER_TOKEN  : preferenceHelper.SessionToken,
         PARAMS.ORDER_ID : currentBooking.selectedOrderId ?? "",
        ]

        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_PAY_TRIP_PAYMENT_INTENT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                self.delegateTripPayment?.didTripPaymentSuccess(orderId: currentBooking.selectedOrderId ?? "")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func wsPayOrderPayment(paymentId: String = "") {
        let isPaymentCash = (Payment.CASH.compare(paymentConfig.paymentId) == ComparisonResult.orderedSame) ? true :false
        var dictParam : [String : Any] = [:]
        dictParam[PARAMS.USER_ID] = preferenceHelper.UserId
        dictParam[PARAMS.SERVER_TOKEN] = preferenceHelper.SessionToken
        dictParam[PARAMS.ORDER_PAYMENT_ID] = currentBooking.orderPaymentId ?? ""
        dictParam[PARAMS.PAYMENT_ID] = paymentConfig.paymentId
        dictParam[PARAMS.ORDER_TYPE] = CONSTANT.TYPE_USER
        dictParam[PARAMS.IS_PAYMENT_MODE_CASH] = isPaymentCash
        dictParam[PARAMS.ORDER_ID] = currentBooking.selectedOrderId
        dictParam[PARAMS.DELIVERY_TYPE] = currentBooking.deliveryType
        dictParam[PARAMS.CURRENCY] = paymentConfig.walletCurrencyCode   //Added for payTabs
        
        let address = preferenceHelper.Address
        var dictParamOrderDetails : [String : Any] = [:]
        dictParamOrderDetails[PARAMS.DELIVERY_USER_NAME] =   "\(preferenceHelper.FirstName) \(preferenceHelper.LastName)"
        dictParamOrderDetails[PARAMS.DELIVERY_USER_PHONE] = "\(preferenceHelper.PhoneNumber)"
        dictParamOrderDetails[PARAMS.IS_SCHEDULE_ORDER] = currentBooking.isFutureOrder
        dictParamOrderDetails[PARAMS.order_Start_At] = currentBooking.futureUTCMilliSecond
        dictParamOrderDetails[PARAMS.order_Start_At2] = currentBooking.futureUTCMilliSecond
        
//        dictParamOrderDetails[PARAMS.ORDER_START_AT] = 0
//        dictParamOrderDetails[PARAMS.ORDER_START_AT2] = 0
        dictParamOrderDetails[PARAMS.DELIVERY_TYPE] = currentBooking.deliveryType
        if  currentBooking.deliveryType == DeliveryType.courier{
            dictParamOrderDetails[PARAMS.SERVICE_ID] = CurrentBooking.shared.service_Id
            dictParamOrderDetails[PARAMS.IS_SCHEDULE_ORDER] = currentBooking.isFutureCourierOrder
            dictParamOrderDetails[PARAMS.order_Start_At] = currentBooking.futureTaxiUTCMilliSecond
            dictParamOrderDetails[PARAMS.order_Start_At2] = currentBooking.futureTaxiUTCMilliSecond2
        }
        dictParamOrderDetails[PARAMS.IS_BRING_CHANGE] = currentBooking.isBringChange
        dictParamOrderDetails[PARAMS.VEHICLE_ID] = currentBooking.selectedVehicleId
//        VEHICLE_ID : currentBooking.selectedVehicleId,
        
        dictParam[PARAMS.ORDER_DETAILS] = dictParamOrderDetails

        if currentBooking.deliveryType == DeliveryType.courier {
            dictParam[PARAMS.STORE_DELIVERY_ID] = currentBooking.courierDeliveryId
        } else if currentBooking.deliveryType == DeliveryType.taxi {
            dictParam[PARAMS.STORE_DELIVERY_ID] = currentBooking.taxiDeliveryId
        } else if currentBooking.cartResponse?.is_admin_services ?? false {
            dictParam[PARAMS.STORE_DELIVERY_ID] = currentBooking.cartResponse?.store_id ?? ""
        }
        if paymentConfig.paymentId == Payment.PAYPAL {
            dictParam[PARAMS.CAPTURE_AMOUNT] = paymentConfig.total
            dictParam[PARAMS.DELIVERY_TYPE]  = currentBooking.deliveryType
            dictParam[PARAMS.IS_PAYPAL] = true
            dictParam[PARAMS.PAYMENT_INTENT_ID] = paymentId
        }
        //print(dictParam)
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_PAY_ORDER_PAYMENT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: self.paymentConfig.paymentId != Payment.PAYSTACK) {
                let isPaymentPaid:Bool = (response.value(forKey: "is_payment_paid") as? Bool) ?? false
                if isPaymentPaid {
                    self.wsCreateOrder()
                } else {
                    Utility.hideLoading()
                    if isPaymentCash {
                        Utility.showToast(message: "MSG_PAYMENT_FAILED".localized)
                    } else {
                        if PaymentConfig.shared.paymentId == Payment.STRIPE{//stripe
                            if let paymentMethod =  response["payment_method"] as? String, let clientSecret: String = response["client_secret"] as? String {
                                self.openStripePaymentMethod(paymentMethod: paymentMethod, clientSecret: clientSecret, isWallet: false)
                            } else {
                                Utility.showToast(message: "MSG_PAYMENT_FAILED".localized)
                            }
                        }else
                        if PaymentConfig.shared.paymentId == Payment.PAYTABS{
                            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                                if (response["authorization_url"] as? String ?? "")! != ""{
                                    let paytab = self.storyboard?.instantiateViewController(withIdentifier: "PaytabsWebViewVC") as! PaytabsWebViewVC
                                    paytab.currency = self.paymentConfig.walletCurrencyCode
                                    paytab.isPayment = true
                                    paytab.delegate = self
                                    paytab.orderType = "2"
                                    paytab.pstkUrl = (response["authorization_url"] as? String ?? "")!
                                    paytab.tran_ref = (response["payment_intent_id"] as? String ?? "")!
                                    self.present(paytab, animated: true)
                                    Utility.hideLoading()
                                    self.startPaytabsListner(orderType: "2", tran_ref: (response["payment_intent_id"] as? String ?? "")!)
//                                    self.startPaytabsPayListner(tran_id: (response["payment_intent_id"] as? String ?? "")!)
                                }else{
                                    Utility.hideLoading()
                                }
                            }
                        }else
                        if PaymentConfig.shared.paymentId == Payment.RAZORPAY{
                            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                                let options = response["options"] as? [String:Any]
                                if (options!["order_id"] as? String ?? "")! != ""{
                                    let vc = UIStoryboard.init(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "RazorPayWebViewVC") as! RazorPayWebViewVC
                                    vc.htmlString = response["html_form"] as? String ?? ""
                                    vc.orderType = "2"
                                    let options = response["options"] as? [String:Any]
                                    vc.tran_ref = (options!["order_id"] as? String ?? "")!
                                    vc.success_url = (response["success_url"] as? String ?? "")!
                                    vc.fail_url = (response["fail_url"] as? String ?? "")!
                                    vc.delegate = self
                                    vc.isComeFromWallet = true//isComingFromWallet
                                    self.startRazorpayListner(orderType: "2", tran_ref: (options!["order_id"] as? String ?? "")!)
                                    self.present(vc, animated: true)
                                 }else{
                                    Utility.hideLoading()
                                }
                                
                            }
                        }
                    }
                }
            } else {
                if PaymentConfig.shared.paymentId == Payment.PAYSTACK {//paystack
                    if (response[PARAMS.REQUIRED_PARAM] as? String)?.count ?? "".count > 0{
                        self.openPaystackPinVerificationDialog(requiredParam: response[PARAMS.REQUIRED_PARAM] as? String ?? "", reference: response["reference"] as? String ?? "", isWallet:false)
                    } else {
                        Utility.showToast(message: response["error_message"] as? String ?? "")
                    }
                }
            }
        }
    }
    
   func wsCreateOrderStripe(paymentID:String = "") {
       Utility.hideLoading()

        socket.updatePaymentOrder(paymentId: paymentID)
       if currentBooking.deliveryType == DeliveryType.courier || currentBooking.deliveryType == DeliveryType.taxi {
           if currentBooking.deliveryType == DeliveryType.taxi {
               self.taxiCreateOrder(paymentID: paymentID)
           } else {
               self.goToOrderPlaced()
           }
       }else{
           self.goToOrderPlaced()
       }
    }
    func  wsCreateOrder(paymentID:String = "") {
        let isPaymentCash = (Payment.CASH.compare(paymentConfig.paymentId) == ComparisonResult.orderedSame) ? true :false
        if currentBooking.deliveryType == DeliveryType.courier || currentBooking.deliveryType == DeliveryType.taxi {
            var dictParam:[String:Any] = [ PARAMS.VEHICLE_ID : currentBooking.selectedVehicleId,
                                           PARAMS.CART_ID:currentBooking.cartId, PARAMS.USER_ID:preferenceHelper.UserId, PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
                                           PARAMS.STORE_DELIVERY_ID : currentBooking.courierDeliveryId,
                                           PARAMS.IS_BRING_CHANGE : currentBooking.isBringChange,//cashVC.switchBringChange.isOn ?? false,
                                           PARAMS.ORDER_TYPE : CONSTANT.TYPE_USER.description]
            dictParam[PARAMS.DELIVERY_TYPE] = "\(currentBooking.deliveryType)"
            dictParam[PARAMS.PAYMENT_INTENT_ID] = paymentID
            dictParam[PARAMS.IS_PAYMENT_MODE_CASH] = isPaymentCash
            dictParam[PARAMS.SERVICE_ID] = CurrentBooking.shared.service_Id
            dictParam[PARAMS.ORDER_PAYMENT_ID] = currentBooking.orderPaymentId!
            dictParam[PARAMS.PROMO_ID] = currentBooking.serviceViewPromoId
            dictParam[PARAMS.PROMO_CODE] = currentBooking.serviceViewPromoName
            dictParam[PARAMS.CURRENCY] = paymentConfig.walletCurrencyCode
            dictParam[PARAMS.TABLE_ID] =  currentBooking.tableID
            dictParam[PARAMS.IS_SCHEDULE_ORDER] = currentBooking.isFutureCourierOrder
            dictParam[PARAMS.order_Start_At] = currentBooking.futureTaxiUTCMilliSecond
            dictParam[PARAMS.order_Start_At2] = currentBooking.futureTaxiUTCMilliSecond2
            
            if currentBooking.isFutureTaxiOrder{
                dictParam[PARAMS.order_Start_At] = currentBooking.futureTaxiUTCMilliSecond
                dictParam[PARAMS.order_Start_At2] = currentBooking.futureTaxiUTCMilliSecond2
            }
            
            if currentBooking.deliveryType == DeliveryType.taxi {
                dictParam[PARAMS.DELIVERY_USER_NAME] = currentBooking.courierDestinationAddress[0].userDetails?.name ?? ""
                dictParam[PARAMS.DELIVERY_USER_PHONE] = currentBooking.courierDestinationAddress[0].userDetails?.phone ?? ""
            }
            let status = isPaymentCash ? false : currentBooking.isContactLessDelivery
            dictParam[PARAMS.IS_ALLOW_CONTACTLESS_DELIVERY] = String(isFullPaymentWallet ? currentBooking.isContactLessDelivery : status)
            let alamoFire:AlamofireHelper = AlamofireHelper()
            alamoFire.getResponseFromURL(url: WebService.WS_CREATE_ORDER, paramData: dictParam, images: currentBooking.courierImage) { (response, error) -> (Void) in
                Utility.hideLoading()
                print(response)
                let isSuccessToast: Bool = {
                    if currentBooking.deliveryType == DeliveryType.taxi {
                        return false
                    }
                    return true
                }()
                if Parser.isSuccess(response: response, withSuccessToast: isSuccessToast, andErrorToast: true) {
                    //preferenceHelper.setRandomCartID = (String.random(length: 20))
                    if currentBooking.deliveryType == DeliveryType.taxi {
                        if let id = response["order_id"] as? String {
                            self.delegate?.orderCreateSuccessfully(orderId: id)
                        }
                        self.dismiss(animated: true) {
                        }
                    } else {
                        self.goToOrderPlaced()
                    }
                }
            }
        } else {
            var isChange = false
            if cashVC != nil{
                if let change = cashVC.switchBringChange  {
                    isChange = change.isOn
                }
            }
            var dictParam:[String:Any] = [PARAMS.CART_ID:currentBooking.cartId, PARAMS.USER_ID:preferenceHelper.UserId, PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
                                          PARAMS.IS_SCHEDULE_ORDER: currentBooking.isFutureOrder,
                                          PARAMS.ORDER_START_AT :currentBooking.futureUTCMilliSecond,
                                          PARAMS.IS_USER_PICK_UP_ORDER : currentBooking.isUserPickUpOrder,
                                          PARAMS.DELIVERY_USER_NAME : currentBooking.deliveryName,
                                          PARAMS.DELIVERY_USER_PHONE : currentBooking.deliveryContact,
                                          PARAMS.ORDER_TYPE : CONSTANT.TYPE_USER,
                                          PARAMS.IS_BRING_CHANGE : isChange,
                                          PARAMS.PROVIDER_ID : currentBooking.manualAssignProviderID,
                                          PARAMS.TABLE_ID :  currentBooking.tableID,
                                          PARAMS.DELIVERY_TYPE:"\(currentBooking.deliveryType)"]
            let status = isPaymentCash ? false : currentBooking.isContactLessDelivery
            dictParam[PARAMS.IS_ALLOW_CONTACTLESS_DELIVERY] = isFullPaymentWallet ? currentBooking.isContactLessDelivery : status
            dictParam[PARAMS.PAYMENT_INTENT_ID] = paymentID
            dictParam[PARAMS.ORDER_PAYMENT_ID] = currentBooking.orderPaymentId!
            let addressDict:[[String:Any]] = [currentBooking.destinationAddress[0].toDictionary()]
            dictParam[Google.DESTINATION_ADDRESSES] = addressDict
            if currentBooking.futureUTCMilliSecond2 > 0 {
                dictParam[PARAMS.ORDER_START_AT2] = currentBooking.futureUTCMilliSecond2
            }

            let alamoFire:AlamofireHelper = AlamofireHelper()
            alamoFire.getResponseFromURL(url: WebService.WS_CREATE_ORDER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
                Utility.hideLoading()
                if Parser.isSuccess(response: response, withSuccessToast: true, andErrorToast: true) {
                    preferenceHelper.RandomCartID = (String.random(length: 20))
                    self.goToOrderPlaced()
                }
            }
        }
    }
    
    func taxiCreateOrder(paymentID : String = ""){
        let isPaymentCash = (Payment.CASH.compare(paymentConfig.paymentId) == ComparisonResult.orderedSame) ? true :false
        if  currentBooking.deliveryType == DeliveryType.taxi {
            var dictParam = CurrentTrip.shared.addItemIntoCartParam
            dictParam[PARAMS.VEHICLE_ID] = currentBooking.selectedVehicleId
            dictParam[PARAMS.CART_ID] = currentBooking.cartId
            dictParam[PARAMS.USER_ID] = preferenceHelper.UserId
            dictParam[PARAMS.SERVER_TOKEN] = preferenceHelper.SessionToken
            dictParam[PARAMS.STORE_DELIVERY_ID] = currentBooking.courierDeliveryId
            dictParam[PARAMS.IS_BRING_CHANGE] = currentBooking.isBringChange//false
            dictParam[PARAMS.ORDER_TYPE] =  CONSTANT.TYPE_USER.description
            
            dictParam[PARAMS.DELIVERY_TYPE] = currentBooking.deliveryType
            dictParam[PARAMS.TOTAL_DISTANCE] = currentBooking.estimateDistance
            dictParam[PARAMS.TOTAL_TIME] = currentBooking.estimateTime
            dictParam[PARAMS.NOTIFICATION] = true
            dictParam[PARAMS.BOOKING_TYPE] = currentBooking.bookingType
            dictParam[PARAMS.NO_OF_PERSONS] = 0
            dictParam[PARAMS.TABLE_NO] = 0
            dictParam[PARAMS.is_round_trip] = false
            dictParam[PARAMS.IS_TAX_INCLUDED] = false
            dictParam[PARAMS.IS_USE_ITEM_TAX] = false
            
            dictParam[PARAMS.PAYMENT_INTENT_ID] = paymentID
            dictParam[PARAMS.IS_PAYMENT_MODE_CASH] = isPaymentCash
            dictParam[PARAMS.SERVICE_ID] = CurrentBooking.shared.service_Id
//            dictParam[PARAMS.ORDER_PAYMENT_ID] = currentBooking.orderPaymentId!
            dictParam[PARAMS.PROMO_ID] = currentBooking.serviceViewPromoId
            dictParam[PARAMS.PROMO_CODE] = currentBooking.serviceViewPromoName
            dictParam[PARAMS.CURRENCY] = paymentConfig.walletCurrencyCode
            dictParam[PARAMS.IS_SCHEDULE_ORDER] = currentBooking.isFutureTaxiOrder
            if currentBooking.isFutureTaxiOrder{
                dictParam[PARAMS.order_Start_At] = currentBooking.futureTaxiUTCMilliSecond
                dictParam[PARAMS.order_Start_At2] = currentBooking.futureTaxiUTCMilliSecond2
            }
            if currentBooking.deliveryType == DeliveryType.taxi {
                dictParam[PARAMS.DELIVERY_USER_NAME] = currentBooking.courierDestinationAddress[0].userDetails?.name ?? ""
                dictParam[PARAMS.DELIVERY_USER_PHONE] = currentBooking.courierDestinationAddress[0].userDetails?.phone ?? ""
            }
            let status = isPaymentCash ? false : currentBooking.isContactLessDelivery
            dictParam[PARAMS.IS_ALLOW_CONTACTLESS_DELIVERY] = String(isFullPaymentWallet ? currentBooking.isContactLessDelivery : status)
            print("Parameter WS_CREATE_ORDER: \(dictParam)")
            
            let afn:AlamofireHelper = AlamofireHelper.init()
            afn.getResponseFromURL(url: WebService.WS_CREATE_ORDER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [weak self] (response,error) -> (Void) in
                self?.btnPayNow.isUserInteractionEnabled = true
                Utility.hideLoading()
                guard let self = self else { return }
                print("response \(response)")
                if let id = response["order_id"] as? String {
                    self.delegate?.orderCreateSuccessfully(orderId: id)
                }
                if CurrentBooking.shared.isPromoApplied{
                    if let id = response["order_payment_id"] as? String {
                        self.applyPromoforTaxi(orderPaymentId: id)
                    }
                }
                self.dismiss(animated: true) {
                }
            }
        }
    }
    func applyPromoforTaxi(orderPaymentId : String){
        var dictParams = CurrentBooking.shared.paramPromoInvoice
        let dictParam: Dictionary<String,Any> = [
            PARAMS.USER_ID:preferenceHelper.UserId,
            PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
            PARAMS.CITY_ID:currentBooking.bookCityId ?? "",
            PARAMS.PROMO_CODE: dictParams[PARAMS.PROMO_CODE] as? String ?? "",
            PARAMS.STORE_DELIVERY_ID : currentBooking.taxiDeliveryId,
            PARAMS.ORDER_PAYMENT_ID : orderPaymentId
        ]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_CHECK_PROMO_COURIER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response,error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                let invoiceResponse:InvoiceResponse = InvoiceResponse.init(dictionary: response)!
                
            } else{
            }
        }
    }
    func openMinAmountDialog(amount:String) {
        let minAmountMessage:String = "TXT_MIN_INVOICE_AMOUNT_MSG".localized + amount
        let dialogForMinAmount = CustomAlertDialog.showCustomAlertDialog(title: "TXT_MIN_AMOUNT".localized, message: minAmountMessage, titleLeftButton: "", titleRightButton: "TXT_ADD_MORE_ITEMS".localizedCapitalized)
        dialogForMinAmount.onClickLeftButton = {
            [unowned dialogForMinAmount] in
            dialogForMinAmount.removeFromSuperview()
        }
        dialogForMinAmount.onClickRightButton = {
            [unowned self,unowned dialogForMinAmount] in
            dialogForMinAmount.removeFromSuperview()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func openStripePaymentMethod(paymentMethod:String, clientSecret: String, isWallet:Bool=false) {
        StripeAPI.defaultPublishableKey = stripePublishableKey
        let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
        paymentIntentParams.paymentMethodId = paymentMethod
        let paymentHandler = STPPaymentHandler.shared()
        Utility.showLoading()
        paymentHandler.confirmPayment(paymentIntentParams, with: self) { [weak self] (status, paymentIntent, error) in
            Utility.showLoading()
            guard let self = self else { return }
            switch (status) {
            case .failed:
                if isWallet  {
                    self.updateWalletView(isUpdate: false)
                }
                Common.alert("", error!.localizedDescription)
                Utility.hideLoading()
                break
            case .canceled:
                if isWallet {
                    self.updateWalletView(isUpdate: false)
                }
                Utility.hideLoading()
                break
            case .succeeded:
                let id = paymentIntent?.stripeId ?? ""
                if isWallet {
                    self.wsAddAmountToWallet(paymentID: id)
                } else {
                    if let isCashShow = self.isCashShow, let isPaymentPending = self.isPaymentPending {
                        if !isCashShow && isPaymentPending && CurrentBooking.shared.deliveryType == DeliveryType.taxi {
                            self.wsTripPayment()
                            break
                        }
                    }
                    self.wsCreateOrderStripe(paymentID: id)
                }
                break
            @unknown default:
                fatalError()
                break
            }
        }
    }
    
    func wsGetPaymentGateways(isWalletUpdate : Bool = false){
        Utility.showLoading()
        var dictParam: [String:Any] = currentBooking.currentPlaceData.toDictionary()
        dictParam[PARAMS.TYPE] = CONSTANT.TYPE_USER
        if btnPayNow.isHidden {
            dictParam[PARAMS.CITY_ID] = ""
        }else {
            if currentBooking.deliveryType == DeliveryType.taxi {
                dictParam[PARAMS.CITY_ID] = currentBooking.bookCityId
            } else {
                dictParam[PARAMS.CITY_ID] = currentBooking.cartCityId
            }
        }
        
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_GET_PAYMENT_GATEWAYS, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            Parser.parsePaymentGateways(response, isShowCash: !self.btnPayNow.isHidden, completion: { (result) in
                preferenceHelper.PaypalClientId = (CONSTANT.PAYPAL_CLIENT_ID)
                if isWalletUpdate{
                    self.updateWalletView(isUpdate:false)
                }else{
                    self.updatePaymentGateWays()
                }
            })
        }
    }
    
    func wsGetStripeIntent(amount:Double, payment_id: String) {
        Utility.showLoading()
        self.amount = amount
        let dictParam : [String : Any] = [PARAMS.USER_ID : preferenceHelper.UserId,
                                          //                                          PARAMS.PAYMENT_ID : Payment.STRIPE,
                                          PARAMS.SERVER_TOKEN : preferenceHelper.SessionToken,
                                          PARAMS.AMOUNT: amount,
                                          PARAMS.TYPE : CONSTANT.TYPE_USER,
                                          PARAMS.ISWEB: false,
                                          PARAMS.WALLET_CURRENCY : paymentConfig.walletCurrencyCode,
                                          PARAMS.PAYMENT_ID : payment_id]
        
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.GET_STRIPE_PAYMENT_INTENT_WALLET, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            //print(response)
            Utility.hideLoading()
            if PaymentConfig.shared.paymentId == Payment.PAYTABS{
                if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                    if (response["authorization_url"] as? String ?? "")! != ""{
                        let paytab = self.storyboard?.instantiateViewController(withIdentifier: "PaytabsWebViewVC") as! PaytabsWebViewVC
                        paytab.currency = self.paymentConfig.walletCurrencyCode
                        paytab.isPayment = true
                        paytab.delegate = self
                        paytab.orderType = "1"
                        paytab.pstkUrl = (response["authorization_url"] as? String ?? "")!
                        paytab.tran_ref = (response["tran_ref"] as? String ?? "")!
                        self.present(paytab, animated: true)
                        Utility.hideLoading()
                        self.startPaytabsListner(orderType: "1", tran_ref: (response["payment_intent_id"] as? String ?? "")!)
//                        self.startPaytabsListner(tran_id: (response["tran_ref"] as? String ?? "")!)
                    }else{
                        Utility.hideLoading()
                    }
                }else{
                    Utility.hideLoading()
                }
            }else
            if PaymentConfig.shared.paymentId == Payment.STRIPE{//stripe
                if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                    if let paymentMethod =  response["payment_method"] as? String, let clientSecret: String = response["client_secret"] as? String {
                        self.openStripePaymentMethod(paymentMethod: paymentMethod, clientSecret: clientSecret,isWallet: true)
                    } else {
                        Utility.hideLoading()
                    }
                } else {
                    Utility.hideLoading()
                    Utility.showToast(message: response["error"] as? String ?? "")
                }
            } else if PaymentConfig.shared.paymentId == Payment.PAYSTACK {
                if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: false) {
                    if let walletResponse:Wallet = Wallet.init(dictionary: response) {
                        self.paymentConfig.wallet = walletResponse.wallet ?? 0.0
                        self.paymentConfig.walletCurrencyCode = walletResponse.walletCurrencyCode
                        self.updateWalletView(isUpdate: false)
                        Utility.hideLoading()
                    }
                } else {
                    Utility.hideLoading()
                    if (response[PARAMS.REQUIRED_PARAM] as? String)?.count ?? "".count > 0 {
                        self.openPaystackPinVerificationDialog(requiredParam: response[PARAMS.REQUIRED_PARAM] as? String ?? "", reference: response["reference"] as? String ?? "", isWallet: true)
                    } else {
                        Utility.showToast(message: response["error_message"] as? String ?? "")
                    }
                }
                }else if PaymentConfig.shared.paymentId == Payment.RAZORPAY {
                    if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: false) {
                        let vc = UIStoryboard.init(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "RazorPayWebViewVC") as! RazorPayWebViewVC
                        vc.htmlString = response["html_form"] as? String ?? ""
                        let options = response["options"] as? [String:Any]
                        vc.tran_ref = (options!["order_id"] as? String ?? "")!
                        vc.success_url = (response["success_url"] as? String ?? "")!
                        vc.fail_url = (response["fail_url"] as? String ?? "")!
                        vc.delegate = self
                        vc.isComeFromWallet = true//isComingFromWallet
                        self.present(vc, animated: true)
                    } else {
                        Utility.showToast(message: response["error_message"] as? String ?? "")
                    }
                }
        }
    }
    
    func updatePaymentGateWays() {
        finalTabItems.removeAll()
        viewControllers?.removeAll()
        containerViews?.removeAll()
        
        if !self.paymentConfig.paymentGateways.isEmpty {
            btnAddToWallet.isHidden = false
            for paymentGateway in self.paymentConfig.paymentGateways {
                //print("RKPAYMENT :- \(paymentGateway.id) :- \(paymentGateway.name)")
                if paymentGateway.id.compare(Payment.CASH) == ComparisonResult.orderedSame {
                    if !currentBooking.isContactLessDelivery{
                        if isCashShow != nil {
                            if !isCashShow! {
                                continue
                            }
                        }
                        cashVC = (storyboard?.instantiateViewController(withIdentifier: "cashVC"))! as? CashVC
                        viewControllers?.append(cashVC)
                        containerViews?.append(containerForCash)
                        initiateTabbarWith(vc: cashVC, container: containerForCash)
                        paymentConfig.paymentId = Payment.CASH
                        self.finalTabItems.append(UITabBarItem.init(title: paymentGateway.name?.capitalized ?? "CASH", image: nil, tag: 0))
                        cashVC.switchBringChange.addTarget(self, action: #selector(self.switchBringChange(_:)), for: .valueChanged)
                        
                    }
                }
                if paymentGateway.id.compare(Payment.STRIPE) == ComparisonResult.orderedSame {
                    stripeVC = (storyboard?.instantiateViewController(withIdentifier: "stripeVC"))! as? StripeVC
                    stripePublishableKey = paymentGateway.paymentKeyId
                    self.viewControllers?.append(stripeVC)
                    self.containerViews?.append(containerForStripe)
                    self.initiateTabbarWith(vc: stripeVC, container: containerForStripe)
                    self.finalTabItems.append(UITabBarItem.init(title: paymentGateway.name?.capitalized ?? "STRIPE", image: nil, tag: 1))
                    paymentConfig.paymentId = Payment.STRIPE
                }
                if paymentGateway.id.compare(Payment.PAYSTACK) == ComparisonResult.orderedSame {
                    paystackVC = (storyboard?.instantiateViewController(withIdentifier: "payStackVC"))! as? PayStackVC
                    self.viewControllers?.append(paystackVC)
                    self.containerViews?.append(containerForPaystack)
                    self.initiateTabbarWith(vc: paystackVC, container: containerForPaystack)
                    self.finalTabItems.append(UITabBarItem.init(title: paymentGateway.name?.capitalized ?? "PAYSTACK", image: nil, tag: 2))
                    paymentConfig.paymentId = Payment.PAYSTACK
                }
                if paymentGateway.id.compare(Payment.PAYPAL) == ComparisonResult.orderedSame {
                    paystackVC = (storyboard?.instantiateViewController(withIdentifier: "payStackVC"))! as? PayStackVC
                    self.viewControllers?.append(paystackVC)
                    self.containerViews?.append(containerForPayPal)
                    self.initiateTabbarWith(vc: paystackVC, container: containerForPayPal)
                    self.finalTabItems.append(UITabBarItem.init(title: paymentGateway.name?.capitalized ?? "PAYPAL", image: nil, tag: 3))
                    paymentConfig.paymentId = Payment.PAYPAL
                }
                if paymentGateway.id.compare(Payment.PAYTABS) == ComparisonResult.orderedSame {
                    payTabsVC = (storyboard?.instantiateViewController(withIdentifier: "PayTabsVC"))! as? PayTabsVC
                    self.viewControllers?.append(payTabsVC)
                    self.containerViews?.append(containerForPaytabs)
                    self.initiateTabbarWith(vc: payTabsVC, container: containerForPaytabs)
                    self.finalTabItems.append(UITabBarItem.init(title: paymentGateway.name?.capitalized ?? "PAYTABS", image: nil, tag: 4))
                    paymentConfig.paymentId = Payment.PAYTABS
                }
                if paymentGateway.id.compare(Payment.RAZORPAY) == ComparisonResult.orderedSame {
                    //print(paymentGateway.id)
                    paymentConfig.paymentId = Payment.RAZORPAY
                    razorPayVC = (storyboard?.instantiateViewController(withIdentifier: "RazorPayVC"))! as? RazorPayVC
                    self.viewControllers?.append(razorPayVC)
                    self.containerViews?.append(containerForRazorpay)
                    self.initiateTabbarWith(vc: razorPayVC, container: containerForRazorpay)
                    self.finalTabItems.append(UITabBarItem.init(title: paymentGateway.name?.capitalized ?? "RAZORPAY", image: nil, tag: 5))
                    paymentConfig.paymentId = Payment.RAZORPAY
                }
            }
            if finalTabItems.count > 0 {
                tabBar(paymentTab, didSelect: finalTabItems[0])
            }
            lblNoGatways.isHidden = true
        } else {
            btnAddToWallet.isHidden = true
            paymentTab.isHidden = true
            btnPayNow.isHidden = true
            scrollView.isHidden = true
            btnAddCard.isHidden = true
            lblNoGatways.text = "TXT_NO_PAYMENT".localized
            lblNoGatways.isHidden = false
        }
        switchWalletStatus.isOn = paymentConfig.isUseWallet
        updateWalletView(isUpdate:false)
        adjustPaymentTabbar()
        self.setMessageAsParPayment(isWalletUsed: paymentConfig.isUseWallet, walletAmount: paymentConfig.wallet, totalOrderInvoiceAmount: paymentConfig.total)
        scrollView.isScrollEnabled = true
    }
    
    func goToOrderPlaced() {
        let storyboard = UIStoryboard(name: "Cart", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderPlacedVC") as!  OrderPlacedVC
        vc.delegateOnCancel = self
        vc.delegateOnTrackOrder = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    func goToOrderList() {
        var isApppointments = false
        var isEcommerce = false
        if currentBookingType == DeliveryType.appoinment ||  currentBookingType == DeliveryType.service {
            isApppointments = true
        }
        if currentBookingType == DeliveryType.ecommerce{
            isEcommerce = true
        }
        APPDELEGATE.goToOrder(isAppointment: isApppointments,isEcoomerce: isEcommerce)
    }
    
    @objc func switchBringChange(_ sender: UISwitch) {
        //print(sender.isOn)
    }
}
//MARK: - Send money
        
extension PaymentVC{
    func setSendMoneyButton(amount : Double){
        self.btnSenMony.isHidden = true
        if preferenceHelper.IsSendMoneyForUser{
            self.btnSenMony.isHidden = false
            walletAmount = amount
            if amount > 0{
                self.btnSenMony.isHidden = false
            }
        }
    }
    
    @IBAction func actionSendMoney(_ sender: Any) {
        let sendMony = SendMoneyPopup.showCustomVerificationDialog(amount:self.walletAmount)
        sendMony.onClickCancelButton = {
            [unowned sendMony] in
            self.wsGetPaymentGateways()
            sendMony.removeFromSuperview()
        }

    }
}

//MARK: - UIScrollView Delegate methods
extension PaymentVC : UIScrollViewDelegate{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if paymentTab.items!.count > 1{
            if decelerate == false {
                let currentPage = scrollView.currentPage
                if currentPage <= ((viewControllers?.count) ?? 0 - 1) && currentPage >= 0 {
                    self.tabBar(paymentTab, didSelect: paymentTab.items![currentPage])
                    paymentTab.selectedItem = paymentTab.items?[currentPage]
                    
                }
            }
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if paymentTab.items!.count > 1{
            let currentPage = scrollView.currentPage
            if currentPage <= (((viewControllers?.count) ?? 0) - 1) && currentPage >= 0 {
                self.tabBar(paymentTab, didSelect: paymentTab.items![currentPage])
                paymentTab.selectedItem = paymentTab.items?[currentPage]
            }
        }
    }
    
}
//MARK: - UITabBar tab select Delegate methods
extension PaymentVC : UITabBarDelegate{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        self.view.endEditing(true)
        switch item.tag {
        case 0:
            paymentConfig.paymentId = Payment.CASH
            goToPoint(point: containerForCash.frame.origin.x)
            self.setUpForPaymentButton()
            btnAddCard.isHidden = true
            break
        case 1:
            paymentConfig.paymentId = Payment.STRIPE
            goToPoint(point: containerForStripe.frame.origin.x)
            self.setUpForPaymentButton()
            btnAddCard.isHidden = false
            break
        case 2:
            paymentConfig.paymentId = Payment.PAYSTACK
            goToPoint(point: containerForPaystack.frame.origin.x)
            self.setUpForPaymentButton()
            btnAddCard.isHidden = false
            break
        case 3:
            paymentConfig.paymentId = Payment.PAYPAL
            goToPoint(point: containerForPayPal.frame.origin.x)
            self.setUpForPaymentButton()
            btnAddCard.isHidden = true
            break
        case 4:
            paymentConfig.paymentId = Payment.PAYTABS
            goToPoint(point: containerForPaytabs.frame.origin.x)
            self.setUpForPaymentButton()
            btnAddCard.isHidden = false
            break
        case 5:
            paymentConfig.paymentId = Payment.RAZORPAY
            goToPoint(point: containerForRazorpay.frame.origin.x)
            self.btnAddCard.isHidden = true
            break
        default:
            break
        }
    }
}
extension PaymentVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let isValid = newString.count > 0
        
        self.vwPayPal.isHidden = !isValid
        self.btnAddToWallet.isHidden = isValid
        if textField == txtWalletAmount {
            let textFieldString = textField.text! as NSString
            let newString = textFieldString.replacingCharacters(in: range, with:string)
            let floatRegEx = "^([0-9]+)?(\\.([0-9]+)?)?$"
            let floatExPredicate = NSPredicate(format:"SELF MATCHES %@", floatRegEx)
            return floatExPredicate.evaluate(with: newString)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func setMessageAsParPayment(isWalletUsed:Bool, walletAmount:Double = PaymentConfig.shared.wallet, totalOrderInvoiceAmount:Double = PaymentConfig.shared.total) {
        var payMessage = "", payAmount = ""
        let youPaid = totalOrderInvoiceAmount - walletAmount
        var remainPay = 0.0
        if (isWalletUsed && paymentConfig.wallet > 0) {
            payMessage = String(format: NSLocalizedString("TXT_PAY_AMOUNT_FROM_WALLET", comment: ""),CurrentBooking.shared.currency,((youPaid > 0 ? totalOrderInvoiceAmount - youPaid : totalOrderInvoiceAmount)).toString())
            remainPay = youPaid > 0 ? youPaid : 0
        }else {
            remainPay = totalOrderInvoiceAmount
        }
        payAmount = String(format: NSLocalizedString("TXT_PAY", comment: ""),CurrentBooking.shared.currency,remainPay.toString())
//        if !isPayment{
//            if currentBooking.deliveryType != DeliveryType.taxi &&   currentBooking.deliveryType != DeliveryType.courier {
//                if remainPay.toString() == "0.00"{
//                    isZeroValue = true
//                    self.wsGetPaymentGateways()
//                }else{
//                    if isZeroValue{
//                        isZeroValue = false
//                        wsGetPaymentGateways()
//                    }
//                }
//            }
//        }
        isFullPaymentWallet = (remainPay == 0 && isWalletUsed)
        if (!payMessage.isEmpty() && !currentBooking.isHidePayNow) {
            if currentBooking.deliveryType == DeliveryType.taxi {
                lblPayMessage.text = "TXT_PAY_AMOUNT_FROM_WALLET_FOR_TRIP".localized
                if let isCashShow = isCashShow, let payPending = isPaymentPending {
                    if !isCashShow && payPending {
                        lblPayMessage.text = payMessage
                    }
                }
            } else {
                lblPayMessage.text = payMessage
            }
            lblPayMessage.isHidden = false
        }else {
            lblPayMessage.isHidden = true
        }
        if currentBooking.deliveryType == DeliveryType.taxi {
            btnPayNow.setTitle("txt_request_trip".localized, for: .normal)
            if let isCashShow = isCashShow, let payPending = isPaymentPending {
                if !isCashShow && payPending {
                    btnPayNow.setTitle(payAmount, for: .normal)
                }
            }
        } else {
            btnPayNow.setTitle(payAmount, for: .normal)
        }
        if cashVC != nil {
            if currentBooking.isQrCodeScanBooking {
                cashVC.lblCashMessage.text = String(format: NSLocalizedString("TXT_PAY_WITH_CASH", comment: ""),CurrentBooking.shared.currency,remainPay.toString())
            } else {
                cashVC.lblCashMessage.text = String(format: NSLocalizedString("TXT_PAY_CASH_AMOUNT", comment: ""),CurrentBooking.shared.currency,remainPay.toString())
            }
        }
    }
}

extension PaymentVC: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        Utility.hideLoading()
        return self
    }
}
//MARK: - PAYPAL Add wallete
extension PaymentVC : addAmountPaypal{
    func addAmount(paymentID: String,amount:String) {
        self.wsAddAmountToPaypalWallet(paymentID: paymentID,amount:amount)
    }
    
    
    func wsAddAmountToPaypalWallet(paymentID: String,amount:String) {
        Utility.showLoading()
        var dictParam : [String : Any] =
        [PARAMS.USER_ID      : preferenceHelper.UserId  ,
         PARAMS.SERVER_TOKEN  : preferenceHelper.SessionToken,
         PARAMS.TYPE : CONSTANT.TYPE_USER,
         PARAMS.PAYMENT_INTENT_ID : paymentID,
         PARAMS.WALLET : Double(amount) ?? 0.0,
         
        ]
        if paymentConfig.paymentId == Payment.STRIPE {
            dictParam[PARAMS.PAYMENT_ID] =  Payment.STRIPE
            dictParam[PARAMS.LAST_FOUR] = stripeVC.selectedCard?.lastFour ?? ""
        } else if paymentConfig.paymentId == Payment.PAYPAL {
            dictParam[PARAMS.PAYMENT_ID] = Payment.PAYPAL
            dictParam[PARAMS.LAST_FOUR] = "Paypal"
        }
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_ADD_WALLET_AMOUNT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                let walletResponse:Wallet = Wallet.init(dictionary: response)!
                self.paymentConfig.wallet = walletResponse.wallet ?? 0.0
                self.paymentConfig.walletCurrencyCode = walletResponse.walletCurrencyCode
                self.updateWalletView(isUpdate: false)
                Utility.hideLoading()
            }
        }
    }
    private func addPayPalOrderButtons() {
        let container = PaymentButtonContainer()
        container.frame = self.vwPayPalOrder.bounds
        container.setRound()
        self.vwPayPalOrder.addSubview(container)
        NSLayoutConstraint.activate(
            [
                container.centerYAnchor.constraint(equalTo: self.vwPayPalOrder.centerYAnchor),
                container.centerXAnchor.constraint(equalTo: self.vwPayPalOrder.centerXAnchor),
                container.widthAnchor.constraint(equalToConstant: self.vwPayPalOrder.frame.size.width),
                container.heightAnchor.constraint(equalToConstant: self.vwPayPalOrder.frame.size.height)
            ]
        )
    }
    func setUpForPaymentButton()  {
        self.btnAddToWallet.isHidden = false
        self.switchWalletStatus.isEnabled = true
        self.vwPayPalOrder.isHidden = true
        self.btnPayNow.isHidden = currentBooking.isHidePayNow
        
        if paymentConfig.paymentId == Payment.PAYPAL{
            self.btnAddCard.isHidden = true
        } else {
            self.updateWalletView(isUpdate: false)
            self.btnAddCard.isHidden = false
        }
    }
    //MARK: - PayPalCheckoutForOrder
    private func configurePayPalCheckoutForOrder() {
        Checkout.setCreateOrderCallback { action in
            let walletCurrencyCode = CurrencyCode.currencyCode(from: self.paymentConfig.walletCurrencyCode ?? "")
            let amount = PurchaseUnit.Amount(currencyCode: .usd, value: self.paymentConfig.total.toString())
            let purchaseUnit = PurchaseUnit(amount: amount)
            let order = OrderRequest(intent: .capture, purchaseUnits: [purchaseUnit])
            action.create(order: order)
        }
        Checkout.setOnApproveCallback { approval in
            approval.actions.capture { response, error in
                //print("Order successfully captured: \(response?.data)", response?.data.id ?? "")
                if response?.data.id != "" {
                    self.wsPayOrderPayment(paymentId: response?.data.id ?? "")
                }
            }
        }
        Checkout.setOnCancelCallback {
            // User has cancelled the payment experience
        }
        Checkout.setOnErrorCallback { error in
        }
    }
}
//MARK: - PAYTAB responce from webview
extension PaymentVC : responceBack{
    func backToPayment(orderType: String,isSuccess :Bool) {
        if isSuccess{
            if orderType == "1"{
                self.wsGetPaymentGateways(isWalletUpdate: true)
            }else if orderType == "2"{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.goToOrderPlaced()
                }
            }else if orderType == "3"{
                self.wsTripPayment()
            }
        }else{
            Utility.showLoading()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                Utility.showLoading()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 60.0) {
                Utility.hideLoading()
            }
        }
    }
    func startPaytabsListner(orderType : String ,tran_ref : String ) {
        let socket =  "'\(tran_ref)'"//"'\(socket.paytabs)\(preferenceHelper.UserId)'"
        self.socket.socket?.on(socket) {
            [weak self] (data, ack) in
            guard let self = self else {
                return
            }
            guard let response = data.first as? [String:Any] else {
                return
            }
            printE("RRRRRR  Soket Response\(response)")
            let status = (response["payment_status"] as? Bool) ?? false
            let error =  (response["error"] as? String ?? "")
            //print(error)
//            self.dismiss(animated: true)
            if status {
                Utility.showToast(message: "MSG_CODE_91".localized)
            }
            if error != ""{
                Utility.showToast(message: "\(error)".localized)
            }else{
                self.backToPayment(orderType: orderType, isSuccess: true)

            }
        }
    }
    func startRazorpayListner(orderType : String ,tran_ref : String,isTrip :Bool = false ) {
        let socket =  "'\(tran_ref)'"//"'\(socket.paytabs)\(preferenceHelper.UserId)'"
        self.socket.socket?.on(socket) {
            [weak self] (data, ack) in
            guard let self = self else {
                return
            }
            guard let response = data.first as? [String:Any] else {
                return
            }
            printE("RRRRRR  Soket Response\(response)")
            
            let status = (response["payment_status"] as? Bool) ?? false
            let error =  (response["error"] as? String ?? "")
            let orderPaymentId =  (response["order_payment_id"] as? String ?? "")
            //print(error)
//            self.dismiss(animated: true)
            if status {
                Utility.showToast(message: "MSG_CODE_91".localized)
            }
            if error != ""{
                Utility.showToast(message: "\(error)".localized)
            }else{
                if orderType == "3"{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        self.delegateTripPayment?.didTripPaymentSuccess(orderId: currentBooking.selectedOrderId ?? "")
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    self.wsCreateOrder(paymentID: orderPaymentId)
                }
            }
        }
    }
}
// Paypal pay amount
extension PaymentVC: PaypalHelperDelegate {
    func paymentSucess(capture: PaypalCaptureResponse) {
        self.wsPayOrderPayment(paymentId:  capture.paymentId)
    }
    
    func paymentCancel() {
        //print("payapal payment cancel")
    }
}
extension PaymentVC: responceBackPaystack {
    func backToPaymentPaystack(orderType: String, isSuccess: Bool) {
//        self.wsGetPaymentGateways()
        paystackVC.wsGetCardList()
    }
    
    
}
