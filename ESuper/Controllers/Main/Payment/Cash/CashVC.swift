//
//  StripeVCViewController.swift
//  
//
//  Created by Elluminati on 17/04/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import UIKit

class CashVC: BaseVC {
    
    //MARK: - Outlets
    @IBOutlet weak var lblCashMessage: UILabel!
    @IBOutlet weak var lblKeepBringChange: UILabel!
    @IBOutlet weak var switchBringChange: UISwitch!
    @IBOutlet weak var viewBringChange: UIView!
    
    //MARK: - View Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.themeViewBackgroundColor
        setLocalization()
        if preferenceHelper.IsAllowBringChange {
            if currentBooking.isUserPickUpOrder || currentBooking.isQrCodeScanBooking || Utility.isTableBooking() {
                viewBringChange.isHidden = true
            } else {
                viewBringChange.isHidden = false
            }
        } else {
            viewBringChange.isHidden = true
        }
    }
    
    func setLocalization() {
        lblCashMessage.textColor = UIColor.themeTextColor
        lblCashMessage.font = FontHelper.textRegular()
        lblCashMessage.text = "TXT_CASH_MESSAGE".localized
        lblCashMessage.textAlignment = .center
        
        lblKeepBringChange.text = "txt_keep_bring_change".localized
        lblKeepBringChange.font = FontHelper.textMedium(size: FontHelper.mediumLarge)
        
        if currentBooking.deliveryType == DeliveryType.taxi {
            lblCashMessage.isHidden = true
        } else {
            lblCashMessage.isHidden = false
        }
    }
    @IBAction func switchKeep (_ sender : UISwitch){
        if sender.isOn{
            print("onnnnn")
            currentBooking.isBringChange = true
        }else{
            print("Offff")
            currentBooking.isBringChange = false
        }
    }
}
