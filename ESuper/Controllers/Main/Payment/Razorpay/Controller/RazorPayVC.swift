//
//  RazorPayVC.swift
//  Edelivery
//
//  Created by Sahil's MacPro on 16/11/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import UIKit

class RazorPayVC: UIViewController {
    @IBOutlet weak var lblMessage : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMessage.textColor = UIColor.gray
        lblMessage.font = FontHelper.textRegular()
        lblMessage.text = "Pay with razorpay simple and better"
        // Do any additional setup after loading the view.
    }

}
