//
//  RazorPayWebViewVC.swift
//  Edelivery
//
//  Created by Sahil's MacPro on 08/11/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import UIKit
import WebKit

class RazorPayWebViewVC: UIViewController {

    // MARK: - OUTLETS
    @IBOutlet var webView: WKWebView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    let socket:SocketHelper = SocketHelper.shared
    
    var tran_ref = ""
    var fail_url = ""
    var success_url = ""
    var orderType = "1"
    var delegate: responceBack?
    var htmlString = ""
    var isComeFromWallet = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        socket.connectSocket()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.navigationBar.isHidden = true
        self.view.addSubview(self.webView)
        self.view.bringSubviewToFront(self.webView)
        //print(htmlString)
        self.startRazorpayListner()
        self.webView.loadHTMLString(htmlString, baseURL: Bundle.main.bundleURL)
        self.webView.navigationDelegate = self
        
    }
    
    // MARK: - ACTION METHOD
    @IBAction func onBtnClickBack(_ sender: UIButton) {
        self.dismiss(animated: true)
    }

    func startRazorpayListner() {
//        let socket =  "'payment_status\(preferenceHelper.UserId)'"
        let socket =  "'\(tran_ref)'"//"'\(socket.paytabs)\(preferenceHelper.UserId)'"
        self.socket.socket?.on(socket) {
            [weak self] (data, ack) in
            
            guard let self = self else {
                return
            }
            guard let response = data.first as? [String:Any] else {
                return
            }
            printE("RRRRRR  Soket Response\(response)")
            let status = (response["payment_status"] as? Bool) ?? false
            let error =  (response["error"] as? String ?? "")
            let message =  (response["message"] as? String ?? "")
            Utility.showToast(message: message)

            //print(error)
            self.dismiss(animated: true)    
            if status {
                Utility.showToast(message: "MSG_CODE_91".localized)
            }
            if error != ""{
                Utility.showToast(message: "\(error)".localized)
            }else{
                if self.orderType == "1"{
                    self.delegate?.backToPayment(orderType: self.orderType, isSuccess: true)
                }

            }
        }
    }
}

extension RazorPayWebViewVC: WKNavigationDelegate {
   
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            if let url = navigationAction.request.url {
            //print("URL->>> \(url)")
            if (url.absoluteString.contains("\(success_url)")){
                self.dismiss(animated: true)
                if isComeFromWallet {
                    delegate?.backToPayment(orderType: "1",isSuccess:true)
                } else {
                    delegate?.backToPayment(orderType: "0",isSuccess:true)
                }
            }else if (url.absoluteString.contains("\(fail_url)")){
                self.dismiss(animated: true)
                if isComeFromWallet {
                    delegate?.backToPayment(orderType: "1",isSuccess:false)
                } else {
                    delegate?.backToPayment(orderType: "0",isSuccess:false)
                }
            }
            if (url.absoluteString.contains("\(WebService.BASE_URL)payments") && url.absoluteString.contains("pay_order_payment") == false) || (url.absoluteString.contains("\(WebService.BASE_URL)payments") && url.absoluteString.contains("add_wallet_amount") == false) {
                
                self.dismiss(animated: true)
                //print("Payment done")
                
                if isComeFromWallet {
                    delegate?.backToPayment(orderType: "1",isSuccess:true)
                } else {
                    delegate?.backToPayment(orderType: "0",isSuccess:true)
                }
                
            } else if url.absoluteString.contains("\(WebService.BASE_URL)payments_fail"){
                
                decisionHandler(.cancel)
                self.dismiss(animated: true)
                //print("Payment failed")
                delegate?.backToPayment(orderType: "1",isSuccess:true)
                self.navigationController?.popViewController(animated: true)
                
            }
        }
        decisionHandler(.allow)
    }
    
}
