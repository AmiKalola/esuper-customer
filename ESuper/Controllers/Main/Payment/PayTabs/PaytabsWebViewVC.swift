//
//  PaytabsWebViewVC.swift
//  Edelivery
//
//  Created by Rohit on 17/06/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import UIKit
import WebKit
//1 for Wallet
//2 for pay
protocol responceBack {
    func backToPayment(orderType : String,isSuccess :Bool)
}
class PaytabsWebViewVC: BaseVC {
    @IBOutlet var webView: WKWebView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var htmlDataString: String!
    let socket:SocketHelper = SocketHelper.shared
    
    var delegate : responceBack?
    var isPayment = false
    var pstkUrl = ""
    var tran_ref = ""
    var currency = ""
    var orderType = "1"
    
    var gotPayUResopnse: ((_ message: String, _ isCallIntentAPI:Bool, _ showPaymentRetryDialog: Bool) -> Void)?
    var iSFrom : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        socket.connectSocket()
        
        if isPayment{
            socket.connectSocket()
        }else{
            initialViewSetup()
        }
        
    }
    
    func initialViewSetup()
    {
        lblTitle.text = "TXT_PAYMENTS".localized
        lblTitle.textColor = UIColor.themeTextColor
        lblTitle.font = FontHelper.textMedium(size: FontHelper.medium)
        self.btnBack.isHidden = false
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.navigationBar.isHidden = true
        if isPayment{
            
            self.startPaytabsListner()
            self.view.addSubview(self.webView)
            self.view.bringSubviewToFront(self.webView)
            let urlRequest = URLRequest.init(url: URL.init(string: pstkUrl)!)
            self.webView.load(urlRequest)
            self.webView.navigationDelegate = self
        }else{
            wsGetStripeIntentPayStack()
        }
        
        
    }

    @IBAction func onClickBtnMenu(_ sender: Any) {
        delegate?.backToPayment(orderType: self.orderType, isSuccess: false)
        self.dismiss(animated: true)
    }
    func startPaytabsListner() {
        let socket =  "'\(tran_ref)'"//"'\(socket.paytabs)\(preferenceHelper.UserId)'"
        self.socket.socket?.on(socket) {
            [weak self] (data, ack) in
            guard let self = self else {
                return
            }
            guard let response = data.first as? [String:Any] else {
                return
            }
            printE("RRRRRR  Soket Response\(response)")
            let status = (response["payment_status"] as? Bool) ?? false
            let error =  (response["error"] as? String ?? "")
            self.dismiss(animated: true)
            if status {
                Utility.showToast(message: "MSG_CODE_91".localized)
            }
            if error != ""{
                Utility.showToast(message: "\(error)".localized)
            }else{
                self.delegate?.backToPayment(orderType: self.orderType, isSuccess: true)

            }
        }
    }
}

extension PaytabsWebViewVC: WKNavigationDelegate {
    
    //MARK:- Get Stripe Intent
    func wsGetStripeIntentPayStack()
    {
        Utility.showLoading()
        let dictParam : [String : Any] =
        [PARAMS.USER_ID      : preferenceHelper.UserId  ,
         PARAMS.SERVER_TOKEN  : preferenceHelper.SessionToken,
         PARAMS.TYPE : CONSTANT.TYPE_USER,
         PARAMS.CURRENCY : currency,
         PARAMS.ISWEB : false,
         PARAMS.PAYMENT_ID : Payment.PAYTABS]
        
        let alamoFire:AlamofireHelper = AlamofireHelper();
        alamoFire.getResponseFromURL(url: WebService.GET_STRIPE_ADD_CARD_INTENT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam)
        {
            (response, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                Utility.hideLoading()
//                print(response["authorization_url"] as? String)
                self.tran_ref = response["tran_ref"] as? String ?? ""
                self.startPaytabsListner()
                let pstkUrl = response["authorization_url"] as? String
                let urlRequest = URLRequest.init(url: URL.init(string: pstkUrl!)!)
                self.view.addSubview(self.webView)
                self.view.bringSubviewToFront(self.webView)
                self.webView.load(urlRequest)
                self.webView.navigationDelegate = self
                
            } else {
                Utility.hideLoading()
            }
        }
    }
    
    //This is helper to get url params
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    // This is a WKNavigationDelegate func we can use to handle redirection
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void))  {
        if let url = navigationAction.request.url {
            if url.absoluteString.contains("\(WebService.BASE_URL)payments"){
                self.dismiss(animated: true)
                delegate?.backToPayment(orderType: self.orderType,isSuccess:true)
                decisionHandler(.allow)
            }else
            if url.absoluteString.contains("add_card_success"){
                self.dismiss(animated: true)
                //self.navigationController?.popViewController(animated: true)
                decisionHandler(.cancel)
            } else if url.absoluteString.contains("\(WebService.BASE_URL)payment_fail"){
                self.dismiss(animated: true)
//                delegate?.backToPayment(orderType: self.orderType,isSuccess:false)
                decisionHandler(.allow)
            } else if url.absoluteString.contains("\(WebService.BASE_URL)fail_stripe_intent_payment"){
                self.dismiss(animated: true)
                delegate?.backToPayment(orderType: self.orderType,isSuccess:false)
                decisionHandler(.allow)
            }else if url.absoluteString.contains("add_card_failed"){
                self.dismiss(animated: true)
                //                self.navigationController?.popViewController(animated: true)
                decisionHandler(.cancel)
            }
            else{
                decisionHandler(.allow)
            }
        }else{
            decisionHandler(.cancel)
        }
    }
}
