//
//  NotificationCell.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit
class NotificationCell: CustomTableCell {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        lblMessage.textColor = UIColor.themeTextColor
        lblTime.textColor = UIColor.themeLightTextColor
        lblMessage.font = FontHelper.textRegular()
        lblTime.font = FontHelper.textSmall()
    }
}

