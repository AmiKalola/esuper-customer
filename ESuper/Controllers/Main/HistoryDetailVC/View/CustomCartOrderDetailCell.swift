//
//  CustomCartOrderDetailCell.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit

class CustomCartOrderDetailCell: CustomTableCell {
    
    /*View For Items*/
    @IBOutlet weak var viewForItem: UIView!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemQty: UILabel!
    @IBOutlet weak var lblnote: UILabel!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var lblItemnote: UILabel!
    @IBOutlet weak var heightForTable: NSLayoutConstraint!
    /*Table For Selected Specification of Ordered Item*/
    @IBOutlet weak var tableForItemSpecification: UITableView!
    var arrForOrderItemSpecification:[Specifications] = []
    var strCurrency:String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        tableForItemSpecification.dataSource = self
        tableForItemSpecification.delegate = self
        tableForItemSpecification.estimatedRowHeight = 35
        tableForItemSpecification.rowHeight = UITableView.automaticDimension
        tableForItemSpecification.estimatedSectionHeaderHeight = 40
        tableForItemSpecification.sectionHeaderHeight = UITableView.automaticDimension
        viewForItem.backgroundColor = UIColor.themeViewBackgroundColor
        lblItemPrice.textColor = UIColor.themeTextColor
        lblItemName.textColor = UIColor.themeTextColor
        lblItemQty.textColor = UIColor.themeTextColor
        lblItemnote.textColor = UIColor.themeTextColor
        footerView.backgroundColor = UIColor.themeViewBackgroundColor
        lblnote.textColor = UIColor.themeTextColor
        lblItemQty.font = FontHelper.textSmall()
        lblItemName.font = FontHelper.textMedium()
        lblItemPrice.font = FontHelper.textMedium()
        lblItemnote.font = FontHelper.textSmall()
        lblnote.font = FontHelper.textSmall()
        lblnote.text = "TXT_NOTE_FOR_ITEM".localizedCapitalized
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        self.tableForItemSpecification.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCellData(itemDetail:CartProductItems, currency:String) {
        
        self.strCurrency = currency
        
        if itemDetail.noteForItem.isEmpty {
            lblItemnote.isHidden = true
            lblnote.isHidden = true
            footerView.isHidden = true
        }else {
            lblItemnote.text =  itemDetail.noteForItem
            lblItemnote.isHidden = false
            lblnote.isHidden = false
            footerView.isHidden = false
        }
        
        if (itemDetail.item_price! > 0.0) {
            lblItemQty.text = "TXT_QTY".localized + " " + String(itemDetail.quantity!)  + " X " + currency + " " + ((itemDetail.item_price) ?? 0.0).toString()
        }else {
            lblItemQty.text = "TXT_QTY".localized + " " + String(itemDetail.quantity!)
        }
        lblItemName.text = itemDetail.item_name!
        lblItemPrice.text = currency + " " + ((itemDetail.total_specification_price! +  itemDetail.item_price!) * Double(itemDetail.quantity!)).toString()
        arrForOrderItemSpecification = itemDetail.specifications
        self.tableForItemSpecification.reloadData()
        heightForTable.constant = tableForItemSpecification.contentSize.height
        self.layoutIfNeeded()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tableForItemSpecification.layer.removeAllAnimations()
        heightForTable.constant = tableForItemSpecification.contentSize.height
        UIView.animate(withDuration: 0.1) {
            self.updateConstraints()
            self.layoutIfNeeded()
        }
        
    }
}

//MARK: - Tableview DataSource Methods
extension CustomCartOrderDetailCell:UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return    arrForOrderItemSpecification.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrForOrderItemSpecification[section].list!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderItemSpecificationCell
        cell.setCellData(listItemDetail:arrForOrderItemSpecification[indexPath.section].list![indexPath.row],currency: strCurrency)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionHeader = tableView.dequeueReusableCell(withIdentifier: "section")! as! OrderItemSpecificationSection
    
        sectionHeader.setCellData(title: arrForOrderItemSpecification[section].name ?? "",price: arrForOrderItemSpecification[section].price!,currency: strCurrency)
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
