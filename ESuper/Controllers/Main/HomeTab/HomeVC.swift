//
//  HomeVC.swift
//  
//
//  Created by Elluminati on 14/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import UIKit
import GooglePlaces
import Alamofire
var isTakeAway = 0
class HomeVC: BaseVC, UIGestureRecognizerDelegate, UIScrollViewDelegate {

    //MARK: - OutLets
    @IBOutlet weak var lblEmpty: UILabel!
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var imgStoreEmpty: UIImageView!
    @IBOutlet weak var viewForHeader: UIView!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var lblGradient: UILabel!
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var collViewStoreAds: UICollectionView?
    @IBOutlet weak var collViewStoreList: UICollectionView?
    @IBOutlet weak var collectionWhatsMind: UICollectionView?
    @IBOutlet weak var lblAddDetail: UILabel!
    @IBOutlet weak var collViewNewList: UICollectionView?
    @IBOutlet weak var containerForStoreVC: UIView!
    @IBOutlet weak var tableForDelivery: UITableView?
    @IBOutlet weak var btnSearchLocation: UIButton!
    @IBOutlet weak var viewDeliveryAds: UIView?
    @IBOutlet weak var viewStoreAds: UIView?
    @IBOutlet weak var viewStoreList: UIView?
    @IBOutlet weak var lblDeliveryAdsTitle: UILabel!
    @IBOutlet weak var lblStoreAdsTitle: UILabel!
    @IBOutlet weak var lblStoreListTitle: UILabel!
    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var btnFilterStore: UIButton!
    @IBOutlet weak var btnSearchStore: UIButton!
    @IBOutlet weak var txtSearchStore: UITextField!
    @IBOutlet weak var viewForOffers: UIView?
    @IBOutlet weak var collViewOffers: UICollectionView?
    @IBOutlet weak var lblOffers: UILabel!
    @IBOutlet weak var viewCartContainer: UIView?
    
    
    @IBOutlet weak var lblCartText: UILabel?
    @IBOutlet weak var lblCartQuantity: UILabel?
    @IBOutlet weak var heightForHeader: NSLayoutConstraint!
    @IBOutlet weak var heightForStoreList: NSLayoutConstraint?
    @IBOutlet weak var stackViewStoreList: UIStackView?
    
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var lblWhatsYourMind: UILabel?
    @IBOutlet weak var heightWhatsYourMind: NSLayoutConstraint?
    @IBOutlet weak var viewNoData: UIStackView?
    
    @IBOutlet weak var lblAppointment: UILabel?
    @IBOutlet weak var colletionViewAppointment: UICollectionView?
    @IBOutlet weak var viewAppointment: UIView?
    
    @IBOutlet weak var lblAdminService: UILabel?
    @IBOutlet weak var viewAdminService: UIView?
    @IBOutlet weak var colletionAdminService: UICollectionView?
    
    @IBOutlet weak var heightDeliveryCategory: NSLayoutConstraint?
    
    var strSearch = ""
    var delegateUpdateMainVC : UpdateMainVC?
    //MARK: - Variables
    var apicallCount : Int = 0
    var deliveryListLength:Int = 0
    var currentAddIndex = 0
    weak var timerForAdd: Timer? = nil
    var selectedDeliveryItem:DeliveriesItem? = nil
    var locationManager : LocationManager? = LocationManager()
    var storeFragmentVC:StoreFragmentVC? = nil
    var selectedStoreItem:StoreItem? = nil
    var indC : Int = 0
    var arrForAddList:[AdItem] = []
    var finalFilteredArray:Array<StoreItem> = []
    var originalArrStoreList:Array<StoreItem>? = nil
    var filteredArrStoreList:Array<StoreItem>? = nil
    var arrPromoCodeList:Array<PromoCodeItem>? = nil
    var storeListLength:Int? = 0
    //SingleTone
    var filterSingleton: SelectedFilterOptions = SelectedFilterOptions.shared
    var isFilterApply:Bool = false
    var isChangeInFavorite:Bool = false
    var isFromUpdateOrder: Bool = false
    var totalStoreCount = 0
    let refreshControl = UIRefreshControl()
    var isVisible = false
    var newList:[[Any]] = [["store_filter_delivery".localized, false],["store_filter_takeaway".localized, false],["store_filter_book_a_table".localized, false]]
    var deliveryArrStoreList:[StoreItem] = []
    var pickupArrStoreList:[StoreItem] = []
    var bookTableArrStoreList:[StoreItem] = []
    var isTableBooking:Bool = false
    var isDeliveryChanged:Bool = false
    var lastSelectedDeliveryIndex:Int = 0
    var lastSelectedColorIndex:Int = 0
    var arrwhtsYourMind: [DeliveriesItem] = []
    
    var deliveryType = DeliveryType.store
    var arrAdminService = [DeliveriesItem]()
    var selectedFilterOptions: SelectedFilterOptions?
    var selectedIndexWhatsInMind: Int = 0
    //MARK: - View life cycle
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        LocationCenter.default.addObservers(self, [#selector(self.locationUpdate(_:)), #selector(self.locationFail(_:))])
        LocationCenter.default.startUpdatingLocation()
        Utility.showLoading()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firebaseAuthentication()
        scrollView?.delegate = self
        self.hideBackButtonTitle()
        currentBookingType = 1
        colletionViewSetUp()
        
        let headerView: UIView = UIView.init(frame: CGRect(x: 1, y: 50, width: 276, height: 30))
        headerView.backgroundColor = .red
        let labelView: UILabel = UILabel.init(frame: CGRect(x: 4, y: 5, width: 276, height: 24))
        labelView.text = "My header view"
        headerView.addSubview(labelView)
        setLocalization()
        txtSearchStore.isHidden = true
        setUpCartView()
        self.viewForOffers?.isHidden = true
        self.txtSearchStore.autocorrectionType = .no
        collectionWhatsMind?.reloadData()
        self.basicSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        deliveryListLength = currentBooking.deliveryStoreList.count
        self.viewDeliveryAds?.isHidden = (currentBooking.deliveryAdsList?.count ?? 0 > 0) ? (false) : (true)
        if deliveryListLength == 0 {
            self.setDataForTheDelivery()
        }
        
        self.setItemCountInBasket()
        self.isVisible = true
        self.collViewNewList?.reloadData() // Solved for Dark Mode change and color issue
        collectionWhatsMind?.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.topViewController?.title = "APP_NAME".localized
        self.navigationController?.isNavigationBarHidden = false
        timerForAdd?.invalidate()
        self.isVisible = false
        
        collectionWhatsMind?.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.wsGetTrips(isNext: false)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        //heightWhatsYourMind?.constant = 180
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
        setupLayout()
    }
    
    func firebaseAuthentication() {
        if firebaseAuth.currentUser == nil {
            firebaseAuth.signIn(withCustomToken:  preferenceHelper.AuthToken) { user, error in
                if error == nil {
                    //print("Firebase authentication successfull...")
                } else {
                    //print(error ?? "Error in firebase authentication")
                }
            }
        }
    }
    
    //MARK: - setup to get delivery list
    func colletionViewSetUp() {
        collectionView?.backgroundColor = UIColor.themeViewBackgroundColor
        collectionView?.isUserInteractionEnabled = true
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(cellType: AdvertiseCell.self)
        
        collViewStoreAds?.backgroundColor = UIColor.themeViewBackgroundColor
        collViewStoreAds?.isUserInteractionEnabled = true
        collViewStoreAds?.showsHorizontalScrollIndicator = false
        collViewStoreAds?.delegate = self
        collViewStoreAds?.dataSource = self
        collViewStoreAds?.register(cellType: AdvertiseCell.self)
        
        collViewStoreList?.backgroundColor = UIColor.themeViewBackgroundColor
        collViewStoreList?.isUserInteractionEnabled = true
        collViewStoreList?.showsHorizontalScrollIndicator = false
        collViewStoreList?.delegate = self
        collViewStoreList?.dataSource = self
        collViewStoreList?.isScrollEnabled = true
        collViewStoreList?.scrollsToTop = true
        collViewStoreList?.register(cellType: StoreCell.self)
        
        collViewOffers?.isUserInteractionEnabled = true
        collViewOffers?.showsHorizontalScrollIndicator = false
        collViewOffers?.delegate = self
        collViewOffers?.dataSource = self
        collViewOffers?.register(cellType: AdvertiseCell.self)
        
        colletionAdminService?.isUserInteractionEnabled = true
        colletionAdminService?.showsHorizontalScrollIndicator = false
        colletionAdminService?.delegate = self
        colletionAdminService?.dataSource = self
        colletionAdminService?.register(cellType: AdvertiseCell.self)
        
        collViewNewList?.backgroundColor = UIColor.themeViewBackgroundColor
        collViewNewList?.isUserInteractionEnabled = true
        collViewNewList?.delegate = self
        collViewNewList?.dataSource = self
        
        collectionWhatsMind?.backgroundColor = UIColor.themeViewBackgroundColor
        collectionWhatsMind?.isUserInteractionEnabled = true
        collectionWhatsMind?.showsHorizontalScrollIndicator = false
        collectionWhatsMind?.delegate = self
        collectionWhatsMind?.dataSource = self
        collectionWhatsMind?.register(cellType: WhatsOnMindCell.self)
        
        colletionViewAppointment?.backgroundColor = UIColor.themeViewBackgroundColor
        colletionViewAppointment?.isUserInteractionEnabled = true
        colletionViewAppointment?.showsHorizontalScrollIndicator = false
        colletionViewAppointment?.delegate = self
        colletionViewAppointment?.dataSource = self
        colletionViewAppointment?.register(cellType: AppointmentCell.self)
    }
    
    func basicSetup() {
        containerForStoreVC.isHidden = true
        deliveryListLength = currentBooking.deliveryStoreList.count
        self.viewDeliveryAds?.isHidden = (currentBooking.deliveryAdsList?.count ?? 0 > 0) ? (false) : (true)
        if deliveryListLength == 0 {
            self.setDataForTheDelivery()
        } else if isChangeInFavorite {
            checkForStoreList()
            isChangeInFavorite =  false
        } else {
            self.collectionView?.reloadData()
            self.collViewStoreAds?.reloadData()
            self.collViewStoreList?.reloadData()
            Utility.hideLoading()
            configureHeaderView()
            checkForStoreList()
            self.hideEmptyScreen(isHide: true)
            self.goToProductScreen()
        }
        
        let dictionary: [String:Any] = currentBooking.currentSendPlaceData.toDictionary()
        if ((currentBooking.currentCity ?? ""  != currentBooking.currentSendPlaceData.city3) || (currentBooking.bookCityId?.isEmpty()) ?? true) && UserSingleton.shared.address.count > 0 {
            self.wsGetDeliveriesInNearestCity()
        }
        if !(currentBooking.bookCityId?.isEmpty ?? true) {
            self.checkForPromoCodeList()
        }
        if isChangedLanguageFromSettings {
            isChangedLanguageFromSettings = false
        }
        if currentBooking.deliveryType == DeliveryType.tableBooking {
            APPDELEGATE.wsClearCart {}
            currentBooking.clearCart()
            currentBooking.clearTableBooking()
            self.setItemCountInBasket()
        } else {
            self.wsGetCart()
        }
    }
    
    func timerSetup() {
        timerForAdd?.invalidate()
        timerForAdd = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HomeVC.handleAddTimer), userInfo: nil, repeats: true)
    }
    
    @objc func handleAddTimer() {
        currentAddIndex = currentAddIndex +  1
        if (currentAddIndex == (currentBooking.deliveryAdsList?.count)!) {
            currentAddIndex = 0
        }
        let indexPath = IndexPath(row: currentAddIndex, section: 0)
        pgControl?.currentPage = currentAddIndex
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        lblAddDetail?.text = currentBooking.deliveryAdsList![currentAddIndex].adsDetail
    }
    
    func setupLayout() {
        if !viewForHeader.isHidden {
            lblGradient.setGradient(startColor:UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.0),endColor:UIColor(red: 26/255, green: 26/255, blue: 25/255, alpha: 0.65))
            viewForHeader.sizeToFit()
            viewForHeader.autoresizingMask = UIView.AutoresizingMask()
        }
        self.btnSearchLocation.applyRoundedCornersWithHeight()
        self.viewCartContainer?.applyRoundedCornersWithHeight()
        lblCartQuantity?.applyRoundedCornersWithHeight()
    }
    
    func setLocalization() {
        view.backgroundColor = UIColor.themeViewBackgroundColor
        viewDeliveryAds?.backgroundColor = UIColor.themeViewBackgroundColor
        viewStoreAds?.backgroundColor = UIColor.themeViewBackgroundColor
        viewStoreList?.backgroundColor = UIColor.themeViewBackgroundColor
        viewForOffers?.backgroundColor = UIColor.themeViewBackgroundColor
        viewAdminService?.backgroundColor = UIColor.themeViewBackgroundColor
        lblDeliveryAdsTitle.font = FontHelper.textMedium(size: FontHelper.large)
        lblStoreListTitle.font = FontHelper.textMedium(size: FontHelper.large)
        lblWhatsYourMind?.font = FontHelper.textMedium(size: FontHelper.large)
        lblAdminService?.font = FontHelper.textMedium(size: FontHelper.large)
        lblStoreAdsTitle.font = FontHelper.textMedium(size: FontHelper.large)
        lblNoData?.font = FontHelper.textRegular()
        lblNoData?.textColor = UIColor.themeLightGrayColor
        lblNoData?.text = "txt_please_select_above_service".localized
        lblDeliveryAdsTitle.text = "TXT_DELIVERIES_ADS".localized
        lblAdminService?.text = "txt_admin_service".localized
        lblStoreListTitle.text = "".localized
        lblStoreAdsTitle.text = "TXT_STORE_ADS".localized
        lblAddDetail.font = FontHelper.textRegular()
        lblAddDetail.textColor = UIColor.themeButtonTitleColor
        lblAddDetail.text = ""
        lblEmpty.textColor = UIColor.themeLightTextColor
        lblEmpty.text = "TXT_NO_DELIVERY_AVAILABLE".localized
        lblEmpty.font = FontHelper.textLarge()
        lblOffers.font = FontHelper.textMedium(size: FontHelper.large)
        lblOffers.text = "TXT_OFFERS".localized
        lblOffers.textColor = .themeTitleColor
        viewForHeader.clipsToBounds = true
        btnSearchLocation.setTitleColor(UIColor.themeButtonTitleColor, for: .normal)
        btnSearchLocation.titleLabel?.font = FontHelper.textRegular()
        btnSearchLocation.setBackgroundColor(color: UIColor.themeColor, forState: .normal)
        btnSearchLocation.setTitle("TXT_SEARCH_LOCATION".localized, for: .normal)
        txtSearchStore.font = FontHelper.textRegular()
        txtSearchStore.textColor = UIColor.themeTextColor
        txtSearchStore.setRound(withBorderColor: UIColor.themeLightTextColor, andCornerRadious: 5.0, borderWidth: 0.5)
        txtSearchStore.leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: txtSearchStore.frame.size.height))
        txtSearchStore.leftViewMode = .always
        txtSearchStore.tintColor = .themeTextColor
        btnSearchStore.setImage(UIImage(named: "cancelIcon")?.imageWithColor(color: UIColor.themeTitleColor), for: .selected)
        self.viewCartContainer?.backgroundColor = UIColor.themeColor
        lblCartQuantity?.textColor = UIColor.themeButtonTitleColor
        lblCartQuantity?.font = FontHelper.tiny()
        lblCartQuantity?.backgroundColor = UIColor.themeRedBGColor
        lblCartQuantity?.textColor = UIColor.themeButtonTitleColor
        lblCartText?.textColor = UIColor.themeButtonTitleColor
        lblCartText?.font = FontHelper.textMedium()
        lblCartText?.text = "TXT_VIEW_CART".localized
        btnFilterStore.tintColor = .themeColor
        btnFilterStore.setImage(UIImage.init(named: "filter_blue")?.imageWithColor(color: .themeColor), for: .normal)
        btnSearchStore.setImage(UIImage.init(named: "search_blue")?.imageWithColor(color: .themeColor), for: .normal)
    }
    
    @IBAction func onClickBtnSearchLocation(_ sender: Any) {
        (self.parent as? MainVC)?.onClickBtnNavigation(self)
    }
    
    @IBAction func onClickBtnSearchStore(_ sender: UIButton) {
        if sender.isSelected {
            txtSearchStore.isHidden = true
            txtSearchStore.text =  ""
            strSearch = ""
            //searchStore("")
            guard let selectedDeliveryItem = selectedDeliveryItem else {
                return
            }
            wsGetStoreList(storeDeliveryId: selectedDeliveryItem._id ?? "", defaultImage: selectedDeliveryItem.default_image_url, isShowLoading: false)
        } else {
            txtSearchStore.isHidden = false
        }
        btnSearchStore.isSelected = !sender.isSelected
    }
    
    @IBAction func onClickBtnFilterStore(_ sender: Any) {
        self.openFilterStoreDialog()
    }
    
    @IBAction func onClickBtnCart(_ sender: AnyObject) {
        if let navigationVC = self.navigationController {
            for controller in navigationVC.viewControllers {
                if controller.isKind(of: CartVC.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    return
                }
            }
        }
        
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "Cart", bundle: nil)
        if let cartvc: CartVC = mainView.instantiateInitialViewController() as? CartVC {
            self.navigationController?.pushViewController(cartvc, animated: true)
        }
    }
    
    @objc func refereshStoreList() {}
    
    func hideEmptyScreen(isHide: Bool)  {
        self.imgEmpty?.isHidden = isHide
        self.lblEmpty?.isHidden = isHide
        self.scrollView?.isHidden = !isHide
        self.btnSearchLocation?.isHidden = isHide
    }
    
    func hideViewScreen(isHide: Bool)  {
        self.btnSearchLocation?.isHidden = isHide
        self.viewDeliveryAds?.isHidden = isHide
        self.viewStoreAds?.isHidden = isHide
        //self.viewStoreList?.isHidden = isHide
        self.collViewNewList?.isHidden = isHide
        self.collViewStoreList?.isHidden = isHide
    }
    
    func setUpCartView()  {
        viewCartContainer?.isHidden = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.onClickBtnCart(_:)))
        gestureRecognizer.delegate = self
        viewCartContainer?.addGestureRecognizer(gestureRecognizer)
    }
    
    func setItemCountInBasket() {
        var numberOfItems = 0
        for cartProduct in currentBooking.cart {
            numberOfItems = numberOfItems + (cartProduct.items?.count)!
        }
        currentBooking.totalItemInCart = numberOfItems
        lblCartQuantity?.text = String(currentBooking.totalItemInCart)
        if numberOfItems > 0 {
            if currentBooking.deliveryStoreList.count > 0 {
                viewCartContainer?.isHidden = false
            } else {
                viewCartContainer?.isHidden = true
            }
        } else {
            viewCartContainer?.isHidden = true
        }
    }
    
    func applyFilter() {
        guard let selectedFilterOptions = selectedFilterOptions else {
            return
        }
        var filteredArrStoreList: [StoreItem] = []
        for storesItem:StoreItem in originalArrStoreList ?? [] {
            if storesItem.deliveryTime <= selectedFilterOptions.deliveryTime && storesItem.distanceFromMyLocation <= selectedFilterOptions.distanceValue {
                if (selectedFilterOptions.arrForPriceRate.count > 0 ) {
                    if selectedFilterOptions.arrForPriceRate.contains(storesItem.price_rating ?? 0)
                    {
                        filteredArrStoreList.append(storesItem)
                    }
                } else {
                    filteredArrStoreList.append(storesItem)
                }
            }
        }
        
        if selectedFilterOptions.arrForSelectedTags.count > 0 {
            filteredArrStoreList =  filteredArrStoreList.filter({ (storeItem) -> Bool in
                var isAdd:Bool = false
                for tag in selectedFilterOptions.arrForSelectedTags {
                    //print("---\(storeItem.famousProductsTags)")
                    if storeItem.famousProductsTags.count > 0{
                        if storeItem.famousProductsTags.contains(where: { (storeTag) -> Bool in
                            //print("***\(storeTag)")
                            if storeTag.count > 0{
                                if preferenceHelper.SelectedLanguage > (storeTag.count - 1) {
                                    return  storeTag[0] == tag
                                }
                                else {
                                    return storeTag[preferenceHelper.SelectedLanguage] == tag
                                }
                            }else{
                                return false
                            }
                        })
                        {
                            isAdd = true
                        }
                    }
                }
                return isAdd
            })
        }
        
        if !selectedFilterOptions.searchText.isEmpty() {
            filteredArrStoreList = filteredArrStoreList.filter { storesItem in
                for  itemName:String in storesItem.productItemNameList
                {
                    if itemName.lowercased().contains(selectedFilterOptions.searchText.lowercased())
                    {
                        return true
                    }
                }
                return false
            }
        }
        
        self.reloadTableWithArray(array: filteredArrStoreList)
    }

    //MARK: - Dialogs
    func openFilterStoreDialog() {
        guard let selectedDeliveryItem = selectedDeliveryItem else {
            return
        }
        let dialogForFilter = CustomFilterDialog.showCustomFilterDialog(title: "TXT_FILTERS".localized, message: "".localized, titleLeftButton: "TXT_APPLY".localized, titleRightButton: "TXT_CLEAR".localized, editTextOneHint: "TXT_PHONE".localized, editTextTwoHint: "", isEdiTextTwoIsHidden: true, selectedDeliveryItem: selectedDeliveryItem,originalStoreList: self.originalArrStoreList)
        
        dialogForFilter.filterObj = selectedFilterOptions
        
        dialogForFilter.onClickLeftButton = { arrList, filterObj in
            dialogForFilter.removeFromSuperview()
            self.isFilterApply = true
            self.selectedFilterOptions = filterObj
            DispatchQueue.main.async {
                let offSetY = self.stackViewStoreList!.frame.origin.y + self.viewStoreList!.frame.origin.y
                if let scrollView = self.scrollView {
                    if scrollView.contentSize.height - scrollView.frame.size.height > offSetY  {
                        scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: offSetY), animated: true)
                    } else {
                        scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height), animated: true)
                    }
                }
            }
            //self.scrollView?.scrollToView(view: self.viewStoreList ?? UIView(), animated: true)
            self.reloadTableWithArray(array: arrList)
            
        }
        dialogForFilter.onClickRightButton = {
            
            dialogForFilter.removeFromSuperview()
        }
        dialogForFilter.onClickClearButton = {
            self.isFilterApply = false
            self.selectedFilterOptions = nil
            self.reloadTableWithArray(array: self.originalArrStoreList)
        }
    }
    func wsGetProductData(storeId:String) {
        let dictParam: Dictionary<String,Any> =
        [PARAMS.STORE_ID:storeId ]
        Utility.showLoading()
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromStoreURL(langInd: "1",url: WebService.WS_GET_STORE_DATA, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            print(response)
            Utility.hideLoading()
            Parser.parseStoreDataDetail(response) { result in
                print(result.store_detail?.table_settings_details?.reservation_max_days ?? 0)
                let reservationMaxDays = result.store_detail?.table_settings_details?.reservation_max_days ?? 0
                if reservationMaxDays > 0{
                    self.openTableBookingDialog(storeID: storeId,reservationDay: reservationMaxDays)
                }else{
                    Utility.showToast(message: "Slot not available")
                }
                
            }
        }
    }
    
    func openTableBookingDialog(storeID:String,reservationDay : Int) {
        let dialogForFilter = CustomTableBookingDialog.showCustomTableBookingDialog(title: "text_table_reservation".localized, titleRightButton: "btn_reserve_a_table".localized, storeID: storeID,reservationMaxDays: reservationDay)
        dialogForFilter.onClickLeftButton = {
            currentBooking.clearTableBooking()
            dialogForFilter.removeFromSuperview()
        }
        dialogForFilter.onClickRightButton = {
            if currentBooking.bookingType == 2 {
                self.goToProductVC()
            } else {
                var mainView: UIStoryboard!
                mainView = UIStoryboard(name: "Cart", bundle: nil)
                if let invoiceVC:InvoiceVC = mainView.instantiateViewController(withIdentifier: "InvoiceVC") as? InvoiceVC {
                    invoiceVC.deliveryType = currentBooking.deliveryType ?? 0
                    self.navigationController?.pushViewController(invoiceVC, animated: true)
                }
            }
            dialogForFilter.removeFromSuperview()
        }
    }
    
    //MARK: - WEB SERVICE CALLS
    func wsGetCart() {
        let dictParam: [String:Any] = APPDELEGATE.getCommonDictionary()
        //print(dictParam)
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_CART, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            currentBooking.clearCart()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                CurrentBooking.shared.PushNotification = false
                //print("CurrentBooking.shared.logout = ",CurrentBooking.shared.PushNotification)
            }
            if Parser.parseCart(response) {
                self.setItemCountInBasket()
                if currentBooking.deliveryType == DeliveryType.tableBooking {
                    APPDELEGATE.wsClearCart {
                        self.setItemCountInBasket()
                    }
                }
            }
        }
    }
    
    func wsGetDeliveriesInNearestCity() {
        
        let parameter = currentBooking.currentSendPlaceData.toDictionary()
        Utility.showLoading()
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url:WebService.WS_GET_NEAREST_DELIVERY_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: parameter) { [weak self] (response, error) -> (Void) in
            guard let self = self else { return }
            print("WS_GET_NEAREST_DELIVERY_LIST response homevc \(response)")
            Utility.hideLoading()
            if Parser.parseDeliveryStore(response) {
//                let deliveryStoreResponse:DeliveryStoreResponse = DeliveryStoreResponse.init(dictionary: response)!
                for deliveryItem in currentBooking.deliveryStoreList {
                    if deliveryItem.deliveryType == 7{
                        print(deliveryItem.deliveryType)
                        print(deliveryItem.default_image_url)
                    }
                }
                self.arrwhtsYourMind.removeAll()
                for obj in arrMainService {
                    if let deliveryType = obj["delivery_type"] as? Int, let imgUrl = obj["image_url"] as? String, let name = obj["delivery_name"] as? String {
                        var defaultImage = ""
                        for deliveryItem in currentBooking.deliveryStoreList {
                            
                            if deliveryItem.deliveryType == deliveryType{
                                defaultImage = deliveryItem.default_image_url
                                //                                let service = DeliveriesItem(delivery_name: name, delivery_type: deliveryType, image_url: imgUrl, default_image_url: deliveryItem.default_image_url)
                                //                                self.arrwhtsYourMind.append(service)
                            }
                        }
                        let service = DeliveriesItem(delivery_name: name, delivery_type: deliveryType, image_url: imgUrl, default_image_url: defaultImage)
                        self.arrwhtsYourMind.append(service)
                    }
                }
                var filterArr = [DeliveriesItem]()
                for objStatic in self.arrwhtsYourMind {
                    if currentBooking.deliveryStoreList.filter({$0.deliveryType == objStatic.deliveryType}).count > 0 {
                        filterArr.append(objStatic)
                    }
                }
                
                self.arrwhtsYourMind = filterArr
                self.viewDeliveryAds?.isHidden = (currentBooking.deliveryAdsList?.count ?? 0 > 0) ? (false) : (true)
                self.configureHeaderView()
                self.imgEmpty?.isHidden = true
                self.lblEmpty?.isHidden = true
                self.scrollView?.isHidden = false
                self.btnSearchLocation?.isHidden = true
                self.setItemCountInBasket()
                self.collectionWhatsMind?.reloadData()
                
                DispatchQueue.main.async {
                    if (self.parent as? MainVC)?.cvForHome.isHidden == false{
                        (self.parent as? MainVC)?.updateTitle()
                    }
                    if let firstsDeliveryType = filterArr.first {
                        if firstsDeliveryType.deliveryType != DeliveryType.taxi && firstsDeliveryType.deliveryType != DeliveryType.courier  {
                            let arrStore = currentBooking.deliveryStoreList.filter({$0.deliveryType == firstsDeliveryType.deliveryType})
                            if let firstDelivery = arrStore.first {
                                self.selectedDeliveryItem = firstDelivery
                                self.showAdminService()
                                self.originalArrStoreList = []
                                self.isDeliveryChanged = true
                                self.checkForStoreList(isFromClick: true)
                                self.selectedIndexWhatsInMind = 0
                            }
                        } else {
                            if firstsDeliveryType.deliveryType == DeliveryType.taxi || firstsDeliveryType.deliveryType == DeliveryType.courier || firstsDeliveryType.deliveryType == DeliveryType.ecommerce {
                                self.viewForOffers?.isHidden = true
                                self.collViewNewList?.isHidden = true
                                self.collViewStoreList?.isHidden = true
                                self.viewNoData?.isHidden = false
                                self.viewForOffers?.isHidden = true
                                self.viewDeliveryAds?.isHidden = true
                                self.btnSearchStore.isHidden = true
                                self.btnFilterStore.isHidden = true
                            }
                        }
                        switch firstsDeliveryType.deliveryType {
                        case DeliveryType.store :
                            self.lblStoreListTitle.text = "TXT_STORE_LIST".localized
                        case DeliveryType.service :
                            self.lblStoreListTitle.text = "txt_service".localized
                        case DeliveryType.appoinment :
                            self.lblStoreListTitle.text = "txt_appointment".localized
                        case DeliveryType.courier :
                            self.lblStoreListTitle.text = "TXT_COURIER".localized
                        case DeliveryType.taxi :
                            self.lblStoreListTitle.text = "TXT_TAXI".localized
                        case DeliveryType.ecommerce :
                            self.lblStoreListTitle.text = "TXT_ECOMMERCE".localized
                        default:
                            self.lblStoreListTitle.text = "TXT_STORE_LIST".localized
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.hideViewScreen(isHide: true)
                    self.imgEmpty?.isHidden = false
                    self.lblEmpty?.isHidden = false
                    self.btnSearchLocation?.isHidden = false
                    self.viewCartContainer?.isHidden = true
                    self.scrollView?.isHidden = true
                }
            }
            self.goToProductScreen()
        }
    }
    
    func wsGetStoreList(storeDeliveryId:String, defaultImage : String, isNeedMoreData: Bool = false, isShowLoading: Bool = true) {
        if !storeDeliveryId.isEmpty {
            if self.selectedDeliveryItem?.deliveryType == DeliveryType.service{
                newList = [["store_filter_delivery".localized, false]]
        }else if self.selectedDeliveryItem?.deliveryType == DeliveryType.appoinment {
                newList = []
            newList = [["store_filter_delivery".localized, false],["store_filter_takeaway".localized, false]]
            collViewNewList?.isHidden = false
            } else {
                if selectedDeliveryItem?.is_provide_table_booking ?? false {
                    newList = [["store_filter_delivery".localized, false],["store_filter_takeaway".localized, false],["store_filter_book_a_table".localized, false]]
                    isTakeAway = 0
                } else {
                    newList = [["store_filter_delivery".localized, false],["store_filter_takeaway".localized, false]]
                }
                collViewNewList?.isHidden = false
            }
            
            if selectedDeliveryItem?.is_provide_table_booking ?? false {
                if let perent = self.parent as? MainVC {
                    perent.viewQrCodeScan.isHidden = false
                }
            } else {
                if let perent = self.parent as? MainVC {
                    perent.viewQrCodeScan.isHidden = true
                }
            }
            
            collViewNewList?.reloadData()
            
            (self.isChangeInFavorite || !isShowLoading) ? () : Utility.showLoading()
            let dictParam: Dictionary<String,Any> = [PARAMS.STORE_DELIVERY_ID:storeDeliveryId,
                                                     PARAMS.CITY_ID :currentBooking.bookCityId ?? "",
                                                     PARAMS.USER_ID:preferenceHelper.UserId,
                                                     PARAMS.IPHONE_ID:preferenceHelper.RandomCartID,
                                                     PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
                                                     PARAMS.LATITUDE:currentBooking.currentLatLng[0],
                                                     PARAMS.LONGITUDE:currentBooking.currentLatLng[1],
                                                     PARAMS.PAGE:CONSTANT.CURRENT_PAGE,
                                                     PARAMS.PER_PAGE: CONSTANT.STORE_PER_PAGE,
                                                     PARAMS.search_text : strSearch
            ]
            //print("WebService.WS_GET_STORELIST \(Utility.convertDictToJson(dict: dictParam))")
            
            let afn:AlamofireHelper = AlamofireHelper.init()
            afn.getResponseFromURL(url: WebService.WS_GET_STORELIST, methodName: "POST", paramData: dictParam) { [weak self] (response, error) -> (Void) in
                //print("self.originalArrStoreList API \(Utility.convertDictToJson(dict: response as! Dictionary<String, Any>))")
                Utility.hideLoading()
                guard let self = self else { return }
                //print(response)
                if CONSTANT.CURRENT_PAGE == 1 {
                    self.originalArrStoreList?.removeAll()
                }
                if Parser.isSuccess(response: response, andErrorToast: false, endEditing: isShowLoading) {
                    let storeResponse:StoreResponse = StoreResponse.init(dictionary: response, defaultImage: defaultImage)!
                    //print(Utility.convertDictToJson(dict: response as! Dictionary<String, Any>))
                    
                    self.pickupArrStoreList.removeAll()
                    self.deliveryArrStoreList.removeAll()
                    self.bookTableArrStoreList.removeAll()
                    self.arrForAddList.removeAll()
                    self.arrForAddList = storeResponse.ads
                    self.viewStoreAds?.isHidden = !(storeResponse.ads.count > 0)
                    self.totalStoreCount = storeResponse.stores?.count ?? 0
                    //self.originalArrStoreList = Parser.parseStoreList(response)

                    if isNeedMoreData {
                        let tempArrStoreList = Parser.parseStoreList(response, defaultImage: defaultImage)
                        for store:StoreItem in tempArrStoreList! {
                            self.originalArrStoreList?.append(store)
                        }
                    } else {
                        self.originalArrStoreList = Parser.parseStoreList(response, defaultImage: defaultImage)
                    }

                    if (self.originalArrStoreList != nil) {
                        self.storeListLength = storeResponse.stores?.results?.count
                    }

                    if self.originalArrStoreList?.isEmpty ?? true {
                        //self.viewStoreList?.isHidden = true
                        self.collViewNewList?.isHidden = true
                        self.collViewStoreList?.isHidden = true
                    } else {
                        //self.viewStoreList?.isHidden = false
                        self.collViewNewList?.isHidden = false
                        self.collViewStoreList?.isHidden = false
                    }

                    DispatchQueue.main.async {
                        if self.selectedFilterOptions != nil {
                            self.applyFilter()
                        } else {
                            self.reloadTableWithArray(array: self.originalArrStoreList)
                        }
                    }

                    self.configureHeaderView()
                    self.scrollView?.scrollsToTop = true
                } else {
                    self.isDeliveryChanged = false
                    self.viewStoreAds?.isHidden = true
                    //self.viewStoreList?.isHidden = true
                    self.collViewNewList?.isHidden = true
                    self.collViewStoreList?.isHidden = true
                }
                if self.originalArrStoreList?.count == 0 {
                    //self.viewStoreList?.isHidden = true
                    self.collViewNewList?.isHidden = true
                    self.collViewStoreList?.isHidden = true
                    self.collectionWhatsMind?.reloadData()
                } else {
                    //self.viewStoreList?.isHidden = false
                    self.collViewNewList?.isHidden = false
                    self.collViewStoreList?.isHidden = false
                }
                self.viewNoData?.isHidden = !(self.collViewStoreList?.isHidden ?? false)
                //print(self.viewNoData?.isHidden)
            }
        } else {
            Utility.hideLoading()
            self.isDeliveryChanged = false
            self.viewStoreAds?.isHidden = true
            //self.viewStoreList?.isHidden = true
            self.collViewNewList?.isHidden = true
            self.collViewStoreList?.isHidden = true
            self.viewNoData?.isHidden = !(self.collViewStoreList?.isHidden ?? false)
            //print(self.viewNoData?.isHidden)
        }
    }
    func wsGetStoreListForEcommerce(storeDeliveryId:String,isNeedMoreData: Bool = false, isShowLoading: Bool = true, indexEcommerce: Int = 0,defaultImage : String = "") {
        (self.isChangeInFavorite || !isShowLoading) ? () : Utility.showLoading()
        let dictParam: Dictionary<String,Any> = [PARAMS.STORE_DELIVERY_ID:storeDeliveryId,
                                                 PARAMS.CITY_ID :currentBooking.bookCityId ?? "",
                                                 PARAMS.USER_ID:preferenceHelper.UserId,
                                                 PARAMS.IPHONE_ID:preferenceHelper.RandomCartID,
                                                 PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
                                                 PARAMS.LATITUDE:currentBooking.currentLatLng[0],
                                                 PARAMS.LONGITUDE:currentBooking.currentLatLng[1],
                                                 PARAMS.PAGE:CONSTANT.CURRENT_PAGE,
                                                 PARAMS.PER_PAGE: CONSTANT.STORE_PER_PAGE,
                                                 PARAMS.search_text : strSearch,
                                                 PARAMS.DELIVERY_TYPE : DeliveryType.ecommerce
        ]
        //print("WebService.WS_GET_STORELIST \(Utility.convertDictToJson(dict: dictParam))")
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_STORELIST, methodName: "POST", paramData: dictParam) { [weak self] (response, error) -> (Void) in
            //print("self.originalArrStoreList API \(Utility.convertDictToJson(dict: response as! Dictionary<String, Any>))")
            Utility.hideLoading()
            guard let self = self else { return }
            //print(response)
            if CONSTANT.CURRENT_PAGE == 1 {
                self.originalArrStoreList?.removeAll()
            }
            self.viewForOffers?.isHidden = true
            if Parser.isSuccess(response: response, andErrorToast: false, endEditing: isShowLoading) {
                let storeResponse:StoreResponse = StoreResponse.init(dictionary: response, defaultImage: "")!
                
                self.totalStoreCount = storeResponse.stores?.count ?? 0
                
                self.storeListLength = storeResponse.stores?.results?.count
                
                if self.storeListLength ?? 0 > 0 {
                    self.selectedStoreItem = storeResponse.stores?.results?.first
                    let ecommerceStoreId = storeResponse.stores?.results?.first?._id
                    self.goToProductVC(storeID: ecommerceStoreId ?? "", isEcommerce: true,defaultImage: defaultImage)
                }
            } else {
                self.isDeliveryChanged = false
                self.viewStoreAds?.isHidden = true
                self.collViewNewList?.isHidden = true
                self.collViewStoreList?.isHidden = true
                self.lblStoreListTitle.text = "TXT_ECOMMERCE".localized
                self.btnSearchStore.isHidden = true
                self.btnFilterStore.isHidden = true
            }
            
            self.viewNoData?.isHidden = (self.originalArrStoreList?.count == 0) ? false : true
            //print(self.viewNoData?.isHidden)
        }
    }
    func wsGetPromoCodeList() {
        let dictParam: Dictionary<String,Any> = [
            PARAMS.DELIVERY_ID:selectedDeliveryItem?._id ?? "",
            PARAMS.CITY_ID: currentBooking.bookCityId ?? ""
        ]
        Utility.showLoading()
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url:WebService.WS_GET_PROMO_CODE_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [self] (response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                let promoListResponse:PromoCodeListResponse = PromoCodeListResponse.init(dictionary: response)!
                //print(response)
                self.arrPromoCodeList = Parser.parsePromoCodeList(response)
                IS_PROMOCODE_AVAILABLE = (promoListResponse.isPromoAvailable ?? false) ? true : false
                if promoListResponse.isPromoAvailable ?? false {
                    self.viewForOffers?.isHidden = ((self.arrPromoCodeList?.count ?? 0) > 0) ? false : true
                    self.collViewOffers?.isHidden = ((self.arrPromoCodeList?.count ?? 0) > 0) ? false : true
                    self.viewForOffers?.updateConstraints()
                    self.collViewOffers?.updateConstraints()
                } else {
                    self.viewForOffers?.isHidden = true
                }
                
                DispatchQueue.main.async {
                    self.collViewOffers?.reloadData()
                }
            }
        }
    }
    
    func wsGetPromoDetail(promoId:String) {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> = [
            PARAMS.PROMO_ID:promoId,
        ]
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_PROMO_DETAIL, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response,error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                //print("WS_GET_PROMO_DETAIL response \(Utility.convertDictToJson(dict: response as! Dictionary<String, Any>))")
                if (Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true)) {
                    let promoDetail:PromoCodeDetail = PromoCodeDetail.init(fromDictionary: response as! [String : Any])
                    if promoDetail.promoCodes.count > 0{
                        
                        self.openOffersDialog(arrPromoCodes: promoDetail.promoCodes)
                    }
                } else {}
            } else {}
        }
    }
    
    func wsGetTrips(isNext: Bool) {
        if preferenceHelper.UserId.count == 0 && preferenceHelper.UserId.isEmpty() {
            Utility.hideLoading()
            return
        }
        if isNext {
            Utility.showLoading()
        }

        let dictParam: Dictionary<String,Any> = [
            PARAMS.USER_ID:preferenceHelper.UserId,
            PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken
        ]
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_TRIP_DETAIL, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [weak self] (response,error) -> (Void) in
            guard let self = self else { return }
            if isNext {
                Utility.hideLoading()
            }
            if Parser.isSuccess(response: response) {
                let vc = UIStoryboard(name: "Eber", bundle: nil).instantiateViewController(withIdentifier: "RideHomeVC") as! RideHomeVC
                vc.delegateBackReloadStore = self
                if let order = response["order"] as? [[String:Any]] {
                    if order.count > 0 {
                        if let obj = order.first {
                            if let orderModel = Order.init(dictionary: obj as NSDictionary) {
                                if isNext {
                                    vc.orderId = orderModel._id ?? ""
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else {
                                    if (orderModel.order_status ?? 0) == (OrderStatus.DELIVERY_MAN_COMPLETE_DELIVERY.rawValue) && !(orderModel.is_payment_paid ?? false) {
                                        vc.orderId = orderModel._id ?? ""
                                        vc.isPaymentPending = true
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                        }
                    } else {
                        if isNext {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func openOffersDialog(arrPromoCodes:Array<PromoCodeModal>)  {
        let dialogProductFilter = CustomOffersDialog.showOffers(title: "Offers", message: "", arrPromoCodes: arrPromoCodes, isFromHome: true)
        dialogProductFilter.onClickLeftButton = {
            dialogProductFilter.removeFromSuperview()
        }
    }
    
    func goToProductScreen() {
        if !currentBooking.selectedBranchIoStore.isEmpty {
            self.goToProductVC(storeID: currentBooking.selectedBranchIoStore)
            return
        }
        if currentBooking.deliveryStoreList.count > 0 {
            if currentBooking.deliveryStoreList[0].deliveryType != DeliveryType.courier {
                if currentBooking.deliveryStoreList.count == 1 {
                    //viewDeliveryList?.isHidden = true
                } else {
                    //viewDeliveryList?.isHidden = false
                    self.containerForStoreVC.isHidden = true
                }
            }
        }
    }
    
    func checkForStoreList(isFromClick: Bool = false) {
        CONSTANT.CURRENT_PAGE = 1
        
        guard let selectedDeliveryItem = selectedDeliveryItem else {
            return
        }
        
        self.wsGetPromoCodeList()
        if selectedDeliveryItem.deliveryType == DeliveryType.courier {
            if isFromClick {
                if preferenceHelper.UserId.isEmpty {
                    self.goToLogin()
                    Utility.showToast(message: "TXT_NEED_LOGIN_FOR_COURIER".localized)
                } else {
                    currentBooking.courierDeliveryId = (selectedDeliveryItem._id) ?? ""
                    self.performSegue(withIdentifier: SEGUE.SEGUE_COURIER, sender: nil)
                }
            }
        } else {
            self.wsGetStoreList(storeDeliveryId: selectedDeliveryItem._id ?? "", defaultImage: selectedDeliveryItem.default_image_url)
        }
    }
    
    func goToLogin() {
        let storyboard = UIStoryboard(name: "Prelogin", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeStoryBoard") as! Home
        if UIDevice.current.userInterfaceIdiom == .pad {
            vc.modalPresentationStyle = .overCurrentContext
        } else {
            vc.modalPresentationStyle = .popover
        }
        self.present(vc, animated: true, completion: nil)
    }
    func checkForPromoCodeList(selectedIndex: Int = 0) {
        if !(selectedDeliveryItem?._id ?? "").isEmpty() {
            self.wsGetPromoCodeList()
        }
    }
    
    func reloadTableWithArray(array:Array<StoreItem>?) {
        if let finalArray = array {
            finalFilteredArray.removeAll()
            for storeItem in finalArray {
                finalFilteredArray.append(storeItem)
            }
        }
        
        if finalFilteredArray.count == 0 {
            self.isDeliveryChanged = false
            Utility.showToast(message: "ERROR_CODE_561".localized, endEditing: false)
        } else {
            if self.isDeliveryChanged {
                self.isDeliveryChanged = false
                //self.scrollView?.scrollToView(view:self.collViewStoreList!, animated: true)
                DispatchQueue.main.async {
                    let offSetY = self.stackViewStoreList!.frame.origin.y + self.viewStoreList!.frame.origin.y
                    if let scrollView = self.scrollView {
                        if scrollView.contentSize.height - scrollView.frame.size.height > offSetY  {
                            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: offSetY), animated: true)
                        } else {
                            if  scrollView.contentSize.height > scrollView.frame.size.height {
                                scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentSize.height - scrollView.frame.size.height), animated: true)
                            }
                        }
                    }
                }
            }
        }

        self.collViewStoreAds?.reloadData()
        self.collViewStoreList?.reloadData()
        self.heightForStoreList?.constant = collViewStoreList?.collectionViewLayout.collectionViewContentSize.height ?? 0
        self.collViewNewList?.reloadData()
    }
    
    fileprivate func searchStore(_ mySearchText: String) {
        if mySearchText.isEmpty() {
            if isFilterApply {
                self.reloadTableWithArray(array: filteredArrStoreList)
            } else {
                self.reloadTableWithArray(array: originalArrStoreList)
            }
        } else {
            let tempArray:Array<StoreItem> = (isFilterApply ? filteredArrStoreList:originalArrStoreList) ?? []
            var tempFiltereArray:Array<StoreItem> = []
            for storesItem:StoreItem in tempArray {
                if (storesItem.name?.lowercased().contains(mySearchText.lowercased()))! {
                    tempFiltereArray.append(storesItem)
                }
            }
            self.reloadTableWithArray(array: tempFiltereArray)
        }
    }

    //MARK: - SET DATA FOR THE DELIVERY
    func setDataForTheDelivery() {
        switch(CLLocationManager.authorizationStatus()) {
        case .notDetermined, .restricted, .denied:
            if CommonFunctions.fetchLocationFromDB(vc: self) {
                
            } else {
                if (currentBooking.deliveryStoreList.isEmpty) && (currentBooking.selectedBranchIoStore.isEmpty) {
                    (self.parent as? MainVC)?.onClickBtnNavigation(self)
                } else {
                    self.goToProductScreen()
                }
            }
        case .authorizedAlways, .authorizedWhenInUse:
            break
        default:
            print("CLLocationManager status not hendle")
        }
    }
    
    @objc func locationUpdate(_ ntf: Notification = Common.defaultNtf) {
        if self.isVisible {
            if !CommonFunctions.fetchLocationFromDB(vc: self) {
                guard let userInfo = ntf.userInfo else { return }
                guard let location = userInfo[Common.locationKey] as? CLLocation else { return }
                let latitude = location.coordinate.latitude
                let longitude = location.coordinate.longitude
                
                self.locationManager?.getAddressFromLatLong(latitude: latitude, longitude: longitude)
                currentBooking.currentAddress = currentBooking.currentSendPlaceData.address
                currentBooking.currentLatLng = [currentBooking.currentSendPlaceData.latitude, currentBooking.currentSendPlaceData.longitude]
                if ((currentBooking.currentCity ?? "" != currentBooking.currentSendPlaceData.city1) || (currentBooking.bookCityId?.isEmpty()) ?? true) {
                    let dictionary: [String:Any] = currentBooking.currentSendPlaceData.toDictionary()
                    //print("wsGetDeliveriesInNearestCity Homevc : \(dictionary)")
                    CommonFunctions.addLocationToLocalDB()
                    self.wsGetDeliveriesInNearestCity()
                } else {
                    Utility.hideLoading()
                    self.goToProductScreen()
                }
            }
        }
        LocationCenter.default.stopUpdatingLocation()
    }
    
    @objc func locationFail(_ ntf: Notification = Common.defaultNtf) {
        guard let userInfo = ntf.userInfo else { return }
        guard let error = userInfo[Common.locationErrorKey] as? Error else { return }
        //print("locationFail: \(error)")
        
        Utility.showToast(message: "MSG_LOCATION_NOT_GETTING".localized)
        Utility.hideLoading()
        self.goToProductScreen()
        (self.parent as! MainVC).onClickBtnNavigation(self)
        
    }

    //MARK: - NAVIGATION METHODS
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier?.compare(SEGUE.SEGUE_STORE_LIST) == ComparisonResult.orderedSame) {
            let myStoreVC:StoreVC = segue.destination as! StoreVC
            myStoreVC.currentDeliveryItem = selectedDeliveryItem
            myStoreVC.isShowGroupItems = selectedDeliveryItem!.is_store_can_create_group!
        }
        if (segue.identifier?.compare(SEGUE.SEGUE_STORE_FRAGMENT_VC) == ComparisonResult.orderedSame) {}
    }
}

//MARK:- Collection Data Source and delegate
extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    /*Header Configuration for advertise ment*/
    func configureHeaderView() {
        if ((currentBooking.deliveryAdsList?.count) ?? 0)  >  0 {
            viewForHeader?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.width/1.75)
            if heightForHeader != nil {
                heightForHeader.constant = view.frame.width/1.75
            }
            viewForHeader?.isHidden = false
            collectionView?.reloadData()
            pgControl?.currentPage = 0
            self.lblAddDetail?.text = currentBooking.deliveryAdsList?[0].adsDetail
            if ((currentBooking.deliveryAdsList?.count)! > 1) {
            }
        } else {
            if heightForHeader != nil {
                heightForHeader.constant = 0
            }
            viewForHeader?.isHidden = true
            viewForHeader?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 0)
        }
        pgControl?.numberOfPages = (currentBooking.deliveryAdsList?.count) ?? 0
    }
    
    //MARK:- UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collViewOffers {
            return (arrPromoCodeList?.count) ?? 0
        } else if collectionView == self.collectionView {
            return (currentBooking.deliveryAdsList?.count) ?? 0
        } else if collectionView == self.collViewStoreAds {
            return (arrForAddList.count)
        } else if collectionView == self.collViewNewList {
            return (newList.count)
        } else if collectionView == self.collectionWhatsMind {
            return arrwhtsYourMind.count
        } else if collectionView == self.colletionViewAppointment {
            return 7
        } else if collectionView == self.colletionAdminService {
            return arrAdminService.count
        } else {
            return finalFilteredArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collViewOffers {
            return viewOffersCell(collectionView, indexPath: indexPath)
        }
        else if collectionView == self.collectionView {
            return advertiseCell(collectionView, indexPath: indexPath)
        }
        else if collectionView == collViewStoreAds {
            return storeAdsCell(collectionView, indexPath: indexPath)
        }
        else if collectionView == collViewNewList {
            return newListCell(collectionView, indexPath: indexPath)
        }
        else if collectionView == collectionWhatsMind {
            return whatsMindCell(collectionView, indexPath: indexPath)
        }
        else if collectionView == colletionViewAppointment {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppointmentCell", for: indexPath) as! AppointmentCell
            return cell
        }
        else if collectionView == colletionAdminService {
            return adminServiceCell(collectionView, indexPath: indexPath)
        }
        else {
            return storeListCell(collectionView, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == self.collViewStoreList{
            let visibleIndexPaths = collViewStoreList!.indexPathsForVisibleItems
            //print(visibleIndexPaths)
            let sec = collectionView.numberOfSections - 1
            let it = collectionView.numberOfItems(inSection: sec) - 1
            if indexPath.section == sec && indexPath.row == it && (self.totalStoreCount >= self.storeListLength ?? 0) {
                //print("willDisplay \(indexPath.item)")
            }
        }
    }
    //MARK: - ViewOffers cell
    func viewOffersCell(_ collectionView: UICollectionView ,indexPath: IndexPath) -> AdvertiseCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertiseCell", for: indexPath) as! AdvertiseCell
        
        cell.bannerImageView.contentMode = .scaleAspectFit
        cell.bannerImageView.downloadedFrom(link: Utility.getDynamicResizeImageURL(width: cell.frame.width, height: cell.frame.height, imgUrl: (arrPromoCodeList?[indexPath.row].imageUrl) ?? ""), placeHolder: "no_offer" , isFromResize: true)
        cell.lblOfferName?.text = arrPromoCodeList?[indexPath.row].promoCodeName ?? ""
        cell.lblOfferName?.isHidden = false
        return cell
    }
    //MARK: - Advirtice cell
    func advertiseCell(_ collectionView: UICollectionView ,indexPath: IndexPath) -> AdvertiseCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertiseCell", for: indexPath) as! AdvertiseCell
        
        cell.bannerImageView.contentMode = .scaleAspectFit
        cell.bannerImageView.downloadedFrom(link: Utility.getDynamicResizeImageURL(width: cell.bannerImageView.frame.width, height: cell.bannerImageView.frame.height, imgUrl: (currentBooking.deliveryAdsList?[indexPath.row].imageForBanner) ?? ""),placeHolder: "no_ads",  isFromResize: true)
        cell.bannerImageView.contentMode = .scaleAspectFill
        return cell
    }
    //MARK: - Store ads cell
    func storeAdsCell(_ collectionView: UICollectionView ,indexPath: IndexPath) -> AdvertiseCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertiseCell", for: indexPath) as! AdvertiseCell
        cell.bannerImageView.contentMode = .scaleAspectFit
        cell.lblOfferName?.isHidden = false
        cell.lblOfferName?.text = (arrForAddList[indexPath.row].adsDetail ?? "")

        cell.bannerImageView.downloadedFrom(link:Utility.getDynamicResizeImageURL(width: cell.bannerImageView.frame.width, height: cell.bannerImageView.frame.height, imgUrl: (arrForAddList[indexPath.row].imageForBanner) ?? ""), placeHolder: "no_ads" , isFromResize: true)
        return cell
    }
    //MARK: - New list cell
    func newListCell(_ collectionView: UICollectionView ,indexPath: IndexPath) -> TableBookCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TableBookCell", for: indexPath) as! TableBookCell
        cell.setData(labelTitle: newList[indexPath.row])
        return cell
    }
    //MARK: - Waht's On mind cell list
    func whatsMindCell(_ collectionView: UICollectionView ,indexPath: IndexPath) -> WhatsOnMindCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WhatsOnMindCell", for: indexPath) as! WhatsOnMindCell
        let obj = arrwhtsYourMind[indexPath.row]
        cell.setData(data: obj)
        if self.selectedIndexWhatsInMind == indexPath.item {
            cell.lblTitle.textColor = UIColor.themeColor
        } else {
            cell.lblTitle.textColor = UIColor.themeTextColor
        }
        return cell
    }
    //MARK: - Advirtice cell list
    func adminServiceCell(_ collectionView: UICollectionView ,indexPath: IndexPath) -> AdvertiseCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertiseCell", for: indexPath) as! AdvertiseCell
        let obj = arrAdminService[indexPath.row]
        cell.bannerImageView.contentMode = .scaleAspectFit
        cell.lblOfferName?.isHidden = true
    
        cell.bannerImageView.downloadedFrom(link:Utility.getDynamicResizeImageURL(width: cell.bannerImageView.frame.width, height: cell.bannerImageView.frame.height, imgUrl: obj.image_url ?? "") , isFromResize: true)
        return cell
    }
    //MARK: - Return Store list cell
    func storeListCell(_ collectionView: UICollectionView ,indexPath: IndexPath) -> StoreCell{
        let cell:StoreCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCell", for: indexPath) as! StoreCell
        let currentStore:StoreItem = finalFilteredArray[indexPath.row]
        cell.setCellData(cellItem: currentStore)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionWhatsMind {
            isTableBooking = false
            let obj = arrwhtsYourMind[indexPath.row]
            self.btnSearchStore.isHidden = false
            self.btnFilterStore.isHidden = false
             if obj.deliveryType == DeliveryType.ecommerce {
                 currentBooking.deliveryType = obj.deliveryType
                 currentBookingType = obj.deliveryType
                currentBooking.ecommerceDeliveryId = currentBooking.deliveryStoreList.filter({$0.deliveryType == obj.deliveryType})[0]._id ?? ""
                 self.wsGetStoreListForEcommerce(storeDeliveryId: currentBooking.ecommerceDeliveryId, indexEcommerce: indexPath.item, defaultImage: obj.default_image_url)
                return
            }
        }
        collectionView.deselectItem(at: indexPath, animated: true)
        if collectionView == self.collViewOffers {
            guard let promoId = (arrPromoCodeList?[indexPath.item])?.id else { return  }
            self.wsGetPromoDetail(promoId: promoId)
        }
        else if collectionView == self.collectionView {
            if currentBooking.deliveryAdsList![indexPath.row].isAdsRedirectToStore{
                let storeId:String = (currentBooking.deliveryAdsList![indexPath.item].storeId)
                if (storeId.isEmpty()) {
                    
                } else {
                    selectedStoreItem = currentBooking.deliveryAdsList![indexPath.item].store_detail
                    currentBooking.isSelectedStoreClosed = (selectedStoreItem?.isStoreClosed)!
                    currentBooking.selectedStore = selectedStoreItem!
                    indC = indexPath.item
                    self.goToProductVC(storeID: storeId)
                }
            }
        }
        else if collectionView == self.collViewStoreAds {
            if self.arrForAddList[indexPath.item].isAdsRedirectToStore {
                let storeId:String = self.arrForAddList[indexPath.row].storeId
                if (storeId.isEmpty()) {
                } else {
                    if self.arrForAddList[indexPath.item].isAdsRedirectToStore{
                        selectedStoreItem = self.arrForAddList[indexPath.item].store_detail
                        currentBooking.isSelectedStoreClosed = (selectedStoreItem?.isStoreClosed) ?? false
                        if let selecteStore = selectedStoreItem{
                            currentBooking.selectedStore = selectedStoreItem
                            self.goToProductVC(storeID: storeId)
                        }
                    }
                }
            }
        }
        else if collectionView == self.collViewStoreList {
            //select on product
            
            selectedStoreItem = finalFilteredArray[indexPath.row]
            currentBooking.isSelectedStoreClosed = (selectedStoreItem?.isStoreClosed)!
            currentBooking.selectedStore = selectedStoreItem!
            if isTableBooking {
//                self.openTableBookingDialog(storeID: selectedStoreItem?._id ?? "")
                self.wsGetProductData(storeId: selectedStoreItem?._id ?? "")
            } else {
                self.goToProductVC()
            }
        }
        else if collectionView == self.collectionWhatsMind {
            //print("Whats your mind tapped at indexPath \(indexPath.row)")
            
            let obj = arrwhtsYourMind[indexPath.row]
            self.btnSearchStore.isHidden = false
            self.btnFilterStore.isHidden = false
            currentBooking.deliveryType = obj.deliveryType
            currentBookingType = obj.deliveryType
            delegateUpdateMainVC?.updatScanerButton(isHide: true)
            if obj.deliveryType == DeliveryType.store {
                delegateUpdateMainVC?.updatScanerButton(isHide: false)
            }
            if obj.deliveryType != DeliveryType.ecommerce && obj.deliveryType != DeliveryType.courier &&  obj.deliveryType != DeliveryType.taxi {
                selectedIndexWhatsInMind = indexPath.row
            }
            switch obj.deliveryType {
            case DeliveryType.store :
                currentBooking.deliveryId = obj._id ?? ""
                showWhichDeliveryPopUp(indexPath: indexPath, deliveryType: obj.deliveryType)
                break
            case DeliveryType.service :
                currentBooking.deliveryId = obj._id ?? ""
                showWhichDeliveryPopUp(indexPath: indexPath, deliveryType: obj.deliveryType)
                break
            case DeliveryType.appoinment :
                currentBooking.deliveryId = obj._id ?? ""
                showWhichDeliveryPopUp(indexPath: indexPath, deliveryType: obj.deliveryType)
                break
            case DeliveryType.courier :
                currentBooking.isFutureCourierOrder = false
                currentBooking.courierDeliveryId = currentBooking.deliveryStoreList.filter({$0.deliveryType == obj.deliveryType})[0]._id ?? ""
                if preferenceHelper.UserId.isEmpty {
                    self.goToLogin()
                    Utility.showToast(message: "TXT_NEED_LOGIN_FOR_COURIER".localized)
                } else {
                    let vc = UIStoryboard(name: "Courier", bundle: nil).instantiateViewController(withIdentifier: "CourierVC") as! CourierVC
                    vc.delegateBackReloadStore = self
                    self.navigationController?.pushViewController(vc, animated: true)
                    /*let vc = UIStoryboard(name: "Eservice", bundle: nil).instantiateViewController(withIdentifier: "BookAppoinmentVC")
                    self.navigationController?.pushViewController(vc, animated: true)
                    let vc = UIStoryboard(name: "Eber", bundle: nil).instantiateViewController(withIdentifier: "RideHomeVC") as! RideHomeVC
                    vc.deliveryType = DeliveryType.courier
                    self.navigationController?.pushViewController(vc, animated: true)*/
                }
                break
            case DeliveryType.taxi :
                currentBooking.isFutureTaxiOrder = false
                currentBooking.taxiDeliveryId = currentBooking.deliveryStoreList.filter({$0.deliveryType == obj.deliveryType})[0]._id ?? ""

                if preferenceHelper.UserId.isEmpty {
                    self.goToLogin()
                    Utility.showToast(message: "TXT_NEED_LOGIN_FOR_TAXI".localized)
                } else {
                    wsGetTrips(isNext: true)
                }
                break
            case DeliveryType.ecommerce :
                currentBooking.ecommerceDeliveryId = currentBooking.deliveryStoreList.filter({$0.deliveryType == obj.deliveryType})[0]._id ?? ""
                self.wsGetStoreListForEcommerce(storeDeliveryId: currentBooking.ecommerceDeliveryId, indexEcommerce: indexPath.item)
                break
            default:
                //print("deivery type not found")
                break
            }
            self.collectionWhatsMind?.reloadData()
        }
        else if collectionView == collViewNewList {
            //print(newList[indexPath.row])
            isTableBooking = false
            for (index, _) in newList.enumerated() {
                newList[index][1] = false
            }
            newList[indexPath.row][1] = true
            collViewNewList?.reloadData()
            
            if indexPath.row == 0 {
                self.reloadTableWithArray(array: self.originalArrStoreList ?? [])
            } else if indexPath.row == 1 {
                isTakeAway = 1
                self.pickupArrStoreList.removeAll()
                for store:StoreItem in self.originalArrStoreList ?? [] {
                    if store.isProvidePickupDelivery {
                        self.pickupArrStoreList.append(store)
                    }
                }
                self.reloadTableWithArray(array: pickupArrStoreList)
            } else {
                isTableBooking = true
                self.bookTableArrStoreList.removeAll()
                for store:StoreItem in self.originalArrStoreList ?? [] {
                    if store.isTableReservation || store.isTableReservationWithOrder {
                        self.bookTableArrStoreList.append(store)
                    }
                }
                self.reloadTableWithArray(array: bookTableArrStoreList)
            }
        }
        else if collectionView == colletionAdminService {
            let obj = arrAdminService[indexPath.row]
            let productVc : ProductVC = ProductVC.init(nibName: "Product", bundle: nil)
            productVc.isShowAdminService = true
            productVc.deliveryType = obj.deliveryType
            productVc.cityId = currentBooking.bookCityId ?? ""
            productVc.storeDelvieryId = self.selectedDeliveryItem?._id ?? ""
            productVc.isFromDeliveryList = true
            productVc.defaultImage = obj.image_url ?? ""
            self.navigationController?.pushViewController(productVc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collViewOffers {
            if UIDevice.current.userInterfaceIdiom == .pad {
                let height = collectionView.frame.size.height-80.0
                let width = height*1.25
                return CGSize(width: width, height: height)
            } else {
                let height = collectionView.frame.size.height - 20
                let width = height*1.25
                //print("Offers size",width,height)
                return CGSize(width: width, height: height)
            }
        }
        else if collectionView == self.collectionView {
            if UIDevice.current.userInterfaceIdiom == .pad {
                let height = collectionView.frame.size.height-80.0
                let width = height*1.25
                return CGSize(width: width, height: height)
            } else {
                let height = 120.0
                let width = height*1.25
                //print("Delivery Ads size",width,height)
                return CGSize(width: width, height: height)
            }
        }
        else if collectionView == self.collViewStoreAds {
            if UIDevice.current.userInterfaceIdiom == .pad {
                let height = collectionView.frame.size.height-80.0
                let width = height*1.25
                return CGSize(width: width, height: height)
            } else {
                let height = 120.0
                let width = height*1.25
                return CGSize(width: width, height: height)
            }
        }
        else if collectionView == self.collViewStoreList {
            var width:CGFloat = 0.0
            var height:CGFloat = 0.0
            if UIDevice.current.userInterfaceIdiom == .pad {
                width = collectionView.frame.size.width
                width -= (collectionView.contentInset.left+collectionView.contentInset.right)
                width -= 50.0
                width = width/2.0
                height =  width*0.8
            } else {
                width = collectionView.frame.size.width
                width -= (collectionView.contentInset.left+collectionView.contentInset.right)
                width -= 30.0
                width = width/2.0
                height =  width/0.8
            }
            if height < 230 {
                height = 230
            }
            return CGSize(width: width, height: height)
        }
        else if collectionView == self.collectionWhatsMind {
            var width:CGFloat = 0.0
            var height:CGFloat = 0.0
            let spacing: CGFloat = 15
            if UIDevice.current.userInterfaceIdiom == .pad {
                width = (UIScreen.main.bounds.size.width - 4 * spacing)/5
                height = width * 1.2
            } else {
                width = (UIScreen.main.bounds.size.width - 4 * spacing)/3
                height = width * 1.2
            }
            heightWhatsYourMind?.constant = height
            return CGSize(width: width, height: height)
        }
        else if collectionView == self.colletionViewAppointment {
            var width:CGFloat = 0.0
            var height:CGFloat = 0.0
            let spacing: CGFloat = 15
            if UIDevice.current.userInterfaceIdiom == .pad {
                width = (UIScreen.main.bounds.size.width - 4 * spacing)/5
                height = width * 48
            } else {
                width = (UIScreen.main.bounds.size.width - 4 * spacing)/3
                height = width * 48
            }
            heightWhatsYourMind?.constant = height
            return CGSize(width: width, height: height)
        }
        else if collectionView == self.colletionAdminService {
            if UIDevice.current.userInterfaceIdiom == .pad {
                let height = collectionView.frame.size.height-80.0
                let width = height*1.25
                return CGSize(width: width, height: height)
            } else {
                let height = 120.0
                let width = screenWidth - 2*20
                return CGSize(width: width, height: height)
            }
        }
        else {
            let labelTitle:[Any] = newList[indexPath.row]
            let label = UILabel(frame: .zero)
            label.text = labelTitle.first as? String ?? ""
            label.sizeToFit()
            return CGSize(width:label.frame.width, height:collViewNewList?.frame.height ?? 40)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView != self.collViewNewList {
            return 10.0
        } else {
            return 0.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collViewStoreList || collectionView == collViewOffers {
            return UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
        } else if collectionView == collectionWhatsMind {
            return UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
        } else {
            return UIEdgeInsets(top: 0.0, left: 15.0, bottom: 0.0, right: 0.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func showWhichDeliveryPopUp(indexPath: IndexPath, deliveryType: Int) {
        let obj = arrwhtsYourMind[indexPath.row]
        
        let arrStore = currentBooking.deliveryStoreList.filter({$0.deliveryType == obj.deliveryType})

        let strTitle: String = {
            switch obj.deliveryType {
            case DeliveryType.store :
                lblStoreListTitle.text = "TXT_STORE_LIST".localized
                return "txt_which_delivery".localized
            case DeliveryType.service :
                lblStoreListTitle.text = "txt_service".localized
                return "txt_which_service".localized
            case DeliveryType.appoinment :
                lblStoreListTitle.text = "txt_appointment".localized
                return "txt_appoinment_for".localized
            default:
                lblStoreListTitle.text = "TXT_STORE_LIST".localized
                return "txt_which_delivery".localized
            }
        }()
        
        let dialog = DeliveryListPopUp.showDeliveryListDialog(title: strTitle, strRightButton: "TXT_DONE".localized, deliveryType: deliveryType, arrDelivery: arrStore)
        dialog.onClickRightButton = { [weak self] obj in
            dialog.removeFromSuperview()
            guard let self = self else { return }
            self.selectedDeliveryItem = obj
            self.showAdminService()
            if self.selectedDeliveryItem!.deliveryType == DeliveryType.courier {
                if preferenceHelper.UserId.isEmpty {
                    self.goToLogin()
                    Utility.showToast(message: "TXT_NEED_LOGIN_FOR_COURIER".localized)
                } else {
                    currentBooking.courierDeliveryId = (self.selectedDeliveryItem?._id) ?? ""
                    self.performSegue(withIdentifier: SEGUE.SEGUE_COURIER, sender: nil)
                }
            } else {
                self.originalArrStoreList = []
                self.isDeliveryChanged = true
                self.selectedFilterOptions = nil
                self.checkForStoreList(isFromClick: true)
            }
        }
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
    }
    
    func showAdminService() {
        guard let selectedDeliveryItem = selectedDeliveryItem else { return }
        if selectedDeliveryItem.is_admin_service {
            viewAdminService?.isHidden = false
            arrAdminService.removeAll()
            arrAdminService.append(selectedDeliveryItem)
            colletionAdminService?.reloadData()
        } else {
            viewAdminService?.isHidden = true
        }
    }
    
    func goToProductVC(storeID:String = "",isAdminService: Bool = false, isEcommerce: Bool = false,defaultImage : String = "") {
        timerForAdd?.invalidate()
        
        let productVc : ProductVC = ProductVC.init(nibName: "Product", bundle: nil)
        
        if isAdminService {
            productVc.isShowAdminService = isAdminService
            productVc.cityId = currentBooking.bookCityId ?? ""
            productVc.storeDelvieryId = self.selectedDeliveryItem?._id ?? ""
        } 
        
        if storeID.isEmpty {} else {
            productVc.selectedStoreId = storeID
        }
        
        productVc.isFromDeliveryList = true
        productVc.selectedDelivery = selectedDeliveryItem
        if currentBooking.deliveryStoreList.count > 0 {
            productVc.isShowGroupItems = currentBooking.deliveryStoreList[0].is_store_can_create_group!
        }
        productVc.selectedStore = self.selectedStoreItem
        
        productVc.isFromEcommerce = isEcommerce
        if isEcommerce{
            productVc.defaultImage = defaultImage//(selectedStoreItem?.image_url)!
        }else{
            if selectedStoreItem?.image_url != nil{
                productVc.defaultImage = (selectedStoreItem?.image_url)!
            }
        }
        var isIndexMatch : Bool = false
        var selectedInd : Int = 0
        
        if preferenceHelper.SelectedLanguageCode != Constants.selectedLanguageCode {
            Constants.selectedLanguageCode = preferenceHelper.SelectedLanguageCode
        }
        
        if storeID.isEmpty {
            if currentBooking.selectedStore != nil {
                for obj in currentBooking.selectedStore!.langItems! {
                    if (obj.code == Constants.selectedLanguageCode) && (obj.is_visible! == true) {
                        isIndexMatch = true
                        selectedInd = currentBooking.selectedStore!.langItems!.firstIndex(where: { $0.code == obj.code })!
                        break
                    } else {
                        isIndexMatch = false
                    }
                }
            }
        } else {
            if self.selectedStoreItem != nil {
                for obj in self.selectedStoreItem!.langItems! {
                    if obj.code == Constants.selectedLanguageCode {
                        isIndexMatch = true
                        selectedInd = self.selectedStoreItem!.langItems!.firstIndex(where: { $0.code == obj.code })!
                        break
                    } else {
                        isIndexMatch = false
                    }
                }
            }
        }
        
        //Change for Crash
        if !isIndexMatch {
            if self.selectedStoreItem != nil {
                if selectedStoreItem!.langItems!.count == 0{
                    productVc.languageCode = "en"
                    productVc.languageCodeInd = "0"
                }else{
                    productVc.languageCode = selectedStoreItem!.langItems![0].code!
                    productVc.languageCodeInd = "0"
                }
            } else {
                productVc.languageCode = "en"
                productVc.languageCodeInd = "0"
            }
        } else {
            productVc.languageCode = Constants.selectedLanguageCode
            productVc.languageCodeInd = "\(selectedInd)"
        }

        self.navigationController?.pushViewController(productVc, animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            if let scrollView = self.scrollView {
                if CGRect(origin: scrollView.contentOffset, size: scrollView.bounds.size).maxY >= CGRect(origin: collViewStoreList!.contentOffset, size: collViewStoreList!.bounds.size).maxY {
                    if (self.totalStoreCount > self.originalArrStoreList?.count ?? 0) && storeListLength! != 0 {
                        apicallCount = apicallCount + 1
                        CONSTANT.CURRENT_PAGE = CONSTANT.CURRENT_PAGE + 1
                        self.wsGetStoreList(storeDeliveryId: selectedDeliveryItem?._id ?? "", defaultImage: selectedDeliveryItem?.default_image_url ?? "", isNeedMoreData: true)
                    }
                }
            }
        }
    }
    
}

extension HomeVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let mySearchText = string.trimmingCharacters(in: .whitespaces)
        let string1 = mySearchText
        let string2 = textField.text
        var finalString = ""

        if string.count > 0 {
            finalString = string2! + string1
        } else if string2?.count ?? 0 > 0 {
            finalString = String(string2!.dropLast())
        }
        strSearch = finalString
        guard let selectedDeliveryItem = selectedDeliveryItem else {
            return true
        }
        CONSTANT.CURRENT_PAGE = 1
        wsGetStoreList(storeDeliveryId: selectedDeliveryItem._id ?? "", defaultImage: selectedDeliveryItem.default_image_url, isShowLoading: false)
        //searchStore(finalString)
        return true
    }
}
extension HomeVC : BackReloadStore{
    func BackfromTaxiCourier() {
        currentBookingType = 1
        self.selectedIndexWhatsInMind = 0
//        colletionViewSetUp()
//        currentBooking.deliveryStoreList = [DeliveriesItem]()
        self.wsGetDeliveriesInNearestCity()
        let headerView: UIView = UIView.init(frame: CGRect(x: 1, y: 50, width: 276, height: 30))
        headerView.backgroundColor = .red
        let labelView: UILabel = UILabel.init(frame: CGRect(x: 4, y: 5, width: 276, height: 24))
        labelView.text = "My header view"
        headerView.addSubview(labelView)
//        setLocalization()
        txtSearchStore.isHidden = true
        setUpCartView()
        self.viewForOffers?.isHidden = true
        self.txtSearchStore.autocorrectionType = .no
        collectionWhatsMind?.reloadData()
        self.basicSetup()
    }    
}
protocol BackReloadStore{
    func BackfromTaxiCourier()
}
