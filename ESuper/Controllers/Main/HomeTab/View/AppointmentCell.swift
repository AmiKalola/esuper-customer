//
//  AppointmentCell.swift
//  Decagon
//
//  Created by MacPro3 on 09/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class AppointmentCell: UICollectionViewCell {

    @IBOutlet weak var imgAppointment: ImageViewDefault!
    @IBOutlet weak var lblAppointment: LabelDefault!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgAppointment.applyRoundedCornersWithHeight(15)
    }
    
    func setData() {
        
    }

}
