//
//  AdvertiseCell.swift
//  ESuper
//
//  Created by Rohit on 07/07/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import Foundation
import UIKit
class AdvertiseCell: CustomCollectionCell {
    
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var lblOfferName: LabelDefault?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.layoutIfNeeded()
        bannerImageView.setRound(withBorderColor: UIColor.clear, andCornerRadious:8.0, borderWidth: 0.5)
        self.setRound(withBorderColor: UIColor.clear, andCornerRadious:8.0, borderWidth: 0.5)
    }
}
