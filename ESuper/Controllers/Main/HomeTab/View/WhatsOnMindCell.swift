//
//  WhatsOnMind.swift
//  Decagon
//
//  Created by MacPro3 on 29/04/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class WhatsOnMindCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: LabelDefault!
    @IBOutlet weak var viewBG: ViewDefault!
    @IBOutlet weak var imgService: ImageViewDefault!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewBG.backgroundColor = UIColor.themeViewBackgroundColor
        viewBG.applyRoundedCornersWithHeight(10)
    }
    
    func setData(data: DeliveriesItem) {
        lblTitle.text = data.delivery_name
        imgService.downloadedFrom(link: data.image_url ?? "", placeHolder: "placeholder", isFromCache: true, isIndicator: false, mode: .scaleAspectFit, isAppendBaseUrl: true, isFromResize: false, completion: nil)
    }
}
