//
//  FareEstimateDialog.swift
//  
//
//  Created by Rohit on 02/01/24.
//

import UIKit

class FareEstimateDialog: CustomDialog{
    @IBOutlet weak var tableServiceCharge : UITableView!
    
    @IBOutlet weak var viewForFareEstimateDialog: UIView!
    @IBOutlet weak var lblServicetypeName: UILabel!
    @IBOutlet weak var imgServicetype: UIImageView!
    
    @IBOutlet weak var vehicleView: UIView!
    @IBOutlet weak var viewForEstimateCost: UIView!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var viewExtra: UIView!
    @IBOutlet weak var viewFormulaCharge: UIView!
    @IBOutlet weak var stackAllCharges: UIStackView!
    
    @IBOutlet weak var viewForPricing: UIView!
    
    @IBOutlet weak var lblTripType: UILabel!
    @IBOutlet weak var lblFixedPriceValue: UILabel!
    @IBOutlet weak var stkFixedSurgePrice: UIStackView!
    @IBOutlet weak var lblFixedSurgePrice: UILabel!
    @IBOutlet weak var lblFixedSurgePriceValue: UILabel!

    @IBOutlet weak var stkBonusPrice: UIStackView!
    @IBOutlet weak var lblBonusPrice: UILabel!
    @IBOutlet weak var lblBonusPriceValue: UILabel!

    
    @IBOutlet weak var lblBasePriceValue: UILabel!
    @IBOutlet weak var lblDistancePriceValue: UILabel!
    @IBOutlet weak var lblTimePriceValue: UILabel!
    @IBOutlet weak var lblAdditionalStopPriceValue: UILabel!
    @IBOutlet weak var lblCancelationPriceValue: UILabel!
    @IBOutlet weak var lblTaxPriceValue: UILabel!
    
    @IBOutlet weak var lblFixedPrice: UILabel!
    @IBOutlet weak var lblBasePrice: UILabel!
    @IBOutlet weak var lblDistancePrice: UILabel!
    @IBOutlet weak var lblTimePrice: UILabel!
    @IBOutlet weak var lblAdditionalStopPrice: UILabel!
    @IBOutlet weak var lblCancelationPrice: UILabel!
    @IBOutlet weak var lblTaxPrice: UILabel!
    
    @IBOutlet weak var stkBasePrice: UIStackView!
    @IBOutlet weak var stkDistancePrice: UIStackView!
    @IBOutlet weak var stkTimePrice: UIStackView!
    @IBOutlet weak var stkAdditionalStopPrice: UIStackView!
    @IBOutlet weak var stkCancelationPrice: UIStackView!
    @IBOutlet weak var stkTaxPrice: UIStackView!
    
    
    @IBOutlet weak var stkPickupAddressView: UIStackView!
    @IBOutlet weak var lblPickupAddress: UILabel!
    
    @IBOutlet weak var stkDestinationAddressView: UIStackView!
    @IBOutlet weak var lblDestinationAddress: UILabel!
    
    @IBOutlet weak var lblDivider: UILabel!
    
    @IBOutlet weak var lblTotalDistance: UILabel!
    @IBOutlet weak var lblTotalDistanceValue: UILabel!
    
    @IBOutlet weak var lblTotalTime: UILabel!
    @IBOutlet weak var lblTotalTimeValue: UILabel!
    
    @IBOutlet weak var lblEstMsg: UILabel!
    
    @IBOutlet weak var viewForFareAmount: UIView!
    
    @IBOutlet weak var stkSurgeView: UIStackView!
    @IBOutlet weak var lblSurgeFare: UILabel!
    @IBOutlet weak var lblSurgeFareValue: UILabel!
    
    @IBOutlet weak var lblFareAmount: UILabel!
    @IBOutlet weak var lblFareAmountValue: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
  
    @IBOutlet weak var imgIconETA: UIImageView!
    @IBOutlet weak var imgIconDistance: UIImageView!
    @IBOutlet weak var imgRoot: UIImageView!
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    
    var tableViewHeight: CGFloat {
        tableServiceCharge.layoutIfNeeded()
        return tableServiceCharge.contentSize.height
    }
    var arrayInvoice = [ServiceCharge]()
    
    override func awakeFromNib() {
        tableServiceCharge.register(ServiceChargeCell.nib, forCellReuseIdentifier: ServiceChargeCell.identifier)
        viewNoData.isHidden = true
        viewFormulaCharge.isHidden = true
        viewExtra.isHidden = true
        self.lblTripType.font = FontHelper.textMedium()
        self.lblTripType.textColor = UIColor.themeTextColor
        
        self.lblFixedPrice.font = FontHelper.textRegular()
        self.lblFixedPrice.textColor = UIColor.themeTextColor
        self.lblFixedPrice.text = "Fixed_Price".localized
        
        self.lblBasePrice.font = FontHelper.textRegular()
        self.lblBasePrice.textColor = UIColor.themeTextColor
        self.lblBasePrice.text = "Base_Price".localized
        
        self.lblDistancePrice.font = FontHelper.textRegular()
        self.lblDistancePrice.textColor = UIColor.themeTextColor
        self.lblDistancePrice.text = "Distance_Price".localized
        
        self.lblTimePrice.font = FontHelper.textRegular()
        self.lblTimePrice.textColor = UIColor.themeTextColor
        self.lblTimePrice.text = "Time_Price".localized
        
        self.lblAdditionalStopPrice.font = FontHelper.textRegular()
        self.lblAdditionalStopPrice.textColor = UIColor.themeTextColor
        self.lblAdditionalStopPrice.text = "Stop_Price".localized
        
        self.lblCancelationPrice.font = FontHelper.textRegular()
        self.lblCancelationPrice.textColor = UIColor.themeTextColor
        self.lblCancelationPrice.text = "Cancalation_Price".localized
        
        self.lblTaxPrice.font = FontHelper.textRegular()
        self.lblTaxPrice.textColor = UIColor.themeTextColor
        self.lblTaxPrice.text = "Tax".localized
        
        self.lblBasePriceValue.font = FontHelper.textMedium()
        self.lblBasePriceValue.textColor = UIColor.themeTextColor
        
        self.lblDistancePriceValue.font = FontHelper.textMedium()
        self.lblDistancePriceValue.textColor = UIColor.themeTextColor
        
        self.lblTimePriceValue.font = FontHelper.textMedium()
        self.lblTimePriceValue.textColor = UIColor.themeTextColor
        
        self.lblAdditionalStopPriceValue.font = FontHelper.textMedium()
        self.lblAdditionalStopPriceValue.textColor = UIColor.themeTextColor
        
        self.lblCancelationPriceValue.font = FontHelper.textMedium()
        self.lblCancelationPriceValue.textColor = UIColor.themeTextColor
        
        self.lblTaxPriceValue.font = FontHelper.textMedium()
        self.lblTaxPriceValue.textColor = UIColor.themeTextColor
        
        self.lblFareAmount.text = "Total_Charges".localized
        self.lblFareAmount.font = FontHelper.textRegular()
        self.lblFareAmountValue.font = FontHelper.textMedium()
        lblFareAmount.textColor = UIColor.themeTextColor
        lblFareAmountValue.textColor = UIColor.themeTextColor
        
        self.lblFixedPriceValue.font = FontHelper.textMedium()
        self.lblFixedPriceValue.textColor = UIColor.themeTextColor
        
        self.lblFixedSurgePrice.font = FontHelper.textRegular()
        self.lblFixedSurgePrice.textColor = UIColor.themeTextColor
        
        self.lblFixedSurgePriceValue.font = FontHelper.textMedium()
        self.lblFixedSurgePriceValue.textColor = UIColor.themeTextColor
        
        self.lblBonusPrice.font = FontHelper.textRegular()
        self.lblBonusPrice.textColor = UIColor.themeTextColor
        
        self.lblBonusPriceValue.font = FontHelper.textMedium()
        self.lblBonusPriceValue.textColor = UIColor.themeTextColor
        
        self.lblSurgeFare.text = "Surge_pricing".localized
        self.lblSurgeFare.font = FontHelper.textRegular()
        self.lblSurgeFareValue.font = FontHelper.textMedium()
        lblSurgeFare.textColor = UIColor.themeTextColor
        lblSurgeFareValue.textColor = UIColor.themeTextColor
        
        
        imgRoot.image = imgRoot.image?.withRenderingMode(.alwaysTemplate)
        imgRoot.tintColor = UIColor.themeImageColor
        imgIconETA.image = UIImage(systemName: "clock.arrow.circlepath")
        //          imgIconETA.image = imgIconETA.image?.withRenderingMode(.alwaysTemplate)
        imgIconETA.tintColor = UIColor.themeImageColor
        
        imgIconDistance.image = imgIconDistance.image?.withRenderingMode(.alwaysTemplate)
        imgIconDistance.tintColor = UIColor.themeImageColor
        imgIconETA.tintColor = UIColor.themeImageColor
        
        btnCancel.setRound(withBorderColor: .clear, andCornerRadious: 25, borderWidth: 1.0)
        btnCancel.backgroundColor = UIColor.themeButtonBackgroundColor
        btnCancel.setTitleColor(UIColor.themeButtonTitleColor, for: .normal)
        btnCancel.setTitle("TXT_CLOSE".localizedCapitalized, for: .normal)
        btnCancel.titleLabel?.font = FontHelper.textMedium()//FontHelper.font(size: FontSize.regular, type: FontType.Bold)
        
        lblPickupAddress.text = "TXT_PICKUP_ADDRESS".localized
        lblPickupAddress.textColor = UIColor.themeTextColor
        lblPickupAddress.font = FontHelper.textRegular()//FontHelper.font(size: FontSize.small, type: FontType.Regular)
        
        lblDestinationAddress.text = "TXT_DESTINATION_ADDRESS".localized
        lblDestinationAddress.textColor = UIColor.themeTextColor
        lblDestinationAddress.font = FontHelper.textRegular()//FontHelper.font(size: FontSize.small, type: FontType.Regular)
        
        lblEstMsg.text = "TXT_FARE_ESTIMATE_MSG".localized
        lblEstMsg.textColor = UIColor.themeTextColor
        lblEstMsg.font = FontHelper.textRegular()//FontHelper.font(size: FontSize.small, type: FontType.Light)
        
        lblTotalDistance.text = "TXT_DISTANCE".localized
        lblTotalDistance.textColor = UIColor.themeTextColor
        lblTotalDistance.font = FontHelper.textRegular()//FontHelper.font(size: FontSize.small, type: FontType.Bold)
        
        lblTotalDistanceValue.text = "5.0 Miles"
        lblTotalDistanceValue.textColor = UIColor.themeTextColor
        lblTotalDistanceValue.font = FontHelper.textMedium()//FontHelper.font(size: FontSize.small, type: FontType.Bold)
        
        lblTotalTime.text = "TXT_ETA".localized
        lblTotalTime.textColor = UIColor.themeTextColor
        lblTotalTime.font = FontHelper.textRegular()//FontHelper.font(size: FontSize.small, type: FontType.Bold)
        
        lblTotalTimeValue.text = "-"
        lblTotalTimeValue.textColor = UIColor.themeTextColor
        lblTotalTimeValue.font = FontHelper.textMedium()//FontHelper.font(size: FontSize.small, type: FontType.Bold)
        
        self.backgroundColor = UIColor.themeOverlayColor
        
        stkSurgeView.isHidden = true
        
    }
    
    class func showFareEstimateDialog(fareEstimateValues : FareEstimateValues) -> FareEstimateDialog
    {
        let myView = Bundle.main.loadNibNamed("FareEstimate", owner: nil, options: nil)![0] as! FareEstimateDialog
        
        myView.viewForFareEstimateDialog.setShadow()
        myView.viewForFareEstimateDialog.setRound(withBorderColor: UIColor.lightText, andCornerRadious: 10.0, borderWidth: 0.5)
        
        myView.setData(fareEstimateValues: fareEstimateValues)
        
        myView.frame = UIScreen.main.bounds
        APPDELEGATE.window?.addSubview(myView)
        
        return myView
    }
    func setData(fareEstimateValues : FareEstimateValues){
//        price_formula 0 - default , 1-zone price, 2- airport price , 3 city-to-city price
        viewNoData.isHidden = true
        viewFormulaCharge.isHidden = true
        viewExtra.isHidden = true
        self.arrayInvoice = fareEstimateValues.arrayInvoice
        
        if fareEstimateValues.price_formula > 0{
            self.viewForPricing.isHidden = true
            self.stkFixedSurgePrice.isHidden = true
            self.stkBonusPrice.isHidden = true
            if fareEstimateValues.price_formula > 0{
                let fiexed_price = (fareEstimateValues.total_service_price).toString()//(fareEstimateValues.total_delivery_price - fareEstimateValues.total_surge_price).toString()
                print(fiexed_price)
                self.lblFixedPriceValue.text = "\(fiexed_price)"
            }
            if fareEstimateValues.surge_multiplier > 1{
                self.stkFixedSurgePrice.isHidden = false
                lblFixedSurgePrice.text = "Surge Price (\(Int(fareEstimateValues.surge_multiplier))X)"
                lblFixedSurgePriceValue.text = "\u{20B9} \(fareEstimateValues.total_surge_price.toString())"
            }
          
            if fareEstimateValues.promo_payment > 1{
                self.stkBonusPrice.isHidden = false
                lblBonusPrice.text = "Promo_Bonus".localized
                lblBonusPriceValue.text = "\u{20B9} \(fareEstimateValues.promo_payment.toString())"
            }
            stackAllCharges.isHidden = true
            viewFormulaCharge.isHidden = false
            switch fareEstimateValues.price_formula {
            case 1:
                lblTripType.text = "Zone Trip"
                break
            case 2:
                lblTripType.text = "Airport Trip"
                break
            case 3:
                lblTripType.text = "City Trip"
                break
            default:
                break
            }
        }else{
            self.viewForPricing.isHidden = false
            var isValue = 0
            viewForEstimateCost.isHidden = false
            
            stkBasePrice.isHidden = true
            stkDistancePrice.isHidden = true
            stkTimePrice.isHidden = true
            stkAdditionalStopPrice.isHidden = true
            stkCancelationPrice.isHidden = true
            stkTaxPrice.isHidden = true
            
            if arrayInvoice.count == 0{
                viewNoData.isHidden = false
            }else{
                viewNoData.isHidden = true
                if arrayInvoice.count < 3{
                    viewExtra.isHidden = false
                }
            }
        }
        if fareEstimateValues.surge_multiplier > 1{
            stkSurgeView.isHidden = false
            self.lblSurgeFareValue.text = "\(fareEstimateValues.surge_multiplier.toStringDouble())X"
        }else{
            stkSurgeView.isHidden = true
        }
        
       
//        self.constraintTableHeight.constant =  CGFloat((arrayInvoice.count * 33))
        self.tableServiceCharge.reloadData()
    
        self.constraintTableHeight.constant = tableViewHeight
        self.lblTotalTimeValue.text = "\(fareEstimateValues.ETA/60) \("UNIT_MIN".localized)"
        self.lblTotalDistanceValue.text = "\((fareEstimateValues.distance / 1000).toString()) \("UNIT_KM".localized)"
        self.lblFareAmountValue.text = "\u{20B9} \(fareEstimateValues.total_delivery_price.toString())"
        self.lblDestinationAddress.text = fareEstimateValues.destinationAddress ?? ""
        self.lblPickupAddress.text = fareEstimateValues.pickupAddress ?? ""
        self.lblServicetypeName.text = fareEstimateValues.vehical_name ?? ""
        if fareEstimateValues.is_min_fare_applied{
            self.lblFareAmount.text = "TXT_MINI_FARE".localized
        }else{
            self.lblFareAmount.text = "Total_Charges".localized
        }
        self.imgServicetype.downloadedFrom(link: fareEstimateValues.vehical_image ?? "", placeHolder: "placeholder", isFromCache: true, isIndicator: false, mode: .scaleAspectFit, isAppendBaseUrl: true, isFromResize: false, completion: nil)

    }
    func goWithTripType(fixedPrice:Double,maxSpace:Int,type:String){
        viewForEstimateCost.isHidden = true
        stkSurgeView.isHidden = true
        lblFareAmount.text = "TXT_FIXED_RATE".localized
        lblEstMsg.text = "TXT_FARE_ESTIMATE_MSG_FIXED_RATE".localized
    }
    @IBAction func onClickBtnCancel(_ sender: Any){
        self.removeFromSuperview()
    }
}
extension FareEstimateDialog : UITableViewDelegate,UITableViewDataSource{
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayInvoice.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ServiceChargeCell.identifier) as! ServiceChargeCell
        cell.lblService.text = arrayInvoice[indexPath.row].strService ?? ""
        cell.lblCharge.text = arrayInvoice[indexPath.row].strCharges ?? ""
        return cell
    }
}
