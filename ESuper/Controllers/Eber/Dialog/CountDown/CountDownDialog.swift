//
//  CountDownDialog.swift
//  ESuper
//
//  Created by Rohit on 09/04/24.
//  Copyright © 2024 Elluminati. All rights reserved.
//

import UIKit

class CountDownDialog: CustomDialog {
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!

    //MARK: Variables
    var timer = Timer()
    var onClickRightButton : (() -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    static let  verificationDialog = "CountDownDialog"
        var counts = 10
    public static func  showCustomSurgePriceDialog
        (title:String,
         titleLeftButton:String = "Cancel",
         titleRightButton:String = "Send",
         tag:Int = 400
         ) ->
    CountDownDialog
    {
        
        
        let view = UINib(nibName: verificationDialog, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CountDownDialog
        view.tag = tag
        view.alertView.setShadow()
        view.alertView.backgroundColor = UIColor.white
        view.backgroundColor = UIColor.themeOverlayColor
        let frame = (APPDELEGATE.window?.frame)!;
        view.frame = frame;
        view.btnLeft.setTitle(titleLeftButton.capitalized, for: UIControl.State.normal)
        view.btnRight.setTitle(titleRightButton.capitalized, for: UIControl.State.normal)
        view.lblTitle.text = title
        view.setLocalization()
        if let view = (APPDELEGATE.window?.viewWithTag(400))
        {
            UIApplication.shared.keyWindow?.bringSubviewToFront(view);
        }
        else
        {
        UIApplication.shared.keyWindow?.addSubview(view)
            UIApplication.shared.keyWindow?.bringSubviewToFront(view);
        }
        return view;
    }
    func setLocalization(){
        alertView.setRound(withBorderColor: UIColor.lightText, andCornerRadious: 10.0, borderWidth: 0.5)
        btnLeft.setTitleColor(UIColor.themeLightTextColor, for: UIControl.State.normal)
        btnLeft.titleLabel?.font =  FontHelper.textRegular(size: FontHelper.regular)
        
        
        btnRight.setTitleColor(UIColor.themeButtonTitleColor, for: UIControl.State.normal)
        btnRight.titleLabel?.font =  FontHelper.textRegular(size: FontHelper.medium)
        btnRight.backgroundColor = UIColor.themeButtonBackgroundColor
        //        btnRight.setupButton()
        btnRight.setRound(withBorderColor: .clear, andCornerRadious: 25, borderWidth: 1.0)
        
        lblTitle.textColor = UIColor.themeTextColor
        lblTitle.font = FontHelper.textRegular(size: FontHelper.large)
        lblCount.font = FontHelper.textRegular(size: FontHelper.largest)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)

    }
    @objc
    func timerAction(){
        counts = counts - 1
        self.lblCount.text = "\(counts)"
        if counts == 0 || counts < 1{
            timer.invalidate()
            if self.onClickRightButton != nil
            {
                self.onClickRightButton!()
            }
        }
    }
    @IBAction func onClickBtnLeft(_ sender: Any)
    {
        if self.onClickLeftButton != nil
        {
            timer.invalidate()
            self.onClickLeftButton!();
        }
        
    }
    @IBAction func onClickBtnRight(_ sender: Any)
    {
        if self.onClickRightButton != nil
        {
            timer.invalidate()
            self.onClickRightButton!()
        }
   }
    
}
