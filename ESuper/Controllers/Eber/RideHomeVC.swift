//
//  RideHomeVC.swift
//  Decagon
//
//  Created by MacPro3 on 10/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit
import GoogleMaps
public struct ScheduleDialogData {
    var isAsap = true
    var strDate = ""
    var strTime = ""
    var miliSecond: Int64 = 0
    var futureDateMillisecond:Int64 = 0
    var futureTimeMillisecond:Int64 = 0
    var date: Date?
    var selectedDateStr : String = ""
    var selectedDayInd : Int = 0
}
class RideHomeVC: BaseVC {
    
    @IBOutlet weak var mapView: GMSMapView?
    @IBOutlet weak var viewAddresses: UIView!
    @IBOutlet weak var txtPickupAddress: UITextField!
    @IBOutlet weak var txtDestinationAddress: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var lblTitle: LabelDefault!
    
    @IBOutlet weak var tableDestinations: UITableView!
    @IBOutlet weak var constraintLocationHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    @IBOutlet weak var viewAddAddress: UIView!
    @IBOutlet weak var imgPick: UIImageView!
    @IBOutlet weak var imgDrop: UIImageView!
    @IBOutlet weak var lblPick: UILabel!
    @IBOutlet weak var lblDrop: UILabel!
    
    
    var data: ScheduleDialogData?
    var orderId: String?
    private var order: Order?
    var delegateBackReloadStore  : BackReloadStore?
    enum locationType {
        case pickUp
        case destination
        case stop
    }
    
    var locationType: locationType = .pickUp
    var stopCount = 0
    var isEdit = false
    var pickUplat: Double = 0
    var pickUplong: Double = 0
    
    var destinationlat: Double = 0
    var destinationlong: Double = 0
    var viewServiceType: SelectServicePopUp = SelectServicePopUp.fromNib()
    var viewTripDetails: TripDetailDialog = TripDetailDialog.fromNib()
    var locationManager : LocationManager? = LocationManager()
    var deliveryType: Int = DeliveryType.taxi
    var estimateTime: Int = 0
    var estimateDistance: Double = 0
    
    var pickupAddress:Address = Address.init()
    var destinationAddress: [Address] = []
    var courierData: [String:Any]?
    
    var arrForInvoice = NSMutableArray()
    
    var dialogFromDriverDetail: CustomDriverDetailDialog?
    var isPaymentPending = false
    
    var promoCode: String?
    var promoID: String?
    var cityId = ""
    
    var dialogForCancelOrder: CustomCancelOrderDialog?
    var delegateBackFromHistoryDetails : BackFromHistoryDetails?
    var isPathDraw:Bool = true
    
    var polyLinePath = GMSPolyline()
    
    var isPickupCode = false
    var isCompleteCode = false
    var strCode = ""
    
    let socket:SocketHelper = SocketHelper.shared
    
    var providerLocation : [Double]!
    
    var providerMarker = GMSMarker()
    var arrProviderLocation = [[Double]]()
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSMutablePath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    var firstTime: Int? = 0
    var listStopLocationAddress = [StopLocationAddress]()
    var arrReason = ["txt_trip_cancel_reason_1".localized, "txt_trip_cancel_reason_2".localized, "txt_trip_cancel_reason_3".localized]
    var dialogForNetwork:CustomAlertDialog?
    var isAskUserForFixedFare = false
    var isFixedFare = false
    var isRunningTip = false
    var googlePathResponse: [String:Any]?
    var isRunninngTrip = false
    var arrForEmegerncyContacts:[EmergencyContactData] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        localized()
        imgPick.image = imgPick.image?.withRenderingMode(.alwaysTemplate)
        imgPick.tintColor = UIColor.themeImageColor
        lblPick.backgroundColor = UIColor.themeImageColor
        imgDrop.image = imgDrop.image?.withRenderingMode(.alwaysTemplate)
        imgDrop.tintColor = UIColor.themeImageColor
        lblDrop.backgroundColor = UIColor.themeImageColor
        
        setUpViews()
        setDelegates()
        setColorFontBG()
        
        providerMarker.map = mapView
        providerMarker.icon = UIImage(named: "car_pin")
        
        self.animationPolyline.strokeColor = UIColor.black
        self.animationPolyline.strokeWidth = 3
        self.animationPolyline.map = self.mapView
        
        btnBack.isHidden = isPaymentPending
        self.tableDestinations.register(LocationAddressCell.nib, forCellReuseIdentifier: LocationAddressCell.identifier)
        self.constraintLocationHeight.constant = 0
        if let order = order {
            orderId = order._id
            wsGetOrderDetail(id: order._id ?? "")
        } else if let id = orderId {
            wsGetOrderDetail(id: id)
            self.isRunninngTrip = true
        } else {
            getCurrentLocation()
            setCurrentLocation()
            
            pickupAddress.addressType = AddressType.PICKUP
            pickupAddress.city = currentBooking.currentCity
            pickupAddress.userType = CONSTANT.TYPE_USER
            
            var destinationAddress:Address = Address.init()
            destinationAddress.addressType = AddressType.DESTINATION
            destinationAddress.userType = CONSTANT.TYPE_USER
            destinationAddress.note = ""
            destinationAddress.city = currentBooking.currentCity
            destinationAddress.deliveryStatus = 0
            destinationAddress.flat_no = ""
            destinationAddress.street = ""
            destinationAddress.landmark = ""
            self.destinationAddress.append(destinationAddress)
            
            destinationAddress.addressType = AddressType.DESTINATION
            destinationAddress.city = currentBooking.currentCity
            destinationAddress.userType = CONSTANT.TYPE_USER
        }
        
        viewServiceType.addIn(vw: self.view, parent: self)
        viewTripDetails.addIn(vw: self.view)
        
        if deliveryType == DeliveryType.courier {
            viewServiceType.btnOffer.isHidden = true
            viewServiceType.viewCalender.isHidden = true
            viewServiceType.lblTitle.text = "TXT_CHOOSE_DELIVERY_VEHICLE".localized
        } else {
            viewServiceType.btnOffer.isHidden = false
            viewServiceType.viewCalender.isHidden = false
            viewServiceType.lblTitle.text = "txt_ride".localized
        }
        
        serviceTypeViewListener()
        tripDetailViewListener()
        
        currentBooking.serviceViewPromoId = ""
        currentBooking.serviceViewPromoName = ""
        self.showMarkers()
        socket.connectSocket()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        if let order = order {
            orderId = order._id
            wsGetOrderDetail(id: order._id ?? "")
        } else if let id = orderId {
//            wsGetOrderDetail(id: id)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterForeground), name: .willEnterForeground, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.removeObserver(self)
        socket.disConnectSocket()
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
    }
    
    @objc func locationUpdate(_ ntf: Notification = Common.defaultNtf) {
        LocationCenter.default.stopUpdatingLocation()
        Utility.hideLoading()
        guard let userInfo = ntf.userInfo else { return }
        guard let location = userInfo[Common.locationKey] as? CLLocation else { return }
        print("locationUpdate: \(location)")
        
        self.locationManager?.getAddressFromLatLong(latitude: (location.coordinate.latitude),
                                                    longitude: (location.coordinate.longitude), isUpdateSignleTone: false, complition: { [weak self] placeData in
            guard let self = self else { return }
            self.txtPickupAddress.text = placeData.address
            if self.deliveryType == DeliveryType.courier {
                self.wsCheckAddress(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            } else {
                self.wsGetDeliveriesInNearestCity(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, placeData: placeData)
            }
        })
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15.0)
            self.mapView?.camera = camera
        }
    }
    
    @objc func locationFail(_ ntf: Notification = Common.defaultNtf) {
        LocationCenter.default.stopUpdatingLocation()
        guard let userInfo = ntf.userInfo else { return }
        guard let error = userInfo[Common.locationErrorKey] as? Error else { return }
        print("locationFail: \(error)")
        Utility.hideLoading()
        Utility.showToast(message: "MSG_LOCATION_NOT_GETTING".localized)
    }
    
    //MARK: Custom Functions
    func setData() {
        guard let order = order else {
            return
        }
        
        self.deliveryType = order.delivery_type ?? DeliveryType.taxi
        currentBooking.bookCityId = order.cartDetail?.cityId
        
        let orderStatus =  OrderStatus.init(rawValue: order.request_detail?.delivery_status ?? 0) ?? OrderStatus.Unknown
        self.orderId = order._id ?? ""
        
        if orderStatus == OrderStatus.DELIVERY_MAN_COMPLETE_DELIVERY && !(order.order_payment_detail?.is_payment_paid ?? false) {
            self.isPaymentPending = true
        }
        
        if self.viewServiceType.isShow {
            self.viewServiceType.hide(complition: nil)
        }
        
        setAddressAndMap()
        
        viewTripDetails.btnCode.isHidden = true
        
        var code = order.request_detail?.confirmation_code_for_pick_up_delivery ?? 0
        
        switch orderStatus {
        case OrderStatus.ORDER_READY:
            print("Eber create req")
            openDriverDetailView()
            viewTripDetails.hide()
            break
        case OrderStatus.WAITING_FOR_DELIVERY_MAN:
            print("WAITING_FOR_DELIVERY_MAN")
            openDriverDetailView()
            viewTripDetails.hide()
            break
        case OrderStatus.STORE_CANCELLED_REQUEST:
            print("STORE_CANCELLED_REQUEST")
            openDriverDetailView()
            viewTripDetails.hide()
            break
        case OrderStatus.DELIVERY_MAN_ACCEPTED:
            print("DELIVERY_MAN_ACCEPTED")
            setTripDetail()
            viewTripDetails.btnCode.isHidden = !isPickupCode
            code = order.request_detail?.confirmation_code_for_pick_up_delivery ?? 0
            viewTripDetails.lblTitle.text = "trip_status_11".localized
            break
        case OrderStatus.DELIVERY_MAN_COMING:
            print("DELIVERY_MAN_COMING")
            setTripDetail()
            viewTripDetails.btnCode.isHidden = !isPickupCode
            code = order.request_detail?.confirmation_code_for_pick_up_delivery ?? 0
            viewTripDetails.lblTitle.text = "trip_status_13".localized
            break
        case OrderStatus.DELIVERY_MAN_ARRIVED:
            print("DELIVERY_MAN_ARRIVED")
            setTripDetail()
            viewTripDetails.btnCode.isHidden = !isPickupCode
            code = order.request_detail?.confirmation_code_for_pick_up_delivery ?? 0
            viewTripDetails.lblTitle.text = "trip_status_15".localized
            break
        case OrderStatus.DELIVERY_MAN_PICKED_ORDER:
            print("DELIVERY_MAN_PICKED_ORDER")
            setTripDetail()
            setSOS()
            viewTripDetails.btnCode.isHidden = !isPickupCode
            code = order.request_detail?.confirmation_code_for_pick_up_delivery ?? 0
            viewTripDetails.lblTitle.text = "trip_status_15".localized
            break
        case OrderStatus.DELIVERY_MAN_STARTED_DELIVERY:
            print("DELIVERY_MAN_STARTED_DELIVERY")
            setTripDetail()
            viewTripDetails.btnCode.isHidden = !isCompleteCode
            code = order.request_detail?.confirmation_code_for_complete_delivery ?? 0
            viewTripDetails.lblTitle.text = "trip_status_19".localized
            setSOS()
            break
        case OrderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
            print("DELIVERY_MAN_ARRIVED_AT_DESTINATION")
            setTripDetail()
            viewTripDetails.btnCode.isHidden = !isCompleteCode
            code = order.request_detail?.confirmation_code_for_complete_delivery ?? 0
            viewTripDetails.lblTitle.text = "trip_status_21".localized
            setSOS()
            break
        case OrderStatus.DELIVERY_MAN_COMPLETE_DELIVERY:
            print("DELIVERY_MAN_COMPLETE_DELIVERY")
            setTripDetail()
            viewTripDetails.btnCode.isHidden = !isCompleteCode
            code = order.request_detail?.confirmation_code_for_complete_delivery ?? 0
            viewTripDetails.lblTitle.text = "trip_status_25".localized
            viewTripDetails.stackViewButtons.isHidden = true
            setInvoice()
            break
        case OrderStatus.NO_DELIVERY_MAN_FOUND:
            //Utility.showToast(message: "trip_status_109".localized)
            //viewTripDetails.hide()
            print("NO_DELIVERY_MAN_FOUND")
            openDriverDetailView()
            viewTripDetails.hide()
            break
        case OrderStatus.DELIVERY_MAN_REJECTED:
            print("DELIVERY_MAN_REJECTED")
            Utility.showToast(message: "trip_status_111".localized)
            viewTripDetails.hide()
            removeDriverSearching()
            break
        case OrderStatus.CANCELED_BY_USER:
            print("CANCELED_BY_USER")
            Utility.showToast(message: "trip_status_101".localized)
            viewTripDetails.hide()
            removeDriverSearching()
            break
        case OrderStatus.DELIVERY_MAN_CANCELLED:
            print("DELIVERY_MAN_CANCELLED")
            openDriverDetailView()
            viewTripDetails.hide()
            break
        case OrderStatus.ADMIN_CANCELLED_REQUEST:
            print("DELIVERY_MAN_CANCELLED")
            openDriverDetailView()
            viewTripDetails.hide()
            break
        default:
            removeDriverSearching()
            viewTripDetails.stackViewButtons.isHidden = true
            print("unknow status found \(orderStatus.rawValue)")
        }
        
        self.strCode = "\(code)"
        
        if firstTime != nil {
            firstTime = nil
            let arrCordinate = order.trip_location_details?.start_trip_to_end_trip_locations ?? []
            for obj in arrCordinate {
                animationPath.addLatitude(obj[0], longitude: obj[1])
            }
            self.animationPolyline.path = self.animationPath
        }
        
        wsDrawPath()
    }
    
    func setTripDetail() {
        //"vehicle_name" = Audi;
         //"vehicle_plate_no" = shocks;
         //"vehicle_model" = hood;
         //"vehicle_color" = blue;
        guard let order = order else {
            return
        }
        
        removeDriverSearching()
        
        self.viewTripDetails.show()
        
        self.viewTripDetails.lblDriverName.text = order.request_detail?.provider_detail?.name ?? ""
        self.viewTripDetails.lblTripNo.text = "txt_trip_no".localized.replacingOccurrences(of: "****", with: "\(order.unique_id ?? 0)")
        self.viewTripDetails.lblVehicalDetail.text = (order.provider_details?.vehicle_name ?? "") + " | " + "\(order.provider_details?.vehicle_plate_no ?? "")" + " | " + "\(order.provider_details?.vehicle_model ?? "")" + " | " + "\(order.provider_details?.vehicle_color ?? "")"
        self.viewTripDetails.btnRating.setTitle("\(order.request_detail?.provider_detail?.user_rate ?? 0)", for: .normal)
        self.viewTripDetails.lblTime.text = (order.order_payment_detail?.total_time ?? 0).toString() + " " + "UNIT_MIN".localized
        
        let isMile: Bool = {
            return order.order_payment_detail?.is_distance_unit_mile ?? false
        }()
        
        self.viewTripDetails.lblDistance.text = (order.order_payment_detail?.total_distance ?? 0).toString() + " " + (isMile ?  "UNIT_MILE".localized : "UNIT_KM".localized)
        
        self.viewTripDetails.imgDriver.downloadedFrom(link: order.request_detail?.provider_detail?.image_url ?? "", placeHolder: "profile_placeholder",isFromCache: false,isIndicator: true)
        
        arrProviderLocation.removeAll()
        
        if let obj = order.provider_detail?.first {
            if obj.location[0] != 0 && obj.location[1] != 0 {
                arrProviderLocation.append(obj.location)
                let providerLocation = CLLocationCoordinate2D(latitude: obj.location[0], longitude: obj.location[1])
                providerMarker.position = providerLocation
            }
        }
        
    }
    
    func setAddressAndMap() {
        guard let order = order else {
            return
        }
        let destinationAddress =  order.destination_addresses?.last
        
        self.txtPickupAddress.text = order.cartDetail?.pickupAddresses.first?.address ?? "-"
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.txtPickupAddress.text = order.cartDetail?.pickupAddresses.first?.address ?? "-"
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.txtPickupAddress.text = order.cartDetail?.pickupAddresses.first?.address ?? "-"
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.txtPickupAddress.text = order.cartDetail?.pickupAddresses.first?.address ?? "-"
                }
            }
        }
        self.txtDestinationAddress.text = destinationAddress?.address ?? "-"
        
        let pickUp = order.cartDetail?.pickupAddresses.first?.location ?? [0,0]
        let destination = destinationAddress?.location ?? [0,0]
        
        self.pickUplat = pickUp[0]
        self.pickUplong = pickUp[1]
        isRunningTip = true
        self.destinationlat = destination[0]
        self.destinationlong = destination[1]
        self.viewAddAddress.isHidden = true
        self.tableDestinations.isUserInteractionEnabled = false
        self.constraintBottom.constant = 10
        self.listStopLocationAddress = [StopLocationAddress]()
        for i in 0 ..< (order.destination_addresses!.count - 1){
            let newStop : StopLocationAddress = StopLocationAddress(fromDictionary: [:])
            newStop.address = order.destination_addresses![i].address
            newStop.latitude = order.destination_addresses![i].location[0]
            newStop.longitude = order.destination_addresses![i].location[1]
            self.listStopLocationAddress.append(newStop)
        }
        self.constraintLocationHeight.constant = CGFloat((self.listStopLocationAddress.count * 50))
        self.tableDestinations.reloadData()
        showMarkers()
    }
    
    func setSOS() {
        self.viewTripDetails.btnStatus.backgroundColor = UIColor.red
        self.viewTripDetails.btnStatus.setTitle("txt_sos".localized, for: .normal)
    }
    
    func setInvoice() {
        self.viewTripDetails.btnStatus.backgroundColor = UIColor.themeColor
        self.viewTripDetails.btnStatus.setTitle("TXT_VIEW_INVOICE".localized, for: .normal)
    }
    
    func setToNormalView() {
        btnBack.isHidden = false
        self.removeDriverSearching()
        self.destinationlat = 0
        self.destinationlong = 0
        //self.pickUplat = 0
        //self.pickUplong = 0
        self.txtDestinationAddress.text = ""
        self.listStopLocationAddress = [StopLocationAddress]()
        self.constraintLocationHeight.constant = 0
        self.constraintBottom.constant = 30
        self.viewAddAddress.isHidden = false
        self.tableDestinations.reloadData()
        //self.txtPickupAddress.text = ""
        if self.viewServiceType.isShow {
            self.viewServiceType.hide(complition: nil)
        }
        if self.viewTripDetails.isShow {
            self.viewTripDetails.hide()
        }
        self.mapView?.clear()
        //self.setCurrentLocation()
        self.txtPickupAddress.isUserInteractionEnabled = true
        self.txtDestinationAddress.isUserInteractionEnabled = true
        dialogForCancelOrder?.removeFromSuperview()
        self.orderId = nil
        self.order = nil
    }
    
    func applePromoCode(str: String) {
        viewServiceType.imgPromoRightCheck.isHidden = false
        self.promoCode = str
    }
    
    func removeDriverSearching() {
        if let view = (APPDELEGATE.window?.viewWithTag(DialogTag.driverDetailDialog)) {
            view.removeFromSuperview()
            btnBack.isHidden = false
        }
    }
    
    func reSetPickUp() {
        self.pickUplat = 0
        self.pickUplong = 0
        self.txtPickupAddress.text = ""
        viewServiceType.hide()
    }
    
    func getCurrentLocation() {
        txtPickupAddress.text = currentBooking.currentAddress
        self.pickUplat = currentBooking.currentLatLng[0]
        self.pickUplong = currentBooking.currentLatLng[1]
        
        if self.deliveryType == DeliveryType.courier {
            self.wsCheckAddress(latitude: pickUplat, longitude: pickUplong)
        } else {
            self.wsGetDeliveriesInNearestCity(latitude: self.pickUplat, longitude: self.pickUplong, placeData: currentBooking.currentSendPlaceData)
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: self.pickUplat, longitude: self.pickUplong, zoom: 15.0)
        self.mapView?.camera = camera
    }
    
    func setDelegates() {
        txtPickupAddress.delegate = self
        txtDestinationAddress.delegate = self
        mapView?.delegate = self
    }
    
    func setUpViews() {
        viewAddresses.applyShadowToView()
    }
    
    func localized() {
        txtPickupAddress.placeholder = "TXT_PICKUP_ADDRESS".localized
        txtDestinationAddress.placeholder = "txt_destination_address".localized
        if deliveryType == DeliveryType.courier {
            lblTitle.text = "TXT_COURIER_DETAIL".localized
        } else {
            
            lblTitle.text = "txt_trip_taxi".localized
//            lblTitle.text = "txt_trip_detail".localized
        }
        if self.traitCollection.userInterfaceStyle == .dark {
            lblTitle.textColor = .white
        }else{
            lblTitle.textColor = .black
        }
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                do {
                    if let styleURL = Bundle.main.url(forResource: "styleable_map", withExtension: "json") {
                        self.mapView?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                    } else {
                        NSLog("Unable to find style.json")
                        
                    }
                } catch {
                    NSLog("One or more of the map styles failed to load. \(error)")
                }
            } else {
                self.mapView?.mapStyle = .none
            }
        } else {
            // Fallback on earlier versions
            self.mapView?.mapStyle = .none
        }
    }
    override func updateUIAccordingToTheme() {
        self.localized()
    }
    func setColorFontBG() {
        txtPickupAddress.delegate = self
        txtDestinationAddress.delegate = self
        
        txtPickupAddress.textColor = UIColor.themeTitleColor
        txtDestinationAddress.textColor = UIColor.themeTitleColor
        
        txtPickupAddress.font = FontHelper.textRegular()
        txtDestinationAddress.font = FontHelper.textRegular()
        
        viewAddresses.backgroundColor = UIColor.themeViewBackgroundColor
        
        lblTitle.font = FontHelper.textRegular(size: FontHelper.mediumLarge)
        
        lblTitle.textColor = .black
        if self.traitCollection.userInterfaceStyle == .dark {
            lblTitle.textColor = .white
            btnBack.tintColor = UIColor.white
            btnMap.tintColor = UIColor.white
//            self.setRightBarItemImage(image: UIImage.init(named: "map_route")!.imageWithColor(color: .white)!)

        }else{
            btnBack.tintColor = UIColor.black
            btnMap.tintColor = UIColor.themeColor//UIColor.black
            lblTitle.textColor = .black
            
//            self.setRightBarItemImage(image: UIImage.init(named: "map_route")!.imageWithColor(color: .themeTextColor)!)

        }
    }
    
    func setCurrentLocation() {
        let camera = GMSCameraPosition.camera(withLatitude: currentBooking.currentPlaceData.latitude, longitude: currentBooking.currentPlaceData.longitude, zoom: 15.0)
        self.mapView?.camera = camera
    }
    
    func getInvoiceData() {
        
        guard let order = order else {
            return
        }
        guard let orderPayment = OrderPayment.init(dictionary: order.order_payment_detail?.dictionaryRepresentation() ?? [:]) else { return }
        var isShowPromoApply: Bool = false
        if orderPayment.promo_payment ?? 0 > 0.0 {
            isShowPromoApply = true
        }
        
        let arrForInvoice = NSMutableArray()
        
        let currency = order.country_detail?.currency_sign ?? ""
        
        Parser.parseInvoice(orderPayment, toArray: arrForInvoice, currency: currency, isTaxIncluded: true, isShowPromo: isShowPromoApply, deliveryType: self.deliveryType, completetion: { (result) in
            if result {
                var arr = [InvoicePopUpData]()
                
                for obj in (arrForInvoice as! [Invoice]) {
                    
                    let title: String = {
                        if (obj.subTitle ?? "").count > 0 {
                            return (obj.title ?? "") + " " + "(\((obj.subTitle ?? "")))"
                        }
                        return obj.title ?? ""
                    }()
                    print("\(title) \(obj.subTitle) \(obj.price) \(obj.type)")
                    if let type = InvoiceCellType(rawValue: obj.type) {
                        arr.append(InvoicePopUpData(title: title, value: obj.price ?? "", type: type))
                    } else {
                        arr.append(InvoicePopUpData(title: title, value: obj.price ?? "", type: .Regular))
                    }
                }
                
                let strButtonTitle: String = {
                    if self.isPaymentPending {
                        return "TXT_PAYMENT".localized
                    }
                    return "TXT_SUBMIT".localized
                }()
                
                let dailog = InvoicePopUp.showInvoicePopUp(title: "TXT_INVOICE".localized, strRight: strButtonTitle, currency : currency, isHidePromo: !isShowPromoApply, isPromoEdit: false, strPromo: orderPayment.promo_code_name, arrInvoice: arr, totalPrice: order.order_payment_detail?.user_pay_payment ?? 0, isButtonDone: false)
                
                dailog.onClickLeftButton = {
                    dailog.removeFromSuperview()
                }
                
                dailog.onClickRightButton = { [weak self] obj in
                    guard let self = self else { return }
                    if self.isPaymentPending {
                        self.goToPayment()
                    } else {
                        self.wsShowInvoice()
                    }
                }
            }
        })
    }
    
    func goToPayment() {
        let payment = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "paymentVC") as! PaymentVC
        payment.height = screenHeight * 0.8
        payment.topCornerRadius = 25
        payment.presentDuration = 0.3
        payment.dismissDuration = 0.3
        payment.shouldDismissInteractivelty = false
        payment.delegate = self
        if isPaymentPending {
            payment.isCashShow = false
            payment.isPaymentPending = true
            currentBooking.selectedOrderId = orderId ?? ""
            payment.delegateTripPayment = self
        }
        if promoCode != nil {
            currentBooking.serviceViewPromoId = promoID ?? ""
            currentBooking.serviceViewPromoName = promoCode ?? ""
        }
        currentBooking.deliveryType = self.deliveryType
        currentBooking.selectedVehicleId = self.viewServiceType.getSelectedVehicle()?.vehicleId ?? ""
        currentBooking.isHidePayNow = false
        self.present(payment, animated: true, completion: nil)
    }
    
    func tripDetailViewListener() {
        viewTripDetails.onClickButtonStatus = { [weak self] obj in
            guard let self = self else { return }
            if obj.btnStatus.titleLabel?.text == "txt_sos".localized {
//                Utility.showToast(message: "SOS")
//                self.sendSOSMessage()
                Utility.showLoading()
                self.wsGetEmergencyContactList()
            } else if obj.btnStatus.titleLabel?.text == "TXT_VIEW_INVOICE".localized {
                self.getInvoiceData()
            } else {
                self.wsGetCancellationCharge()
            }
        }
        
        viewTripDetails.onClickButtonShare = { [weak self] obj in
            guard let self = self else { return }
            print("share")
            self.shareRide()
        }
        
        viewTripDetails.onClickButtonChat = { [weak self] obj in
            guard let self = self else { return }
            self.openChatDialog()
            print("share")
        }
        
        viewTripDetails.onClickButtonCode = { [weak self] obj in
            guard let self = self else { return }
            self.openConfirmationDialog()
        }
        
        viewTripDetails.onClickButtonCall = { [weak self] obj in
            guard let self = self else { return }
            self.onClickCallToProvider()
        }
    }
    func sendSOSMessage(){
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
         PARAMS.TYPE:CONSTANT.TYPE_USER]
        Utility.showLoading()
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_SEND_SMS, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            //467
            Utility.hideLoading()
            
            if let sucess = response["success"] as? Bool{
                if sucess{
                    let code = response["status_phrase"] as? String
                    Utility.showToast(message: ("\(code!)"))
                }else{
                    let code = response["error_code"] as? Int
                    Utility.showToast(message: ("HTTP_ERROR_CODE_\((code!))").localized)
                }
            }
            print(response)
        }
    }
    func wsGetEmergencyContactList(){
        Utility.showLoading()
        var  dictParam : [String : Any] = [:]
        dictParam[PARAMS.USER_ID] = preferenceHelper.UserId
        dictParam[PARAMS.DEVICE_TOKEN] = preferenceHelper.DeviceToken
        
        let afh:AlamofireHelper = AlamofireHelper.init()
        afh.getResponseFromURL(url: WebService.WS_get_emergency_contact_list, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, error) -> (Void) in
            Utility.hideLoading()
            if let sucess = response["success"] as? Bool{
                if sucess{
                }else{
                    let code = response["error_code"] as? Int
                    
                    if (code ?? 0) == 467{
                        self.arrForEmegerncyContacts.removeAll()
                        Utility.showToast(message: ("HTTP_ERROR_CODE_\((code!))").localized)
                        return
                    }
                }
            }

            if (error != nil){
                Utility.hideLoading()
            }
            else{
                if Parser.isSuccess(response: response,withSuccessToast: false,andErrorToast: false){
                    self.arrForEmegerncyContacts.removeAll()
                    let responseContact:EmergencyContactResponse = EmergencyContactResponse.init(fromDictionary:( response as! [String:Any]))
                    
                    for data in responseContact.emergencyContactData{
                        self.arrForEmegerncyContacts.append(data)
                    }
                    if self.arrForEmegerncyContacts.count > 0{
//                        self.sendSOSMessage()
                        self.count()
                    }
                }
            }
        }
    }
        func count(){
            let dialogForSurgePrice = CountDownDialog.showCustomSurgePriceDialog(title: "SOS")
            dialogForSurgePrice.onClickLeftButton = { [/*unowned self, */unowned dialogForSurgePrice] in
                dialogForSurgePrice.removeFromSuperview()
            }
            dialogForSurgePrice.onClickRightButton = { [unowned self, unowned dialogForSurgePrice] in
                dialogForSurgePrice.removeFromSuperview()
                print("Send")
                self.sendSOSMessage()
    //Send
            }
            dialogForSurgePrice.onClickLeftButton = { [unowned self, unowned dialogForSurgePrice] in
                dialogForSurgePrice.removeFromSuperview()
                //Csncel
                print("Cancel")
            }
        }
    
    //MARK: On Click
    func serviceTypeViewListener() {
        viewServiceType.onClickCreateTrip = { [weak self] obj in
            guard let self = self else { return }
            let selectedVehicle = obj.getSelectedVehicle()
            print(obj.isPromoApplied)
            print(obj.paramPromoInvoice)
            CurrentBooking.shared.service_Id = obj.getSelectedVehicle()?.service_id ?? ""
            currentBooking.deliveryType = self.deliveryType
            CurrentBooking.shared.isPromoApplied = obj.isPromoApplied
            CurrentBooking.shared.paramPromoInvoice = obj.paramPromoInvoice
//
            print(obj.fareEstimateValues.surge_multiplier)
            if obj.fareEstimateValues.surge_multiplier > 1{
                Utility.hideLoading()
                self.surgeCharge(charge: obj.fareEstimateValues.surge_multiplier)
            }else{
                if self.isAskUserForFixedFare{
                    self.openFixedPriceDialog()
                }else{
                    self.wsAddItemInServerCart()
                }
            }
        }
        
        viewServiceType.onClickPromoCode = { [weak self] obj in
            guard let self = self else {
                return
            }
//            self.showPromoCode()
        }
    }
    
    func startLocationListner() {
        
        guard let order = order else {
            return
        }
        
        let orderStatus =  OrderStatus.init(rawValue: order.request_detail?.delivery_status ?? 0) ?? OrderStatus.Unknown
        
        let arrStatusCheck = [OrderStatus.WAITING_FOR_ACCEPT_STORE, OrderStatus.STORE_ACCEPTED, OrderStatus.STORE_PREPARING_ORDER, OrderStatus.ORDER_READY, OrderStatus.WAITING_FOR_DELIVERY_MAN,OrderStatus.DELIVERY_MAN_COMPLETE_DELIVERY,OrderStatus.CANCELED_BY_USER,OrderStatus.NO_DELIVERY_MAN_FOUND,OrderStatus.ORDER_READY,OrderStatus.DELIVERY_MAN_CANCELLED,OrderStatus.DELIVERY_MAN_REJECTED]
        
        if arrStatusCheck.contains(orderStatus) {
            self.stopLocationListner()
            return
        }
        
        self.stopLocationListner()
        let myTripid = "'\(order.request_detail?.provider_detail?._id ?? "")'"
        
        self.socket.socket?.on(myTripid) {
            [weak self] (data, ack) in
            guard let self = self else {
                return
            }
            guard let response = data.first as? [String:Any] else {
                return
            }
            printE("Soket Response\(response)")
            if let value = response["total_time"] as? Double {
                self.viewTripDetails.lblTime.text = value.toString() + " " + "UNIT_MIN".localized
            } else if let value = response["total_time"] as? Int {
                self.viewTripDetails.lblTime.text = value.toString() + " " + "UNIT_MIN".localized
            }
            
            let isMile: Bool = {
                return order.order_payment_detail?.is_distance_unit_mile ?? false
            }()
            
            if let value = response["total_distance"] as? Double {
                self.viewTripDetails.lblDistance.text = value.toString() + " " + (isMile ? "UNIT_MILE".localized : "UNIT_KM".localized)
            } else if let value = response["total_distance"] as? Int {
                self.viewTripDetails.lblDistance.text = value.toString() + " " + (isMile ? "UNIT_MILE".localized : "UNIT_KM".localized)
            }
            
            let location = (response["provider_location"] as? [Double]) ?? [0,0]
            self.arrProviderLocation.append(location)
            
            if location[0] != 0.0 && location[1] != 0.0 {
                let position = CLLocationCoordinate2D(latitude: location[0], longitude: location[1])
                if self.arrProviderLocation.count > 0 {
                    self.animate(from: self.providerMarker.position, to: position)
                } else {
                    self.providerMarker.position = position
                }
            }
        }
    }
    
    func startTripStatusUpdate(id: String) {
        
        guard let order = order else {
            return
        }
        
        let orderStatus =  OrderStatus.init(rawValue: order.request_detail?.delivery_status ?? 0) ?? OrderStatus.Unknown
        
        let arrStatusCheck = [OrderStatus.CANCELED_BY_USER]
        
        if arrStatusCheck.contains(orderStatus) {
            self.stopStatusUpdateListner()
            return
        }
        
        self.stopStatusUpdateListner()
        let idListner = "'\(order._id ?? "")'"
        
        self.socket.socket?.on(idListner) {
            [weak self] (data, ack) in
            guard let self = self else {
                return
            }
            guard let response = data.first as? [String:Any] else {
                return
            }
            printE("Soket Response\(response)")
            self.wsGetOrderDetail(id: order._id ?? "")
        }
    }
    
    func stopLocationListner() {
        
        guard let order = order else {
            return
        }
        let myTripid = "'\(order.request_detail?.provider_detail?._id ?? "")'"
        self.socket.socket?.off(myTripid)
    }
    
    func stopStatusUpdateListner() {
        guard let order = order else {
            return
        }
        let id = "'\(order._id ?? "")'"
        self.socket.socket?.off(id)
    }
    
    func updateProviderMarker(providerLocation: [Double], bearing: CGFloat) {
        let providerPrevioustLocation = providerMarker.position
        if arrProviderLocation.count > 1 {
            let providerCoordinate = CLLocationCoordinate2D.init(latitude: providerLocation[0], longitude: providerLocation[1])
            providerMarker.map = mapView
            providerMarker.position = providerCoordinate
            //let mapBearing = self.calculateBearing(source: providerPrevioustLocation, to: providerCoordinate)
        }
    }
    
    func animate(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) {
        // Keep Rotation Short
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.5)
        CATransaction.setCompletionBlock({
            // you can do something here
        })
        providerMarker.rotation = source.bearing(to: destination)
        providerMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        CATransaction.commit()
        
        let array = getLocationsBetweenTwoPoints(noOfLocation: 10, startPoint: source, endPoint: destination)
        
        self.path.removeAllCoordinates()
        self.i = 0
        for obj in array {
            path.addLatitude(obj.latitude, longitude: obj.longitude)
        }
        
        if timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: 0.15, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
        }
        
        let bottomPadding: CGFloat = {
            if viewServiceType.isShow {
                return viewServiceType.frame.size.height
            } else if viewTripDetails.isShow {
                return viewTripDetails.frame.size.height
            } else {
                return 0
            }
        }()
        
        mapView?.padding = UIEdgeInsets(top: 0, left: 0, bottom: bottomPadding * 0.5, right: 0)
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.5)
        providerMarker.position = destination
        
        let camera = GMSCameraUpdate.setTarget(destination, zoom: 17)
        mapView?.animate(with: camera)
        CATransaction.commit()
    }
    
    @objc func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.i += 1
        }
        else {
            if timer != nil {
                timer.invalidate()
                timer = nil
            }
        }
    }
    
    func getLocationsBetweenTwoPoints(noOfLocation: Double, startPoint: CLLocationCoordinate2D, endPoint: CLLocationCoordinate2D) -> [CLLocationCoordinate2D] {
        
        let yourTotalCoordinates = Double(10)
        let latitudeDiff = startPoint.latitude - endPoint.latitude
        let longitudeDiff = startPoint.longitude - endPoint.longitude
        let latMultiplier = latitudeDiff / (yourTotalCoordinates + 1)
        let longMultiplier = longitudeDiff / (yourTotalCoordinates + 1)
        
        var array = [CLLocationCoordinate2D]()
        
        array.insert(startPoint, at: 0)
        
        for index in 1...Int(yourTotalCoordinates) {
            let lat  = startPoint.latitude - (latMultiplier * Double(index))
            let long = startPoint.longitude - (longMultiplier * Double(index))
            let point = CLLocationCoordinate2D(latitude: lat, longitude: long)
            array.append(point)
            self.arrProviderLocation.append([point.latitude,point.longitude])
        }
        array.append(endPoint)
        return array
    }
    
    func calculateBearing(source:CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) -> Double {
        let lat1 = Double.pi * source.latitude / 180.0
        let long1 = Double.pi * source.longitude / 180.0
        let lat2 = Double.pi * destination.latitude / 180.0
        let long2 = Double.pi * destination.longitude / 180.0
        let rads = atan2(
            sin(long2 - long1) * cos(lat2),
            cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(long2 - long1))
        let degrees = rads * 180 / Double.pi
        return (degrees+360).truncatingRemainder(dividingBy: 360)
    }
    @IBAction func actionAddStop(_ sender: UIButton) {
        self.locationType = .stop
        isEdit = false
        goToAddress(location: [currentBooking.currentLatLng[0],currentBooking.currentLatLng[1]])
    }
    @IBAction func onClickBack(_ sender: UIButton) {
        if !isPaymentPending {
            self.delegateBackFromHistoryDetails?.backAction()
            self.delegateBackReloadStore?.BackfromTaxiCourier()
            self.navigationController?.popViewController(animated: true)
            socket.disConnectSocket()
            if timer != nil {
                timer.invalidate()
                timer = nil
            }
            self.mapView?.clear()
            self.mapView = nil
            self.stopLocationListner()
        }
    }
    
    @IBAction func onClickShowMap(_ sender: UIButton) {
        self.showPathOnMap()
    }
    
    func showPathOnMap(){
        self.view.endEditing(true)
        if self.getAddressForFullMaps().count > 1{
            if let googlePathResponse = googlePathResponse {
                let vc = UIStoryboard(name: "Courier", bundle: nil).instantiateViewController(withIdentifier: "CourierStopPathVC") as! CourierStopPathVC
                vc.googlePathResponse = googlePathResponse
                vc.arrAddress = self.getDestinationAddress(isMapPath: true)
                vc.isTaxi = true
                vc.pickUplat = self.pickUplat
                vc.pickUplong = self.pickUplong
                vc.destinationlat = self.destinationlat
                vc.destinationlong = self.destinationlong
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            }
        } else {
            Utility.showToast(message: "txt_add_at_least_two_address".localized)
        }
    }
    
    @objc func willEnterForeground() {
        if let orderId = orderId {
            wsGetOrderDetail(id: orderId)
        }
    }
    
    func onClickCallToProvider() {
        guard let order = order else {
            return
        }
        
        if preferenceHelper.IsTwillowMaskEnable {
            TwilioCallMasking.shared.wsTwilloCallMasking(id: order._id ?? "", type: "\(CONSTANT.TYPE_PROVIDER)")
        } else {
            order.request_detail?.provider_detail?.phone?.toCall()
        }
    }
    
    func shareRide() {
        
        guard let order = order else {
            return
        }
        
        if order.destination_addresses?.count ?? 0 == 0 {
            Utility.showToast(message: "VALIDATION_MSG_PLEASE_ENTER_DESTINATION_FIRST".localized)
        } else {
            
            var arrDestination = order.destination_addresses ?? []
            
            let providerLocation: CLLocationCoordinate2D = {
                var lat: Double = 0
                var long: Double = 0
                if order.request_detail?.provider_location?.count ?? 0 > 0 {
                    lat = order.request_detail?.provider_location?[0] ?? 0
                    long = order.request_detail?.provider_location?[1] ?? 0
                }
                return CLLocationCoordinate2D.init(latitude: lat, longitude: long)
            }()
            
            let destinationLocation: CLLocationCoordinate2D = {
                var lat: Double = 0
                var long: Double = 0
                if arrDestination.count > 0 {
                    if let obj = arrDestination.first {
                        if obj.location.count > 0 {
                            lat = obj.location[0]
                            long = obj.location[1]
                        }
                    }
                }
                return CLLocationCoordinate2D.init(latitude: lat, longitude: long)
            }()
            
            let request = URL(string: "\(Google.DISTANCEMATRIX)origins=\(providerLocation.latitude),\(providerLocation.longitude)&destinations=\(destinationLocation.latitude),\(destinationLocation.longitude)&key=\(preferenceHelper.CustomerAppGoogleDistanceMatrixKey)")
            //print(request)
            let parseData = Utility.parseJSON(inputData: Utility.getJSON(urlToRequest: request!))
            
            let googleRsponse: GoogleDistanceMatrixResponse = GoogleDistanceMatrixResponse(dictionary:parseData)!
            if ((googleRsponse.status?.compare("OK")) == ComparisonResult.orderedSame) {
                Utility.hideLoading()
                let eta = (googleRsponse.rows?[0].elements?[0].duration?.value) ?? 0
                let distance = Double((googleRsponse.rows?[0].elements?[0].distance?.value) ?? 0)
                let etaMin = "\(self.estimateTime/60) \("UNIT_MIN".localized)"
                let distanceUnit = "\(self.estimateDistance/1000) \("UNIT_KM".localized)"
                
                var myString = "MSG_SHARE_ETA".localized.replacingOccurrences(of: "****", with: (arrDestination.first?.address ?? ""))
                myString = myString.replacingOccurrences(of: "^^^^", with: order.request_detail?.provider_detail?.name ?? "")
                myString = myString.replacingOccurrences(of: "~~~~", with: "\(eta)")
                //String(format: NSLocalizedString("MSG_SHARE_ETA", comment: ""), arrDestination.first?.address ?? "",order.request_detail?.provider_detail?.name ?? "",eta)
                
                let textToShare = [ myString ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                self.navigationController?.present(activityViewController, animated: true, completion: nil)
                
            }else{
                print("DISTANCEMATRIX:- \(googleRsponse)")
            }
        }
    }
    
    //MARK: API CALL
    func wsGetOrderDetail(id: String, isShowLoader: Bool = true) {
        if isShowLoader {
            Utility.showLoading()
        }
        txtPickupAddress.isUserInteractionEnabled = false
        txtDestinationAddress.isUserInteractionEnabled = false
        
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
         PARAMS.ORDER_ID:id]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_ORDER_DETAIL, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                if let value = response["is_confirmation_code_required_at_pickup_delivery"] as? Bool {
                    self.isPickupCode = value
                }
                if let value = response["is_confirmation_code_required_at_complete_delivery"] as? Bool {
                    self.isCompleteCode = value
                }
                let orderDictionary:NSDictionary = response["order"] as! NSDictionary
                if let order = Order.init(dictionary: orderDictionary) {
                    self.order = order
                    self.setData()
                    self.startTripStatusUpdate(id: order._id ?? "")
                    self.startLocationListner()
                    self.getCurrentLocation()
                    self.setCurrentLocation()
                    
                }
            }
        }
    }
    
    func wsGetInvoice() {
        Utility.showLoading()
        let selectedVehicalId = self.viewServiceType.getSelectedVehicle()?.vehicleId ?? ""
        
        var dictParam:[String:Any] = APPDELEGATE.getCommonDictionary()
        dictParam[PARAMS.TOTAL_TIME] = estimateTime
        dictParam[PARAMS.TOTAL_DISTANCE] = estimateDistance
        dictParam[PARAMS.COUNTRY_ID] = currentBooking.bookCountryId ?? ""
        dictParam[PARAMS.CITY_ID] = currentBooking.bookCityId ?? ""
        dictParam[PARAMS.VEHICLE_ID] = selectedVehicalId
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_COURIER_INVOICE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [weak self] (response,error) -> (Void) in
            guard let self = self else {
                return
            }
            if (Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true)) {
                self.goToPayment()
            }
            ////print(response)
        }
    }
    
    func wsShowInvoice() {
        Utility.showLoading()
        
        let dictParam:[String:Any] =
        [PARAMS.SERVER_TOKEN : preferenceHelper.SessionToken,
         PARAMS.USER_ID : preferenceHelper.UserId,
         PARAMS.ORDER_ID : orderId ?? "",
         PARAMS.TYPE: CONSTANT.TYPE_USER,
         PARAMS.IS_USER_SHOW_INVOICE: true]
        
        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_SHOW_INVOICE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam, block: { [weak self] (response, error) -> (Void) in
            Utility.hideLoading()
            guard let self = self else { return }
            if Parser.isSuccess(response: response) {
                self.openFeedbackDialogue()
            }
        })
    }
    
    func wsRateToProvider(obj: DailogForFeedback) {
        var review:String = obj.txtComment.text ?? ""
        if review.compare("TXT_COMMENT_PLACEHOLDER".localized) == .orderedSame {
            review = ""
        }
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
         PARAMS.ORDER_ID:orderId ?? "",
         PARAMS.USER_REVIEW_TO_PROVIDER:review,
         PARAMS.USER_RATING_TO_PROVIDER:obj.rate
        ]
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_USER_RATE_TO_PROVIDER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if (Parser.isSuccess(response: response,withSuccessToast:true, andErrorToast: true)) {
                APPDELEGATE.goToMain()
            }
        }
    }
    
  
    
    func openChatDialog() {
        
        guard let order = order else {
            return
        }
        
        CurrentBooking.shared.selectedOrderId = order._id ?? ""
        
        MessageHandler.ReceiverID = order.request_detail?.provider_detail?._id ?? ""
        let mainView = UIStoryboard(name: "Order", bundle: nil)
        if let vc : MyCustomChatVC = mainView.instantiateViewController(withIdentifier: "chatVC") as? MyCustomChatVC {
            MessageHandler.chatType = CONSTANT.CHATTYPES.USER_AND_PROVIDER
            vc.navTitle = order.request_detail?.provider_detail?.name ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func openFeedbackDialogue()  {
        
        guard let order = order else {
            return
        }
        let providerName = self.order?.request_detail?.provider_detail?.name ?? ""
        
        let dialogForFeedback = DailogForFeedback.showCustomFeedbackDialog(true, false, order._id ?? "", name: providerName, needCallBack: true)
        
        dialogForFeedback.onClickApplyButton = { [weak self]
            (obj) in
            guard let self = self else { return }
            dialogForFeedback.removeFromSuperview()
            self.wsRateToProvider(obj: obj)
        }
    }
    
    func openConfirmationDialog() {
        
        let dialogForConfirmCode = CustomAlertDialog.showCustomAlertDialog(title: "TXT_CONFIRMATION_CODE".localized, message: String("\(strCode)") , titleLeftButton: "".localizedCapitalized, titleRightButton: "TXT_SHARE".localizedCapitalized)
        
        dialogForConfirmCode.onClickLeftButton = {
            dialogForConfirmCode.removeFromSuperview()
        }
        
        dialogForConfirmCode.onClickRightButton = { [weak self] in
            guard let self = self else { return }
            let myString = String(format: NSLocalizedString("SHARE_CONFIRM_CODE", comment: ""),String("\(self.strCode)")) + " \(self.strCode)"
            let textToShare = [ myString ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
            dialogForConfirmCode.removeFromSuperview()
        }
    }
    
    func getTimeAndDistance(srcLat:Double, srcLong:Double, destLat:Double, destLong:Double) {
        
        Utility.showLoading()
        let src_lat: String = String(srcLat)
        let src_long: String = String(srcLong)
        let dest_lat: String = String(destLat)
        let dest_long: String = String(destLong)
        let request = URL(string: "\(Google.DISTANCEMATRIX)origins=\(src_lat),\(src_long)&destinations=\(dest_lat),\(dest_long)&key=\(preferenceHelper.CustomerAppGoogleDistanceMatrixKey)")
        let parseData = Utility.parseJSON(inputData: Utility.getJSON(urlToRequest: request!))
        let googleRsponse: GoogleDistanceMatrixResponse = GoogleDistanceMatrixResponse(dictionary:parseData)!
        if ((googleRsponse.status?.compare("OK")) == ComparisonResult.orderedSame) {
            Utility.hideLoading()
            self.estimateTime = (googleRsponse.rows?[0].elements?[0].duration?.value) ?? 0
            self.estimateDistance = Double((googleRsponse.rows?[0].elements?[0].distance?.value) ?? 0)
//            viewServiceType.lblTime.text = ("\(self.estimateTime/60) \("UNIT_MIN".localized)")
//            viewServiceType.lblDistance.text = ("\(self.estimateDistance/1000) \("UNIT_KM".localized)")
            currentBooking.estimateTime = self.estimateTime
            currentBooking.estimateDistance = self.estimateDistance
            wsGetVehiclelist()
        } else {
            print("DISTANCEMATRIX:- \(googleRsponse)")
            Utility.showToast(message: "MSG_PLEASE_SELECT_VALID_ADDRESS")
            Utility.hideLoading()
        }
    }
    func getUserDetaisls() -> CartUserDetail {
        let cartUserDetail:CartUserDetail = CartUserDetail()
        cartUserDetail.email = preferenceHelper.Email
        cartUserDetail.countryPhoneCode = preferenceHelper.CountryCode
        cartUserDetail.name = preferenceHelper.FirstName + " " + preferenceHelper.LastName
        cartUserDetail.phone = preferenceHelper.PhoneNumber
        return cartUserDetail
    }
    func getDestinationAddress(isMapPath : Bool = false) -> [Address]{
        var destinationAddresss:[Address] = []
        for addres in self.listStopLocationAddress{
            var destinationAddress:Address = Address.init()
            destinationAddress.addressType = AddressType.DESTINATION
            destinationAddress.location = [addres.latitude, addres.longitude]
            destinationAddress.address = addres.address
            destinationAddress.userType = CONSTANT.TYPE_USER
            destinationAddress.note = ""
            destinationAddress.city = currentBooking.currentSendPlaceData.city1
            destinationAddress.deliveryStatus = 0
            destinationAddress.flat_no = ""
            destinationAddress.street = ""
            destinationAddress.landmark = ""
            destinationAddress.userDetails = self.getUserDetaisls()
            destinationAddresss.append(destinationAddress)
        }
        if !isMapPath{
            if txtDestinationAddress.text != "" && destinationlat != 0 && destinationlong != 0 {
                let destinationAddress:Address = Address.init()
                destinationAddress.addressType = AddressType.DESTINATION
                destinationAddress.location = [destinationlat, destinationlong]
                destinationAddress.address = txtDestinationAddress.text!
                destinationAddress.userType = CONSTANT.TYPE_USER
                destinationAddress.note = ""
                destinationAddress.city = currentBooking.currentSendPlaceData.city1
                destinationAddress.deliveryStatus = 0
                destinationAddress.flat_no = ""
                destinationAddress.street = ""
                destinationAddress.landmark = ""
                destinationAddress.userDetails = self.getUserDetaisls()
                destinationAddresss.append(destinationAddress)
            }
        }
        self.destinationAddress = destinationAddresss
        return destinationAddresss
    }
    func getPickupAddress() -> Address{
        let pickupAddress:Address = Address.init()
        pickupAddress.addressType = AddressType.PICKUP
        pickupAddress.location = [pickUplat, pickUplong]
        pickupAddress.address = txtPickupAddress.text!
        pickupAddress.userType = CONSTANT.TYPE_USER
        pickupAddress.note = ""
        pickupAddress.city = currentBooking.currentSendPlaceData.city1
        pickupAddress.deliveryStatus = 0
        pickupAddress.flat_no = ""
        pickupAddress.street = ""
        pickupAddress.landmark = ""
        pickupAddress.userDetails = self.getUserDetaisls()
        self.pickupAddress = pickupAddress
        return pickupAddress
    }
    func getAddressForFullMaps() -> [Address]{
        var destinationAddresss:[Address] = []
        destinationAddresss.append(self.getPickupAddress())
        destinationAddresss.append(contentsOf: self.getDestinationAddress())
        return destinationAddresss
    }
    func wsGetVehiclelist() {
        viewServiceType.selectedIndex = 0
        let addresss = self.getDestinationAddress()
        let address = addresss[addresss.count - 1]
        let dictData:[String:Any] =
        [PARAMS.DELIVERY_TYPE:deliveryType,
         PARAMS.TOTAL_DISTANCE : self.estimateDistance,
         PARAMS.TOTAL_TIME : self.estimateTime,
         PARAMS.PICKUP_ADDRESSES : self.getPickupAddress().toDictionary(),
         PARAMS.DESTINATION_ADDRESS : address.toDictionary(), //myDestinationArray,
         PARAMS.CITY_ID:currentBooking.bookCityId ?? ""]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_VEHICLES_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictData) { [weak self] (response,error) -> (Void) in
            guard let self = self else {
                return
            }
//            print(response)
            Utility.hideLoading()
            if (Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true)) {
                let vehicleListResponse:VehicleListResponse = VehicleListResponse.init(fromDictionary: response as! [String : Any])
                
                self.isAskUserForFixedFare = vehicleListResponse.is_ask_user_for_fixed_fare
                self.viewServiceType.listPickupAddres = self.getPickupAddress()
                self.viewServiceType.listDestinationAddres = self.getDestinationAddress()
                
                if vehicleListResponse.is_distance_unit_mile {
                    self.viewServiceType.lblDistance.text = ("\((self.estimateDistance*MileMultiplerForMeter).toStringDouble()) \("UNIT_MILE".localized)")
                }else {
                    self.viewServiceType.lblDistance.text = ("\((self.estimateDistance/1000).toStringDouble()) \("UNIT_KM".localized)")
                }
                
                if vehicleListResponse.adminVehicles.count > 0 {
                    self.viewServiceType.arrServiceType = vehicleListResponse.adminVehicles
                    self.viewServiceType.show(pickupOreder: self.txtPickupAddress.text ?? "", destinationAddress: self.txtDestinationAddress.text ?? "")
                } else {
                    Utility.showToast(message: "ERROR_CODE_851".localized)
                }
            }
        }
    }
    
    func wsAddItemInServerCart() {
        Utility.showLoading()
        let cartOrder:CartOrder = CartOrder.init()
        
        cartOrder.server_token = preferenceHelper.SessionToken
        cartOrder.user_id = preferenceHelper.UserId
        cartOrder.store_id = ""
        cartOrder.order_details = []
        cartOrder.orderPaymentId = ""
        cartOrder.totalCartPrice = 0.0
        cartOrder.totalItemTax = 0.0
        
        cartOrder.pickupAddress = [self.getPickupAddress()]
        cartOrder.destinationAddress = self.getDestinationAddress()
        
        currentBooking.courierDestinationAddress = self.destinationAddress
        
        let dictData:NSDictionary = (cartOrder.dictionaryRepresentation())
        dictData.setValue(currentBooking.bookCountryId ?? "", forKey: PARAMS.COUNTRY_ID)
        dictData.setValue(currentBooking.bookCityId ?? "", forKey: PARAMS.CITY_ID)
        dictData.setValue(DeliveryType.taxi, forKey: PARAMS.DELIVERY_TYPE)
        dictData.setValue(self.isFixedFare, forKey: PARAMS.IS_FIXED_FARE)
        dictData.setValue(self.listStopLocationAddress.count, forKey: PARAMS.no_of_stop)
        
        CurrentTrip.shared.addItemIntoCartParam = dictData as! [String : Any]
        self.prepareInvoicePARAMS()
    }
    
    //    func surgeCharge(charge : Double){
    //        let dialogForSurgePrice = CustomSurgePriceDialog.showCustomSurgePriceDialog(title: "TXT_SURGE_PRICE".localized, message: "TXT_SURGE_PRICE_MESSAGE".localized, surgePrice: "\(charge.toStringDouble())" + "x", titleLeftButton: "TXT_CANCEL".localized, titleRightButton: "TXT_CONFIRM".localized)
    //        dialogForSurgePrice.onClickLeftButton = { [/*unowned self, */unowned dialogForSurgePrice] in
    //            dialogForSurgePrice.removeFromSuperview()
    //        }
    //        dialogForSurgePrice.onClickRightButton = { [unowned self, unowned dialogForSurgePrice] in
    //            dialogForSurgePrice.removeFromSuperview()
    //
    //                if self.isAskUserForFixedFare{
    //                self.openFixedPriceDialog()
    //            }else{
    //                self.wsAddItemInServerCart()
    //            }
    //        }
    //    }
    func openFixedPriceDialog(futureTrip:Bool = false){
        dialogForNetwork = CustomAlertDialog.showCustomAlertDialog(title: "TXT_FIXED_RATE_AVAILABLE".localized, message: "MSG_FIXED_RATE_DIALOG".localized, titleLeftButton: "TXT_NO".localizedCapitalized, titleRightButton: "TXT_YES".localizedCapitalized)
        self.dialogForNetwork!.onClickLeftButton = { [unowned self] in
            self.dialogForNetwork!.removeFromSuperview()
            //           true
            self.isFixedFare = true
            self.wsAddItemInServerCart()
        }
        self.dialogForNetwork!.onClickCancel = { [unowned self] in
            self.dialogForNetwork?.removeFromSuperview()
        }
        
        self.dialogForNetwork!.onClickRightButton = { [unowned self] in
            self.dialogForNetwork?.removeFromSuperview()
            self.isFixedFare =   false
            self.wsAddItemInServerCart()
        }
    }
    func surgeCharge(charge : Double){
        dialogForNetwork = CustomAlertDialog.showCustomAlertDialog(title: "TXT_SURGE_PRICE".localized, message: "TXT_SURGE_PRICE_MESSAGE".localized, titleLeftButton: "TXT_CANCEL".localizedCapitalized, titleRightButton: "TXT_CONFIRM".localizedCapitalized,isSurgeCharge: true ,surgeChargeValue: "\(charge.toStringDouble())" + "x")
        self.dialogForNetwork!.onClickLeftButton = { [unowned self] in
            self.dialogForNetwork!.removeFromSuperview()
        }
        self.dialogForNetwork!.onClickRightButton = { [unowned self] in
            self.dialogForNetwork?.removeFromSuperview()
            if self.isAskUserForFixedFare{
                self.openFixedPriceDialog()
            }else{
                self.wsAddItemInServerCart()
            }
        }
    }
}

extension RideHomeVC: UITextFieldDelegate, LocationSelectDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var locations = [currentBooking.currentLatLng[0],currentBooking.currentLatLng[1]]
        if textField == txtPickupAddress {
            self.locationType = .pickUp
            if (pickUplat != 0 ||  pickUplat != 0.0) && (pickUplong != 0 || pickUplong != 0.0){
                locations = [pickUplat,pickUplong]
            }
            goToAddress(location: locations)
            return false
        } else if textField == txtDestinationAddress {
            self.locationType = .destination
            if (destinationlat != 0 ||  destinationlat != 0.0) && (destinationlong != 0 || destinationlong != 0.0){
                locations = [destinationlat,destinationlong]
            }
            goToAddress(location: locations)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtPickupAddress {
            if textField.text?.count == 0 {
                
            }
        } else if textField == txtDestinationAddress {
            if textField.text?.count == 0 {
                
            }
        }
    }
    
    func goToAddress(location : [Double]) {
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "MainStoryboard", bundle: nil)
        let locationVC : CartLocationVC = mainView.instantiateViewController(withIdentifier: "cartLocationVC") as! CartLocationVC
        locationVC.delegateDidSelectLocation = self
        locationVC.deliveryType = DeliveryType.taxi
        locationVC.isUpdateSigletone = false
        locationVC.location = location
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    func finalAddressAndLocation(address:String,latitude:Double,longitude:Double, placeData: CurrentPlaceData) {
        if self.locationType == .stop{
            let newStop : StopLocationAddress = StopLocationAddress(fromDictionary: [:])
            newStop.address = address
            newStop.latitude = latitude
            newStop.longitude = longitude
            if isEdit{
                self.listStopLocationAddress[stopCount].address = address
                self.listStopLocationAddress[stopCount].latitude = latitude
                self.listStopLocationAddress[stopCount].longitude = longitude
            }else{
                self.listStopLocationAddress.append(newStop)
            }
            self.tableDestinations.reloadData()
            self.constraintLocationHeight.constant = CGFloat((self.listStopLocationAddress.count * 50))
            wsDrawPath()
        }else
        if self.locationType == .pickUp {
            pickUplat = latitude
            pickUplong = longitude
            
           
            if destinationlat == pickUplat && destinationlong == pickUplong{
                Utility.showToast(message: "TXT_SAME_LOCATION".localized)
                txtPickupAddress.text = ""
                if self.viewServiceType.isShow {
                    self.viewServiceType.hide(complition: nil)
                }
            }else{
                if deliveryType == DeliveryType.courier {
                    self.wsCheckAddress(latitude: latitude, longitude: longitude)
                } else {
                    self.wsGetDeliveriesInNearestCity(latitude: latitude, longitude: longitude, placeData: placeData)
                }
                txtPickupAddress.text = address
            }
        } else {
            destinationlat = latitude
            destinationlong = longitude
            
            if destinationlat == pickUplat && destinationlong == pickUplong{
                Utility.showToast(message: "TXT_SAME_LOCATION".localized)
                txtDestinationAddress.text = ""
                if self.viewServiceType.isShow {
                    self.viewServiceType.hide(complition: nil)
                }
            }else{
                txtDestinationAddress.text = address
                wsDrawPath()
                if deliveryType == DeliveryType.courier {
                    if deliveryType == DeliveryType.courier {
                        self.wsCheckAddress(latitude: latitude, longitude: longitude)
                    } else {
                        self.wsGetDeliveriesInNearestCity(latitude: latitude, longitude: longitude, placeData: placeData)
                    }
                } else {
                    //                self.showNearByService()
                }
            }
        }
        if self.listStopLocationAddress.count >= (preferenceHelper.MaxTaxiStop) {
            viewAddAddress.isHidden = true
        } else {
            viewAddresses.isHidden = false
        }
        
    }
    
    func showNearByService() {
        if isValidAddress() {
            showMarkers()
//            getTimeAndDistance(srcLat: pickUplat, srcLong: pickUplong, destLat: destinationlat, destLong: destinationlong)
        }
    }
  
    
    func isValidAddress() -> Bool {
        if txtPickupAddress.text != "" && txtDestinationAddress.text != "" && pickUplat != 0 && destinationlat != 0 &&  pickUplong != 0 && destinationlong != 0 {
            return true
        }
        return false
    }
}

extension RideHomeVC {
    func wsCheckAddress(latitude:Double,longitude:Double) {
        
        let dictParam:[String:Any] =
        [PARAMS.LATITUDE:latitude,
         PARAMS.LONGITUDE:longitude,
         PARAMS.CITY_ID:currentBooking.bookCityId ?? ""]
        
        Utility.showLoading()

        let alamoFire:AlamofireHelper = AlamofireHelper()
        alamoFire.getResponseFromURL(url: WebService.WS_CHECK_DELIVERY_AVAILABLE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true) {
                if self.locationType == .pickUp {
                    self.pickupAddress.address = self.txtPickupAddress.text
                    self.pickupAddress.location = [latitude,longitude]
                    self.pickUplat = latitude
                    self.pickUplong = longitude
                } else {
                    if self.destinationAddress.count > 0{
                        self.destinationAddress[0].address = self.txtDestinationAddress.text
                        self.destinationAddress[0].location = [latitude,longitude]
                    }
                  
                    self.destinationlat = latitude
                    self.destinationlong = longitude
                }
                self.wsDrawPath()
//                self.showNearByService()
            } else {
                if self.locationType == .pickUp {
                    self.pickupAddress.address = ""
                    self.pickupAddress.location = [0.0,0.0]
                    self.txtPickupAddress.text = ""
                    self.pickUplat = 0
                    self.pickUplong = 0
                } else {
                    if self.destinationAddress.count > 0{
                        self.destinationAddress[0].address = ""
                        self.destinationAddress[0].location = [0.0,0.0]
                    }
                    self.txtDestinationAddress.text = ""
                    self.destinationlat = 0
                    self.destinationlong = 0
                }
            }
        }
    }
    
    func wsGetDeliveriesInNearestCity(latitude:Double,longitude:Double, placeData: CurrentPlaceData) {
        
        var parameter = placeData.toDictionary()
        parameter[PARAMS.DELIVERY_TYPE] = self.deliveryType
        
        print("WS_GET_NEAREST_DELIVERY_LIST \(parameter)")
        ////print(Utility.convertDictToJson(dict: parameter))
        Utility.showLoading()
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url:WebService.WS_GET_NEAREST_DELIVERY_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: parameter) { [weak self] (response, error) -> (Void) in
            guard let self = self else { return }
            print("WS_GET_NEAREST_DELIVERY_LIST response homevc \(response)")
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                let deliveryStoreResponse:DeliveryStoreResponse = DeliveryStoreResponse.init(dictionary: response)!
                
                let taxiAvailable = deliveryStoreResponse.deliveries.filter({$0.deliveryType == DeliveryType.taxi})
                
                if taxiAvailable.count > 0 {
                    if let city = response["city"] as? [String:Any] {
                        if let id = city["_id"] as? String {
                            currentBooking.bookCityId = id
                            if self.locationType == .pickUp {
                                self.pickupAddress.address = self.txtPickupAddress.text
                                self.pickupAddress.location = [latitude,longitude]
                                self.pickUplat = latitude
                                self.pickUplong = longitude
                            } else {
                                if self.destinationAddress.count > 0{
                                    self.destinationAddress[0].address = self.txtDestinationAddress.text
                                    self.destinationAddress[0].location = [latitude,longitude]
                                }
                                self.destinationlat = latitude
                                self.destinationlong = longitude
                            }
                            self.wsDrawPath()
//                            self.showNearByService()
                        }
                        if let id = city["country_id"] as? String {
                            currentBooking.bookCountryId = id
                        }
                    }
                } else {
                    self.setToNormalView()
                    Utility.showToast(message: "ERROR_CODE_813".localized)
                }
            } else {
                if self.locationType == .pickUp {
                    self.pickupAddress.address = ""
                    self.pickupAddress.location = [0.0,0.0]
                    self.txtPickupAddress.text = ""
                    self.pickUplat = 0
                    self.pickUplong = 0
                } else {
                    if self.destinationAddress.count > 0{
                        self.destinationAddress[0].address = ""
                        self.destinationAddress[0].location = [0.0,0.0]
                    }
                    self.txtDestinationAddress.text = ""
                    self.destinationlat = 0
                    self.destinationlong = 0
                }
            }
        }
    }
}

extension RideHomeVC {
    func didSubmitCourierData(dics: [String : Any]) {
        self.viewServiceType.isCourierDataFilled = true
        let cartOrder = CartOrder(dictionary: dics as NSDictionary)
        if cartOrder?.pickupAddress.count ?? 0 > 0 {
            cartOrder?.pickupAddress[0].address = txtPickupAddress.text!
            cartOrder?.pickupAddress[0].location = [pickUplat,pickUplong]
        }
        if cartOrder?.destinationAddress.count ?? 0 > 0 {
            cartOrder?.destinationAddress[0].address = txtDestinationAddress.text!
            cartOrder?.destinationAddress[0].location = [destinationlat,destinationlong]
        }
        self.viewServiceType.btnCreateTrip.setTitle("txt_request_courier_service".localized, for: .normal)
        
        if let dictData = cartOrder?.dictionaryRepresentation() {
            dictData.setValue(currentBooking.bookCountryId ?? "", forKey: PARAMS.COUNTRY_ID)
            dictData.setValue(currentBooking.bookCityId ?? "", forKey: PARAMS.CITY_ID)
            dictData.setValue(DeliveryType.courier, forKey: PARAMS.DELIVERY_TYPE)
            
            courierData = dictData as? [String : Any]
        }
    }
    
    func addItemIntoCartCourier(dics: [String : Any]) {
        Utility.showLoading()
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_ADD_ITEM_IN_CART, methodName: AlamofireHelper.POST_METHOD, paramData: dics) { [weak self] (response,error) -> (Void) in
            guard let self = self else { return }
            print("response \(response)")
            if (Parser.isSuccess(response: response, withSuccessToast: false, andErrorToast: true)) {
                currentBooking.cartId = (response.value(forKey: "cart_id") as? String) ?? ""
                self.prepareInvoicePARAMS()
            }
            Utility.hideLoading()
        }
    }
    func parmaforGetInvoiceAPI() {
        var dictParam:[String:Any] = APPDELEGATE.getCommonDictionary()
        dictParam[PARAMS.TOTAL_TIME] = self.estimateTime
        dictParam[PARAMS.TOTAL_DISTANCE] = self.estimateDistance
        dictParam[PARAMS.COUNTRY_ID] = currentBooking.bookCountryId ?? ""
        dictParam[PARAMS.CITY_ID] = currentBooking.bookCityId ?? ""
        dictParam[PARAMS.VEHICLE_ID] = self.viewServiceType.getSelectedVehicle()?.vehicleId ?? ""
        dictParam[PARAMS.DELIVERY_TYPE] = self.deliveryType
        dictParam[PARAMS.CART_ID] = currentBooking.cartId
        CurrentTrip.shared.inVoiceParam = dictParam
        if deliveryType == DeliveryType.courier {
            wsGetInvoice(dictParam: dictParam)
        } else {
            self.goToPayment()
        }
    }
    func prepareInvoicePARAMS() {
        var dictParam:[String:Any] = APPDELEGATE.getCommonDictionary()
        dictParam[PARAMS.TOTAL_TIME] = self.estimateTime
        dictParam[PARAMS.TOTAL_DISTANCE] = self.estimateDistance
        dictParam[PARAMS.COUNTRY_ID] = currentBooking.bookCountryId ?? ""
        dictParam[PARAMS.CITY_ID] = currentBooking.bookCityId ?? ""
        dictParam[PARAMS.VEHICLE_ID] = self.viewServiceType.getSelectedVehicle()?.vehicleId ?? ""
        dictParam[PARAMS.DELIVERY_TYPE] = self.deliveryType
        dictParam[PARAMS.CART_ID] = currentBooking.cartId
        CurrentTrip.shared.inVoiceParam = dictParam
        if deliveryType == DeliveryType.courier {
            wsGetInvoice(dictParam: dictParam)
        } else {
            self.goToPayment()
        }
    }
    
    func wsGetCancellationCharge() {
        Utility.showLoading()
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
         PARAMS.ORDER_ID:orderId ?? "",
         PARAMS.DELIVERY_TYPE : DeliveryType.taxi
        ]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_CANCELLATION_CHARGES, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) {(response, error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                self.wsGetCancelReasonList(cancellationCharge: response[PARAMS.CANCELLATION_CHARGE] as? Double ?? 0)
            }
        }
    }
    
    func wsGetInvoice(dictParam:Dictionary<String,Any>){
        Utility.showLoading()
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_COURIER_INVOICE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response,error) -> (Void) in
            Utility.hideLoading()
            if Parser.isSuccess(response: response) {
                let invoiceResponse:InvoiceResponse = InvoiceResponse.init(dictionary: response)!
                currentBooking.currentServerTime = invoiceResponse.serverTime
                let timeZone = TimeZone.init(identifier:invoiceResponse.timeZone)!
                
                currentBooking.currentDateMilliSecond = Utility.convertServerDateToMilliSecond(serverDate:currentBooking.currentServerTime , strTimeZone: timeZone.identifier)
                
                Parser.parseInvoice(invoiceResponse.order_payment!, toArray: self.arrForInvoice , currency:currentBooking.currency,isTaxIncluded: invoiceResponse.isTaxIncluded, deliveryType: self.deliveryType, completetion: { (result) in
                    if self.deliveryType == DeliveryType.courier {
                        self.openInvoiceCourier(totalPrice: invoiceResponse.order_payment?.userPayPayment ?? 0)
                    } else {
                        self.goToPayment()
                    }
                })
            }else {
                let isSuccess:IsSuccessResponse = IsSuccessResponse.init(dictionary: response)!
                if isSuccess.errorCode! == CONSTANT.ERROR_MINMUM_INVOICE_AMOUNT {
                    let minAmount = (response.value(forKey: PARAMS.MIN_ORDER_PRICE) as? Double)?.roundTo() ?? 0.0
                    self.openMinAmountDialog(amount:String(minAmount))
                }
            }
        }
    }

    func openMinAmountDialog(amount:String) {
        let minAmountMessage:String = "TXT_MIN_INVOICE_AMOUNT_MSG".localized + amount
        let dialogForMinAmount = CustomAlertDialog.showCustomAlertDialog(title: "TXT_MIN_AMOUNT".localized, message: minAmountMessage, titleLeftButton: "", titleRightButton: "TXT_ADD_MORE_ITEMS".localizedCapitalized)
        dialogForMinAmount.onClickLeftButton = {
            [unowned dialogForMinAmount] in
            dialogForMinAmount.removeFromSuperview()
        }
        dialogForMinAmount.onClickRightButton = {
            [unowned self,unowned dialogForMinAmount] in
            dialogForMinAmount.removeFromSuperview()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func openInvoiceCourier(totalPrice: Double) {
        var arr = [InvoicePopUpData]()
        for obj in (arrForInvoice as! [Invoice]) {
            print("\(obj.title) \(obj.subTitle) \(obj.price) \(obj.type)")
            if let type = InvoiceCellType(rawValue: obj.type) {
                arr.append(InvoicePopUpData(title: obj.title ?? "", value: obj.price ?? "", type: type))
            } else {
                arr.append(InvoicePopUpData(title: obj.title ?? "", value: obj.price ?? "", type: .Regular))
            }
        }
        let dailog = InvoicePopUp.showInvoicePopUp(title: "TXT_INVOICE".localized, strRight: "TXT_PLACEORDER".localized, currency: "", isHidePromo: true, isPromoEdit: false, strPromo: promoCode ?? "", arrInvoice: arr, totalPrice: totalPrice, isButtonDone: false)
        dailog.onClickLeftButton = {
            dailog.removeFromSuperview()
        }
        
        dailog.onClickRightButton = { obj in
            dailog.removeFromSuperview()
            self.goToPayment()
        }
        
        dailog.onClickApplyPromoCode = { obj in
            dailog.removeFromSuperview()
        }
    }
    
}

extension RideHomeVC: PaymentVCDelegate, PaymentVCTripPaymentDelegate {
    
    func orderCreateSuccessfully(orderId: String) {
        print("Trip create order id:- \(orderId)")
        if self.deliveryType == DeliveryType.taxi {
            currentBooking.clearBooking(isClearCart: false)
            if !(viewServiceType.scheduleData?.isAsap ?? true) {
                APPDELEGATE.goToOrder()
            } else {
                self.orderId = orderId
                self.startTripStatusUpdate(id: orderId)
                openDriverDetailView()
            }
        }
    }
    
    func openDriverDetailView() {
        dialogFromDriverDetail = CustomDriverDetailDialog.showCustomDriverDetailDialog(message: "TXT_CONNECTING_NEAR_BY_DRIVER".localized, cancelButton: "txt_cancel_trip".localized)
        
        btnBack.isHidden = true
        
        dialogFromDriverDetail!.onClickCancelTrip = {
            self.wsGetCancelReasonList(cancellationCharge: 0)
        }
        
        dialogFromDriverDetail!.onClickBack = {
            self.btnBack.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func wsGetCancelReasonList(cancellationCharge: Double) {
        
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.DELIVERY_TYPE : self.deliveryType,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken]
        
        Utility.showLoading()
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_GET_CANCEL_REASON_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [weak self] (response, error) -> (Void) in
            guard let self = self else { return }
            Utility.hideLoading()
            var arrList = [String]()
            if Parser.isSuccess(response: response) {
                ////print(response)
                if let list = response["reasons"] as? [String] {
                    arrList.append(contentsOf: list)
                }
            }
            self.openCancelOrderDialog(list: arrList, cancelcharge: cancellationCharge)
        }
    }
    
    func openCancelOrderDialog(list: [String], cancelcharge: Double) {
        
        let arrList: [String] = {
            if list.count > 0 {
                return list
            }
           return arrReason
        }()
        
        var cancellationCharge:String = ""
        if cancelcharge > 0.0 {
            cancellationCharge = currentBooking.currency + " " + cancelcharge.toString()
        } else {
            cancellationCharge = ""
        }
        
        dialogForCancelOrder = CustomCancelOrderDialog.showCustomCancelOrderDialog(title: "TXT_CANCEL_ORDER".localized, message: "", cancelationCharge: cancellationCharge, deliveryType: self.deliveryType, titleLeftButton: "TXT_CANCEL".localizedCapitalized, titleRightButton: "TXT_OK".localizedCapitalized, list: arrList)
        
        dialogForCancelOrder?.onClickLeftButton = { [weak self] in
            guard let self = self else { return }
            self.dialogForCancelOrder?.removeFromSuperview()
        }
        dialogForCancelOrder?.onClickRightButton = { [weak self] (cancelReason:String) in
            guard let self = self else { return }
            self.wsCancelOrder(reason: cancelReason)
            self.dialogForCancelOrder?.removeFromSuperview()
        }
    }
    
    func wsCancelOrder(reason:String) {
        let dictParam: Dictionary<String,Any> =
        [PARAMS.USER_ID:preferenceHelper.UserId,
         PARAMS.SERVER_TOKEN:preferenceHelper.SessionToken,
         PARAMS.ORDER_ID: self.orderId ?? "",
         PARAMS.ORDER_STATUS:OrderStatus.CANCELED_BY_USER.rawValue,
         PARAMS.CANCEL_REASON:reason
        ]
        
        let afn:AlamofireHelper = AlamofireHelper.init()
        afn.getResponseFromURL(url: WebService.WS_CANCEL_TRIP, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { [weak self] (response, error) -> (Void) in
            guard let self = self else { return }
            if (Parser.isSuccess(response: response)) {
                self.setToNormalView()
            }
        }
    }
    
    func didTripPaymentSuccess(orderId: String) {
        self.isPaymentPending = false
        self.btnBack.isHidden = isPaymentPending
        self.getInvoiceData()
    }
}

extension CLLocationCoordinate2D {
    func bearing(to point: CLLocationCoordinate2D) -> Double {
        func degreesToRadians(_ degrees: Double) -> Double { return degrees * Double.pi / 180.0 }
        func radiansToDegrees(_ radians: Double) -> Double { return radians * 180.0 / Double.pi }
        
        let fromLatitude = degreesToRadians(latitude)
        let fromLongitude = degreesToRadians(longitude)
        
        let toLatitude = degreesToRadians(point.latitude)
        let toLongitude = degreesToRadians(point.longitude)
        
        let differenceLongitude = toLongitude - fromLongitude
        
        let y = sin(differenceLongitude) * cos(toLatitude)
        let x = cos(fromLatitude) * sin(toLatitude) - sin(fromLatitude) * cos(toLatitude) * cos(differenceLongitude)
        let radiansBearing = atan2(y, x);
        let degree = radiansToDegrees(radiansBearing)
        return (degree >= 0) ? degree : (360 + degree)
    }
}

extension RideHomeVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listStopLocationAddress.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LocationAddressCell.identifier) as! LocationAddressCell
        cell.lblAddress.text = self.listStopLocationAddress[indexPath.row].address ?? ""
        cell.index = indexPath
        cell.imgDrow.image = cell.imgDrow.image?.withRenderingMode(.alwaysTemplate)
        cell.imgDrow.tintColor = UIColor.themeImageColor
        cell.lblDrow.backgroundColor = UIColor.themeImageColor
        if isRunningTip{
            cell.btnCancel.isHidden = true
        }
        cell.onClickCancel = { (index) in
            print(index.row)
            self.listStopLocationAddress.remove(at: index.row)
            self.constraintLocationHeight.constant = CGFloat((self.listStopLocationAddress.count * 50))
            self.tableDestinations.reloadData()
            if self.listStopLocationAddress.count >= (preferenceHelper.MaxTaxiStop) {
                self.viewAddAddress.isHidden = true
            } else {
                self.viewAddAddress.isHidden = false
            }
            self.wsDrawPath()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        self.locationType = .stop
        isEdit = true
        stopCount = indexPath.row
        goToAddress(location: [self.listStopLocationAddress[indexPath.row].latitude,self.listStopLocationAddress[indexPath.row].longitude])
        return indexPath
    }
}
extension RideHomeVC: GMSMapViewDelegate  {
    
}
//MARK: - Drow map path

extension RideHomeVC{
    func wsDrawPath() {
//        if !isRunninngTrip{
//            self.showMarkers()
//        }
        let saddr = "\(pickUplat),\(pickUplong)"
        let daddr = "\(destinationlat),\(destinationlong)"
        
        if (pickUplat == 0 || pickUplong == 0) && (destinationlat == 0 || destinationlong == 0){
            return
        }
        var wayPoint: [String] = []
        for i in 0..<listStopLocationAddress.count {
            let obj = listStopLocationAddress[i]
            wayPoint.append("\(obj.latitude!),\(obj.longitude!)")
        }
        var url = Google.DIRECTION_URL + "\(saddr)&destination=\(daddr)&waypoints=optimize:false|\(wayPoint.joined(separator: "|"))&key=\(preferenceHelper.CustomerAppGoogleDirectionMatrixKey)"
        //        let url = Google.DIRECTION_URL +  "\(saddr)&destination=\(daddr)&key=\(preferenceHelper.CustomerAppGoogleDirectionMatrixKey)"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        guard let url =  URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? url) else {
            return
        }
        do {
            DispatchQueue.main.async {  /*[unowned self, unowned session] in*/
                session.dataTask(with: url) { [self] (data, response, error) in
                    guard let data = data else {
                        return
                    }
                    do {
                        guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
                            ////print(error?.localizedDescription as Any)
                            return
                        }
                        self.googlePathResponse = json
                        //print(json)
                        self.isPathDraw = true
                        if let routes = json["routes"] as? [[String:Any]] {
                            if let first = routes.first {
                                //print(first)
                                if let legs = first["legs"] as? [[String:Any]] {
                                    //                                    print(legs)
                                    var durationValues = 0
                                    var distanceValues = 0
                                    
                                    var values = 0
                                    for leg in legs {
                                        if let duration = leg["duration"] as? [String:Any] {
                                            durationValues  = durationValues + (duration["value"] as? Int ?? 0)
                                            
                                        }
                                        if let distance = leg["distance"] as? [String:Any] {
                                            distanceValues  = distanceValues + (distance["value"] as? Int ?? 0)
                                        }
                                    }
                                    self.estimateTime = durationValues
                                    self.estimateDistance = Double(distanceValues)
                                    
                                    currentBooking.estimateTime = self.estimateTime
                                    currentBooking.estimateDistance = self.estimateDistance
                                    DispatchQueue.main.async { [self] in
                                        var mins = Int(self.estimateTime/60)
                                        let hours = Int(mins/60)
                                        mins = mins - (hours * 60)
                                        
                                        var timeString = ("\(Int(mins)) \("UNIT_MIN".localized)")
                                        if hours > 0{
                                            if mins > 0{
                                                timeString = ("\(Int(hours)) \("UNIT_HRS".localized)") + "\n" + ("\(Int(mins)) \("UNIT_MIN".localized)")
                                            }else{
                                                timeString = ("\(Int(hours)) \("UNIT_HRS".localized)")
                                            }
                                        }
                                        viewServiceType.lblTime.text = (timeString)
                                        viewServiceType.lblDistance.text = ("\((self.estimateDistance/1000).toStringDouble()) \("UNIT_KM".localized)")
                                    }
                                    wsGetVehiclelist()
                                }
                                if let overview_polyline = first["overview_polyline"] as? [String:Any] {
                                    if let points = overview_polyline["points"] as? String {
                                        self.drawPath(with: points)
                                        self.drawPath(with: points)
                                    }
                                }
                            }
                        }
                    } catch let error {
                        printE(session)
                        printE("Failed to draw ",error.localizedDescription)
                    }
                }.resume()
            }
        }
    }
    
    private func drawPath(with points : String) {
        
        DispatchQueue.main.async { [weak self] in
            if self?.polyLinePath.map != nil {
                self?.polyLinePath.map = nil
            }
            if let path = GMSPath(fromEncodedPath: points) {
                self?.polyLinePath = GMSPolyline(path: path)
                self?.polyLinePath.strokeColor = UIColor.themePoliline
                self?.polyLinePath.strokeWidth = 5.0
                self?.polyLinePath.geodesic = true
                self?.polyLinePath.map = self?.mapView
            }
        }
    }
    
    private func drawPath() {
        DispatchQueue.main.async { [weak self] in
            self?.polyLinePath.strokeColor = UIColor.themeBlackColor
            self?.polyLinePath.strokeWidth = 5.0
            self?.polyLinePath.geodesic = true
            self?.polyLinePath.map = self?.mapView
        }
    }
    
    func showMarkers() {
        mapView?.clear()
        providerMarker.map = mapView
        self.animationPolyline.map = self.mapView
        
        let pickUpCoordinate = CLLocationCoordinate2D(latitude: pickUplat, longitude: pickUplong)
        let destinationCoordinate = CLLocationCoordinate2D(latitude: destinationlat, longitude: destinationlong)
        let providerCoordinate = providerMarker.position
        
        
        let pickupMarker = GMSMarker(position: pickUpCoordinate)
        pickupMarker.icon = UIImage.init(named: "icons8-radio-button-96")
        
        pickupMarker.map = mapView
        if destinationlat != 0 {
            let destinationMarker = GMSMarker(position: destinationCoordinate)
            destinationMarker.icon = UIImage.init(named: "icons8-square-96")
            destinationMarker.icon?.withTintColor(.black)
            destinationMarker.map = mapView
        }
        var bounds = GMSCoordinateBounds()
        
        var count = 1
        for arrAddress in listStopLocationAddress {
            let marker = GMSMarker.init(position: CLLocationCoordinate2D(latitude: arrAddress.latitude, longitude: arrAddress.longitude))
            marker.map = mapView
            bounds = bounds.includingCoordinate(marker.position)
            
            let img = UIImageView(image: UIImage(named: "map_fill")!.imageWithColor(color: .themeColor))
            img.frame.size = CGSize(width: 30, height: 30)
            
            let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            lbl.text = "\(count)"
            lbl.textColor = .white
            lbl.backgroundColor = .themeColor
            lbl.textAlignment = .center
            lbl.setRound()
            img.addSubview(lbl)
            marker.iconView = img
            //marker.iconView?.addSubview(lbl)
            lbl.center = marker.iconView?.center ?? CGPoint(x: 0, y: 0)
            lbl.center.y = img.frame.size.height/2.5
            
            count += 1
        }
        
        let bottomPadding: CGFloat = {
            if viewServiceType.isShow {
                return viewServiceType.frame.size.height
            } else if viewTripDetails.isShow {
                return viewTripDetails.frame.size.height
            } else {
                return 0
            }
        }()
        
        if let order = order {
            let orderStatus =  OrderStatus.init(rawValue: order.request_detail?.delivery_status ?? 0) ?? OrderStatus.Unknown
            
            let arrStatusCheck = [OrderStatus.ORDER_READY, OrderStatus.WAITING_FOR_DELIVERY_MAN, OrderStatus.DELIVERY_MAN_COMPLETE_DELIVERY, OrderStatus.CANCELED_BY_USER, OrderStatus.NO_DELIVERY_MAN_FOUND, OrderStatus.DELIVERY_MAN_REJECTED, OrderStatus.DELIVERY_MAN_CANCELLED, OrderStatus.DELIVERY_MAN_COMPLETE_DELIVERY]
            
            if arrStatusCheck.contains(orderStatus) {
                bounds = bounds.includingCoordinate(pickUpCoordinate)
                bounds = bounds.includingCoordinate(destinationCoordinate)
                providerMarker.map = nil
                
                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 0, left: 15, bottom: bottomPadding + 30, right: 15))
                mapView?.animate(with: update)
            } else {
                bounds = bounds.includingCoordinate(providerCoordinate)
                mapView?.padding = UIEdgeInsets(top: 0, left: 0, bottom: bottomPadding * 0.5, right: 0)
                let camera = GMSCameraUpdate.setTarget(providerCoordinate, zoom: 17)
                mapView?.animate(with: camera)
            }
        } else {
            if destinationlat == 0 {
                bounds = bounds.includingCoordinate(pickUpCoordinate)
                bounds = bounds.includingCoordinate(pickUpCoordinate)
                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 0, left: 15, bottom: bottomPadding + 30, right: 15))
                mapView?.animate(with: update)
            }else{
                bounds = bounds.includingCoordinate(pickUpCoordinate)
                bounds = bounds.includingCoordinate(destinationCoordinate)
                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 0, left: 15, bottom: bottomPadding + 30, right: 15))
                mapView?.animate(with: update)
            }
        }
    }
}
