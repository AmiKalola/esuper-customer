//
//  LocationAddressCell.swift
//  ESuper
//
//  Created by Rohit on 27/12/23.
//  Copyright © 2023 Elluminati. All rights reserved.
//

import UIKit

class LocationAddressCell: UITableViewCell {
    @IBOutlet weak var imgDrow : UIImageView!
    @IBOutlet weak var lblDrow : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var btnCancel : UIButton!
    var index : IndexPath?
    var onClickCancel : ((IndexPath) -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    @IBAction func actionCancel (_ sender : UIButton){
        self.onClickCancel?(index!)
    }
}
