//
//  CompanyListVC.swift
//  Decagon
//
//  Created by MacPro3 on 19/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import Foundation
import UIKit

class CompanyListVC: BaseVC {
    
    @IBOutlet weak var tblCompnay: UITableView!
    var arrCompnay = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
    }
    
    func setUpTableView() {
        tblCompnay.delegate = self
        tblCompnay.dataSource = self
        tblCompnay.register(UINib(nibName: "CompanyCell", bundle: nil), forCellReuseIdentifier: "CompanyCell")
    }
}

extension CompanyListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10//arrCompnay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell", for: indexPath)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
