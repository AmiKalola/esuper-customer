//
//  EseriviceProviderListVC.swift
//  Decagon
//
//  Created by MacPro3 on 19/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class EseriviceProviderListVC: BaseVC {
    
    @IBOutlet weak var tblProvider: UITableView!
    var arrProvider = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
    }
    
    func setUpTableView() {
        tblProvider.delegate = self
        tblProvider.dataSource = self
        tblProvider.register(UINib(nibName: "CompanyCell", bundle: nil), forCellReuseIdentifier: "CompanyCell")
    }
}

extension EseriviceProviderListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10//arrProvider.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell", for: indexPath)
        cell.selectionStyle = .none
        return cell
    }
}
