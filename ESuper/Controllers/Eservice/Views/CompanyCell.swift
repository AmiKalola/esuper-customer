//
//  CompanyCell.swift
//  Decagon
//
//  Created by MacPro3 on 19/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {
    
    @IBOutlet weak var lblName: LabelDefault!
    @IBOutlet weak var lblPrice: LabelDefault!
    @IBOutlet weak var lblDescription: LabelDefault!
    @IBOutlet weak var lblService: LabelDefault!
    @IBOutlet weak var lblServiceValue: LabelDefault!
    @IBOutlet weak var btnRate: ButtonDefault!
    
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblDescription.font = FontHelper.textSmall()
        lblService.font = FontHelper.textMedium(size: FontHelper.small)
        lblServiceValue.font = FontHelper.textRegular(size: FontHelper.small)
        btnRate.titleLabel?.font = FontHelper.textRegular(size: FontHelper.small)
        
        lblDescription.textColor = UIColor.themeLightTextColor
        lblService.textColor = UIColor.themeLightGrayColor
        lblServiceValue.textColor = UIColor.themeLightTextColor
        
        self.backgroundColor = UIColor.themeViewBackgroundColor
        self.contentView.backgroundColor = UIColor.themeViewBackgroundColor
        
        img.applyRoundedCornersWithHeight(img.frame.size.height/2)
        
        self.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }

    
}
