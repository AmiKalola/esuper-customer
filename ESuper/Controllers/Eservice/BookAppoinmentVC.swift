//
//  BookAppoinmentVC.swift
//  Decagon
//
//  Created by MacPro3 on 19/05/22.
//  Copyright © 2022 Elluminati. All rights reserved.
//

import UIKit

class BookAppoinmentVC: BaseVC, LeftDelegate, RightDelegate {
    
    //MARK: OutLets
    
    @IBOutlet weak var tabForTopBar: UITabBar!
    @IBOutlet weak var scrollViewForTab: UIScrollView!
    @IBOutlet weak var containerCompany: UIView!
    @IBOutlet weak var containerProvider: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    
    //MARK: Variables
    var compnayListVC:CompanyListVC? = nil
    var providerListVC:EseriviceProviderListVC? = nil
    var filterPopUp: FilterProviderPopUp?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBackBarItem(isNative: true)
        //Utility.showLoading()
        scrollViewForTab.delegate = self
        setLocalization()
        self.setNavigationTitle(title:"txt_book_appointment".localized)
        
        self.setRightBarItem(isNative: false)
        self.delegateRight = self
        self.setRightBarItemImage(image: UIImage.init(named: "filter_blue")!.imageWithColor(color: .red)!)
    }
 
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tabForTopBar.tintColor = UIColor.themeTitleColor
        tabForTopBar.unselectedItemTintColor = UIColor.themeLightGrayColor
        viewForShadow.setShadow(shadowColor: UIColor.lightGray.cgColor, shadowOffset: CGSize.init(width: 0.0, height: 3.0), shadowOpacity: 0.2, shadowRadius: 3.0)
        tabForTopBar.selectionIndicatorImage = APPDELEGATE.getImageWithColorPosition(color: UIColor.themeSectionBackgroundColor, size: CGSize(width:(APPDELEGATE.window?.frame.size.width)!/2,height: 49), lineSize: CGSize(width:(APPDELEGATE.window?.frame.size.width)!/2, height:2))
    }
    override func updateUIAccordingToTheme() {
        self.setBackBarItem(isNative: true)
    }
    func onClickLeftButton() {
        self.navigationController?.popViewController(animated: true)
    }
    func onClickRightButton() {
        openFilterList()
    }
    
    func openFilterList() {
        let daiLog = FilterProviderPopUp.showProvierFilterPopUp(title: "TXT_FILTERS".localized, strRight: "TXT_APPLY".localized, obj: self.filterPopUp)
        daiLog.onClickRightButton = { obj in
            self.filterPopUp = obj
            daiLog.removeFromSuperview()
        }
        daiLog.onClickClearButton = { obj in
            daiLog.removeFromSuperview()
            self.filterPopUp = nil
        }
    }

    func goToPoint(point:CGFloat){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveLinear, animations:
            {
                    self.scrollViewForTab.contentOffset.x = point
                    self.view.layoutIfNeeded()
            }, completion: nil)
            
        }
    }
    
    //MARK:- Navigation Methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier?.compare(SEGUE.SEGUE_TO_HISTORY) == ComparisonResult.orderedSame {
            compnayListVC = (segue.destination as! CompanyListVC)
        }
        else if segue.identifier?.compare(SEGUE.SEGUE_TO_CURRENT_ORDER) == ComparisonResult.orderedSame {
            providerListVC = (segue.destination as! EseriviceProviderListVC)
        }else {
            printE("No Segue performed")
        }
    }
}
//MARK: - UITabBar and UIScrollView Delegate methods
extension BookAppoinmentVC : UITabBarDelegate, UIScrollViewDelegate{
    
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        tabForTopBar.selectedItem = tabForTopBar.items![item.tag]

        switch(item.tag) {

        case 0:
            self.scrollViewForTab.setContentOffset(self.containerCompany.frame.origin, animated: true)
            break
        case 1:
            self.scrollViewForTab.setContentOffset(self.containerProvider.frame.origin, animated: true)
            break
        default:
            break
        }
        self.containerCompany.endEditing(true)
        self.containerProvider.endEditing(true)
        self.view.layoutIfNeeded()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)    {
        if !decelerate {
            let pageWidth:CGFloat = scrollView.frame.size.width
            let fractionalPage:CGFloat = scrollView.contentOffset.x / pageWidth
            let page:Int = lroundf(Float(fractionalPage))
            switch (page) {
            case 0:
                if UIApplication.isRTL() {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![1])
                } else {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![0])
                }
                break
            case 1:
                if UIApplication.isRTL() {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![0])
                } else {
                    self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![1])
                }
                break
            default:
                break
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth:CGFloat = scrollView.frame.size.width
        let fractionalPage:CGFloat = scrollView.contentOffset.x / pageWidth
        let page:Int = lroundf(Float(fractionalPage))
        
        switch (page) {
        case 0:
            if UIApplication.isRTL() {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![1])
            } else {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![0])
            }
            break
       
        case 1:
            if UIApplication.isRTL() {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![0])
            } else {
                self.tabBar(tabForTopBar, didSelect: tabForTopBar.items![1])
            }
            break
       
        default:
            break
        }
    }
}
//MARK: - set Localization text and pree launch setup
extension BookAppoinmentVC{
    func setLocalization() {
        /*set colors*/
        self.view.backgroundColor = UIColor.themeViewBackgroundColor
        viewForShadow.backgroundColor =
            UIColor.clear
        let item1 = tabForTopBar.addTabBarItem(title: "txt_with_company".localized, imageName: "", selectedImageName: "", tagIndex: 0)
        let item2 = tabForTopBar.addTabBarItem(title: "txt_with_provider".localized, imageName: "", selectedImageName: "", tagIndex: 1)
        tabForTopBar.setItems([item1,item2], animated: false)
        tabForTopBar.selectedItem = tabForTopBar.items![0]
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.scrollViewForTab.setContentOffset(self.containerCompany.frame.origin, animated: false)
        }
        self.hideBackButtonTitle()
    }
}
